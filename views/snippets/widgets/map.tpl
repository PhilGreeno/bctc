

<div id="map" class="map" style='height:280px'>&nbsp;</div>

<script src="http://maps.google.com/maps/api/js?sensor=false&amp;key=AIzaSyCHapXCh6JyfkpnJVUGG1zBSwaHjZUI70c" type="text/javascript"></script>
<script type='text/javascript'>
//<![CDATA[ 
	// Closure - Just to be safe :)
	(function(){
    var map;
    var last_post = 0;
    var infoWindow;
    var gmarkers = [];
    var points = [
    	{foreach $mapper as $map name=foo}
    	 	{if $map->lat != null && $map->lat > 50.759100 }
    			['latlong', '{$map->lat|default: 50.760106},{$map->long|default:-1.994442}', '{$map->id|md5}', {$smarty.foreach.foo.index + 1}],
    		{/if} 
    	{foreachelse}
    		['latlong', '50.76010},-1.994442', '', 1],
    	{/foreach}
    ];

    var arrayPoints = new Array(points.length);

    for (var i = 0; i < points.length; i++) {
       arrayPoints[i] = new Array(3); 
    }

    function init() {

        var myOptions = { 
            zoom: 15,
            center: new google.maps.LatLng(0, 0),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById('map'), myOptions);
        infoWindow = new google.maps.InfoWindow();
        codeAddress(points);

        var styles = [ { "elementType": "labels.text", "stylers": [ { "visibility": "off" } ] },{ "featureType": "road.arterial", "elementType": "geometry.fill", "stylers": [ { "visibility": "on" }, { "saturation": 1 }, { "lightness": 1 }, { "color": "#00B4AB" } ] },{ "featureType": "road.local", "stylers": [ { "color": "#b9efe9" } ] },{ "featureType": "road.arterial", "elementType": "geometry.stroke", "stylers": [ { "visibility": "on" }, { "weight": 5 }, { "color": "#ffffff" } ] },{ "featureType": "landscape", "elementType": "geometry", "stylers": [ { "color": "#ffffff" } ] },{ } ];
        map.setOptions({ styles: styles });
        
        var opt = { minZoom: 15, maxZoom: 18 };
        map.setOptions(opt);

        
    }
                                  
    function codeAddress(locations) {
        
        for (var i = 0; i < locations.length; i++) {
            var location = locations[i];
            var lat_long = location[1].split(',');
            var myLatLng = new google.maps.LatLng(lat_long[0], lat_long[1]);
            fillArrayPositions(myLatLng, i, location[2], location[3]);
        }
        
        google.maps.event.addListenerOnce(map, 'idle', function(){
           setMarkerPosition(arrayPoints);
        });
    }
                                  
    function fillArrayPositions(myLatLng, pos, ident, number){
        if (!pos || pos == 'undefined'){
             pos = last_post;
             last_post = last_post + 1;
        }
        arrayPoints[pos][0] = myLatLng;
        arrayPoints[pos][1] = ident;
        arrayPoints[pos][2] = number;
    }
                                  
  function setMarkerPosition(points){
      
      var bounds = new google.maps.LatLngBounds();
      
      function createMarker(map, position, ident, number, bounds){
      
       		var pinColor = "FE7569";
    			var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld="+number+"|" + pinColor,
        	new google.maps.Size(21, 34),
        	new google.maps.Point(0,0),
        	new google.maps.Point(10, 34));
    			var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
        	new google.maps.Size(40, 37),
        	new google.maps.Point(0, 0),
        	new google.maps.Point(12, 35));

          var marker = new google.maps.Marker({
              position: position,
              map: map,
              draggable: false,
              animation: google.maps.Animation.DROP,
              icon: pinImage,
              shadow: pinShadow
          	});
          
          bounds.extend(position);
          map.fitBounds(bounds);

          google.maps.event.addListener(marker, 'mouseover', function() {
          	$('#item_'+ident).parent().addClass('bizHigh');
            //  infoWindow.setContent(html);
            //  infoWindow.open(map, marker);
          }); 
          
          google.maps.event.addListener(marker, 'mouseout', function() {
          	$('#item_'+ident).parent().removeClass('bizHigh');
          });
          gmarkers[number] = marker;
          
           // Make the info window close when clicking anywhere on the map.
         //  google.maps.event.addListener(map, 'click', function() { infoWindow.close(); });
          
       }
          
      for (var i = 0; i < points.length; i++) {
          var point = points[i];
          if (point[0]){

              createMarker(map, point[0], point[1], point[2], bounds);

          }
      }        
      map.setZoom(15);            
		}
                                   
		$().ready(function(){
        init();
    });
	})();
//]]> 
</script>