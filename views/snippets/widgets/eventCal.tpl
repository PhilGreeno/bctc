<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/fullcalendar.css" />	
<script src="/{$WEBPATH}admin_resources/js/fullcalendar.min.js"></script>

<script>
$().ready(function(){

	$('.calendar').fullCalendar({
		header: {
			left: 'prev,next',
			center: 'title',
			right: 'month,basicWeek,basicDay'
		},
		editable: false,
		events: [
		{foreach $mostEvents as $event}
			{
				title: '{$event->title}',
				start: new Date('{$event->startdate}'),
				end: new Date('{$event->enddate}'),
				url: '/{$WEBPATH}events/detail/{$event->id|md5}',

			},
		{foreachelse}
		{/foreach}
		]
	});
});
</script>