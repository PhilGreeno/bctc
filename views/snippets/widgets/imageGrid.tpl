{if $obj->image1}		
  <a href="/{$WEBPATH}image/{$type}_{$obj->id|md5}_1/800/540/image.jpg" class="thumbnail" data-fancybox-group="group1" {*title="Description 1"*}>
  	<img alt="" src="/{$WEBPATH}image/{$type}_{$obj->id|md5}_1/320/240/">
  </a>												
{else}
	<a href="/{$WEBPATH}image/placeholder/800/540/image.jpg" class="thumbnail" data-fancybox-group="group1" {*title="Description 1"*}>
  	<img alt="" src="/{$WEBPATH}image/placeholder/320/240/">
  </a>		
{/if}
<ul class="thumbnails small">				
{if $obj->image2}				
	<li class="span4">
		<a href="/{$WEBPATH}image/{$type}_{$obj->id|md5}_2/800/540/image.jpg" class="thumbnail" data-fancybox-group="group1"><img src="/{$WEBPATH}image/{$type}_{$obj->id|md5}_2/300/180/" alt=""></a>
	</li>						
{/if}		
{if $obj->image3}		
	<li class="span4">
		<a href="/{$WEBPATH}image/{$type}_{$obj->id|md5}_3/800/540/image.jpg" class="thumbnail" data-fancybox-group="group1"><img src="/{$WEBPATH}image/{$type}_{$obj->id|md5}_3/300/180/" alt=""></a>
	</li>
{/if}			
{if $obj->image4}													
	<li class="span4">
		<a href="/{$WEBPATH}image/{$type}_{$obj->id|md5}_4/800/540/image.jpg" class="thumbnail" data-fancybox-group="group1"><img src="/{$WEBPATH}image/{$type}_{$obj->id|md5}_4/300/180/" alt=""></a>
	</li>
{/if}
{if $obj->image5}													
	<li class="span4">
		<a href="/{$WEBPATH}image/{$type}_{$obj->id|md5}_5/800/540/image.jpg" class="thumbnail" data-fancybox-group="group1"><img src="/{$WEBPATH}image/{$type}_{$obj->id|md5}_5/300/180/" alt=""></a>
	</li>
{/if}
{if $obj->image6}													
	<li class="span4">
		<a href="/{$WEBPATH}image/{$type}_{$obj->id|md5}_6/800/540/image.jpg" class="thumbnail" data-fancybox-group="group1"><img src="/{$WEBPATH}image/{$type}_{$obj->id|md5}_6/300/180/" alt=""></a>
	</li>
{/if}	
</ul>