
		
		{if isset($active) && $active != ''}
		
				<h3 class="title justTitle hidden-phone">Current Category</h3>
					<div class="accordion right-col" id="accordion">
					{if $active == 'business'}
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
									<h3 class="browseTitle nm nl yo">Businesses <i class="icon-chevron-up"></i></h3>
								</a>
							</div>
							<div id="collapseOne" class="accordion-body collapse in">
								<div class="accordion-inner">
									<ul class="nav nav-list">
										<li><a href="/{$WEBPATH}business{if $keyword}?keyword={$keyword}{/if}">&raquo; All</a>{*<span>(35)</span>*}</li>
									{foreach $merchantCategories as $category}
										<li><a href="/{$WEBPATH}business/category/{$category->name|trim|replace:' ':'+'}{if $keyword}?keyword={$keyword}{/if}">&raquo; {$category->name}</a><span>({$category->counter})</span></li>
									{/foreach}
									
									</ul>
								</div>
							</div>
						</div>
					{/if}
						
					{if $active == 'association'}
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
									<h3 class="browseTitle nl nm">Associations <i class="icon-chevron-up"></i></h3>
								</a>
							</div>
							<div id="collapseSeven" class="accordion-body collapse in">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/{$WEBPATH}associations{if $keyword}?keyword={$keyword}{/if}">&raquo; All</a></li>
									{foreach $associationCategories as $article}
										<li><a href="/{$WEBPATH}associations/category/{$article->name|trim|replace:' ':'+'}{if $keyword}?keyword={$keyword}{/if}">&raquo; {$article->name}</a><span>({$article->counter})</span></li>
									{/foreach}
									</ul>
								</div>
							</div>
						</div>
					{/if}
						
					{if $active == 'classifieds'}
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									<h3 class="browseTitle nl nm">Classifieds <i class="icon-chevron-up"></i></h3>
								</a>
							</div>
							<div id="collapseTwo" class="accordion-body collapse in">
								<div class="accordion-inner">
									<ul class="nav nav-list">
										<li><a href="/{$WEBPATH}classifieds{if $keyword}?keyword={$keyword}{/if}">&raquo; All</a>{*<span>(35)</span>*}</li>
									{foreach $classifiedCategories as $category}
										<li><a href="/{$WEBPATH}classifieds/category/{$category->name|trim|replace:' ':'+'}{if $keyword}?keyword={$keyword}{/if}">&raquo; {$category->name}</a><span>({$category->counter})</span></li>
									{/foreach}
									</ul>
								</div>
							</div>
						</div>
					{/if}
						
					{if $active == 'deals'}
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
									<h3 class="browseTitle nl nm">Current Offers<i class="icon-chevron-up"></i></h3>
								</a>
							</div>
							<div id="collapseThree" class="accordion-body collapse in">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/{$WEBPATH}deals{if $keyword}?keyword={$keyword}{/if}">&raquo; All</a>{*<span>(35)</span>*}</li>
									{foreach $dealCategories as $deal}
										<li><a href="/{$WEBPATH}deals/category/{$deal->name|trim|replace:' ':'+'}{if $keyword}?keyword={$keyword}{/if}">&raquo; {$deal->name}</a><span>({$deal->counter})</span></li>
									{/foreach}
									</ul>
								</div>
							</div>
						</div>	
					{/if}
						
					{if $active == 'events'}
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
									<h3 class="browseTitle nl nm">Events <i class="icon-chevron-up"></i></h3>
								</a>
							</div>
							<div id="collapseFour" class="accordion-body collapse in">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/{$WEBPATH}events{if $keyword}?keyword={$keyword}{/if}">&raquo; All</a>{*<span>(35)</span>*}</li>
									{foreach $eventCategories as $event}
										<li><a href="/{$WEBPATH}events/category/{$event->name|trim|replace:' ':'+'}{if $keyword}?keyword={$keyword}{/if}">&raquo; {$event->name}</a><span>({$event->counter})</span></li>
									{/foreach}
									</ul>
								</div>
							</div>
						</div>
					{/if}
					
					{if $active == 'articles'}
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
									<h3 class="browseTitle nl nm">Articles <i class="icon-chevron-up"></i></h3>
								</a>
							</div>
							<div id="collapseFive" class="accordion-body collapse in">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/{$WEBPATH}articles{if $keyword}?keyword={$keyword}{/if}">&raquo; All</a>{*<span>(35)</span>*}</li>
									{foreach $articleCategories as $article}
										<li><a href="/{$WEBPATH}articles/category/{$article->name|trim|replace:' ':'+'}{if $keyword}?keyword={$keyword}{/if}">&raquo; {$article->name}</a><span>({$article->counter})</span></li>
									{/foreach}
									</ul>
								</div>
							</div>
						</div>
					{/if}
					
					{if $active == 'blog'}
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
									<h3 class="browseTitle nl nm">Blog <i class="icon-chevron-up"></i></h3>
								</a>
							</div>
							<div id="collapseSix" class="accordion-body collapse in">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/{$WEBPATH}blog{if $keyword}?keyword={$keyword}{/if}">&raquo; All</a></li>
									{foreach $blogCategories as $article}
										<li><a href="/{$WEBPATH}blog/category/{$article->name|trim|replace:' ':'+'}{if $keyword}?keyword={$keyword}{/if}">&raquo; {$article->name}</a><span>({$article->counter})</span></li>
									{/foreach}
									</ul>
								</div>
							</div>
						</div>
					{/if}
					</div>					
				{/if}
				
				
				<div class="hidden-phone"><!--Not visible on phones-->
				<h3 class="title justTitle">Other by Category</h3>
					<div class="accordion right-col" id="accordion2">
					{if $active != 'business'}
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
									<h3 class="browseTitle nm nl">Businesses <i class="icon-chevron-down"></i></h3>
								</a>
							</div>
							<div id="collapseOne" class="accordion-body collapse {if $active == 'business'}in{/if}">
								<div class="accordion-inner">
									<ul class="nav nav-list">
										<li><a href="/{$WEBPATH}business{if $keyword}?keyword={$keyword}{/if}">&raquo; All</a>{*<span>(35)</span>*}</li>
									{foreach $merchantCategories as $category}
										<li><a href="/{$WEBPATH}business/category/{$category->name|trim|replace:' ':'+'}{if $keyword}?keyword={$keyword}{/if}">&raquo; {$category->name}</a><span>({$category->counter})</span></li>
									{/foreach}
									
									</ul>
								</div>
							</div>
						</div>
					{/if}
						
					{if $active != 'association'}
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseSeven">
									<h3 class="browseTitle nl nm">Associations <i class="icon-chevron-down"></i></h3>
								</a>
							</div>
							<div id="collapseSeven" class="accordion-body collapse {if $active == 'association'}in{/if}">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/{$WEBPATH}associations{if $keyword}?keyword={$keyword}{/if}">&raquo; All</a></li>
									{foreach $associationCategories as $article}
										<li><a href="/{$WEBPATH}associations/category/{$article->name|trim|replace:' ':'+'}{if $keyword}?keyword={$keyword}{/if}">&raquo; {$article->name}</a><span>({$article->counter})</span></li>
									{/foreach}
									</ul>
								</div>
							</div>
						</div>
					{/if}
						
					{if $active != 'classifieds'}
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
									<h3 class="browseTitle nl nm">Classifieds <i class="icon-chevron-down"></i></h3>
								</a>
							</div>
							<div id="collapseTwo" class="accordion-body collapse {if $active == 'classifieds'}in{/if}">
								<div class="accordion-inner">
									<ul class="nav nav-list">
										<li><a href="/{$WEBPATH}classifieds{if $keyword}?keyword={$keyword}{/if}">&raquo; All</a>{*<span>(35)</span>*}</li>
									{foreach $classifiedCategories as $category}
										<li><a href="/{$WEBPATH}classifieds/category/{$category->name|trim|replace:' ':'+'}{if $keyword}?keyword={$keyword}{/if}">&raquo; {$category->name}</a><span>({$category->counter})</span></li>
									{/foreach}
									</ul>
								</div>
							</div>
						</div>
					{/if}
						
					{if $active != 'deals'}
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
									<h3 class="browseTitle nl nm">Current Offers<i class="icon-chevron-down"></i></h3>
								</a>
							</div>
							<div id="collapseThree" class="accordion-body collapse {if $active == 'deals'}in{/if}">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/{$WEBPATH}deals{if $keyword}?keyword={$keyword}{/if}">&raquo; All</a>{*<span>(35)</span>*}</li>
									{foreach $dealCategories as $deal}
										<li><a href="/{$WEBPATH}deals/category/{$deal->name|trim|replace:' ':'+'}{if $keyword}?keyword={$keyword}{/if}">&raquo; {$deal->name}</a><span>({$deal->counter})</span></li>
									{/foreach}
									</ul>
								</div>
							</div>
						</div>	
					{/if}
						
					{if $active != 'events'}
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
									<h3 class="browseTitle nl nm">Events <i class="icon-chevron-down"></i></h3>
								</a>
							</div>
							<div id="collapseFour" class="accordion-body collapse {if $active == 'events'}in{/if}">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/{$WEBPATH}events{if $keyword}?keyword={$keyword}{/if}">&raquo; All</a>{*<span>(35)</span>*}</li>
									{foreach $eventCategories as $event}
										<li><a href="/{$WEBPATH}events/category/{$event->name|trim|replace:' ':'+'}{if $keyword}?keyword={$keyword}{/if}">&raquo; {$event->name}</a><span>({$event->counter})</span></li>
									{/foreach}
									</ul>
								</div>
							</div>
						</div>
					{/if}
					
					{if $active != 'articles'}
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">
									<h3 class="browseTitle nl nm">Articles <i class="icon-chevron-down"></i></h3>
								</a>
							</div>
							<div id="collapseFive" class="accordion-body collapse {if $active == 'articles'}in{/if}">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/{$WEBPATH}articles{if $keyword}?keyword={$keyword}{/if}">&raquo; All</a>{*<span>(35)</span>*}</li>
									{foreach $articleCategories as $article}
										<li><a href="/{$WEBPATH}articles/category/{$article->name|trim|replace:' ':'+'}{if $keyword}?keyword={$keyword}{/if}">&raquo; {$article->name}</a><span>({$article->counter})</span></li>
									{/foreach}
									</ul>
								</div>
							</div>
						</div>
					{/if}
					
					{if $active != 'blog'}
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseSix">
									<h3 class="browseTitle nl nm">Blog <i class="icon-chevron-down"></i></h3>
								</a>
							</div>
							<div id="collapseSix" class="accordion-body collapse {if $active == 'blog'}in{/if}">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/{$WEBPATH}blog{if $keyword}?keyword={$keyword}{/if}">&raquo; All</a></li>
									{foreach $blogCategories as $article}
										<li><a href="/{$WEBPATH}blog/category/{$article->name|trim|replace:' ':'+'}{if $keyword}?keyword={$keyword}{/if}">&raquo; {$article->name}</a><span>({$article->counter})</span></li>
									{/foreach}
									</ul>
								</div>
							</div>
						</div>
					{/if}
				</div>	
			</div><!--Not visible on phones-->				