
        <div class="simplePagination"></div>

		<link type="text/css" rel="stylesheet" href="/{$WEBPATH}css/simplePagination.css"/>
		<script type="text/javascript" src="/{$WEBPATH}js/simplePagination.js"></script>
		<script>
  		$(function() {
        $('.simplePagination').pagination({
            items: {$resultCount},
            itemsOnPage: {$resultLimit},
            cssStyle: 'light-theme',
            hrefTextPrefix: '',
            hrefTextSuffix: '',
            currentPage: {$page|default:1},
            selectOnClick: false,
            onPageClick: function(page, event){
            	
            	var removeQueryFields = function (url) {
                var fields = [].slice.call(arguments, 1).join('|'),
                    parts = url.split( new RegExp('[&?](' + fields + ')=[^&]*') ),
                    length = parts.length - 1;
                    
                if(document.URL.indexOf("?") >= 0){ 
                	return parts[0] + (length ? parts[length].slice(1) : '');
                } else {
                	return parts[0] + "?" + (length ? parts[length].slice(1) : '');
                }
            	}
            	
            	document.location.href = removeQueryFields(document.location.href, 'page') + "&page=" + page;
                        	
            }
        });
   	 	});
   	</script>