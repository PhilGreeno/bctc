							<div class="span7">
								<div class="featured-item">
									<div class="pull-left deal-tag-info">
										{assign var=calculated value=$primaryDeal->value * (1 - ($primaryDeal->discount / 100))}
										<div class="deal-tag">&pound;{$calculated|string_format:"%.2f"}</div>
										<div class="deal-discount">{$primaryDeal->discount}% OFF</div>
									</div>
									<div class="pull-left">
										<a href="/{$WEBPATH}deals/detail/{$primaryDeal->id|md5}">
											<img src="/{$WEBPATH}image/deal_{$primaryDeal->id|md5}_1/440/248/" alt="{$primaryDeal->discount}% off - {$primaryDeal->title}" title="{$primaryDeal->discount}% off - {$primaryDeal->title}" class="img deal-featured">
										</a>
										<p><strong><a href="/{$WEBPATH}deals/detail/{$primaryDeal->id|md5}" title="{$primaryDeal->discount}% off - {$primaryDeal->title}">{$primaryDeal->discount}% off - {$primaryDeal->title}</a></strong>
										<br/>by <a href="/{$WEBPATH}business/detail/{$primaryDeal->companyname|trim|replace:' ':'+'}" title="{$primaryDeal->companyname}">{$primaryDeal->companyname}</a></p>
									</div>
								</div>
							</div>
							<div class="span5">
								<div class="other_info">
								{foreach $deals as $deal}
									<div class="item">
										{assign var=calculated value=$deal->value * (1 - ($deal->discount / 100))}
										<div class="deal-tag">&pound;{$calculated|string_format:"%.2f"}</div>
										<strong><a href="/{$WEBPATH}deals/detail/{$deal->id|md5}" title="{$deal->discount}% OFF {$deal->title}">{$deal->discount}% OFF {$deal->title}</a></strong>
										<br/>by <a href="/{$WEBPATH}business/detail/{$deal->companyname|trim|replace:' ':'+'}" title="{$deal->companyname}">{$deal->companyname}</a>
									</div>
								{/foreach}
								</div>
							</div>