						<div class="span7">
							{foreach $primaryArticles as $article}
								<div class="featured-item">
									<a href="/{$WEBPATH}articles/detail/{$article->id|md5}"><img src="/{$WEBPATH}image/article_{$article->id|md5}_1/152/85/" alt="{$article->title}" title="{$article->title}" class="img pull-left"></a>
									<strong><a href="/{$WEBPATH}articles/detail/{$article->id|md5}">{$article->title}</a></strong>
									<p>Published: {$article->publicationdate|date_format} by {$article->author} in 
									
										{foreach from=$article->categories item=category key=i name=twat}{if not $smarty.foreach.twat.last}, {/if}
												<a href="/{$WEBPATH}articles/category/{$category|trim|replace:' ':'+'}" class="subinfo">{$category|trim}</a> 
											{/foreach}
									</p>
									<p>{$article->abstract}</p>
								</div>
							{/foreach}
							
							</div>
							<div class="span5">
								<div class="other_info">
								{foreach $articles as $article}
									<p>
										<strong><a href="/{$WEBPATH}articles/detail/{$article->id|md5}">{$article->title}</a></strong>
										<br/>in 
										{foreach from=$article->categories item=category key=i name=twat}{if not $smarty.foreach.twat.last}, {/if}
												<a href="/{$WEBPATH}articles/category/{$category|trim|replace:' ':'+'}" class="subinfo">{$category|trim}</a> 
											{/foreach}
										 - {$article->publicationdate|date_format}			
									</p>
								{/foreach}
									
								</div>
							</div>