							<div class="span12">
								<div class="other_info">
								{foreach $deals as $deal}

								<div class="span2">
									<div class="item">
										{assign var=calculated value=$deal->value * (1 - ($deal->discount / 100))}

										

										<a href="/{$WEBPATH}deals/detail/{$deal->id|md5}">

											{if $deal->image1}

											<img src="/{$WEBPATH}image/deal_{$deal->id|md5}_1/100/75/" alt="{$deal->discount}% off - {$deal->title}" title="{$deal->discount}% off - {$deal->title}" class="img deal-featured">

											{else}
											<img class="img" src="/{$WEBPATH}image/placeholder/100/75/" alt="{$deal->discount}% off - {$deal->title}" title="{$deal->discount}% off - {$deal->title}" class="img pull-left"/>
											{/if}

										
										</a>

										<div class="clearfix"></div>

									
										<strong><a href="/{$WEBPATH}deals/detail/{$deal->id|md5}" title="{$deal->discount}% OFF {$deal->title}">{$deal->title}</a>
										</strong>

										<br/>by <a href="/{$WEBPATH}business/detail/{$deal->companyname|trim|replace:' ':'+'}" title="{$deal->companyname}">{$deal->companyname}</a>

									


									</div>
								</div>
								{/foreach}
								</div>
							</div>