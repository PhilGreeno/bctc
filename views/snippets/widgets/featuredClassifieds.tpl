							{foreach $classifieds as $classified}
							
								<div class="span3">
									<div class="item">
										<a href="/{$WEBPATH}classifieds/detail/{$classified->id|md5}">
										{if $classified->image1}
											<img class="img" src="/{$WEBPATH}image/classified_{$classified->id|md5}_1/200/128/" alt="{$classified->title}" title="{$classified->title}">
										{else}
											<img class="img" src="/{$WEBPATH}image/placeholder/200/128/" alt="{$classified->title}" title="{$classified->title}">
										
										{/if}
										</a>
										<div class="clearfix"></div> 
										<a href="/{$WEBPATH}classifieds/detail/{$classified->id|md5}" class="title">{$classified->title}</a> 
										<span class="label ">&pound;{$classified->price|string_format:"%.2f"}</span>

										<br/>Found in the
										{foreach from=$classified->categories item=category key=i name=twat}{if not $smarty.foreach.twat.last}, {/if}
											<a href="/{$WEBPATH}classifieds/category/{$category|trim|replace:' ':'+'}" class="subinfo">"{$category|trim}"</a>
										{/foreach}Category<br>
										Supported by {$classified->companyname}
									</div>
								</div>
								
							{/foreach}