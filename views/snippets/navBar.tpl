		<div class="navbar center">
			<div class="navbar-inner main-menu">
				<div class="container-fluid">
					

						
						
						<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">

							Menu
						</a>						
						<div class="nav-collapse collapse">							 		                 
							<ul class="nav" id="main-nav">								
								<li {if $active == 'home'}class="active"{/if}><a href="/{$WEBPATH}">Home</a></li>

								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										<span id="dropdown2">About Broadstone
										<b class="caret"></b>
										</span>
									</a>
									<ul class="dropdown-menu">

									
					
									{foreach $cmsPages as $page}
										<li><a href="/{$WEBPATH}static/{$page->title|trim|replace:' ':'+'}">{$page->link}</a></li>
									{foreachelse}
									{/foreach}

									<li {if $active == 'association'}class="active"{/if}><a href="/{$WEBPATH}associations">Associations</a></li>

								</ul>
								</li>
								
								
								<li {if $active == 'business'}class="active"{/if}><a href="/{$WEBPATH}business">Businesses</a></li>	

								<li {if $active == 'deals'}class="active"{/if}><a href="/{$WEBPATH}deals">Offers</a></li>

										
								
								<li {if $active == 'events'}class="active"{/if}><a href="/{$WEBPATH}events">Events</a></li>	

								<li {if $active == 'classifieds'}class="active"{/if}><a href="/{$WEBPATH}classifieds">Classifieds</a></li>								
								
								<li {if $active == 'articles'}class="active"{/if}><a href="/{$WEBPATH}articles">Articles</a></li>								
								<!-- <li {if $active == 'blog'}class="active"{/if}><a href="/{$WEBPATH}blog">Blog</a></li>	 -->

								<li {if $active == 'contact'}class="active"{/if}><a href="/{$WEBPATH}static/Contact+Us">Contact Us</a></li>	
								
								
					
													
							</ul>
						</div>
						
					
				</div>
			</div>		
		</div>
		<div class="clearfix"></div>