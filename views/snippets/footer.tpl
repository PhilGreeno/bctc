		<div class="container-fluid">
			
			<div class="row-fluid broadstoneMessage">

				<p class="footerBlurb">"Meeting the Broadstone Customer Expectation to feel welcome and respected, to have choice and value, to be satisfied and safe"</p>	

			</div>
		</div>




		<div class="container-fluid footer">		
			

			<div class="row-fluid">					
				 <div class="span3">   
					<h4 class="">Information</h4>
					<ul>
					
					{foreach $cmsPages as $page}
						<li><a href="/{$WEBPATH}static/{$page->title|trim|replace:' ':'+'}">{$page->link}</a></li>
					{foreachelse}
					{/foreach}
					
					</ul>
				</div>
				
				<div class="span6">
					<h4>Connect with us</h4>
					<a href="https://www.facebook.com/stepintobroadstone?ref=hl" target="_blank"><img src="/{$WEBPATH}img/social-facebook.png" alt="Facebook" /></a>
					<a href="https://twitter.com/BroadstoneVilge" target="_blank"><img src="/{$WEBPATH}img/social-twitter.png" alt="Twitter" /></a>
					<!-- <a href="#"><img src="/{$WEBPATH}img/social-rss.png" alt="RSS" /></a> -->
					<!-- <a href="#"><img src="/{$WEBPATH}img/social-flickr.png" alt="Flickr" /></a> -->
				</div>
				<div class="span3">
					<div class="company_info">
						<p>Proudly presented by:
						<h4>Broadstone Chamber of Trade &amp; Commerce</h4>
						<p>Website Designed &amp; Developed by:</p>
						<a href="http://flare-web.co.uk" target="_blank">Flare Web Design</a>
						
					</div>
				</div>					
			</div>	
		</div>