<div id="businessList" class="listing-item">
							<div class="row-fluid">							
								<div class="span7">
									<div class="featured-item" id='item_{$merchant->id|md5}'>
										<div class="span4">		
										
											<a href="/{$WEBPATH}business/detail/{$merchant->companyname|trim|replace:' ':'+'}">
										{if $merchant->image1}
												<img class="img" src="/{$WEBPATH}image/merchant_{$merchant->id|md5}_1/152/85/" alt="{$merchant->companyname}" title="{$merchant->companyname}" class="img pull-left"/>
										{else}
												<img class="img" src="/{$WEBPATH}image/placeholder/152/85/" alt="{$merchant->companyname}" title="{$merchant->companyname}" class="img pull-left"/>
										{/if}
											</a>
											
										
										</div>
										<div class="span8">
											<h5>{$smarty.foreach.foo.index + 1}. <a href="/{$WEBPATH}business/detail/{$merchant->companyname|trim|replace:' ':'+'}" class="title">{$merchant->companyname}</a> </h5>
											in 
											{foreach from=$merchant->categories item=category key=i name=twat}{if not $smarty.foreach.twat.last}, {/if}
												<a href="/{$WEBPATH}business/category/{$category|trim|replace:' ':'+'}" class="subinfo">{$category|trim}</a>
											{/foreach}
											<hr/>
											<p>{$merchant->summary|truncate:250:"...":true}</p>

										</div>								
									</div>								
								</div>
								<div class="span5">
									<div class="other_info">
										<address>				
											<i>{$merchant->address1},<br/>
											{if $merchant->address2 != ''}{$merchant->address2}, {/if}
											{if $merchant->address3 != ''}{$merchant->address3}, {/if}
											{$merchant->county} {$merchant->postcode}.</i>				
										</address>
										<p><strong>Contact Name:</strong> {$merchant->firstname} {$merchant->lastname}<br/>
										
									{if $merchant->phone}
										<strong>Phone:</strong> {$merchant->phone|phone_format:'UK'}<br/>
									{/if}
									
									{if $merchant->url}
										<strong>URL:</strong> <a href="{$merchant->url}" target="_blank">{$merchant->url|url}</a>
									{/if}	
										</p>

									</div>
								</div>
							</div>
						</div>