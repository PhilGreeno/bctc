{foreach $merchants as $merchant}
							
								<div class="span3">
									<div class="item">
										<a href="/{$WEBPATH}business/detail/{$merchant->companyname|trim|replace:' ':'+'}">
										{if $merchant->image1}
											<img class="img" src="/{$WEBPATH}image/merchant_{$merchant->id|md5}_1/200/128/" alt="{$merchant->companyname}" title="{$merchant->companyname}">
										{else}
											<img class="img" src="/{$WEBPATH}image/placeholder/200/128/" alt="{$merchant->companyname}" title="{$merchant->companyname}">
										{/if}
										</a>
										<a href="/{$WEBPATH}business/detail/{$merchant->companyname|trim|replace:' ':'+'}" class="title">{$merchant->companyname}</a> 
									{*	<span class="label label-success">new</span> *}
										{assign var=category value=reset($merchant->categories)}
										<br/>in
										{foreach from=$merchant->categories item=category key=i name=twat}{if not $smarty.foreach.twat.last}, {/if} 
											<a href="/{$WEBPATH}business/category/{$category|trim|replace:' ':'+'}" class="subinfo">{$category|trim}</a>
										{/foreach}
										<p>{$merchant->summary|truncate:80:"...":true}</p>
									</div>
								</div>
								
							{/foreach}