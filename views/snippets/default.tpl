<!doctype html>
<!--[if lt IE 7]>      <html class="no-js ie6"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie7"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js">     <!--<![endif]-->

	<head>
		
		<title>Broadstone Village : {block name="pageName"}Default Page{/block}</title>
		
		<script>
			document.documentElement.className = document.documentElement.className.replace(/(\s|^)no-js(\s|$)/, '$1' + 'js' + '$2');
		</script>
		
		{block name="meta"}
		<meta charset="UTF-8" />
		<meta name="robots" content="index, follow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		{/block}

		<link rel='stylesheet' type='text/css' href='/{$WEBPATH}css/bootstrap.min.css'>
		<link rel='stylesheet' type='text/css' href='/{$WEBPATH}css/bootstrap-responsive.min.css'>
		<link rel='stylesheet' type='text/css' href='/{$WEBPATH}css/main.css'>
		<link rel='stylesheet' type='text/css' href='/{$WEBPATH}css/responsiveslides.css'>
		
		<link rel="stylesheet" type="text/css" href="/{$WEBPATH}css/app.css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
	
		{block name="css"}{/block}

		<!--<script src="/{$WEBPATH}js/less-1.3.3.min.js" type="text/javascript"></script>-->
		<script type="text/javascript" src="//use.typekit.net/mlu5jbc.js"></script>
{literal} <script type="text/javascript">try{Typekit.load();}catch(e){}</script> {/literal}
		<!--<script src="/{$WEBPATH}js/jquery.js"></script>
		<script src="/{$WEBPATH}js/bootstrap.min.js"></script>
		<script src="/{$WEBPATH}js/app.js"></script>
		<script src="/{$WEBPATH}js/custom.js"></script>-->
		<script src="/{$WEBPATH}js/prod-ck.js"></script>

		
		{block name="additionalJavascripts"}



		{/block}



	</head>
	
	<body lang="en" id="body">



		{block name="header"}
			{include file='snippets/navBar.tpl'}	
			{include file='snippets/topBar.tpl'}
			
			{include file='snippets/widgets/searchBar.tpl' active='business'}
					
		{/block}

		<div id="content">
		{block name="content"}{/block}
		</div>
		
		{block name='footer'}
			{include file='snippets/footer.tpl'}
		{/block}
		
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-40328774-1']);
  //_gaq.push(['_setAccount', 'UA-42105691-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>




		
	</body>


</html>