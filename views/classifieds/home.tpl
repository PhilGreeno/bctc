{extends "snippets/default.tpl"}

{block name="pageName"}Classifieds{/block}

{block name="css"}{/block}

{block name="additionalJavascripts"}{/block}

{block name="header"}
			{include file='snippets/navBar.tpl' active='classifieds'}
			{include file='snippets/topBar.tpl'}
{/block}

{block name="content"}

		<div class="container-fluid">
			<div class="row-fluid">


				<div class="span3">
					
					{include file='snippets/widgets/browseByCategory.tpl' active='classifieds'}
					
				</div>




				<div class="span9">  
				                 

					
					
					
					<div class="main_content detail-product">						
						<h3 class="title classTitle justTitle">Community For Sale Board


							<span class="pull-right hidden-phone">{include file='snippets/widgets/paginator.tpl'}</span>



						</h3>


						
					{foreach $classifieds as $classified name=foo}
						<div class="listing-item">
							<div class="row-fluid">		
												
								<div class="span7">
									<div class="featured-item" id='item_{$classified->id|md5}'>
									
										<div class="span4">		
										
											<a href="/{$WEBPATH}classifieds/detail/{$classified->id|md5}">
										{if $classified->image1}
												<img class="img" src="/{$WEBPATH}image/classified_{$classified->id|md5}_1/152/85/" alt="{$classified->title}" title="{$classified->title}" class="img pull-left"/>
										{else}
												<img class="img" src="/{$WEBPATH}image/placeholder/152/85/" alt="{$classified->title}" title="{$classified->title}" class="img pull-left"/>
										{/if}
											</a>
						
										</div>
										<div class="span8">
											<h5>{$smarty.foreach.foo.index + 1}. <a href="/{$WEBPATH}classifieds/detail/{$classified->id|md5}" class="title">{$classified->title}</a> </h5>
											in 
											{foreach from=$classified->categories item=category key=i name=twat}{if not $smarty.foreach.twat.last}, {/if} 
												<a href="/{$WEBPATH}classifieds/category/{$category|trim|replace:' ':'+'}" class="subinfo">{$category|trim}</a>
											{/foreach}
											by {$classified->contact}
											<hr/>
											<p>{$classified->summary|truncate:250:"...":true}</p>

										</div>								
									</div>								
								</div>
								<div class="span5">
									<div class="other_info hidden-phone">
										<address>				
											<i>{$classified->address1},<br/>
											{if $classified->address2 != ''}{$classified->address2}, {/if}
											{if $classified->address3 != ''}{$classified->address3}, {/if}
											{$classified->county} {$classified->postcode}.</i>				
										</address>
										<p><strong>Price:</strong> &pound;{$classified->price|string_format:"%.2f"}<br/>
									{if $classified->phone}
										<strong>Phone:</strong> {$classified->phone|phone_format:'UK'}<br/>
									{/if}
									
									{if $classified->url}
										<strong>URL:</strong> <a href="{$classified->url}" target="_blank">{$classified->url|url}</a>
									{/if}	
										</p>
									</div>
								</div>
							</div>
						</div>
					{foreachelse}
						<h5>There are no classifieds listed that match your criteria</h5>
					{/foreach}

					<div class="clearfix"></div>


					<div class="bottomPageCount">{include file='snippets/widgets/paginator.tpl'}</div>

					</div>
				</div>
				
			</div>			
		</div>


{/block}