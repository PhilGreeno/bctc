{extends "snippets/default.tpl"}

{block name="pageName"}{$event->title|capitalize}{/block}

{block name="css"}
<link href="/{$WEBPATH}css/jquery.fancybox.css" rel="stylesheet">
{/block}

{block name="additionalJavascripts"}

<script src="/{$WEBPATH}js/jquery.fancybox.js"></script>
<script>
	$(document).ready(function() {
		$('.thumbnail').fancybox({
			openEffect  : 'none',
			closeEffect : 'none'
		});
	});
</script>

{/block}

{block name="header"}
			{include file='snippets/navBar.tpl' active='events'}
			{include file='snippets/topBar.tpl'}
{/block}

{block name="content"}


		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span9">                   
					

					
					<div class="main_content detail-product detail-back">						
						<h3 class="title justTitle">{$event->title} - Starts on {$event->startdate|date_format:'%b %e, %Y at %H:%M'}</h3>			
						<div class="row-fluid">
							

							<div class="span8">
								
								<ul class="social pull-right">
									<li>									
										<div class="fb-like" send="false" layout="button_count" ></div>
										<script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script>
									</li>
									<li>
										<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal" >Tweet</a>
										<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
									</li>
								</ul>

		
							
								<h5>Overview</h5>
								<p>{$deal->event|default:'n/a'}</p>				
								
								<p><strong>When :</strong> {$event->startdate|date_format:'%b %e, %Y at %H:%M'} - {$event->enddate|date_format:'%b %e, %Y at %H:%M'}</p>		
										
								<div class="contact-information">
									<h5>Contact Information</h5>
									<address>				
											<i>{$event->address1},<br/> 
											{if $event->address2 != ''}{$event->address2}, {/if}
											{if $event->address3 != ''}{$event->address3}, {/if}
											{$event->county} {$event->postcode}.</i>				
									</address>
									<p>
								{*		<strong>Offered By:</strong> <a href="/{$WEBPATH}business/detail/{$deal->companyname|trim|replace:' ':'+'}">{$deal->companyname}</a><br/> *}
										<strong>Contact Name:</strong> {$event->contactname|capitalize}<br/>
										<strong>Phone:</strong> {$event->phone|default:'n/a'}<br/>
										<strong>E-mail:</strong> <a href="#emailModal" role="button" data-toggle="modal">send an e-mail</a><br/>
										<strong>URL:</strong> <a href="{$event->url}" target="_blank">{$event->url}</a></p>
								</div>							
							</div>
							
							<div class="span4">
							
								{include file='snippets/widgets/imageGrid.tpl' type=event obj=$event}
							
							</div>
														
						</div>
						<div class="row-fluid">
							<div class="span12">

								<h3 class="title">Description</h3>	
								<p>{$event->description|default:'n/a'}</p>							
							</div>							
						</div>						
					</div>
				</div>
				
				<div class="span3">
					
					<h3 class="title justTitle">Location</h3><br/>
					{include file='snippets/widgets/map.tpl' mapper=array($event)}
					
				{*	<h3 class="title justTitle">Event Calendar</h3>
					<div class="calendar"></div><br/> *}
					
					<h3 class="title justTitle">Comments</h3>
					
					{include file='snippets/widgets/comments.tpl' comment_ident="event/detail/{$event->id|md5}"}
					
					{* include file='snippets/widgets/browseByCategory.tpl' *}
					
					
				</div>
			</div>			
		</div>
		
		{*include file='snippets/widgets/eventCal.tpl'*}
		{include file='snippets/modals/emailModal.tpl' type=event ident=$event->id|md5}
{/block}