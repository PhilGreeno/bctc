{extends "snippets/default.tpl"}

{block name="pageName"}Events{/block}

{block name="css"}
	
{/block}

{block name="additionalJavascripts"}
	
{/block}

{block name="header"}
			{include file='snippets/navBar.tpl' active='events'}
			{include file='snippets/topBar.tpl'}
{/block}

{block name="content"}

		<div class="container-fluid">
			<div class="row-fluid">



				<div class="span3">
					
					
					

					{include file='snippets/widgets/browseByCategory.tpl' active='events'}
										
					
				</div>





				<div class="span9">  
				                 
					

					<div class="row-fluid">


						{include file='snippets/widgets/map.tpl' mapper=$events}
						

					</div>
					
					<div class="main_content detail-product">						
						<h3 class="title justTitle">Upcoming Events in Broadstone

							<span class="pull-right hidden-phone">{include file='snippets/widgets/paginator.tpl'}</span>


						</h3>


						


					
					{foreach $events as $event name=foo}
						<div class="listing-item">
							<div class="row-fluid">							
								<div class="span7">
									<div class="featured-item" id='item_{$event->id|md5}'>
										<div class="span4">								
											<a href="/{$WEBPATH}events/detail/{$event->id|md5}">
											{if $event->image1}
												<img class="img" src="/{$WEBPATH}image/event_{$event->id|md5}_1/152/85/" alt="{$event->title}" title="{$event->title}" class="img pull-left"/>
											{else}
												<img class="img" src="/{$WEBPATH}image/placeholder/152/85/" alt="{$event->title}" title="{$event->title}" class="img pull-left"/>
											{/if}
											</a>									
										</div>
										<div class="span8">
											<h5>{$smarty.foreach.foo.index + 1}. <a href="/{$WEBPATH}events/detail/{$event->id|md5}" class="title">{$event->title}</a> - Starts on {$event->startdate|date_format}</h5>
											in 
											{foreach from=$event->categories item=category key=i name=twat}{if not $smarty.foreach.twat.last}, {/if}
												<a href="/{$WEBPATH}events/category/{$category|trim|replace:' ':'+'}" class="subinfo">{$category|trim}</a> 
											{/foreach}
											by {$event->contactname|capitalize}
											<hr/>
											<p>{$event->summary|stripslashes|truncate:250:"...":true}</p>
										</div>								
									</div>								
								</div>
								<div class="span5">
									<div class="other_info hidden-phone">
										
										<address>				
											<i>{$event->address1},<br/>
											{if $event->address2 != ''}{$event->address2}, {/if}
											{if $event->address3 != ''}{$event->address3}, {/if}
											{$event->county} {$event->postcode}.</i>				
										</address>
										<strong>Phone:</strong> {$event->phone}<br/>
								{*		<strong>E-mail:</strong> <a href="#myModal" role="button" data-toggle="modal">send an e-mail</a><br/> *}
										<strong>URL:</strong> <a href="{$deal->url}" target="_blank">{$event->url}</a></p>
									</div>
								</div>
							</div>
						</div>
					{foreachelse}
						<h5>There are no events listed that match your criteria</h5>
					{/foreach}

					<div class="clearfix"></div>

					<div class="bottomPageCount">{include file='snippets/widgets/paginator.tpl'}</div>

					</div>


					 <div class="row-fluid">
						<div class="span12">


							<h3 class="title justTitle">Event Calender</h3>
							<div class="calendar">
								
								{include file='snippets/widgets/eventCal.tpl'}
							</div><br/>


							

						</div>
					</div> 
				

					



				</div><!--Span9-->
				
			</div>			
		</div>

		
		
{/block}