{extends "snippets/default.tpl"}

{block name="pageName"}Home{/block}

{block name="css"}{/block}

{block name="additionalJavascripts"}{/block}

{block name="header"}
			{include file='snippets/navBar.tpl' active='home'}	
			{include file='snippets/topBar.tpl'}
			
			
{/block}



{block name="content"}

<div class="heroBack">

		<div class="container">

			<div class="slideshow">
					
					{include file='snippets/widgets/slideshow.tpl'}

				</div>
		</div>

</div><!--heroBack-->
		<div class="container-fluid home">
			<div class="row-fluid">


			
				
			
				<div class="span9">    

						<h3 class="title dealTitle">
							<span class="titleText pull-left">Special Offers</span>
							<span class="pull-right CTAbutton">
								<a href="/{$WEBPATH}deals">View all offers 
									<i class="icon-chevron-right icon-white"></i>

								</a></span>
						</h3>	

						<div class="container-fluid tabletShift">
							<div class="row-fluid">
							
							{include file='snippets/widgets/featuredDeals.tpl'}

							</div>
							
						</div>
				               
					
						<h3 class="title bizTitle"><span class="titleText pull-left">Shopping &amp; Services</span>
							<span class="pull-right CTAbutton">
								<a href="/{$WEBPATH}business">View all shops<i class="icon-chevron-right icon-white"></i>
								</a>
							</span>
						</h3>
						<div class="container-fluid tabletShift">
							<div class="row-fluid product_listing">
	
							
							{include file='snippets/business/bus2cusLoop.tpl'}

							</div>
						</div>



						<h3 class="title bizTitle"><span class="titleText pull-left">Eating, Drinking &amp; Leisure</span>
							<span class="pull-right CTAbutton">
								<a href="/{$WEBPATH}business">View all activities<i class="icon-chevron-right icon-white"></i>
								</a>
							</span>
						</h3>
						<div class="container-fluid tabletShift">
							<div class="row-fluid product_listing">
	
							
							{include file='snippets/business/leisureBusinessLoop.tpl'}
							

							</div>
						</div>



						<h3 class="title bizTitle"><span class="titleText pull-left">Business &amp; Other Services</span>
							<span class="pull-right CTAbutton">
								<a href="/{$WEBPATH}business">View all services<i class="icon-chevron-right icon-white"></i>
								</a>
							</span>
						</h3>
						<div class="container-fluid tabletShift">
							<div class="row-fluid product_listing">
	
							
							{include file='snippets/business/bus2busLoop.tpl'}
							
							

							</div>
						</div>


						<h3 class="title classTitle"><span class="titleText pull-left">Classified Ads</span>
							<span class="pull-right CTAbutton"><a href="/{$WEBPATH}classifieds">View all classifieds <i class="icon-chevron-right icon-white"></i> </a></span></h3>
						<div class="container-fluid tabletShift">
							<div class="row-fluid product_listing">
							
								{include file='snippets/widgets/featuredClassifieds.tpl'}

							</div>
						</div>
					
						<h3 class="title artTitle"><span class="titleText pull-left">Recent Broadstone Articles</span>
							<span class="pull-right CTAbutton">
								<a href="/{$WEBPATH}articles">View all articles

									<i class="icon-chevron-right icon-white"></i>

								</a>
							</span>
						</h3>
						<div class="row-fluid">
							
								{include file='snippets/widgets/recentArticles.tpl'}
							
						</div>


						
					
				</div> <!-- Span9 -->

				{include file='snippets/sidebar.tpl'}
				
				
				
			</div <!-- Row Fluid -->			
		</div>

		

{/block}