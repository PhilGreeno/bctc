{extends "snippets/default.tpl"}

{block name="pageName"}Home{/block}

{block name="css"}{/block}

{block name="additionalJavascripts"}{/block}

{block name="header"}
			{include file='snippets/navBar.tpl' active='home'}	
			{include file='snippets/topBar.tpl'}
			
			{include file='snippets/widgets/searchBar.tpl' active='business'}
{/block}



{block name="content"}

<div class="heroBack">

		<div class="container">

			<div class="slideshow">
					
					{include file='snippets/widgets/slideshow.tpl'}

				</div>
		</div>

</div><!--heroBack-->
		<div class="container-fluid">
			<div class="row-fluid">


			{include file='snippets/sidebar.tpl'}
				
			
				<div class="span9">    

						<h3 class="title dealTitle">
							<span class="titleText pull-left">Featured Broadstone Offers &amp; Deals</span>
							<span class="pull-right CTAbutton">
								<a href="/{$WEBPATH}deals">View all deals 
									<i class="icon-chevron-right icon-white"></i>

								</a></span>
						</h3>	

						<div class="container-fluid">
							<div class="row-fluid">
							
							{include file='snippets/widgets/featuredDeals.tpl'}

							</div>
							
						</div>
				               
					
						<h3 class="title bizTitle"><span class="titleText pull-left">Featured Broadstone Businesses</span>
							<span class="pull-right CTAbutton">
								<a href="/{$WEBPATH}business">View all shops<i class="icon-chevron-right icon-white"></i>
								</a>
							</span>
						</h3>
						<div class="container-fluid">
							<div class="row-fluid product_listing">
	
								{include file='snippets/widgets/featuredBusinesses.tpl'}

							</div>
						</div>

						<h3 class="title classTitle"><span class="titleText pull-left">Featured Broadstone Classifieds</span>
							<span class="pull-right CTAbutton"><a href="/{$WEBPATH}classifieds">View all classifieds <i class="icon-chevron-right icon-white"></i> </a></span></h3>
						<div class="container-fluid">
							<div class="row-fluid product_listing">
							
								{include file='snippets/widgets/featuredClassifieds.tpl'}

							</div>
						</div>
					
						<h3 class="title artTitle"><span class="titleText pull-left">Recent Broadstone Articles</span>
							<span class="pull-right CTAbutton">
								<a href="/{$WEBPATH}articles">View all articles

									<i class="icon-chevron-right icon-white"></i>

								</a>
							</span>
						</h3>
						<div class="row-fluid">
							
								{include file='snippets/widgets/recentArticles.tpl'}
							
						</div>


						
					
				</div> <!-- Span9 -->
				
				
				
			</div <!-- Row Fluid -->			
		</div>

		

{/block}