{extends "snippets/default.tpl"}

{block name="pageName"}Businesses{/block}

{block name="css"}{/block}

{block name="additionalJavascripts"}{/block}

{block name="header"}
			{include file='snippets/navBar.tpl' active='business'}
			{include file='snippets/topBar.tpl'}
{/block}

{block name="content"}

		<div class="container-fluid">


			
			<div class="row-fluid">

			

				

				<div class="span2">

					
					{include file='snippets/widgets/browseByCategory.tpl' active='business'}
					
											
				</div>




				<div id="mainContent" class="span10"> 

					{if $merchants}
				    
					    {include file='snippets/widgets/alphabetSearch.tpl'}

						
							<div class="hidden-phone">
							{include file='snippets/widgets/map.tpl' mapper=$merchants}
							</div>

					
						
					{/if}
					
					
					
					<div class="main_content detail-product">						
						<h3 class="title bizTitle justTitle hidden-phone">

							<span class="titleText pull-left">{if isset($currentCategory)}{$currentCategory} in Broadstone
								{elseif isset($keyword)} Business Search results for {$keyword}
								{else}Businesses &amp; Merchants of Broadstone
								{/if}</span>

						

						<span class="pull-right hidden-phone">{include file='snippets/widgets/paginator.tpl'}</span>


						</h3>

					{if $merchants}

						<div class="row-fluid">

						{foreach $merchants as $merchant name=foo}


							{include file='snippets/business/businessGridItem.tpl' idx=$smarty.foreach.foo.index}
						
							
						{/foreach}



						<div class="clearfix"></div>

						
						<div class="bottomPageCount">{include file='snippets/widgets/paginator.tpl'}</div>

						</div>
					


					{else}
						
						
						<div class="sosa bigSearch">\</div>
						<h5 class="missingSearch">Sorry nothing currently matches your search...please try again.</h5>
						
					{/if}

						
						

					</div>
				</div>

				
				
			</div>			
		</div>


{/block}