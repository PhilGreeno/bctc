{extends "snippets/default.tpl"}

{block name="pageName"}{$merchant->companyname|capitalize}{/block}

{block name="css"}
<link href="/{$WEBPATH}css/jquery.fancybox.css" rel="stylesheet">
{/block}

{block name="additionalJavascripts"}

<script src="/{$WEBPATH}js/jquery.fancybox.js"></script>
<script>
	$(document).ready(function() {
		$('.thumbnail').fancybox({
			openEffect  : 'none',
			closeEffect : 'none'
		});
	});
</script>

{/block}

{block name="header"}
			{include file='snippets/navBar.tpl' active='business'}
			{include file='snippets/topBar.tpl'}
			
{/block}

{block name="content"}


		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span9">                   
					
					
					
					<div class="main_content detail-product detail-back">						
						<h3 class="title justTitle">{$merchant->companyname|capitalize}</h3>			
						<div class="row-fluid">
							

							<div class="span8">
								
								<ul class="social pull-right">
									<li>									
										<div class="fb-like" send="false" layout="button_count" ></div>
										<script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script>
									</li>
									<li>
										<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal" >Tweet</a>
										<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
									</li>
								</ul>

							
								<h5>Who we are:</h5>
								
								{if $merchant->summary}
								<p>{$merchant->summary|stripslashes|default:'n/a'}</p>		
								{/if}	

								<h5>What we offer:</h5>
								{if $merchant->keywords}
								<p>{$merchant->keywords}</p>
								{/if}
											
								<div class="contact-information">
									<h5>Contact &amp; Visit Us:</h5>
									<address>				
											<i>{$merchant->address1},<br/> 
											{if $merchant->address2 != ''}{$merchant->address2}, {/if}
											{if $merchant->address3 != ''}{$merchant->address3}, {/if}
											{$merchant->county} {$merchant->postcode}.</i>				
									</address>
									
									{if $merchant->extrafield1}
										<p><strong>Open Hours:</strong> {$merchant->extrafield1}</p>
									{/if}
									
									
									<ul>
										<li><i class="sosa squareIcon">Ğ</i><strong>Contact Name:</strong> {$merchant->firstname} {$merchant->lastname}</li>
									
									{if $merchant->phone}
										<li><i class="sosa squareIcon">4</i><strong>Phone:</strong> {$merchant->phone|phone_format:'UK'}</li>
									{/if}	
										
									{if $merchant->mobile}
										<li><i class="sosa squareIcon">5</i><strong>Mobile:</strong> {$merchant->mobile|phone_format:'UK'}</li>
									{/if}
									
									{if $merchant->fax}
										<li><i class="sosa squareIcon">3</i><strong>Fax:</strong> {$merchant->fax|phone_format:'UK'}</li>
									{/if}
									
										<li><i class="sosa squareIcon">É</i><strong>E-mail:</strong> <a href="#emailModal" role="button" data-toggle="modal">send an e-mail</a></li>

										<li><i class="sosa web squareIcon">8</i><strong>Website Address:</strong> <a href="{$merchant->url}" target="_blank">{$merchant->url|url}</a></li>
										
									{if $merchant->facebook}
										<li><i class="sosa fb squareIcon">v</i><strong>Facebook:</strong> <a href="{$merchant->facebook}" target="_blank">{$merchant->facebook|url}</a></li>
									{/if}
						
									{if $merchant->twitter}
										<li><i class="sosa twit squareIcon">u</i><strong>Twitter:</strong> <a href="{$merchant->twitter}" target="_blank">{$merchant->twitter|url}</a></li>
									{/if}
										</ul>
								</div>							
							</div>
							
							<div class="span4">
								
								{include file='snippets/widgets/imageGrid.tpl' type=merchant obj=$merchant}
								
							</div>
														
						</div>
						
						{if $merchant->description}
						
						<div class="row-fluid">
							<div class="span8">
								<h5>More About Our Business:</h5>	
								<p>{$merchant->description|markdown}</p>							
							</div>	

							{if $deals}

							<div class="span4">
					<h5>Current Offers:</h5>
					
							<div class="other_info">
								{foreach $deals as $deal}
									<div class="item">
										{assign var=calculated value=$deal->value * (1 - ($deal->discount / 100))}
										<div class="deal-tag">&pound;{$calculated|string_format:"%.2f"}</div>
										<strong><a href="/{$WEBPATH}deals/detail/{$deal->id|md5}" title="{$deal->discount}% OFF {$deal->title}">{$deal->discount}% Off {$deal->title}</a></strong>
										<br/>by <a href="/{$WEBPATH}business/detail/{$deal->companyname|trim|replace:' ':'+'}" title="{$deal->companyname}">{$deal->companyname}</a>
									</div>
								{/foreach}
								</div>
						</div>
					
					{/if}



						</div>
						
						{/if}						
					</div>
				</div>
				
				<div class="span3">

				{if $merchant->long+$merchant->lat }
					
					<h3 class="title justTitle">Location</h3><br/>
					{include file='snippets/widgets/map.tpl' mapper=array($merchant)}

				{/if}
					
					
				{*	<h3 class="title ">Articles</h3> *}
					
					<h3 class="title justTitle">Comments</h3>
					
					{include file='snippets/widgets/comments.tpl' comment_ident="business/detail/{$merchant->companyname|trim|replace:' ':'+'}"}
				
				</div>
			</div>			
		</div>
		
				{include file='snippets/modals/emailModal.tpl' type=merchant ident=$merchant->id|md5}
{/block}