{extends "snippets/default.tpl"}

{block name="pageName"}Offers{/block}

{block name="css"}{/block}

{block name="additionalJavascripts"}{/block}

{block name="header"}
			{include file='snippets/navBar.tpl' active='deals'}
			{include file='snippets/topBar.tpl'}
{/block}

{block name="content"}

		<div class="container-fluid">
			<div class="row-fluid">

				<div class="span3">
					
					{include file='snippets/widgets/browseByCategory.tpl' active='deals'}
					
				</div>

				<div class="span9"> 
					
					<div class="main_content detail-product">						
						<h3 class="title dealTitle justTitle">Broadstone Business Offers


							<span class="pull-right hidden-phone">{include file='snippets/widgets/paginator.tpl'}</span>



						</h3>

						
					
					{foreach $deals as $deal}
						<div class="listing-item">
							<div class="row-fluid">							
								<div class="span7">
									<div class="featured-item">
										<div class="span4">								
											<a href="/{$WEBPATH}deals/detail/{$deal->id|md5}">
											{if $deal->image1}
												<img class="img" src="/{$WEBPATH}image/deal_{$deal->id|md5}_1/152/85/" alt="{$deal->title}" title="{$deal->title}" class="img pull-left"/>
											{else}
												<img class="img" src="/{$WEBPATH}image/placeholder/152/85/" alt="{$deal->title}" title="{$deal->title}" class="img pull-left"/>
											{/if}
											</a>									
										</div>
										<div class="span8">
											<h5><a href="/{$WEBPATH}deals/detail/{$deal->id|md5}" class="title">{$deal->title}</a> - expires on {$deal->enddate|date_format}</h5>

											Category: 

											{foreach from=$deal->categories item=category key=i name=twat}{if not $smarty.foreach.twat.last}, {/if}
												<a href="/{$WEBPATH}deals/category/{$category|trim|replace:' ':'+'}" class="subinfo">{$category|trim}</a>
											{/foreach}
											
										 <div class="pull-right">by <a href="/{$WEBPATH}business/detail/{$deal->companyname|trim|replace:' ':'+'}">{$deal->companyname}</a></div>

										 <div class="clearfix"></div>

											
											<hr/>
											<p>{$deal->summary|truncate:250:"...":true}</p>
										</div>								
									</div>								
								</div>
								<div class="span5">
									<div class="other_info hidden-phone">
										{if $deal->value}
									<p>
										<strong>Price:</strong> {assign var=calculated value=$deal->value * (1 - ($deal->discount / 100))}
										<span class="label label-important price">&pound;{$calculated|string_format:"%.2f"}</span>
									</p>
								{/if}
								
								{if $deal->discount}
									<p> <strong>Save:</strong> <span class="label label-important price">{$deal->discount|default:0}%<span></p>
								{/if}
										<hr/>
										<address>				
											<i>{$deal->address1},<br/>
											{if $deal->address2 != ''}{$deal->address2}, {/if}
											{if $deal->address3 != ''}{$deal->address3}, {/if}
											{$deal->county} {$deal->postcode}.</i>				
										</address>
										{if $deal->phone}
										<strong>Phone:</strong> {$deal->phone|phone_format:'UK'}<br/>
										{/if}
										{if $deal->url}
										<strong>URL:</strong> <a href="{$deal->url}" target="_blank">{$deal->url|url}</a>
										{/if}
										</p>
									</div>
								</div>
							</div>
						</div>
					{foreachelse}
						<h5>There are no deals listed that match your criteria</h5>
					{/foreach}

					<div class="clearfix"></div>

					<div class="bottomPageCount">{include file='snippets/widgets/paginator.tpl'}</div>

					</div>
				</div>
				
			</div>			
		</div>


{/block}