{extends "snippets/default.tpl"}

{block name="pageName"}{$deal->title|capitalize}{/block}

{block name="css"}
<link href="/{$WEBPATH}css/jquery.fancybox.css" rel="stylesheet">
{/block}

{block name="additionalJavascripts"}

<script src="/{$WEBPATH}js/jquery.fancybox.js"></script>
<script>
	$(document).ready(function() {
		$('.thumbnail').fancybox({
			openEffect  : 'none',
			closeEffect : 'none'
		});
	});
</script>

{/block}

{block name="header"}
			{include file='snippets/navBar.tpl' active='deals'}
			{include file='snippets/topBar.tpl'}
{/block}

{block name="content"}


		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span9">                   
					

					
					<div class="main_content detail-product detail-back">						
						<h3 class="title justTitle">{$deal->title} - expires on {$deal->enddate|date_format}</h3>			
						<div class="row-fluid">
							

							<div class="span8">
								
								<ul class="social pull-right">
									<li>									
										<div class="fb-like" send="false" layout="button_count" ></div>
										<script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script>
									</li>
									<li>
										<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal" >Tweet</a>
										<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
									</li>
								</ul>

		
							
								<h5>Overview</h5>
								{if $deal->summary}
								<p>{$deal->summary}</p>		
								{/if}		
								
								{if $deal->value}
									<p>
										<strong>Price:</strong> {assign var=calculated value=$deal->value * (1 - ($deal->discount / 100))}
										<span class="label label-important price">&pound;{$calculated|string_format:"%.2f"}</span>
									</p>
								{/if}
								
								{if $deal->discount}
									<p> <strong>Save:</strong> <span class="label label-important price">{$deal->discount|default:0}%<span></p>
								{/if}
										
								<div class="contact-information">
									<h5>Contact Information</h5>
									<address>				
											<i>{$deal->address1},<br/> 
											{if $deal->address2 != ''}{$deal->address2}, {/if}
											{if $deal->address3 != ''}{$deal->address3}, {/if}
											{$deal->county} {$deal->postcode}.</i>				
									</address>
									<p>
										<strong>Offered By:</strong> <a href="/{$WEBPATH}business/detail/{$deal->companyname|trim|replace:' ':'+'}">{$deal->companyname}</a><br/>
										<strong>Contact Name:</strong> {$deal->firstname|capitalize} {$deal->lastname|capitalize}<br/>
									{if $deal->phone}
										<strong>Phone:</strong> {$deal->phone|phone_format:'UK'}<br/>
									{/if}
										<strong>E-mail:</strong> <a href="#emailModal" role="button" data-toggle="modal">send an e-mail</a><br/>
									{if $deal->url}
										<strong>URL:</strong> <a href="{$deal->url}" target="_blank">{$deal->url|url}</a>
									{/if}
										</p>
								</div>							
							</div>
							
							<div class="span4">

								{include file='snippets/widgets/imageGrid.tpl' type=deal obj=$deal}

							</div>
														
						</div>
						<div class="row-fluid">
							<div class="span12">

								<h3 class="title">Description</h3>	
								<p>{$deal->description|default:'n/a'}</p>
							
								<h3 class="title">Conditions</h3>	
								<p>{$deal->conditions|default:'n/a'}</p>				
							
															
							</div>							
						</div>						
					</div>
				</div>
				
				<div class="span3">
					
					<h3 class="title justTitle">Location</h3><br/>
					{include file='snippets/widgets/map.tpl' mapper=array($deal)}
					
					<h3 class="title justTitle">Comments</h3>
					
					{include file='snippets/widgets/comments.tpl' comment_ident="deal/detail/{$deal->id|md5}"}
					
					{* include file='snippets/widgets/browseByCategory.tpl' *}
					
					
				</div>
			</div>			
		</div>
		
		{include file='snippets/modals/emailModal.tpl' type=deal ident=$deal->id|md5}
		
{/block}