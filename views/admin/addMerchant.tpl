{extends "admin/snippets/default.tpl"}

{block name="pageName"}Dashboard{/block}

{block name="css"}
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/uniform.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/select2.css" />		
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/markdown/style.css" />	
{/block}

{block name="additionalJavascripts"}
	<script src="/{$WEBPATH}admin_resources/js/jquery.uniform.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/select2.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/bootstrap-colorpicker.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/unicorn.form_common.js"></script>
	<script src="/{$WEBPATH}admin_resources/markdown/jquery.markitup.js"></script>
	<script src="/{$WEBPATH}admin_resources/markdown/set.js"></script>
	
	<script type="text/javascript" >
  $(document).ready(function()	{
	
  	$('#markItUp').markItUp(mySettings, 
  								{ 	root:'admin_resources/markdown/', 
  									previewParserPath:'ajax.ws/markdownPreview',
  									previewAutoRefresh:false 
  								}
  							);	
	
	});
	</script>
{/block}


{block name="sidebar"}
	{include file='admin/snippets/sidebar.tpl' sub='merchant' item='add'}
{/block}

{block name="content"}

	<div id="content-header">
				<h1>Add a New Merchant</h1>
			
			</div>
			<div id="breadcrumb">
				<a href="/{$WEBPATH}admin" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="/{$WEBPATH}admin/merchant/listing" class="tip-bottom">Merchants</a>
				<a href="#" class="current">Add Merchant</a>
			</div>
			<div class="container-fluid">
			
				<div class="row-fluid">
					<div class="span12">
					
					{include file='admin/snippets/errors.tpl'}
				
					{if isset($updated) }
						<div class="alert alert-success">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Success!</strong> The merchant data has been added.
						</div>
					{/if}
				
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>Create a new Merchant</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="#" method="post" class="form-horizontal" enctype='multipart/form-data'>
								
									<div class="control-group">
										<label class="control-label">User Account</label>
										<div class="controls">
											<select name='user_id'>
												{foreach $users as $user}
													<option value='{$user->id}'>{$user->email}</option>
												{/foreach}
											</select>
											<span class="help-block">This is the top-level account owner. User accounts must be created before they can be assigned to merchants.</span>
										</div>
									</div>
								
									<div class="control-group">
										<label class="control-label">Member Level</label>
										<div class="controls">
											<select name='level'>
												<option value='bronze' selected=selected>Bronze</option>
												<option value='silver'>Silver</option>
												<option value='gold'>Gold</option>
											</select>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Association</label>
										<div class="controls">
											<select name='association'>
												<option value='1'>Yes</option>
												<option value='0' selected=selected>No</option>
											</select>
											<span class="help-block">Should this entry be presented as an Association on the main site?</span>
										</div>
									</div>
								
									<div class="control-group">
										<label class="control-label">First Name</label>
										<div class="controls">
											<input type="text" name='firstname' value='' required maxlength=45/>
											<input type='hidden' name='action' value='add'/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Last Name</label>
										<div class="controls">
											<input type="text" name='lastname' value='' required maxlength=45/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Username</label>
										<div class="controls">
											<input type="text" name='username' value='' required maxlength=45/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Company Name</label>
										<div class="controls">
											<input type="text" name='companyname' value='' required maxlength=128/>
										</div>
									</div>
									
									<div class="control-group">

										<label class="control-label">Logo</label>
										<div class="controls">
											<input type="file" name='logo[1]' />
											<span class="help-block">Be aware that images will be scaled and cropped to ensure they fit within the confines of the page.</span>
										</div>
									</div>
									
									
									<div class="control-group">
										<label class="control-label">Address Line 1</label>
										<div class="controls">
											<input type="text" name='address1' value='' required maxlength=128/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Address Line 2</label>
										<div class="controls">
											<input type="text" name='address2' value='' maxlength=128/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Address Line 3</label>
										<div class="controls">
											<input type="text" name='address3' value='' maxlength=128/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">County</label>
										<div class="controls">
											<input type="text" name='county' value='' required maxlength=45/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Postcode</label>
										<div class="controls">
											<input type="text" name='postcode' value='' required maxlength=45/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Country</label>
										<div class="controls">
											<input type="text" name='country' value='UK' required maxlength=45/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Lat</label>
										<div class="controls">
											<input type="text" name='lat' value='' />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Long</label>
										<div class="controls">
											<input type="text" name='long' value='' maxlength=45/>
										</div>
									</div>
									
									
									<div class="control-group">
										<label class="control-label">Phone</label>
										<div class="controls">
											<input type="text" name='phone' value='' required maxlength=12/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Fax</label>
										<div class="controls">
											<input type="text" name='fax' value='' maxlength=12/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Mobile</label>
										<div class="controls">
											<input type="text" name='mobile' value='' maxlength=12/>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">URL</label>
										<div class="controls">
											<input type="text" name='url' value='' maxlength=128/>
											<span class="help-block">Please ensure this is in the format of http://www.website.com</span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Twitter URL</label>
										<div class="controls">
											<input type="text" name='twitter' value='' maxlength=128/>
											<span class="help-block">Please ensure this is in the format of http://www.website.com</span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Facebook URL</label>
										<div class="controls">
											<input type="text" name='facebook' value='' maxlength=128/>
											<span class="help-block">Please ensure this is in the format of http://www.website.com</span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Who we are:</label>
										<div class="controls">
											<textarea name='summary' required></textarea>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label">What we offer:</label>
										<div class="controls">
											<input type="text" name='keywords' value=''/>
											<span class="help-block">This appears under your name in the listings and home page.</span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Open Hours</label>
										<div class="controls">
											<input type='text' name='extrafield1' value=''/>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">More about our business:</label>
										<div class="controls">
											<textarea name='description' id='markItUp' required></textarea>
										</div>
									</div>
									
									
									
									<div class="control-group">
										<label class="control-label">Business Categories</label>
										<div class="controls">
											<select multiple name='categories[]'>
											{foreach $categories as $category}
											
												<option value='{$category->id}'>{$category->name|capitalize}</option>
											
											{/foreach}

											</select>
										</div>
									</div>
									
									<div class="control-group">

										<label class="control-label">Images</label>
										<div class="controls">
											<input type="file" name='logo[2]' />
											<div class='clearfix'></div>
											<input type="file" name='logo[3]' />
											<div class='clearfix'></div>
											<input type="file" name='logo[4]' />
											<div class='clearfix'></div>
											<input type="file" name='logo[5]' />
											<div class='clearfix'></div>
											<input type="file" name='logo[6]' />
											<span class="help-block">Be aware that images will be scaled and cropped to ensure they fit within the confines of the page.</span>
										</div>
									</div>

									<div class="form-actions">
										<button type="submit" class="btn btn-primary">Save</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			
			
				
				<div class="row-fluid">
					{include file='admin/snippets/footer.tpl'}
				</div>
			</div>

{/block}