{extends "admin/snippets/default.tpl"}

{block name="pageName"}Dashboard{/block}

{block name="css"}
    <link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/jquery-ui.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/uniform.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/select2.css" />		
{/block}

{block name="additionalJavascripts"}
	<script src="/{$WEBPATH}admin_resources/js/jquery.uniform.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/select2.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/jquery.dataTables.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/unicorn.tables.js"></script>
{/block}


{block name="sidebar"}
	{include file='admin/snippets/sidebar.tpl' sub='event' item='listing'}
{/block}

{block name="content"}

	<div id="content-header">
				<h1>Manage Events</h1>
			
			</div>
			<div id="breadcrumb">
				<a href="/{$WEBPATH}admin" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="#" class="current">Events</a>

			</div>
			<div class="container-fluid">
			
				{include file='admin/snippets/errors.tpl'}
			
				<div class="row-fluid">
					<div class="span12">

						<a href='/{$WEBPATH}admin/event/add{if $sess_user->isadmin != 1}/{$merchant->id|md5}{/if}' class='btn addButton'>
							<i class="icon-plus"></i><span>Add Event</span>
						</a> 

						<div class="widget-box">
							<div class="widget-title">
								<span class="icon iconCreate"><a href='/{$WEBPATH}admin/event/add{if $sess_user->isadmin != 1}/{$merchant->id|md5}{/if}' class='tip-right' title="Add New Event"><i class="icon-plus-sign"></i></a></span>
								<h5>Event Listing</h5>
							</div>
							<div class="widget-content nopadding">
								<table class="table table-bordered table-striped table-hover data-table">
									<thead>
  									<tr>
  										<th>Title</th>
  										<th>Merchant</th>
  										<th>Event Date</th>
  										<th>Created</th>
  										<th>Updated</th>
  										<th>Published</th>
  										<th>Views</th>
  										<th></th>
  									</tr>
									</thead>
									<tbody>
									{foreach $events as $event}
  									<tr class="gradeA">
    									<td>

    										<a href="/{$WEBPATH}admin/event/edit/{$event->id|md5}" class="viewAdminPage tip-left" title="View &amp; Edit Details">
											<i class="icon-edit friendlyIcon"></i>


    											{$event->title}
    										</a>

    									</td>
    									<td>
    											{$event->merchant->companyname}
    									</td>
    									<td class='center'>{$event->startdate|date_format:"%d %b %Y, %R"}</td>
    									<td class='center'>{$event->created|date_format:'%d %b %Y, %R'}</td>
    									<td class='center'>{$event->updated|date_format:'%d %b %Y, %R'}</td>
    									<td class='center'>{$event->publicationdate|date_format:'%d %b %Y, %R'|default:'-'}</td>
    									<td>{$event->pageviews}</td>
    									<td class="taskOptions">
    										<a href="/{$WEBPATH}admin/event/edit/{$event->id|md5}" class="btn btn-primary btn-mini">Edit</a>
    										
    								{if $sess_user->isadmin == 1}
    									{if $event->publicationdate}
    										<a href="/{$WEBPATH}admin/event/unpublish/{$event->id|md5}" class="btn btn-warning btn-mini">unPublish</a>
    									{else}
    										{if count($event->sharedCategory) == 0}
      										<a href="javascript://" onclick="alert('You must assign at least one category to an item before you publish it.');" class="btn btn-success btn-mini">Publish</a>
      									{else}
    											<a href="/{$WEBPATH}admin/event/publish/{$event->id|md5}" class="btn btn-success btn-mini">Publish</a>
    										{/if}
    									{/if}
    								{/if}
    										<a href="javascript://" onclick="admin.deleteEvent('{$event->id|md5}');" class="btn btn-danger btn-mini">Delete</a>
    									</td>
  									</tr>
  								{foreachelse}
  									
  								{/foreach}
									</tbody>
								</table>
							</div>
						</div>
								
					</div>
				</div>
			
			
				
				<div class="row-fluid">
					{include file='admin/snippets/footer.tpl'}
				</div>
			</div>

{/block}