{extends "admin/snippets/default.tpl"}

{block name="pageName"}Dashboard{/block}

{block name="css"}
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/uniform.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/select2.css" />		
{/block}

{block name="additionalJavascripts"}
	<script src="/{$WEBPATH}admin_resources/js/jquery.uniform.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/select2.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/unicorn.form_common.js"></script>
{/block}


{block name="sidebar"}
	{include file='admin/snippets/sidebar.tpl' sub='user' item='listing'}
{/block}

{block name="content"}

	<div id="content-header">
			{if $sess_user->isadmin == 1}
				<h1>Edit User</h1>
			{else}
				<h1>User Profile</h1>
			{/if}
				
			</div>
			<div id="breadcrumb">
				<a href="/{$WEBPATH}admin" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				{if $sess_user->isadmin == 1}
				<a href="/{$WEBPATH}admin/user/listing" class="tip-bottom">Users</a>
				<a href="#" class="current">Edit User</a>
				{else}
				<a href="#" class="current">User Profile</a>
				{/if}
				
			</div>
			<div class="container-fluid">
			
				
			
				<div class="row-fluid">
					<div class="span12">
					
					{include file='admin/snippets/errors.tpl'}
					
					{if isset($updated) }
						<div class="alert alert-success">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Success!</strong> The user data has been updated.
						</div>
					{/if}
						
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify icon-pencil"></i>									
								</span>
								<h5>Currently Editing : {$user->email}</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="#" method="post" class="form-horizontal">
							
								{if $sess_user->isadmin == 1}
									<div class="control-group">
										<label class="control-label">Site Admin</label>
										<div class="controls">
											<select name='isadmin'>
												<option value='1' {if $user->isadmin == true}selected=selected{/if}>True</option>
												<option value='0' {if $user->isadmin == false}selected=selected{/if}>False</option>
											</select>
										</div>
									</div>
								{/if}
									<div class="control-group">
										<label class="control-label">Email Address</label>
										<div class="controls">
											<input type="email" name='email' value='{$user->email}' autocomplete=off />
											<input type='hidden' name='action' value='edit'/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Password</label>
										<div class="controls">
											<input type="password" name='password' value='' placeholder='enter a new password to change' autocomplete='off'/>
										</div>
									</div>
									
									
									<div class="form-actions">
										<button type="submit" class="btn btn-primary">Save</button>
									</div>
								</form>
							</div>
						</div>
								
					</div>
				</div>
			
			
				
				<div class="row-fluid">
					{include file='admin/snippets/footer.tpl'}
				</div>
			</div>

{/block}