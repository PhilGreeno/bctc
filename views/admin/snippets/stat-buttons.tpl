							
						<ul class="stat-boxes">
							<li>
								<a class="statButton" href="/{$WEBPATH}admin/merchant/listing">
									<div class="right">
										<strong>{$merchantCount}</strong>
										Merchants
									</div>
								</a>
							</li>

							<li>
								<a class="statButton"href="/{$WEBPATH}admin/article/listing{if $sess_user->isadmin != 1}/{$merchant->id|md5}{/if}">
									<div class="right">
										<strong>{$articleCount}</strong>
										Articles
									</div>
								</a>
							</li>
							<li>
								<a class="statButton" href="/{$WEBPATH}admin/event/listing{if $sess_user->isadmin != 1}/{$merchant->id|md5}{/if}">
									<div class="right">
										<strong>{$eventCount}</strong>
										Events
									</div>
								</a>
							</li>
							<li>
								<a class="statButton" href="/{$WEBPATH}admin/classified/listing{if $sess_user->isadmin != 1}/{$merchant->id|md5}{/if}">
									<div class="right">
										<strong>{$classifiedCount}</strong>
										Classifieds
									</div>
								</a>
							</li>
							
							<li>
								<a class="statButton" href="/{$WEBPATH}admin/deal/listing{if $sess_user->isadmin != 1}/{$merchant->id|md5}{/if}">
									<div class="right">
										<strong>{$dealCount}</strong>
										Offers
									</div>
								</a>
							</li>
						</ul>
					