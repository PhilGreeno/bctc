<div id="sidebar">
	<a href="/{$WEBPATH}admin" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
	<ul>
		<li class="{if !$sub}active{/if}">
			<a href="/{$WEBPATH}admin"><i class="icon icon-home"></i> <span>Dashboard</span></a>
		</li>
		
{if $sess_user->isadmin == 1}

		<!-- <li class="{if $sub == 'blog'}active{/if}">
			<a href="/{$WEBPATH}admin/blog"><i class="icon icon-th-list"></i> <span>Blog</span></a>
		</li> -->

		<li>
			<a href="https://www.google.com/analytics/web/?hl=en-GB&pli=1#report/visitors-overview/a40328774w69503203p71605446/" target="_blank"><i class="icon icon-signal"></i> <span>Analytics</span></a>
		</li>

		<li class="{if $sub == 'category'}active{/if}">
			<a href="/{$WEBPATH}admin/category/listing"><i class="icon icon-folder-open"></i> <span>Categories</span></a>
		</li>

		<li class="{if $sub == 'user'}active{/if}">
			<a href="/{$WEBPATH}admin/user/listing"><i class="icon icon-user"></i> <span>Users</span></a>
		</li>
				
		<li class="{if $sub == 'merchant'}active{/if}">
			<a href="/{$WEBPATH}admin/merchant/listing"><i class="icon icon-th-list"></i> <span>Merchants</span></a>
		</li>
{/if}			
{if $sess_user->isadmin == 0}
		<li class="{if $sub == 'merchant'}active{/if}">
			<a href="/{$WEBPATH}admin/merchant/edit/{$merchant->id|md5}"><i class="icon icon-th-list"></i> <span>Listing</span></a>
		</li>
{/if}
		<li class="{if $sub == 'article'}active{/if}">
			<a href="/{$WEBPATH}admin/article/listing{if $sess_user->isadmin != 1}/{$merchant->id|md5}{/if}"><i class="icon icon-th-list"></i> <span>Articles</span></a>
		</li>
		
		<li class="{if $sub == 'classified'}active{/if}">
			<a href="/{$WEBPATH}admin/classified/listing{if $sess_user->isadmin != 1}/{$merchant->id|md5}{/if}"><i class="icon icon-th-list"></i> <span>Classifieds</span></a>
		</li>
		
		<li class="{if $sub == 'event'}active{/if}">
			<a href="/{$WEBPATH}admin/event/listing{if $sess_user->isadmin != 1}/{$merchant->id|md5}{/if}"><i class="icon icon-th-list"></i> <span>Events</span></a>
		</li>
		
		<li class="{if $sub == 'deal'}active{/if}">
			<a href="/{$WEBPATH}admin/deal/listing{if $sess_user->isadmin != 1}/{$merchant->id|md5}{/if}"><i class="icon icon-th-list"></i> <span>Offers</span></a>
		</li>
		
{if $sess_user->isadmin == 1}
		<li class="{if $sub == 'cms'}active{/if}">
			<a href="/{$WEBPATH}admin/cms/listing"><i class="icon icon-th-list"></i> <span>CMS Pages</span></a>
		</li>
{/if}
	
	
	</ul>

</div>