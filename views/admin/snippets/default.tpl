<!DOCTYPE html>
<html lang="en">
	<head>
		<title>BCTC Admin :: {block name="pageName"}Default Page{/block}</title>
		
		{block name="meta"}
		<meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		{/block}
		
		
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/fullcalendar.css" />	
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/unicorn.main.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/unicorn.blue.css" class="skin-color" />
		<link rel="stylesheet/less" href="/{$WEBPATH}admin_resources/css/custom.less" />

		
		{block name="css"}{/block}
		
	<script src="/{$WEBPATH}js/less-1.3.3.min.js" type="text/javascript"></script>
	<script src="/{$WEBPATH}admin_resources/js/excanvas.min.js"></script>
    <script src="/{$WEBPATH}admin_resources/js/jquery.min.js"></script>
    <script src="/{$WEBPATH}admin_resources/js/jquery-ui.custom.js"></script>
    <script src="/{$WEBPATH}admin_resources/js/bootstrap.min.js"></script>
    <script src="/{$WEBPATH}admin_resources/js/unicorn.js"></script>
   	<script src="/{$WEBPATH}admin_resources/js/admin.js"></script>
   	<script src="/{$WEBPATH}admin_resources/js/custom.js"></script>



		
		<script>
			var WEBPATH = '{$WEBPATH}';
		</script>

		{block name="additionalJavascripts"}{/block}
		
		
	</head>
	
	<body lang="en" id="body">
	
		{if $sess_user->isadmin != 1}
			{assign var=merchant value=reset($sess_user->ownMerchant)}
		{/if}

		{block name="header"}
			{include file='admin/snippets/navbar.tpl'}
		{/block}
		
		{block name="sidebar"}
			{include file='admin/snippets/sidebar.tpl' sub=null item=null}
		{/block}

		<div id="content">
		{block name="content"}{/block}
		</div>
		
		
	</body>

</html>