{if isset($errors)}
<div class="container">

	{foreach $errors as $error}
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<h4>{$error.title}</h4>
			{$error.message}
		</div>
	{/foreach}

</div>
{/if}