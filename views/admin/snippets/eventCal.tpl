<script>
$().ready(function(){

	$('.calendar').fullCalendar({
		header: {
			left: 'prev,next',
			center: 'title',
			right: 'month,basicWeek,basicDay'
		},
		editable: false,
		events: [
		{foreach $allevents as $event}
			{
				title: '{$event->title}',
				start: new Date('{$event->startdate}'),
				end: new Date('{$event->enddate}'),
				url: '/{$WEBPATH}admin/event/edit/{$event->id|md5}',

			},
		{foreachelse}
		{/foreach}
		]
	});
});
</script>