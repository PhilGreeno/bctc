{extends "admin/snippets/default.tpl"}

{block name="pageName"}Dashboard{/block}

{block name="css"}
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/uniform.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/select2.css" />		
{/block}

{block name="additionalJavascripts"}
	<script src="/{$WEBPATH}admin_resources/js/jquery.uniform.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/select2.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/unicorn.form_common.js"></script>
{/block}


{block name="sidebar"}
	{include file='admin/snippets/sidebar.tpl' sub='category' item='add'}
{/block}

{block name="content"}

	<div id="content-header">
				<h1>Add Category</h1>
				<div class="btn-group">
					<a class="btn btn-large tip-bottom" title="Manage Files"><i class="icon-file"></i></a>
					<a class="btn btn-large tip-bottom" title="Manage Users"><i class="icon-user"></i></a>
					<a class="btn btn-large tip-bottom" title="Manage Comments"><i class="icon-comment"></i><span class="label label-important">5</span></a>
					<a class="btn btn-large tip-bottom" title="Manage Orders"><i class="icon-shopping-cart"></i></a>
				</div>
			</div>
			<div id="breadcrumb">
				<a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="/{$WEBPATH}admin/category/listing" class="tip-bottom">Categories</a>
				<a href="#" class="current">Add Category</a>
			</div>
			<div class="container-fluid">
			
				
			
				<div class="row-fluid">
					<div class="span12">
					
					{include file='admin/snippets/errors.tpl'}
					
					{if isset($updated) }
						<div class="alert alert-success">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Success!</strong> The user data has been updated.
						</div>
					{/if}
						
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>Create a new Category</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="#" method="post" class="form-horizontal">

									<div class="control-group">
										<label class="control-label">Name</label>
										<div class="controls">
											<input type="text" name='name' value=''  required/>
											<input type='hidden' name='action' value='add'/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Description</label>
										<div class="controls">
											<textarea name='description' required></textarea>
											<span class="help-block">Be as descriptive as you can.</span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Business Category</label>
										<div class="controls">
											<select name='business'>
												<option value='0' selected=selected>False</option>
												<option value='1'>True</option>
											</select>
											<span class="help-block">Select YES if this category applies to a Business, i.e. Florest, or Bank. Non-business categories are for use with Articles, Deals, Events etc..</span>
										</div>
									</div>
									
									
									<div class="form-actions">
										<button type="submit" class="btn btn-primary">Save</button>
									</div>
								</form>
							</div>
						</div>
								
					</div>
				</div>
			
			
				
				<div class="row-fluid">
					{include file='admin/snippets/footer.tpl'}
				</div>
			</div>

{/block}