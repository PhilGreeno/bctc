{extends "admin/snippets/default.tpl"}

{block name="pageName"}Dashboard{/block}

{block name="css"}
    <link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/jquery-ui.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/uniform.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/select2.css" />		
{/block}

{block name="additionalJavascripts"}
	<script src="/{$WEBPATH}admin_resources/js/jquery.uniform.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/select2.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/jquery.dataTables.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/unicorn.tables.js"></script>
{/block}


{block name="sidebar"}
	{include file='admin/snippets/sidebar.tpl' sub='classified' item='listing'}
{/block}

{block name="content"}

	<div id="content-header">
				<h1>Manage Classifieds</h1>
				
			</div>
			<div id="breadcrumb">
				<a href="/{$WEBPATH}admin" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="#" class="current">Classifieds</a>

			</div>
			<div class="container-fluid">
			
				{include file='admin/snippets/errors.tpl'}
			
				<div class="row-fluid">
					<div class="span12">

						<a href='/{$WEBPATH}admin/classified/add{if $sess_user->isadmin != 1}/{$merchant->id|md5}{/if}' class='btn addButton'>
							<i class="icon-plus"></i><span>Add Classified</span>
						</a> 

						<div class="widget-box">
							<div class="widget-title">
								<span class="icon iconCreate"><a href='/{$WEBPATH}admin/article/add{if $sess_user->isadmin != 1}/{$merchant->id|md5}{/if}' class='tip-right' title="Add New Article"><i class="icon-plus-sign"></i></a></span>
								<h5>Classified Listing</h5>
							</div>
							<div class="widget-content nopadding">
								<table class="table table-bordered table-striped table-hover data-table">
									<thead>
  									<tr>
  										<th>Title</th>
  										<th>Merchant</th>
  										<th>Created</th>
  										<th>Updated</th>
  										<th>Published</th>
  										<th>Views</th>
  										<th></th>
  									</tr>
									</thead>
									<tbody>
									{foreach $classifieds as $classified}
  									<tr class="gradeA">
    									<td>
											<a href="/{$WEBPATH}admin/classified/edit/{$classified->id|md5}" class="viewAdminPage tip-left" title="View &amp; Edit Details">
											<i class="icon-edit friendlyIcon"></i>
    										{$classified->title}
    										</a>

    									</td>
    									<td>
    											{$classified->merchant->companyname}
    									</td>
    									<td class='center'>{$classified->created|date_format:'%d %b %Y, %R'}</td>
    									<td class='center'>{$classified->updated|date_format:'%d %b %Y, %R'}</td>
    									<td class='center'>{$classified->publicationdate|date_format:'%d %b %Y, %R'|default:'-'}</td>
    									<td>{$classified->pageviews}</td>
    									<td class="taskOptions">
    										<a href="/{$WEBPATH}admin/classified/edit/{$classified->id|md5}" class="btn btn-primary btn-mini">Edit</a>
    										
    								{if $sess_user->isadmin == 1}		
    									{if $classified->publicationdate}
    										<a href="/{$WEBPATH}admin/classified/unpublish/{$classified->id|md5}" class="btn btn-warning btn-mini">unPublish</a>
    									{else}
    										{if count($classified->sharedCategory) == 0}
      										<a href="javascript://" onclick="alert('You must assign at least one category to an item before you publish it.');" class="btn btn-success btn-mini">Publish</a>
      									{else}
    											<a href="/{$WEBPATH}admin/classified/publish/{$classified->id|md5}" class="btn btn-success btn-mini">Publish</a>
    										{/if}
    									{/if}
    								{/if}

    										<a href="javascript://" onclick="admin.deleteClassified('{$classified->id|md5}');" class="btn btn-danger btn-mini">Delete</a>
    									</td>
  									</tr>
  								{foreachelse}
  									
  								{/foreach}
									</tbody>
								</table>
							</div>
						</div>
								
					</div>
				</div>
			
			
				
				<div class="row-fluid">
					{include file='admin/snippets/footer.tpl'}
				</div>
			</div>

{/block}