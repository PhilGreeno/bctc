{extends "admin/snippets/default.tpl"}

{block name="pageName"}Dashboard{/block}

{block name="css"}
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/uniform.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/select2.css" />		
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/markdown/style.css" />		
{/block}

{block name="additionalJavascripts"}
	<script src="/{$WEBPATH}admin_resources/js/jquery.uniform.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/select2.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/bootstrap-colorpicker.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/unicorn.form_common.js"></script>
	<script src="/{$WEBPATH}admin_resources/markdown/jquery.markitup.js"></script>
	<script src="/{$WEBPATH}admin_resources/markdown/set.js"></script>
	
	<script type="text/javascript" >
  $(document).ready(function()	{
	{*
  	$('#markItUp').markItUp(mySettings, 
  								{ 	root:'admin_resources/markdown/', 
  									previewParserPath:'/{$WEBPATH}ajax.ws/markdownPreview',
  									previewAutoRefresh:false 
  								}
  							);	
	*}
	});
	</script>
	
{/block}


{block name="sidebar"}
	{include file='admin/snippets/sidebar.tpl' sub='classified' item='add'}
{/block}

{block name="content"}

			<div id="content-header">
				<h1>Add a New Classified</h1>
				
			</div>
			<div id="breadcrumb">
				<a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="/{$WEBPATH}admin/classified/listing{if $sess_user->isadmin != 1}/{$merchant->id|md5}{/if}" class="tip-bottom">Classifieds</a>
				<a href="#" class="current">Add Classified</a>
			</div>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
			
  				{include file='admin/snippets/errors.tpl'}
  				
  					{if isset($updated) }
  						<div class="alert alert-success">
  							<button class="close" data-dismiss="alert">×</button>
  							<strong>Success!</strong> The Classified data has been added.
  						</div>
  					{/if}
			
				
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>Create a new Classified</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="#" method="post" class="form-horizontal" enctype='multipart/form-data'>
								
								{if $sess_user->isadmin == 1}
									<div class="control-group">
										<label class="control-label">Merchant Account</label>
										<div class="controls">
											<select name='merchant_id' class='span5'>
												{foreach $merchants as $merchant}
													<option value='{$merchant->id}'>{$merchant->companyname}</option>
												{/foreach}
											</select>
											<div class='clearfix'></div>
											<span class="help-block">This is the Classified owner.</span>
										</div>
									</div>
								{/if}

									<div class="control-group">
										<label class="control-label">Title</label>
										<div class="controls">
											<input type="text" name='title' value='' required/>
											<input type='hidden' name='action' value='add'/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Contact Name</label>
										<div class="controls">
											<input type="text" name='contact' value='' required maxlength=255/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Phone</label>
										<div class="controls">
											<input type="text" name='phone' value='' required maxlength=12/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Email</label>
										<div class="controls">
											<input type="email" name='email' value='' required maxlength=128/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Url</label>
										<div class="controls">
											<input type="text" name='url' value='' maxlength=128/>
											<span class="help-block">Ensure in the correct format http://www.web.com</span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Price (&pound;)</label>
										<div class="controls">
											<input type="text" name='price' value='' placeholder='10.00' required/>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Address 1</label>
										<div class="controls">
											<input type="text" name='address1' value='' required maxlength=128/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Address 2</label>
										<div class="controls">
											<input type="text" name='address2' value='' maxlength=128/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Address 3</label>
										<div class="controls">
											<input type="text" name='address3' value='' maxlength=128/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Postcode</label>
										<div class="controls">
											<input type="text" name='postcode' value='' required maxlength=45/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">County</label>
										<div class="controls">
											<input type="text" name='county' value='' required maxlength=45/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Country</label>
										<div class="controls">
											<input type="text" name='country' value='UK' required maxlength=45/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Lat</label>
										<div class="controls">
											<input type="text" name='lat' value='' required/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Long</label>
										<div class="controls">
											<input type="text" name='long' value='' required/>
										</div>
									</div>
									
									
									<div class="control-group">
										<label class="control-label">Summary</label>
										<div class="controls">
											<textarea name='summary' required></textarea>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Description</label>
										<div class="controls">
											<textarea id='markItUp' name='description' required rows=8></textarea>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Keywords</label>
										<div class="controls">
											<input type="text" name='keywords' value='' maxlength=64/>
											<span class="help-block">Keywords are used in search, seperate keyword phrases using a comma.</span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Classified Categories</label>
										<div class="controls">
											<input type="text" name='suggested' value='' required maxlength=128/>
											<span class="help-block">Please enter your suggested categories, seperate category phrases using a comma.</span>
										</div>
									</div>
									
									<div class="control-group">

										<label class="control-label">Images</label>
										<div class="controls">
											<input type="file" name='logo[1]' />  <span>Primary Image</span>
											<div class='clearfix'></div>
											<input type="file" name='logo[2]' />
											<div class='clearfix'></div>
											<input type="file" name='logo[3]' />
											<div class='clearfix'></div>
											<input type="file" name='logo[4]' />
											<span class="help-block">Be aware that images will be scaled and cropped to ensure they fit within the confines of the page.</span>
										</div>
									</div>

									<div class="form-actions">
										<button type="submit" class="btn btn-primary">Save</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			
			
				
				<div class="row-fluid">
					{include file='admin/snippets/footer.tpl'}
				</div>
			</div>

{/block}