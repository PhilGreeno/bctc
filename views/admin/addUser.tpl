{extends "admin/snippets/default.tpl"}

{block name="pageName"}Dashboard{/block}

{block name="css"}
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/uniform.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/select2.css" />		
{/block}

{block name="additionalJavascripts"}
	<script src="/{$WEBPATH}admin_resources/js/jquery.uniform.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/select2.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/unicorn.form_common.js"></script>
{/block}


{block name="sidebar"}
	{include file='admin/snippets/sidebar.tpl' sub='user' item='listing'}
{/block}

{block name="content"}

	<div id="content-header">
				<h1>Add User</h1>
			
			</div>
			<div id="breadcrumb">
				<a href="/{$WEBPATH}admin" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="/{$WEBPATH}admin/user/listing" class="tip-bottom">Users</a>
				<a href="#" class="current">Add User</a>
			</div>
			<div class="container-fluid">
			
				
			
				<div class="row-fluid">
					<div class="span12">
					
					{include file='admin/snippets/errors.tpl'}
					
					{if isset($updated) }
						<div class="alert alert-success">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Success!</strong> The user data has been added.
						</div>
					{/if}
						
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>Create a new user</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="#" method="post" class="form-horizontal">
								
									<div class="control-group">
										<label class="control-label">Site Admin</label>
										<div class="controls">
											<select name='isadmin'>
												<option value='0' selected=selected>False</option>
												<option value='1'>True</option>
											</select>
										</div>
									</div>
								
									<div class="control-group">
										<label class="control-label">Email Address</label>
										<div class="controls">
											<input type="email" name='email' value='' autocomplete=off />
											<input type='hidden' name='action' value='add'/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Password</label>
										<div class="controls">
											<input type="password" name='password' value='' autocomplete='off'/>
										</div>
									</div>
									
									
									<div class="form-actions">
										<button type="submit" class="btn btn-primary">Create</button>
									</div>
								</form>
							</div>
						</div>
								
					</div>
				</div>
			
			
				
				<div class="row-fluid">
					{include file='admin/snippets/footer.tpl'}
				</div>
			</div>

{/block}