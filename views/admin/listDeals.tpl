{extends "admin/snippets/default.tpl"}

{block name="pageName"}Dashboard{/block}

{block name="css"}
    <link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/jquery-ui.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/uniform.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/select2.css" />		
{/block}

{block name="additionalJavascripts"}
	<script src="/{$WEBPATH}admin_resources/js/jquery.uniform.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/select2.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/jquery.dataTables.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/unicorn.tables.js"></script>
{/block}


{block name="sidebar"}
	{include file='admin/snippets/sidebar.tpl' sub='deal' item='listing'}
{/block}

{block name="content"}

	<div id="content-header">
				<h1>Manage Offers</h1>
			
			</div>
			<div id="breadcrumb">
				<a href="/{$WEBPATH}admin" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="#" class="current">Offers</a>

			</div>
			<div class="container-fluid">
			
				{include file='admin/snippets/errors.tpl'}
			
				<div class="row-fluid">
					<div class="span12">

						<a href='/{$WEBPATH}admin/deal/add{if $sess_user->isadmin != 1}/{$merchant->id|md5}{/if}' class='btn addButton'>
							<i class="icon-plus"></i><span>Add Offers</span>
						</a> 

						<div class="widget-box">
							<div class="widget-title">
								<span class="icon iconCreate"><a href='/{$WEBPATH}admin/deal/add{if $sess_user->isadmin != 1}/{$merchant->id|md5}{/if}' class='tip-right' title="Add New Deal"><i class="icon-plus-sign"></i></a></span>
								<h5>Offer Listings</h5>
							</div>
							<div class="widget-content nopadding">
								<table class="table table-bordered table-striped table-hover data-table">
									<thead>
  									<tr>
  										<th>Title</th>
  										<th>Merchant</th>
  										<th>Offer Date</th>
  										<th>Created</th>
  										<th>Updated</th>
  										<th>Published</th>
  										<th>Views</th>
  										<th></th>
  									</tr>
									</thead>
									<tbody>
									{foreach $deals as $deal}
  									<tr class="gradeA">
    									<td>

    										<a href="/{$WEBPATH}admin/deal/edit/{$deal->id|md5}" class="viewAdminPage tip-left" title="View &amp; Edit Details">
											<i class="icon-edit friendlyIcon"></i>

    											{$deal->title}
    										</a>


    									</td>
    									<td>
    											{$deal->merchant->companyname}
    									</td>
    									<td class='center'>{$deal->startdate|date_format:"%d %b %Y, %R"}</td>
    									<td class='center'>{$deal->created|date_format:'%d %b %Y, %R'}</td>
    									<td class='center'>{$deal->updated|date_format:'%d %b %Y, %R'}</td>
    									<td class='center'>{$deal->publicationdate|date_format:'%d %b %Y, %R'|default:'-'}</td>
    									<td>{$deal->pageviews}</td>
    									<td class="taskOptions">
    										<a href="/{$WEBPATH}admin/deal/edit/{$deal->id|md5}" class="btn btn-primary btn-mini">Edit</a>
    							
    								{if $sess_user->isadmin == 1}
    									{if $deal->publicationdate}
    										<a href="/{$WEBPATH}admin/deal/unpublish/{$deal->id|md5}" class="btn btn-warning btn-mini">unPublish</a>
    									{else}
    										{if count($deal->sharedCategory) == 0}
      											<a href="javascript://" onclick="alert('You must assign at least one category to an item before you publish it.');" class="btn btn-success btn-mini">Publish</a>
      									{else}
    											<a href="/{$WEBPATH}admin/deal/publish/{$deal->id|md5}" class="btn btn-success btn-mini">Publish</a>
    										{/if}
    									{/if}
    								{/if}
    									
    										<a href="javascript://" onclick="admin.deleteDeal('{$deal->id|md5}');" class="btn btn-danger btn-mini">Delete</a>
    									</td>
  									</tr>
  								{foreachelse}
  									
  								{/foreach}
									</tbody>
								</table>
							</div>
						</div>
								
					</div>
				</div>
			
			
				
				<div class="row-fluid">
					{include file='admin/snippets/footer.tpl'}
				</div>
			</div>

{/block}