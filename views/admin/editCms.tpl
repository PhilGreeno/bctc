{extends "admin/snippets/default.tpl"}

{block name="pageName"}Dashboard{/block}

{block name="css"}
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/uniform.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/select2.css" />		
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/markdown/style.css" />		
{/block}

{block name="additionalJavascripts"}
	<script src="/{$WEBPATH}admin_resources/js/jquery.uniform.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/select2.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/bootstrap-colorpicker.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/unicorn.form_common.js"></script>
	<script src="/{$WEBPATH}admin_resources/markdown/jquery.markitup.js"></script>
	<script src="/{$WEBPATH}admin_resources/markdown/set.js"></script>
	
	<script type="text/javascript" >
  $(document).ready(function()	{

  	$('#markItUp').markItUp(mySettings, 
  								{ 	root:'admin_resources/markdown/', 
  									previewParserPath:'ajax.ws/markdownPreview',
  									previewAutoRefresh:false 
  								}
  							);	
	
	});
	</script>
	
{/block}


{block name="sidebar"}
	{include file='admin/snippets/sidebar.tpl' sub='cms' item='add'}
{/block}

{block name="content"}

	<div id="content-header">
				<h1>Edit a Static Page</h1>
				
			</div>
			<div id="breadcrumb">
				<a href="/{$WEBPATH}admin" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="/{$WEBPATH}admin/cms/listing" class="tip-bottom">CMS</a>
				<a href="#" class="current">Edit Page</a>
			</div>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
					
  					{include file='admin/snippets/errors.tpl'}
  				
  					{if isset($updated) }
  						<div class="alert alert-success">
  							<button class="close" data-dismiss="alert">×</button>
  							<strong>Success!</strong> The Page data has been updated.
  						</div>
  					{/if}
				
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify icon-pencil"></i>									
								</span>
								<h5>Currently editing - {$page->title}</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="#" method="post" class="form-horizontal" enctype='multipart/form-data'>
								
							

									<div class="control-group">
										<label class="control-label">Title</label>
										<div class="controls">
											<input type="text" name='title' value='{$page->title}' required/>
											<input type='hidden' name='action' value='edit'/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Link Text</label>
										<div class="controls">
											<input type="text" name='link' value='{$page->link}' required maxlength=128/>
										</div>
									</div>
								
									
									<div class="control-group">
										<label class="control-label">Content</label>
										<div class="controls">
											<textarea id='markItUp' name='content' required rows=8>{$page->content|stripslashes}</textarea>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Link Position</label>
										<div class="controls">
											<input type="text" name='position' value='{$page->position}' required maxlength=4/>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label">Active</label>
										<div class="controls">
											<select name='active'>
												<option value='1' {if $page->active == 1}SELECTED=SELECTED{/if}>Yes</option>
												<option value='0' {if $page->active == 0}SELECTED=SELECTED{/if}>No</option>
											</select>
											<span class="help-block">Is the page active on the site?</span>
										</div>
									</div>

									<div class="form-actions">
										<button type="submit" class="btn btn-primary">Save</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			
			
				
				<div class="row-fluid">
					{include file='admin/snippets/footer.tpl'}
				</div>
			</div>

{/block}