{extends "admin/snippets/default.tpl"}

{block name="pageName"}Dashboard{/block}

{block name="css"}
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/uniform.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/select2.css" />		
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/markdown/style.css" />		
{/block}

{block name="additionalJavascripts"}
	<script src="/{$WEBPATH}admin_resources/js/jquery.uniform.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/select2.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/bootstrap-colorpicker.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/unicorn.form_common.js"></script>
	<script src="/{$WEBPATH}admin_resources/markdown/jquery.markitup.js"></script>
	<script src="/{$WEBPATH}admin_resources/markdown/set.js"></script>
	
	<script type="text/javascript" >
  $(document).ready(function()	{
	
  	$('#markItUp').markItUp(mySettings, 
  								{ 	root:'admin_resources/markdown/', 
  									previewParserPath:'ajax.ws/markdownPreview',
  									previewAutoRefresh:false 
  								}
  							);	
	
	});
	</script>
	
{/block}


{block name="sidebar"}
	{include file='admin/snippets/sidebar.tpl' sub='article' item='edit'}
{/block}

{block name="content"}

	<div id="content-header">
				<h1>Edit Article</h1>
				
			</div>
			<div id="breadcrumb">
				<a href="/{$WEBPATH}admin" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="/{$WEBPATH}admin/article/listing{if $sess_user->isadmin != 1}/{$merchant->id|md5}{/if}" class="tip-bottom">Articles</a>
				<a href="#" class="current">Edit Article</a>
			</div>
			<div class="container-fluid">
			
				<div class="row-fluid">
					<div class="span12">
			
  				{include file='admin/snippets/errors.tpl'}
  				
  				{if isset($updated) }
						<div class="alert alert-success">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Success!</strong> The Article data has been updated.
						</div>
					{/if}
			
				
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify icon-pencil"></i>									
								</span>
								<h5>Currently editing - {$article->title}</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="#" method="post" class="form-horizontal" enctype='multipart/form-data'>
								
								{if $sess_user->isadmin == 1}
									<div class="control-group">
										<label class="control-label">Merchant Account</label>
										<div class="controls">
											<select name='merchant_id' class='span5'>
												{foreach $merchants as $merchant}
													<option value='{$merchant->id}' {if $merchant->id == $article->merchant->id}SELECTED=SELECTED{/if}>{$merchant->companyname}</option>
												{/foreach}
											</select>
											<div class='clearfix'></div>
											<span class="help-block">This is the Article owner.</span>
										</div>
									</div>
								{/if}
								
									<div class="control-group">
										<label class="control-label">Title</label>
										<div class="controls">
											<input type="text" name='title' value='{$article->title}' required/>
											<input type='hidden' name='action' value='edit'/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Author</label>
										<div class="controls">
											<input type="text" name='author' value='{$article->author}' required maxlength=128/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">url</label>
										<div class="controls">
											<input type="text" name='url' value='{$article->url}' maxlength=128/>
											<span class="help-block">Ensure in the correct format http://www.web.com</span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Summary</label>
										<div class="controls">
											<textarea name='abstract' required>{$article->abstract|stripslashes}</textarea>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Description</label>
										<div class="controls">
											<textarea id='markItUp' name='content' required rows=8>{$article->content|stripslashes}</textarea>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Keywords</label>
										<div class="controls">
											<input type="text" name='keywords' value='{$article->keywords}' maxlength=64/>
											<span class="help-block">Keywords are used in search, seperate keyword phrases using a comma.</span>
										</div>
										
									</div>
									
									<div class="control-group">
										<label class="control-label">Suggested Categories</label>
										<div class="controls">
											<input type="text" name='suggested' value='{$article->suggested}' required maxlength=255/>
											<span class="help-block">Please enter your suggested categories, seperate category phrases using a comma.</span>
										</div>
									</div>
									
								{if $sess_user->isadmin == 1 || $merchant->level != 'bronze'}
									<div class="control-group">
										<label class="control-label">Moderated Categories</label>
										<div class="controls">
											<select multiple name='categories[]'>
											{foreach $categories as $category}
											
												<option value='{$category->id}' {if isset($category->selected)}selected=selected{/if}>{$category->name|capitalize}</option>
											
											{/foreach}

											</select>
											<span class="help-block">The real categories that the article will be published under - this is controlled by site admin or elevated merchants.</span>
										</div>
									</div>
								{/if}
									
									<div class="control-group">

										<label class="control-label">Images</label>
										<div class="controls">
										
											{if $article->image1}
												<img src='/{$WEBPATH}image/article_{$article->id|md5}_1/50/50/'/>
												<input type='checkbox' name='delete[1]'> Delete
											{/if}
											<input type="file" name='logo[1]' /> <span>Primary Image</span>
											<div class='clearfix'></div>
											{if $article->image2}<img src='/{$WEBPATH}image/article_{$article->id|md5}_2/50/50/'/> <input type='checkbox' name='delete[2]'> Delete{/if}
											<input type="file" name='logo[2]' />
											<div class='clearfix'></div>
											{if $article->image3}<img src='/{$WEBPATH}image/article_{$article->id|md5}_3/50/50/'/> <input type='checkbox' name='delete[3]'> Delete{/if}
											<input type="file" name='logo[3]' />
											<div class='clearfix'></div>
											{if $article->image4}<img src='/{$WEBPATH}image/article_{$article->id|md5}_4/50/50/'/> <input type='checkbox' name='delete[4]'> Delete{/if}
											<input type="file" name='logo[4]' />
											<span class="help-block">Be aware that images will be scaled and cropped to ensure they fit within the confines of the page.</span>
										</div>
									</div>

									<div class="form-actions">
										<button type="submit" class="btn btn-primary">Save</button>
										
										{if $sess_user->isadmin == 1}
  										{if $article->publicationdate}
  										<button type="submit" name='unpublish' value='true' class="btn btn-warning">Save & unPublish</button>
  										{else}
  											{if count($article->sharedCategory) == 0}
      									{*	<a href="javascript://" onclick="alert('You must assign at least one category to an item before you publish it.');" class="btn btn-primary">Save & Publish</a> *}
      									{else}
  												<button type="submit" name='publish' value='true' class="btn btn-primary">Save & Publish</button>
  											{/if}
  										{/if} 
  									{else}
  										Saving changes will remove this article from the site pending moderation.
  									{/if}
										
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="row-fluid">
					{include file='admin/snippets/footer.tpl'}
				</div>
			</div>

{/block}