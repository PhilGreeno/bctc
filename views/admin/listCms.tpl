{extends "admin/snippets/default.tpl"}

{block name="pageName"}Dashboard{/block}

{block name="css"}
    <link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/jquery-ui.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/uniform.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/select2.css" />		
{/block}

{block name="additionalJavascripts"}
	<script src="/{$WEBPATH}admin_resources/js/jquery.uniform.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/select2.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/jquery.dataTables.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/unicorn.tables.js"></script>
{/block}


{block name="sidebar"}
	{include file='admin/snippets/sidebar.tpl' sub='cms' item='listing'}
{/block}

{block name="content"}

	<div id="content-header">
				<h1>Manage Static Pages</h1>

			</div>
			<div id="breadcrumb">
				<a href="/{$WEBPATH}admin" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="#" class="current">CMS</a>

			</div>
			<div class="container-fluid">
			
				{include file='admin/snippets/errors.tpl'}
			
				<div class="row-fluid">
					<div class="span12">

						<a href='/{$WEBPATH}admin/cms/add' class='btn addButton'>
							<i class="icon-plus"></i><span>Add Page</span>
						</a> 

						<div class="widget-box">
							<div class="widget-title">
								<span class="icon iconCreate"><a href='/{$WEBPATH}admin/cms/add' class='tip-right' title="Add New Page"><i class="icon-plus-sign"></i></a></span>
								<h5>Page Listing</h5>
							</div>
							<div class="widget-content nopadding">
								<table class="table table-bordered table-striped table-hover data-table">
									<thead>
  									<tr>
  										<th>Title</th>
  										<th>Active?</th>
  										<th></th>
  									</tr>
									</thead>
									<tbody>
									{foreach $pages as $page}
  									<tr class="gradeA">
    									<td>

    										<a href="/{$WEBPATH}admin/cms/edit/{$page->id|md5}" class="viewAdminPage tip-left" title="View &amp; Edit Details">
											<i class="icon-edit friendlyIcon"></i>

    											{$page->title}

    										</a>


    									</td>
    									<td>
    											{if $page->active}Active{/if}
    									</td>
    								
    									<td class="taskOptions">
    										<a href="/{$WEBPATH}admin/cms/edit/{$page->id|md5}" class="btn btn-primary btn-mini">Edit</a>
    										{*<a href="javascript://" onclick="admin.deleteCategory('{$category->id|md5}');" class="btn btn-danger btn-mini">Delete</a> *}
    									</td>
  									</tr>
  								{/foreach}
									</tbody>
								</table>
							</div>
						</div>
								
					</div>
				</div>
			
			
				
				<div class="row-fluid">
					{include file='admin/snippets/footer.tpl'}
				</div>
			</div>

{/block}