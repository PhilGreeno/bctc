{extends "admin/snippets/default.tpl"}

{block name="pageName"}Dashboard{/block}

{block name="css"}
    <link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/jquery-ui.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/uniform.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/select2.css" />		
{/block}

{block name="additionalJavascripts"}
	<script src="/{$WEBPATH}admin_resources/js/jquery.uniform.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/select2.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/jquery.dataTables.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/unicorn.tables.js"></script>
{/block}


{block name="sidebar"}
	{include file='admin/snippets/sidebar.tpl' sub='user' item='listing'}
{/block}

{block name="content"}

	<div id="content-header">
				<h1>Manage Users</h1>
			
			</div>
			<div id="breadcrumb">
				<a href="/{$WEBPATH}admin" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="#" class="current">Users</a>

			</div>
			<div class="container-fluid">
			
				{include file='admin/snippets/errors.tpl'}
			
				<div class="row-fluid">
					<div class="span12">

						<a href='/{$WEBPATH}admin/user/add' class='btn addButton'>
							<i class="icon-plus"></i><span>Add User</span>
						</a> 

						<div class="widget-box">
							<div class="widget-title">
								<span class="icon iconCreate"><a href='/{$WEBPATH}admin/user/add' class='tip-right' title="Add New User"><i class="icon-plus-sign"></i></a></span>
								<h5>User Listing</h5>
							</div>
							<div class="widget-content nopadding">
								<table class="table table-bordered table-striped table-hover data-table">
									<thead>
  									<tr>
  										<th>Email</th>
  										<th>Company Name</th>
  										<th>Site Admin?</th>
  										<th></th>
  									</tr>
									</thead>
									<tbody>
									{foreach $users as $user}
  									<tr class="gradeA">
    									<td>

    										<a href="/{$WEBPATH}admin/user/edit/{$user->id|md5}" class="viewAdminPage tip-left" title="View &amp; Edit Details">
											<i class="icon-edit friendlyIcon"></i>


    										{$user->email}

    										</a>

    									</td>
    									<td>
    											{assign var=merchant value=reset($user->ownMerchant)}
    											{$merchant->companyname|default:"n/a"|capitalize}
    									</td>
    									<td class='center'>{if $user->isadmin}Yes{/if}</td>
    									<td class="taskOptions">
    										<a href="/{$WEBPATH}admin/user/edit/{$user->id|md5}" class="btn btn-primary btn-mini">Edit</a>
    										<a href="javascript://" onclick="admin.deleteUser('{$user->id|md5}');" class="btn btn-danger btn-mini">Delete</a>
    									</td>
  									</tr>
  								{/foreach}
									</tbody>
								</table>
							</div>
						</div>
								
					</div>
				</div>
			
			
				
				<div class="row-fluid">
					{include file='admin/snippets/footer.tpl'}
				</div>
			</div>

{/block}