{extends "admin/snippets/default.tpl"}

{block name="pageName"}Dashboard{/block}

{block name="css"}
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/uniform.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/select2.css" />		
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/markdown/style.css" />	
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/bootstrap-datetimepicker.min.css" />	
{/block}

{block name="additionalJavascripts"}
	<script src="/{$WEBPATH}admin_resources/js/jquery.uniform.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/select2.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/bootstrap-colorpicker.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/bootstrap-datetimepicker.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/unicorn.form_common.js"></script>
	<script src="/{$WEBPATH}admin_resources/markdown/jquery.markitup.js"></script>
	<script src="/{$WEBPATH}admin_resources/markdown/set.js"></script>
	
	<script type="text/javascript" >
  $(document).ready(function()	{
	{*
  	$('#markItUp').markItUp(mySettings, 
  								{ 	root:'admin_resources/markdown/', 
  									previewParserPath:'/{$WEBPATH}ajax.ws/markdownPreview',
  									previewAutoRefresh:false 
  								}
  							);	
  *}							
		$('#startdatepicker, #enddatepicker').datetimepicker({
      language: 'en'
    });
    
    // Set defaults
	 	$('#startdatepicker').data('datetimepicker').setLocalDate(new Date( '{$event->startdate|date_format:"%B %e, %Y %H:%M:%S"}' ));
    $('#enddatepicker').data('datetimepicker').setLocalDate(new Date( '{$event->enddate|date_format:"%B %e, %Y %H:%M:%S"}' ));

    
	});
	</script>
	
{/block}


{block name="sidebar"}
	{include file='admin/snippets/sidebar.tpl' sub='event' item='edit'}
{/block}

{block name="content"}

	<div id="content-header">
				<h1>Edit Event</h1>
				
			</div>
			<div id="breadcrumb">
				<a href="/{$WEBPATH}admin" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="/{$WEBPATH}admin/event/listing{if $sess_user->isadmin != 1}/{$merchant->id|md5}{/if}" class="tip-bottom">Event</a>
				<a href="#" class="current">Edit Event</a>
			</div>
			<div class="container-fluid">
			
				<div class="row-fluid">
					<div class="span12">
			
  				{include file='admin/snippets/errors.tpl'}
  				
  				{if isset($updated) }
						<div class="alert alert-success">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Success!</strong> The Event data has been added.
						</div>
					{/if}
			
				
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify icon-pencil"></i>									
								</span>
								<h5>Currently editing Event : {$event->title}</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="#" method="post" class="form-horizontal" enctype='multipart/form-data'>
								
								{if $sess_user->isadmin == 1}
									<div class="control-group">
										<label class="control-label">Merchant Account</label>
										<div class="controls">
											<select name='merchant_id' class='span5'>
												{foreach $merchants as $merchant}
													<option value='{$merchant->id}' {if $merchant->id == $event->merchant->id}SELECTED=SELECTED{/if}>{$merchant->companyname}</option>
												{/foreach}
											</select>
											<div class='clearfix'></div>
											<span class="help-block">This is the Event owner.</span>
										</div>
									</div>
								{/if}

									<div class="control-group">
										<label class="control-label">Title</label>
										<div class="controls">
											<input type="text" name='title' value='{$event->title}' required/>
											<input type='hidden' name='action' value='edit'/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Contact Name</label>
										<div class="controls">
											<input type="text" name='contactname' value='{$event->contactname}' required maxlength=128/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Phone</label>
										<div class="controls">
											<input type="text" name='phone' value='{$event->phone}' maxlength=12/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Email</label>
										<div class="controls">
											<input type="email" name='email' value='{$event->email}' maxlength=128/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Url</label>
										<div class="controls">
											<input type="text" name='url' value='{$event->url}' maxlength=128/>
											<span class="help-block">Ensure in the correct format http://www.web.com</span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Address 1</label>
										<div class="controls">
											<input type="text" name='address1' value='{$event->address1}' required maxlength=128/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Address 2</label>
										<div class="controls">
											<input type="text" name='address2' value='{$event->address2}' maxlength=128/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Address 3</label>
										<div class="controls">
											<input type="text" name='address3' value='{$event->address3}' maxlength=128/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Postcode</label>
										<div class="controls">
											<input type="text" name='postcode' value='{$event->postcode}' required maxlength=45/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">County</label>
										<div class="controls">
											<input type="text" name='county' value='{$event->county}' maxlength=45/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Country</label>
										<div class="controls">
											<input type="text" name='country' value='{$event->country}' maxlength=45/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Lat</label>
										<div class="controls">
											<input type="text" name='lat' value='{$event->lat}' />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Long</label>
										<div class="controls">
											<input type="text" name='long' value='{$event->long}' />
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Start Date & Time</label>
										<div class="controls input-append date" id='startdatepicker'>
											<input type="text" name='startdate' value='' data-format="yyyy/MM/dd hh:mm:ss" required/>
											<span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                      </span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">End Date & Time</label>
										<div class="controls input-append date" id='enddatepicker'>
											<input type="text" name='enddate' value='' data-format="yyyy/MM/dd hh:mm:ss" required/>
											<span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                      </span>
										</div>
									</div>
									
									
									<div class="control-group">
										<label class="control-label">Summary</label>
										<div class="controls">
											<textarea name='summary' required>{$event->summary|stripslashes}</textarea>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Description</label>
										<div class="controls">
											<textarea id='markItUp' name='description' required rows=8>{$event->description|stripslashes}</textarea>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Keywords</label>
										<div class="controls">
											<input type="text" name='keywords' value='{$event->keywords}' maxlength=64/>
											<span class="help-block">Keywords are used in search, seperate keyword phrases using a comma.</span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Event Categories</label>
										<div class="controls">
											<input type="text" name='suggested' value='{$event->suggested}' required maxlength=128/>
											<span class="help-block">Please enter your suggested categories, seperate category phrases using a comma.</span>
										</div>
									</div>
									
								{if $sess_user->isadmin == 1|| $merchant->level != 'bronze'}
									<div class="control-group">
										<label class="control-label">Moderated Categories</label>
										<div class="controls">
											<select multiple name='categories[]'>
											{foreach $categories as $category}
											
												<option value='{$category->id}' {if isset($category->selected)}selected=selected{/if}>{$category->name|capitalize}</option>
											
											{/foreach}

											</select>
											<span class="help-block">The real categories that the classified will be published under - this is controlled by site admin or elevated users.</span>
										</div>
									</div>
								{/if}
									
									<div class="control-group">

										<label class="control-label">Images</label>
										<div class="controls">
										
											{if $event->image1}
												<img src='/{$WEBPATH}image/event_{$event->id|md5}_1/50/50/'/>
												<input type='checkbox' name='delete[1]'> Delete
											{/if}
											<input type="file" name='logo[1]' /> <span>Primary Image</span>
											<div class='clearfix'></div>
											{if $event->image2}<img src='/{$WEBPATH}image/event_{$event->id|md5}_2/50/50/'/> <input type='checkbox' name='delete[2]'> Delete{/if}
											<input type="file" name='logo[2]' />
											<div class='clearfix'></div>
											{if $event->image3}<img src='/{$WEBPATH}image/event_{$event->id|md5}_3/50/50/'/> <input type='checkbox' name='delete[3]'> Delete{/if}
											<input type="file" name='logo[3]' />
											<div class='clearfix'></div>
											{if $event->image4}<img src='/{$WEBPATH}image/event_{$event->id|md5}_4/50/50/'/> <input type='checkbox' name='delete[4]'> Delete{/if}
											<input type="file" name='logo[4]' />
											<span class="help-block">Be aware that images will be scaled and cropped to ensure they fit within the confines of the page.</span>
										</div>
									</div>

									<div class="form-actions">
										<button type="submit" class="btn btn-primary">Save</button>
										
									{if $sess_user->isadmin == 1}
										{if $event->publicationdate}
										<button type="submit" name='unpublish' value='true' class="btn btn-warning">Save & unPublish</button>
										{else}
											{if count($event->sharedCategory) == 0}
      								{else}
												<button type="submit" name='publish' value='true' class="btn btn-primary">Save & Publish</button>
											{/if}
										{/if}
									{else}
  										Saving changes will remove this Event from the site pending moderation.
  									{/if}
										
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			
			
				
				<div class="row-fluid">
					{include file='admin/snippets/footer.tpl'}
				</div>
			</div>

{/block}