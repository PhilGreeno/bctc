{extends "admin/snippets/default.tpl"}

{block name="pageName"}Dashboard{/block}

{block name="css"}
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/uniform.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/select2.css" />		
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/markdown/style.css" />	
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/bootstrap-datetimepicker.min.css" />	
{/block}

{block name="additionalJavascripts"}
	<script src="/{$WEBPATH}admin_resources/js/jquery.uniform.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/select2.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/bootstrap-colorpicker.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/bootstrap-datetimepicker.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/unicorn.form_common.js"></script>
	<script src="/{$WEBPATH}admin_resources/markdown/jquery.markitup.js"></script>
	<script src="/{$WEBPATH}admin_resources/markdown/set.js"></script>
	
	<script type="text/javascript" >
  $(document).ready(function()	{
	{*
  	$('#markItUp').markItUp(mySettings, 
  								{ 	root:'admin_resources/markdown/', 
  									previewParserPath:'/{$WEBPATH}ajax.ws/markdownPreview',
  									previewAutoRefresh:false 
  								}
  							);	
 	*} 							
		$('#startdatepicker, #enddatepicker').datetimepicker({
      language: 'en'
    });
    
    // Set defaults
    $('#startdatepicker').data('datetimepicker').setLocalDate(new Date( '{$deal->startdate|date_format:"%B %e, %Y %H:%M:%S"}' ));
    $('#enddatepicker').data('datetimepicker').setLocalDate(new Date( '{$deal->enddate|date_format:"%B %e, %Y %H:%M:%S"}' ));
    
	});
	</script>
	
{/block}


{block name="sidebar"}
	{include file='admin/snippets/sidebar.tpl' sub='deal' item='edit'}
{/block}

{block name="content"}

	<div id="content-header">
				<h1>Edit Offer</h1>
			
			</div>
			<div id="breadcrumb">
				<a href="/{$WEBPATH}admin" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="/{$WEBPATH}admin/deal/listing{if $sess_user->isadmin != 1}/{$merchant->id|md5}{/if}" class="tip-bottom">Deals</a>
				<a href="#" class="current">Edit Deal</a>
			</div>
			<div class="container-fluid">
			
				<div class="row-fluid">
					<div class="span12">
			
  				{include file='admin/snippets/errors.tpl'}
  				
  				{if isset($updated) }
						<div class="alert alert-success">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Success!</strong> The Offer data has been updated.
						</div>
					{/if}
			
				
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify icon-pencil"></i>									
								</span>
								<h5>Edit Offer</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="#" method="post" class="form-horizontal" enctype='multipart/form-data'>
								
								{if $sess_user->isadmin == 1}
									<div class="control-group">
										<label class="control-label">Merchant Account</label>
										<div class="controls">
											<select name='merchant_id' class='span5'>
												{foreach $merchants as $merchant}
													<option value='{$merchant->id}' {if $merchant->id == $deal->merchant->id}SELECTED=SELECTED{/if}>{$merchant->companyname}</option>
												{/foreach}
											</select>
											<div class='clearfix'></div>
											<span class="help-block">This is the Offer owner.</span>
										</div>
									</div>
								{/if}

									<div class="control-group">
										<label class="control-label">Title</label>
										<div class="controls">
											<input type="text" name='title' value='{$deal->title|stripslashes}' required/>
											<input type='hidden' name='action' value='edit'/>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Summary</label>
										<div class="controls">
											<textarea name='summary' required>{$deal->summary|stripslashes}</textarea>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Description</label>
										<div class="controls">
											<textarea id='markItUp' name='description' required rows=8>{$deal->description|stripslashes}</textarea>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Conditions</label>
										<div class="controls">
											<textarea name='conditions' required>{$deal->conditions|stripslashes}</textarea>
											<span class="help-block">Please specify and be clear about any conditions regarding this offer.</span>
										</div>
									</div>
									
									
									<div class="control-group">
										<label class="control-label">Start Date & Time</label>
										<div class="controls input-append" id='startdatepicker'>
											<input type="text" name='startdate' value='' data-format="yyyy/MM/dd hh:mm:ss" required/>
											<span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                      </span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">End Date & Time</label>
										<div class="controls input-append" id='enddatepicker'>
											<input type="text" name='enddate' value='' data-format="yyyy/MM/dd hh:mm:ss" required/>
											<span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                      </span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Price (&pound;)</label>
										<div class="controls">
											<input type="text" name='value' value='{$deal->value}' />
											<span class="help-block">Please enter the retail cost of this offer - this is the price *BEFORE* discount.</span><br/>
											<span class="help-block">A value of '0' will not show</span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Discount %</label>
										<div class="controls">
											<input type="text" name='discount' value='{$deal->discount}' />
											<span class="help-block">A value of '0' will not show</span>
										</div>
									</div>
	
									<div class="control-group">
										<label class="control-label">Keywords</label>
										<div class="controls">
											<input type="text" name='keywords' value='{$deal->keywords}' maxlength=64/>
											<span class="help-block">Keywords are used in search, seperate keyword phrases using a comma.</span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Offer Categories</label>
										<div class="controls">
											<input type="text" name='suggested' value='{$deal->suggested}' required maxlength=128/>
											<span class="help-block">Please enter your suggested categories, seperate category phrases using a comma.</span>
										</div>
									</div>
									
								{if $sess_user->isadmin == 1 || $merchant->level != 'bronze'}
									<div class="control-group">
										<label class="control-label">Moderated Categories</label>
										<div class="controls">
											<select multiple name='categories[]'>
											{foreach $categories as $category}
											
												<option value='{$category->id}' {if isset($category->selected)}selected=selected{/if}>{$category->name|capitalize}</option>
											
											{/foreach}

											</select>
											<span class="help-block">The real categories that the classified will be published under - this is controlled by site admin or elevated users.</span>
										</div>
									</div>
								{/if}
									
									<div class="control-group">

										<label class="control-label">Images</label>
										<div class="controls">
										
											{if $deal->image1}
												<img src='/{$WEBPATH}image/deal_{$deal->id|md5}_1/50/50/'/>
												<input type='checkbox' name='delete[1]'> Delete
											{/if}
											<input type="file" name='logo[1]' /> <span>Primary Image</span>
											<div class='clearfix'></div>
											{if $deal->image2}<img src='/{$WEBPATH}image/deal_{$deal->id|md5}_2/50/50/'/> <input type='checkbox' name='delete[2]'> Delete{/if}
											<input type="file" name='logo[2]' />
											<div class='clearfix'></div>
											{if $deal->image3}<img src='/{$WEBPATH}image/deal_{$deal->id|md5}_3/50/50/'/> <input type='checkbox' name='delete[3]'> Delete{/if}
											<input type="file" name='logo[3]' />
											<div class='clearfix'></div>
											{if $deal->image4}<img src='/{$WEBPATH}image/deal_{$deal->id|md5}_4/50/50/'/> <input type='checkbox' name='delete[4]'> Delete{/if}
											<input type="file" name='logo[4]' />
											<span class="help-block">Be aware that images will be scaled and cropped to ensure they fit within the confines of the page.</span>
										</div>
									</div>

									<div class="form-actions">
										<button type="submit" class="btn btn-primary">Save</button>
									{if $sess_user->isadmin == 1}
										{if $deal->publicationdate}
										<button type="submit" name='unpublish' value='true' class="btn btn-warning">Save & unPublish</button>
										{else}
											{if count($deal->sharedCategory) == 0}
      								{else}
												<button type="submit" name='publish' value='true' class="btn btn-primary">Save & Publish</button>
											{/if}
										{/if}
									{else}
										Saving changes will remove this deal from the site pending moderation.
									{/if}
										
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			
			
				
				<div class="row-fluid">
					{include file='admin/snippets/footer.tpl'}
				</div>
			</div>

{/block}