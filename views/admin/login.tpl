<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Broadstone Village Admin</title>
		<meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
				<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/bootstrap.min.css" />
				<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/unicorn.login.css" />
    </head>
    <body>
        <div id="logo">
            <img src="/{$WEBPATH}admin_resources/img/bctc.gif" width="50px" alt="" />
        </div>
        <div id="loginbox">            
            <form id="loginform" class="form-vertical" action="" method='post'>
							<p>Enter email and password to continue.</p>
                <div class="control-group">
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-user"></i></span><input type="email" name='email' placeholder="Email" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-lock"></i></span><input type="password" name='password' placeholder="Password" />
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link" id="to-recover">Lost password?</a></span>
                    <span class="pull-right"><input type="submit" class="btn btn-inverse" value="Login" /></span>
                </div>
            </form>
            <form id="recoverform" action="#" class="form-vertical">
        				<p>Enter your e-mail address below and we will send you instructions how to recover a password.</p>
        				<div class="control-group">
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-envelope"></i></span><input type="text" placeholder="E-mail address" />
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link" id="to-login">&lt; Back to login</a></span>
                    <span class="pull-right"><input type="submit" class="btn btn-inverse" value="Recover" /></span>
                </div>
                <div class='clearfix'></div>
            </form>
        </div>
        
        {if isset($errors)}
        	<br/><br/>
    			<div class="container">
    			
      			{foreach $errors as $error}
      				<div class="alert alert-error">
      					<button type="button" class="close" data-dismiss="alert">&times;</button>
        				<h4>{$error.title}</h4>
        				{$error.message}
      				</div>
      			{/foreach}
    			
    			</div>
  			{/if}
        
        <script src="/{$WEBPATH}admin_resources/js/jquery.min.js"></script>  
        <script src="/{$WEBPATH}admin_resources/js/unicorn.login.js"></script> 
    </body>
</html>
