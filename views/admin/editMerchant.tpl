{extends "admin/snippets/default.tpl"}

{block name="pageName"}Dashboard{/block}

{block name="css"}
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/uniform.css" />
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/css/select2.css" />	
		<link rel="stylesheet" href="/{$WEBPATH}admin_resources/markdown/style.css" />			
{/block}

{block name="additionalJavascripts"}
	<script src="/{$WEBPATH}admin_resources/js/jquery.uniform.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/select2.min.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/bootstrap-colorpicker.js"></script>
	<script src="/{$WEBPATH}admin_resources/js/unicorn.form_common.js"></script>
	<script src="/{$WEBPATH}admin_resources/markdown/jquery.markitup.js"></script>
	<script src="/{$WEBPATH}admin_resources/markdown/set.js"></script>
	
	<script type="text/javascript" >
  $(document).ready(function()	{
	
  	$('#markItUp').markItUp(mySettings, 
  								{ 	root:'admin_resources/markdown/', 
  									previewParserPath:'ajax.ws/markdownPreview',
  									previewAutoRefresh:false 
  								}
  							);	
	
	});
	</script>
{/block}


{block name="sidebar"}
	{include file='admin/snippets/sidebar.tpl' sub='merchant' item='listing'}
{/block}

{block name="content"}

	<div id="content-header">
				<h1>Edit Merchant</h1>
			
			</div>
			<div id="breadcrumb">
				<a href="/{$WEBPATH}admin" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="/{$WEBPATH}admin/merchant/listing" class="tip-bottom">Merchants</a>
				<a href="#" class="current">Edit Merchant</a>
			</div>
			<div class="container-fluid">

				<div class="row-fluid">
					<div class="span12">
					
					{include file='admin/snippets/errors.tpl'}
					
					{if isset($updated) }
						<div class="alert alert-success">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Success!</strong> The merchant data has been updated.
						</div>
					{/if}
					
						<a href='/{$WEBPATH}admin/user/edit/{$merchant->user->id|md5}' class='btn addButton'>
							<i class="icon-edit friendlyIcon"></i>Edit User</a>
						<a href='/{$WEBPATH}admin/article/listing/{$merchant->id|md5}' class='btn addButton'>
							<i class=" icon-eye-open friendlyIcon"></i>View Articles</a>
						<a href='/{$WEBPATH}admin/classified/listing/{$merchant->id|md5}' class='btn addButton'><i class=" icon-eye-open friendlyIcon"></i>View Classifieds</a>
						<a href='/{$WEBPATH}admin/event/listing/{$merchant->id|md5}' class='btn addButton'>
							<i class=" icon-eye-open friendlyIcon"></i>View Events</a>
						<a href='/{$WEBPATH}admin/deal/listing/{$merchant->id|md5}' class='btn addButton'>
							<i class=" icon-eye-open friendlyIcon"></i>View Deals</a>
						
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify icon-pencil"></i>									
								</span>
								<h5>Currently Editing : {$merchant->companyname|capitalize}</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="#" method="post" class="form-horizontal" enctype='multipart/form-data'>
								
								{if $sess_user->isadmin == 1}
									<div class="control-group">
										<label class="control-label">Member Level</label>
										<div class="controls">
											<select name='level'>
												<option value='bronze' {if $merchant->level == 'bronze'}selected=selected{/if}>Bronze</option>
												<option value='silver' {if $merchant->level == 'silver'}selected=selected{/if}>Silver</option>
												<option value='gold' {if $merchant->level == 'gold'}selected=selected{/if}>Gold</option>
											</select>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Association</label>
										<div class="controls">
											<select name='association'>
												<option value='1' {if $merchant->association == 1}selected=selected{/if}>Yes</option>
												<option value='0' {if $merchant->association == 0}selected=selected{/if}>No</option>
											</select>
											<span class="help-block">Should this entry be presented as an Association on the main site?</span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Stick to Home?</label>
										<div class="controls">
											<select name='sticky'>
												<option value='0' {if $merchant->sticky == 0}selected=selected{/if}>No</option>
												<option value='1' {if $merchant->sticky == 1}selected=selected{/if}>1</option>
												<option value='2' {if $merchant->sticky == 2}selected=selected{/if}>2</option>
												<option value='3' {if $merchant->sticky == 3}selected=selected{/if}>3</option>
												<option value='4' {if $merchant->sticky == 4}selected=selected{/if}>4</option>
											</select>
											<span class="help-block">Should this Merchant always be present on the home page? Please choose the position. A sticky merchant will not appear in the featured listing.</span>
										</div>
									</div>
									
								{/if}
								
									
								
									<div class="control-group">
										<label class="control-label">First Name</label>
										<div class="controls">
											<input type="text" name='firstname' value='{$merchant->firstname}' maxlength=45/>
											<input type='hidden' name='action' value='edit'/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Last Name</label>
										<div class="controls">
											<input type="text" name='lastname' value='{$merchant->lastname}'  maxlength=45/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Username</label>
										<div class="controls">
											<input type="text" name='username' value='{$merchant->username}'  maxlength=45/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Company Name</label>
										<div class="controls">
											<input type="text" name='companyname' value='{$merchant->companyname}'  maxlength=128/>
										</div>
									</div>
									
									<div class="control-group">

										<label class="control-label">Logo</label>
										<div class="controls">
										
											{if $merchant->image1}
												<img src='/{$WEBPATH}image/merchant_{$merchant->id|md5}_1/50/50/'/>
												<input type='checkbox' name='delete[1]'> Delete
											{/if}
											<input type="file" name='logo[1]' />
											
											<span class="help-block">Be aware that images will be scaled and cropped to ensure they fit within the confines of the page.</span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Address Line 1</label>
										<div class="controls">
											<input type="text" name='address1' value='{$merchant->address1}'  maxlength=128/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Address Line 2</label>
										<div class="controls">
											<input type="text" name='address2' value='{$merchant->address2}'  maxlength=128/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Address Line 3</label>
										<div class="controls">
											<input type="text" name='address3' value='{$merchant->address3}'  maxlength=128/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">County</label>
										<div class="controls">
											<input type="text" name='county' value='{$merchant->county}' maxlength=45/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Postcode</label>
										<div class="controls">
											<input type="text" name='postcode' value='{$merchant->postcode}' maxlength=45/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Country</label>
										<div class="controls">
											<input type="text" name='country' value='{$merchant->country}' maxlength=45/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Lat</label>
										<div class="controls">
											<input type="text" name='lat' value='{$merchant->lat}' />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Long</label>
										<div class="controls">
											<input type="text" name='long' value='{$merchant->long}' maxlength=45/>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Phone</label>
										<div class="controls">
											<input type="text" name='phone' value='{$merchant->phone}' maxlength=12/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Fax</label>
										<div class="controls">
											<input type="text" name='fax' value='{$merchant->fax}' maxlength=12/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Mobile</label>
										<div class="controls">
											<input type="text" name='mobile' value='{$merchant->mobile}' maxlength=12/>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">URL</label>
										<div class="controls">
											<input type="text" name='url' value='{$merchant->url}' maxlength=128/>
											<span class="help-block">Please ensure this is in the format of http://www.website.com</span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Twitter URL</label>
										<div class="controls">
											<input type="text" name='twitter' value='{$merchant->twitter}' maxlength=128/>
											<span class="help-block">Please ensure this is in the format of http://www.website.com</span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Facebook URL</label>
										<div class="controls">
											<input type="text" name='facebook' value='{$merchant->facebook}' maxlength=128/>
											<span class="help-block">Please ensure this is in the format of http://www.website.com</span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Who we are:</label>
										<div class="controls">
											<textarea name='summary' required>{$merchant->summary|stripslashes}</textarea>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label">What We Offer:</label>
										<div class="controls">
											<input type="text" name='keywords' value='{$merchant->keywords}'/>
											<span class="help-block">This appears under your name in the listings and home page.</span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Open Hours</label>
										<div class="controls">
											<input type='text' name='extrafield1' value='{$merchant->extrafield1}'/>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">More about our business:</label>
										<div class="controls">
											<textarea name='description' id='markItUp' required>{$merchant->description|stripslashes}</textarea>
										</div>
									</div>
									
									
									
									<div class="control-group">
										<label class="control-label">Business Categories</label>
										<div class="controls">
											<select multiple name='categories[]'>
											{foreach $categories as $category}
											
												<option value='{$category->id}' {if isset($category->selected)}selected=selected{/if}>{$category->name|capitalize}</option>
											
											{/foreach}

											</select>
										</div>
									</div>
									
									<div class="control-group">

										<label class="control-label">Images</label>
										<div class="controls">
										
										
											{if $merchant->image2}<img src='/{$WEBPATH}image/merchant_{$merchant->id|md5}_2/50/50/'/> <input type='checkbox' name='delete[2]'> Delete{/if}
											<input type="file" name='logo[2]' />
											<div class='clearfix'></div>
											{if $merchant->image3}<img src='/{$WEBPATH}image/merchant_{$merchant->id|md5}_3/50/50/'/> <input type='checkbox' name='delete[3]'> Delete{/if}
											<input type="file" name='logo[3]' />
											<div class='clearfix'></div>
											{if $merchant->image4}<img src='/{$WEBPATH}image/merchant_{$merchant->id|md5}_4/50/50/'/> <input type='checkbox' name='delete[4]'> Delete{/if}
											<input type="file" name='logo[4]' />
											<div class='clearfix'></div>
											{if $merchant->image5}<img src='/{$WEBPATH}image/merchant_{$merchant->id|md5}_5/50/50/'/> <input type='checkbox' name='delete[5]'> Delete{/if}
											<input type="file" name='logo[5]' />
											<div class='clearfix'></div>
											{if $merchant->image6}<img src='/{$WEBPATH}image/merchant_{$merchant->id|md5}_5/50/50/'/> <input type='checkbox' name='delete[6]'> Delete{/if}
											<input type="file" name='logo[6]' />
											<span class="help-block">Be aware that images will be scaled and cropped to ensure they fit within the confines of the page.</span>
										</div>
									</div>

									<div class="form-actions">
										<button type="submit" class="btn btn-primary">Save</button>
									</div>
								</form>
							</div>
						</div>
								
					</div>
				</div>
			
			
				
				<div class="row-fluid">
					{include file='admin/snippets/footer.tpl'}
				</div>
			</div>

{/block}