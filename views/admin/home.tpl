{extends "admin/snippets/default.tpl"}

{block name="pageName"}Dashboard{/block}

{block name="css"}{/block}

{block name="additionalJavascripts"}
	 {* <script src="/{$WEBPATH}admin_resources/js/jquery.flot.min.js"></script>
    <script src="/{$WEBPATH}admin_resources/js/jquery.flot.resize.min.js"></script> 
    <script src="/{$WEBPATH}admin_resources/js/jquery.peity.min.js"></script>*}
    <script src="/{$WEBPATH}admin_resources/js/fullcalendar.min.js"></script>
    <script src="/{$WEBPATH}admin_resources/js/unicorn.dashboard.js"></script>
{/block}


{block name="content"}

		<div id="content-header">
				<h1>Dashboard</h1>
				<div class="btn-group">
					<a href='/{$WEBPATH}admin/imageCache' class="btn " title=""><i class="icon-trash"></i>Clean Image Caches</a>
		{*			<a class="btn btn-large tip-bottom" title="Manage Users"><i class="icon-user"></i></a>
					<a class="btn btn-large tip-bottom" title="Manage Comments"><i class="icon-comment"></i><span class="label label-important">5</span></a>
					<a class="btn btn-large tip-bottom" title="Manage Orders"><i class="icon-shopping-cart"></i></a> *}
				</div> 	
			</div>
			<div id="breadcrumb">
				<a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="#" class="current">Dashboard</a>
			</div>
			<div class="container-fluid">
			
				{include file='admin/snippets/errors.tpl'}

				<div class="row-fluid">
					<div class="span12 center" style="text-align: center;">		

						{include file='admin/snippets/stat-buttons.tpl'}

					</div>	
				</div>
			
				

				<div class="row-fluid">
					<div class="span6">
						<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-file"></i></span><h5>Recent Articles</h5><span title="{count($articles)} recent articles" class="label label-info tip-left">{count($articles)}</span></div>
							<div class="widget-content nopadding">
								<ul class="recent-posts">
								
								{foreach $articles as $article key=i name=foo}
									{if $smarty.foreach.foo.index == 10}
                    {break}
                  {/if}
									<li>
										<div class="user-thumb">
											<img src='/{$WEBPATH}image/article_{$article->id|md5}_1/40/40/'/>
										</div>
										<div class="article-post">
											<span class="user-info"> By: {$article->author} on {$article->updated|date_format:'%d %b %Y, %R'} </span>
											<p>
												<a href="#">{$article->title|truncate}</a>
											</p>
											<a href="/{$WEBPATH}admin/article/edit/{$article->id|md5}" class="btn btn-primary btn-mini">Edit</a>
											{if count($article->sharedCategory) == 0}
      									<a href="javascript://" onclick="alert('You must assign at least one category to an item before you publish it.');" class="btn btn-success btn-mini">Publish</a>
      								{else} 
												<a href="/{$WEBPATH}admin/article/publish/{$article->id|md5}" class="btn btn-success btn-mini">Publish</a> 
											{/if}
											<a href="javascript://" onclick="admin.deleteArticle('{$article->id|md5}');" class="btn btn-danger btn-mini">Delete</a>
										</div>
									</li>
								{foreachelse}
									<li>
										There are no unpublished articles
									</li>
								{/foreach}
									
									<li class="viewall">
										<a title="View all Articles" class="tip-top" href="/{$WEBPATH}admin/article/listing"> + View all + </a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					
					<div class="span6">
						<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-file"></i></span><h5>Recent Classifieds</h5><span title="{count($classifieds)} recent articles" class="label label-info tip-left">{count($classifieds)}</span></div>
							<div class="widget-content nopadding">
								<ul class="recent-posts">
								
								{foreach $classifieds as $classified key=i name=foo}
									{if $smarty.foreach.foo.index == 10}
                    {break}
                  {/if}
								
									<li>
										<div class="user-thumb">
											<img src='/{$WEBPATH}image/classified_{$classified->id|md5}_1/40/40/'/>
										</div>
										<div class="article-post">
											<span class="user-info"> By: {$classified->contact} on {$classified->updated|date_format:'%d %b %Y, %R'} </span>
											<p>
												<a href="#">{$classified->title|truncate}</a>
											</p>
											<a href="/{$WEBPATH}admin/classified/edit/{$classified->id|md5}" class="btn btn-primary btn-mini">Edit</a>
											{if count($classified->sharedCategory) == 0}
      									<a href="javascript://" onclick="alert('You must assign at least one category to an item before you publish it.');" class="btn btn-success btn-mini">Publish</a>
      								{else} 
												<a href="/{$WEBPATH}admin/classified/publish/{$classified->id|md5}" class="btn btn-success btn-mini">Publish</a> 
											{/if}
											<a href="javascript://" onclick="admin.deleteClassified('{$classified->id|md5}');" class="btn btn-danger btn-mini">Delete</a>
										</div>
									</li>
								{foreachelse}
									<li>
										There are no unpublished classifieds
									</li>
								{/foreach}
									
									<li class="viewall">
										<a title="View all Classifieds" class="tip-top" href="/{$WEBPATH}admin/classified/listing"> + View all + </a>
									</li>
								</ul>
							</div>
					</div>
				</div>
				<div class='clearfix'></div>
				<div class="row-fluid">
					<div class="span6">
						<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-file"></i></span><h5>Recent Deals</h5><span title="{count($deals)} recent deals" class="label label-info tip-left">{count($deals)}</span></div>
							<div class="widget-content nopadding">
								<ul class="recent-posts">
								
								{foreach $deals as $deal key=i name=foo}
									{if $smarty.foreach.foo.index == 10}
                    {break}
                  {/if}
								
									<li>
										<div class="user-thumb">
											<img src='/{$WEBPATH}image/deal_{$deal->id|md5}_1/40/40/'/>
										</div>
										<div class="article-post">
											<span class="user-info"> By: {$deal->merchant->companyname} on {$deal->updated|date_format:'%d %b %Y, %R'} </span>
											<p>
												<a href="#">{$deal->title|truncate}</a>
											</p>
											<a href="/{$WEBPATH}admin/deal/edit/{$deal->id|md5}" class="btn btn-primary btn-mini">Edit</a> 
											{if count($deal->sharedCategory) == 0}
      									<a href="javascript://" onclick="alert('You must assign at least one category to an item before you publish it.');" class="btn btn-success btn-mini">Publish</a>
      								{else}
												<a href="/{$WEBPATH}admin/deal/publish/{$deal->id|md5}" class="btn btn-success btn-mini">Publish</a> 
											{/if}
											<a href="javascript://" onclick="admin.deleteDeal('{$deal->id|md5}');" class="btn btn-danger btn-mini">Delete</a>
										</div>
									</li>
								{foreachelse}
									<li>
										There are no unpublished deals
									</li>
								{/foreach}
									
									<li class="viewall">
										<a title="View all Deals" class="tip-top" href="/{$WEBPATH}admin/deal/listing"> + View all + </a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					
					<div class="span6">
						<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-file"></i></span><h5>Recent Events</h5><span title="{count($events)} recent events" class="label label-info tip-left">{count($events)}</span></div>
							<div class="widget-content nopadding">
								<ul class="recent-posts">
								
								{foreach $events as $event key=i name=foo}
									{if $smarty.foreach.foo.index == 10}
                    {break}
                  {/if}
								
									<li>
										<div class="user-thumb">
											<img src='/{$WEBPATH}image/event_{$event->id|md5}_1/40/40/'/>
										</div>
										<div class="article-post">
											<span class="user-info"> By: {$event->contactname} on {$event->updated|date_format:'%d %b %Y, %R'} </span>
											<p>
												<a href="#">{$event->title|truncate}</a>
											</p>
											<a href="/{$WEBPATH}admin/event/edit/{$event->id|md5}" class="btn btn-primary btn-mini">Edit</a>
											{if count($event->sharedCategory) == 0}
      									<a href="javascript://" onclick="alert('You must assign at least one category to an item before you publish it.');" class="btn btn-success btn-mini">Publish</a>
      								{else} 
												<a href="/{$WEBPATH}admin/event/publish/{$event->id|md5}" class="btn btn-success btn-mini">Publish</a> 
											{/if}
											<a href="javascript://" onclick="admin.deleteEvent('{$event->id|md5}');" class="btn btn-danger btn-mini">Delete</a>
										</div>
									</li>
								{foreachelse}
									<li>
										There are no unpublished articles
									</li>
								{/foreach}
									
									<li class="viewall">
										<a title="View all Events" class="tip-top" href="/{$WEBPATH}admin/event/listing"> + View all + </a>
									</li>
								</ul>
							</div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box widget-calendar">
							<div class="widget-title"><span class="icon"><i class="icon-calendar"></i></span><h5>Event Calendar</h5></div>
							<div class="widget-content nopadding">
								<div class="calendar"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="row-fluid">
					{include file='admin/snippets/footer.tpl'}
				</div>
			</div>
			{include file='admin/snippets/eventCal.tpl'}

{/block}