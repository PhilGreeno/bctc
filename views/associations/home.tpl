{extends "snippets/default.tpl"}

{block name="pageName"}Associations{/block}

{block name="css"}{/block}

{block name="additionalJavascripts"}{/block}

{block name="header"}
			{include file='snippets/navBar.tpl' active='association'}
			
			{include file='snippets/topBar.tpl'}
{/block}

{block name="content"}

		<div class="container-fluid">
			<div class="row-fluid">

				<div class="span3">
					{include file='snippets/widgets/browseByCategory.tpl' active='association'}
				</div>




				<div class="span9">  
					
					<div class="main_content detail-product">						
						<h3 class="title justTitle">Broadstone Associations

							<span class="pull-right hidden-phone">{include file='snippets/widgets/paginator.tpl'}</span>


						</h3>

						
					{foreach $merchants as $merchant name=foo}
						<div class="listing-item">
							<div class="row-fluid">							
								<div class="span7">
									<div class="featured-item" id='item_{$merchant->id|md5}'>
										<div class="span4">		
										
											<a href="/{$WEBPATH}associations/detail/{$merchant->companyname|trim|replace:' ':'+'}">
										{if $merchant->image1}
												<img class="img" src="/{$WEBPATH}image/merchant_{$merchant->id|md5}_1/152/85/" alt="{$merchant->companyname}" title="{$merchant->companyname}" class="img pull-left"/>
										{else}
												<img class="img" src="/{$WEBPATH}image/placeholder/152/85/" alt="{$merchant->companyname}" title="{$merchant->companyname}" class="img pull-left"/>
										{/if}
											</a>
											
										
										</div>
										<div class="span8">
											<h5>{$smarty.foreach.foo.index + 1}. <a href="/{$WEBPATH}associations/detail/{$merchant->companyname|trim|replace:' ':'+'}" class="title">{$merchant->companyname}</a> </h5>
											in 
											{foreach from=$merchant->categories item=category key=i name=twat}{if not $smarty.foreach.twat.last}, {/if}
												<a href="/{$WEBPATH}associations/category/{$category|trim|replace:' ':'+'}" class="subinfo">{$category|trim}</a>
											{/foreach}
											<hr/>
											<p>{$merchant->summary|truncate:250:"...":true}</p>

										</div>								
									</div>								
								</div>
								<div class="span5">
									<div class="other_info hidden-phone">
										<address>				
											<i>{$merchant->address1},<br/>
											{if $merchant->address2 != ''}{$merchant->address2}, {/if}
											{if $merchant->address3 != ''}{$merchant->address3}, {/if}
											{$merchant->county} {$merchant->postcode}.</i>				
										</address>
										<p><strong>Contact Name:</strong> {$merchant->firstname} {$merchant->lastname}<br/>
										
									{if $merchant->phone}
										<strong>Phone:</strong> {$merchant->phone|phone_format:'UK'}<br/>
									{/if}
									
									{if $merchant->url}
										<strong>URL:</strong> <a href="{$merchant->url}" target="_blank">{$merchant->url|url}</a>
									{/if}	
										</p>

									</div>
								</div>
							</div>
						</div>
					{foreachelse}
						<h5>There are no businesses listed that match your criteria</h5>
					{/foreach}

					<div class="clearfix"></div>

					<div class="bottomPageCount">{include file='snippets/widgets/paginator.tpl'}</div>

					</div>
				</div>
				
			</div><!--Row Fluid-->			
		</div>


{/block}