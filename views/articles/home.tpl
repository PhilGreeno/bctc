{extends "snippets/default.tpl"}

{block name="pageName"}Articles{/block}

{block name="css"}{/block}

{block name="additionalJavascripts"}{/block}

{block name="header"}
			{include file='snippets/navBar.tpl' active='articles'}
			
			{include file='snippets/topBar.tpl'}
{/block}

{block name="content"}

		<div class="container-fluid">
			<div class="row-fluid">

				<div class="span3">
					
					{include file='snippets/widgets/browseByCategory.tpl' active='articles'}
										
				</div>


				<div class="span9">  
				                 

					
					
					
					<div class="main_content detail-product">	


						<h3 class="title artTitle justTitle">Broadstone Articles &amp; News


							<span class="pull-right hidden-phone">{include file='snippets/widgets/paginator.tpl'}</span>



						</h3>

						


					{foreach $articles as $article}
						<div class="listing-item">
							<div class="row-fluid">		
												
								<div class="span12">
									<div class="featured-item">
									
										<div class="span2 hidden-phone">		
											<a href="/{$WEBPATH}articles/detail/{$article->id|md5}">
										{if $article->image1}
												<img class="img" src="/{$WEBPATH}image/article_{$article->id|md5}_1/152/85/" alt="{$article->title}" title="{$article->title}" class="img pull-left"/>
										{else}
												<img class="img" src="/{$WEBPATH}image/placeholder/152/85/" alt="{$article->title}" title="{$article->title}" class="img pull-left"/>
										{/if}
											</a>
						
										</div>
										<div class="span8">
											<h5><a href="/{$WEBPATH}articles/detail/{$article->id|md5}" class="title">{$article->title|stripslashes}</a> </h5>
											Published : {$article->publicationdate|date_format} by {$article->author|capitalize} in
											{foreach from=$article->categories item=category key=i name=twat}
												{if not $smarty.foreach.twat.last},&nbsp;{/if}<a href="/{$WEBPATH}articles/category/{$category|trim|replace:' ':'+'}" class="subinfo">{$category|trim}</a>
											{/foreach}
											<hr/>
											<p class="hidden-phone">{$article->abstract|truncate:250:"...":true}</p>

										</div>								
									</div>								
								</div>
								
							</div>
						</div>
					{foreachelse}
						<h5>There are no articles listed that match your criteria</h5>
					{/foreach}

					<div class="clearfix"></div>

					<div class="bottomPageCount">{include file='snippets/widgets/paginator.tpl'}</div>

					</div>
				</div>
				
			</div>			
		</div>


{/block}