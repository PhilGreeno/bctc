{extends "snippets/default.tpl"}

{block name="pageName"}{$article->title|capitalize}{/block}

{block name="css"}
<link href="/{$WEBPATH}css/jquery.fancybox.css" rel="stylesheet">
{/block}

{block name="additionalJavascripts"}

<script src="/{$WEBPATH}js/jquery.fancybox.js"></script>
<script>
	$(document).ready(function() {
		$('.thumbnail').fancybox({
			openEffect  : 'none',
			closeEffect : 'none'
		});
	});
</script>

{/block}

{block name="header"}
			{include file='snippets/navBar.tpl' active='articles'}
			
			{include file='snippets/topBar.tpl'}
{/block}

{block name="content"}


		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span9">                   
					
					
					<div class="main_content detail-product detail-back">						
						<h3 class="title justTitle">{$article->title|stripslashes}</h3>			
						<div class="row-fluid">
							

							<div class="span8">
								
								<ul class="social pull-right">
									<li>									
										<div class="fb-like" send="false" layout="button_count" ></div>
										<script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script>
									</li>
									<li>
										<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal" >Tweet</a>
										<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
									</li>
								</ul>

		
							
								<h5>Overview</h5>
								<p>{$article->abstract|stripslashes|default:'n/a'}</p>				
								
								<p><strong>Published:</strong> {$article->publicationdate|date_format}</p>
								<p><strong>By:</strong> {$article->author|capitalize}</p>
								
								<p>{$article->content|stripslashes|default:'n/a'|markdown}</p>
										
														
							</div>
							
							<div class="span4">
								
								{include file='snippets/widgets/imageGrid.tpl' type=article obj=$article}
								
							</div>
														
						</div>
											
					</div>
				</div>
				
				<div class="span3">
					
					<h3 class="title justTitle">Comments</h3><br/>
					
					{include file='snippets/widgets/comments.tpl' comment_ident="articles/detail/{$article->id|md5}"}
					
					{* include file='snippets/widgets/browseByCategory.tpl' *}
					
						
				</div>
			</div>			
		</div>
		
{/block}