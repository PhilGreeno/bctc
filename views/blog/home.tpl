{extends "snippets/default.tpl"}

{block name="pageName"}Blog{/block}

{block name="css"}{/block}

{block name="additionalJavascripts"}{/block}

{block name="header"}
			{include file='snippets/navBar.tpl' active='blog'}
			{include file='snippets/topBar.tpl'}
{/block}

{block name="content"}

		<div class="container-fluid">
			<div class="row-fluid">

				<div class="span3">
					
					{include file='snippets/widgets/browseByCategory.tpl' active='blog'}
					
					<h3 class="title justTitle">Comments</h3><br/>
					
					{include file='snippets/widgets/comments.tpl' comment_ident="blog"}
				
				</div>
				


				<div class="span9">  
				                 

					
					
					
					<div class="main_content detail-product">						
						<h3 class="title justTitle">News &amp; Updates from the BCTC


							<span class="pull-right hidden-phone">{include file='snippets/widgets/paginator.tpl'}</span>



						</h3>

					

					{foreach $articles as $article}
						<div class="listing-item">
							<div class="row-fluid">		
												
								<div class="span12">
									<div class="featured-item">
									
										<div class="span2">		
										
											<a href="/{$WEBPATH}blog/detail/{$article->id|md5}">
										{if $article->image1}
												<img class="img" src="/{$WEBPATH}image/article_{$article->id|md5}_1/152/85/" alt="{$article->title}" title="{$article->title}" class="img pull-left"/>
										{else}
												<img class="img" src="/{$WEBPATH}image/placeholder/152/85/" alt="{$article->title}" title="{$article->title}" class="img pull-left"/>
										{/if}
											</a>
						
										</div>
										<div class="span8">
											<h5><a href="/{$WEBPATH}blog/detail/{$article->id|md5}" class="title">{$article->title}</a> </h5>
											Published : {$article->publicationdate|date_format} by {$article->author|capitalize} in  
											{foreach from=$article->categories item=category key=i name=twat}{if not $smarty.foreach.twat.last}, {/if}
												<a href="/{$WEBPATH}blog/category/{$category|trim|replace:' ':'+'}" class="subinfo">{$category|trim}</a> 
											{/foreach}
											<hr/>
											<p>{$article->abstract|truncate:250:"...":true}</p>
											<p><a href="/{$WEBPATH}blog/detail/{$article->id|md5}" class="title">Read more.....</a> </p>
										</div>								
									</div>								
								</div>
								
							</div>
						</div>
					{foreachelse}
						<h5>There are no articles listed that match your criteria</h5>
					{/foreach}

					<div class="clearfix"></div>

					<div class="bottomPageCount">{include file='snippets/widgets/paginator.tpl'}</div>

					</div>
				</div>
				
			</div>			
		</div>


{/block}