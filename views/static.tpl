{extends "snippets/default.tpl"}

{block name="pageName"}{$page->title}{/block}

{block name="css"}{/block}

{block name="additionalJavascripts"}{/block}

{block name="header"}
			{include file='snippets/navBar.tpl' active='other'}
			{include file='snippets/topBar.tpl'}
{/block}

{block name="hero"}



{/block}

{block name="content"}

		<div class="container-fluid">
			<div class="row-fluid">
				
			
				<div class="span9">    
					<div class="main_content detail-product detail-back">

						<h3 class="title justTitle">{$page->title}</h3>						
						
						{$page->content|stripslashes|markdown}
				               
					
						</div>
					
				</div>
				
				
				<div class="span3 sideBar">

					<div class="well">
					
						{include file='snippets/widgets/browseByCategory.tpl' active=''}
						
						<h3 class="title">Event Calendar</h3>
						<div class="calendar"></div>
						{include file='snippets/widgets/eventCal.tpl'}

						<h3 class="title {*nmb*}">Comments</h3>
						{include file='snippets/widgets/comments.tpl' comment_ident="static_{$page->id|md5}"}
					</div>
					
				</div>
			</div>			
		</div>

		

{/block}