<?php
/*
 *
* -------------------------------------------------------
* CLASSNAME:        Class_User
* GENERATION DATE:  22.03.2013
* -------------------------------------------------------
* @Author - Tim Fisher - Tartan Tangerine Limited
* -------------------------------------------------------
*
*/

class Model_User extends RedBean_SimpleModel
{

    public $id;

    public $isadmin;
    public $email;
    public $password;

    public function create($isadmin, $email, $password)
    {
        $this->unbox()
        ->setAttr('isadmin',      $isadmin)
        ->setAttr('email',         $email)
        ->setAttr('password',      sha1($password));

        R::store($this);

        return $this;
    }

    public function createAdmin()
    {
        return $this->create(true, 'admin@admin.com', 't466btmm');
    }

}

?>