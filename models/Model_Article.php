
<?php
/*
 *
* -------------------------------------------------------
* CLASSNAME:        Class_Article
* GENERATION DATE:  22.03.2013
* -------------------------------------------------------
* @Author - Tim Fisher - Tartan Tangerine Limited
* -------------------------------------------------------
*
*/

class Model_Article  extends RedBean_SimpleModel
{

    public $id;

    public $merchant_id;
    public $title;
    public $author;
    public $url;
    public $publicationdate;
    public $abstract;
    public $content;
    public $keywords;
    public $images;
    public $created;
    public $suggested;

} 

?>