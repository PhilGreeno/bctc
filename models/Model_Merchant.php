
<?php
/*
 *
* -------------------------------------------------------
* CLASSNAME:        Class_Merchant
* GENERATION DATE:  22.03.2013
* -------------------------------------------------------
* @Author - Tim Fisher - Tartan Tangerine Limited
* -------------------------------------------------------
*
*/

class Model_Merchant extends RedBean_SimpleModel
{

    public $id;

    public $firstname;
    public $lastname;
    public $username;
    public $companyname;
    public $address1;
    public $address2;
    public $address3;
    public $county;
    public $postcode;
    public $country;
    public $phone;
    public $fax;
    public $mobile;
    public $url;
    public $lat;
    public $long;
    public $user_id;
    public $images;
    public $level; // Bronze, Silver, Gold
    
    // Mark the merchant as deleted
    public function delete()
    {
        $this->unbox()->setAttr('deleted', true);
        // Articles
        foreach ($this->ownArticle as $article){
            $article->setAttr('deleted', true);
        }
        // Deals
        foreach ($this->ownDeal as $deal){
            $deal->setAttr('deleted', true);
        }
        // Events
        foreach ($this->ownEvent as $event){
            $event->setAttr('deleted', true);
        }
        // Classifieds
        foreach ($this->ownClassified as $classified){
            $classified->setAttr('deleted', true);
        }
    }

} 

?>