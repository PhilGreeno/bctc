
<?php
/*
 *
* -------------------------------------------------------
* CLASSNAME:        Class_Deal
* GENERATION DATE:  22.03.2013
* -------------------------------------------------------
* @Author - Tim Fisher - Tartan Tangerine Limited
* -------------------------------------------------------
*
*/

class Model_Deal extends RedBean_SimpleModel
{

    public $id;

    public $merchant_id;
    public $titile;
    public $summary;
    public $description;
    public $conditions;
    public $dealcol;
    public $keywords;
    public $startdate;
    public $enddate;
    public $value;
    public $discount;
    public $image;

}

?>