
<?php
/*
 *
* -------------------------------------------------------
* CLASSNAME:        Class_Event
* GENERATION DATE:  22.03.2013
* -------------------------------------------------------
* @Author - Tim Fisher - Tartan Tangerine Limited
* -------------------------------------------------------
*
*/

class Model_Event extends RedBean_SimpleModel
{

    public $id;

    public $merchant_id;
    public $title;
    public $email;
    public $url;
    public $phone;
    public $contactname;
    public $address1;
    public $address2;
    public $address3;
    public $county;
    public $postcode;
    public $country;
    public $lat;
    public $long;
    public $startdate;
    public $enddate;
    public $summary;
    public $description;
    public $keywords;
    public $images;

} 

?>