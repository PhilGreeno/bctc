
<?php
/*
 *
* -------------------------------------------------------
* CLASSNAME:        Class_Classified
* GENERATION DATE:  22.03.2013
* -------------------------------------------------------
* @Author - Tim Fisher - Tartan Tangerine Limited
* -------------------------------------------------------
*
*/

class Model_Classified extends RedBean_SimpleModel
{

    public $id;

    public $merchant_id;
    public $title;
    public $summary;
    public $phone;
    public $email;
    public $price;
    public $address1;
    public $address2;
    public $address3;
    public $postcode;
    public $county;
    public $country;
    public $lat;
    public $long;
    public $keywords;
    public $image;

} 

?>