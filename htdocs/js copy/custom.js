$(document).ready(function() {

	//Sidebar toggle Function for mobiles TBC on later version.
	$('#sideToggle').click(function(){

		$('#sideToggle i').toggleClass('icon-chevron-left icon-chevron-right');

		$('#mobileSlide').animate({

			width:'toggle'

		},100);

		$('#mainContent').toggleClass('span12 span10');
		$('#mainContent').toggleClass('no-sidebar');

	});

	//Map Toggle Function
	(function() {


		//CSS Variables
		var heroHeight = $('#herowrap').height();
		var mapHeight = 500; //change this to target map container if needed.

		//Dom variables
		var sliderwrap = $('#sliderwrap');
		var mapwrap = $('#mapwrap').css('height', mapHeight);
		var toggle = $('#toggle');

		function activateMap() {
			toggle.toggleClass('map slider');

			sliderwrap.hide();
			mapwrap.show();
			heroMapInit();
				/*.css('opacity', 0)
				.show()
				.animate({
					'opacity'	: 1,
					'height'	: mapHeight
				},{ queue: false, duration: 'slow' }); */
		}

		function activateSlider() {
			toggle.toggleClass('slider map');

			sliderwrap.show();
			mapwrap.hide();
			/*	.animate({
					'height'	: heroHeight,
					'opacity'	: 0
				},
				function() {
					sliderwrap.show();
					mapwrap.hide();
				}); */
		}

		//Trigger 
		$('#toggle').toggle(function(e){
			e.preventDefault();
			return activateMap();
		}, function() {
			return activateSlider();
		});




	})();


	$('.accordion').on('show hide', function (n) {
    $(n.target).siblings('.accordion-heading').find('.accordion-toggle i').toggleClass('icon-chevron-up icon-chevron-down');
	
	});

	//Equal Heights on ".block" class
	(function($) {


			$.fn.equalHeights = function(minHeight, maxHeight) {
				tallest = (minHeight) ? minHeight : 0;
				this.each(function() {
					if($(this).height() > tallest) {
						tallest = $(this).height();
					}
				});
				if((maxHeight) && tallest > maxHeight) tallest = maxHeight;
				return this.each(function() {
					$(this).height(tallest).css("overflow","auto");
				});
			};

			$('.blocks').equalHeights();
		})(jQuery);


});





