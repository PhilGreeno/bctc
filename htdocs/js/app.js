(function(w) { 
	
    var app = function(){
    	// Constructor
    	this.init();
    };
    
    app.prototype = {

    	init : function (){
 
    	},
    	
    	sendEmail : function(){
    		var name = $('#emailModal').find('INPUT#name').val();
    		var email = $('#emailModal').find('INPUT#email').val();
    		var message = $('#emailModal').find('TEXTAREA#message').val();
    		var captcha = $('#emailModal').find('INPUT#captcha').val();
    		var ident = $('#emailModal').find('INPUT#ident').val();
    		
    		$('#emailError').hide();
    		
    		if (name == ''){
    			$('#emailError').empty().html('Please enter your name in the correct box.').show(); return;
    		}
    		
    		if (email == '' || !validateEmail(email)){
    			$('#emailError').empty().html('Email address is incorrect.').show(); return;
    		}
    		
    		if (message == ''){
    			$('#emailError').empty().html('You must enter a message.').show(); return;
    		}
    		
    		if (captcha == ''){
    			$('#emailError').empty().html('You must provide an answer to the sum.').show(); return;
    		}
    		
    		$.ajax({
				type: 'POST',
				url: 'ajax.ws/postEmail',
				data: { name: name, email: email, message: message, captcha: captcha, ident:ident },
    			 
				success:function(response){
					var data = JSON.parse(response);
					
					console.log(data);
					
					if (data.payload == -2){
						$('#emailError').empty().html('The result of the sum is incorrect - please try again.').show(); return;
					}
					$('#emailModal').modal('hide');
					alert ('Email has been sent!');
				},
				error:function(){
					alert ("There was an error, please try again");
				}
			});
    		
    		
    	}
	    
    };
    
    // Make method available globally.
    w.app = new app();
   
	function validateEmail(id) {
		var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$/;
		return emailPattern.test(id);
	}; 
   
})(window); 
