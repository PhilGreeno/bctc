<!doctype html>
<head>
	<title>Broadstone Virtual Village</title>

<style>	
body {
    background: none repeat scroll 0 0 #F9F9F9;
}
#wrapper {
    border: 3px double rgba(0, 170, 150, 0.52);
    box-shadow: 0 0 4px #999999;
    margin: 0 auto;
    overflow: hidden;
    position: relative;
    width: 830px;
    background:url('http://broadstonevillage.co.uk/img/banner2.png')top right no-repeat #fff;
    
}
.button {
    background: none repeat scroll 0 0 #F8F8F8;
    border: 1px solid #FFFFFF;
    border-radius: 4px 4px 4px 4px;
    box-shadow: 0 0 1px #999999;
    color: #C9C9C9;
    display: block;
    font-family: Helvetica,sans-serif;
    font-size: 23px;
    left: 41%;
    padding: 8px 10px;
    position: absolute;
    text-decoration: none;
    top: 70%;
}
a.button:hover {
    box-shadow: 0 0 1px #777777;
    color: #999999;
    transition: all 0.3s ease-out 0s;
}
</style>

	
	    <link rel='stylesheet' type='text/css' href='http://broadstonevillage.co.uk/css/bootstrap.min.css'>
		<link rel='stylesheet' type='text/css' href='http://broadstonevillage.co.uk/css/bootstrap-responsive.min.css'>
		<link rel='stylesheet' type='text/css' href='http://broadstonevillage.co.uk/css/main.css'>
		<link rel='stylesheet' type='text/css' href='http://broadstonevillage.co.uk/css/responsiveslides.css'>
		<link rel="stylesheet" type="text/css" href="http://broadstonevillage.co.uk/css/app.css" />
	
		
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
</head>
<body>
	<div id="wrapper" class="container">

		<div class="span8">


		<div class="brand"></div>
		
		<h1 class="text-center">Thanks for visiting...We're still adding content!</h1>
		<p class="lead text-center">Our team is working hard to add all of the merchants, their offers and other goodies to the website, so please pop back another time.</p>

		<div class="hero-unit">

			<h2>"Step In To Broadstone" Music Track! <i class="icon-heart"></i></h2>

			<p>To lift your spirits about the website not being ready just yet how about downloading an excellent track by Owen Moore titled "Step In To Broadstone"</p>

		<p><i>The song costs just 89p from Amazon and is a really catchy tune!</i></p>

		<a href="http://www.amazon.co.uk/Step-into-Broadstone/dp/B00CDP1360" class="btn btn-large btn-warning">

			<i class="icon-thumbs-up"></i> Download this catchy jingle from Amazon <i class="icon-thumbs-up"></i></a>
			<small>...Show the world you're a true Broadstone resident!</small>
		</div>




		<p class="lead text-center">admin@broadstonevillage.co.uk</p>

		</div>

	</div>




</body>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-40328774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</html>