/**
 * Written by Tim Fisher - 2013 Tartan Tangerine Limited
 */
(function(w) { 
	
    var admin = function(){
    	// Constructor
    	this.init();
    };
    
    admin.prototype = {

    	
    	init : function (){

    		/* When the document is ready we need to check the cache to see if we have a custom event theme */
    		$().ready(function(){

    		});
    	},
    	
    	deleteUser : function(id){
    		if (confirm('Deleting a user will remove that user and all associated posts from the site.. Are you sure you wish to proceed?')){
    			document.location.href = '/'+WEBPATH+'admin/user/delete/'+id;
    		}
    	},
    	
    	deleteMerchant : function(id){
    		if (confirm('Deleting a merchant will remove that user and all associated posts from the site.. Are you sure you wish to proceed?')){
    			document.location.href = '/'+WEBPATH+'admin/merchant/delete/'+id;
    		}
    	},
    	
    	deleteCategory : function(id){
    		if (confirm('Deleting a category will remove it from all assignments and has the potential to break listings. Are you sure you wish to proceed?')){
    			document.location.href = '/'+WEBPATH+'admin/category/delete/'+id;
    		}
    	},
    	
    	deleteArticle : function(id){
    		if (confirm('Deleting an article will remove it from the site. Are you sure you wish to proceed?')){
    			document.location.href = '/'+WEBPATH+'admin/article/delete/'+id;
    		}
    	},
    	
    	deleteClassified : function(id){
    		if (confirm('Deleting an classified will remove it from the site. Are you sure you wish to proceed?')){
    			document.location.href = '/'+WEBPATH+'admin/classified/delete/'+id;
    		}
    	},
    	
    	deleteEvent : function(id){
    		if (confirm('Deleting an event will remove it from the site. Are you sure you wish to proceed?')){
    			document.location.href = '/'+WEBPATH+'admin/event/delete/'+id;
    		}
    	},
    	
    	deleteDeal : function(id){
    		if (confirm('Deleting an deal will remove it from the site. Are you sure you wish to proceed?')){
    			document.location.href = '/'+WEBPATH+'admin/deal/delete/'+id;
    		}
    	}
    	
    	
	    
    };
    
    // Make method available globally.
    w.admin = new admin();
   
})(window); 
