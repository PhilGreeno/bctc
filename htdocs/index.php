<?php
/**
 * Created on 28 Aug 2012
 *
 * @author Tim Fisher
 *
 * Description: Dispatches requests
 */


ob_start("ob_gzhandler");

// Common files like autoloader, smarty etc...
include("../includes/common.php");

if (isset($_REQUEST['nuke'])){
    R::nuke();
}



/* DO NORMAL STUFF */

/*
 * Dispatch to a relevant webservices
 */
Dispatcher::route("POST */ajax.ws/@method", function($args){

	global $wsClasses;

	$response 	= new stdClass();
	$method 	= $args['method'];

	$response->payload = -1;

	if (isset($wsClasses['ajax']) && in_array($method, $wsClasses['ajax']) ){

		$class = new ajax();
		$response->payload  = $class->$method();

	}

	echo json_encode($response);
});


/*
 * Route the image loader - bypasses the main Dispatcher function
 */
Dispatcher::route("GET image/@ident/@width/@height/*", function($args){
    R::close();
    image::serveImage($args['ident'], $args['width'], $args['height'], $cache = true);
});


/*
 * Route the searches - from the search form only
 */
 Dispatcher::route("POST search", function($args){
     R::close();
     $type     = $_REQUEST['type'].'';
     $keyword  = $_REQUEST['keyword'];
     
     if ( class_exists( $type.'Controller' ) ){
         header("Location: /".WEBPATH."{$type}?keyword={$keyword}"); die;
     } else {
         header('Location: '.$_SERVER['HTTP_REFERER']); die;
     } 
     
 });
 
 // Route the static requests 
 Dispatcher::route("GET static/@ident", function($args){

     $pages = R::findAll('cms', 'WHERE active = 1 ORDER BY position ASC');
     SingleSmarty::getInstance()->assign('cmsPages', $pages);
     
     $ident = $args['ident'];
     $cont = new staticController();
     $cont->index($ident);
 });
 
 // Ensures that CMS pages are assigned to everything :)
 Dispatcher::addHook(function(){
     // Assign the CMS pages
     $pages = R::findAll('cms', 'WHERE active = 1 ORDER BY position ASC');
     SingleSmarty::getInstance()->assign('cmsPages', $pages);
 });

// If the session dies or there is an error, log it and return to the main page.
try {
	Dispatcher::run();
} catch (Exception $e){
	//error_log($e);
	//session_destroy();
	//throw new Exception($e);
	p($e); die;
//	header("Location: ?nuke"); die;
}

?>
