<?php
/**
 * blog Handling class - common functions
 * @author Tim Fisher - Tartan Tangerine Ltd 2013
 *
 */

class blogManager extends modelManager
{
    /**
     * Create the search string
     * @return string
     */
    private static function searchString(){
        // Keyword search
        if ($keyword = helpers\request('keyword')) {
            $search = "AND (UPPER(c.name) LIKE :keyword OR UPPER(a.title) LIKE :keyword OR UPPER(a.keywords) LIKE :keyword )";
        } else {
            $search = '';
        }
        return $search;
    }
    
    
    /**
     * Get Recent Articles
     * @param INT $limit
     * @return Model_Article[] <multitype:, multitype:Ambigous <RedBean_OODBBean, unknown> >
     */
    public static function featured($limit=4)
    {
        $results = (array)self::all();
        return array_slice($results, 0, $limit);
    }
    
    
    /**
     * Find all Articles, sorted by name with category data
     * @return Model_Article[] <multitype:, multitype:Ambigous <RedBean_OODBBean, unknown> >
     */
    public static function all()
    {
        $letterFilter = helpers\request('letter')?'AND UPPER(SUBSTRING(a.title FROM 1 FOR 1)) = :letter':'';
        $page = helpers\request('page')?:1;
        $offset = ($page - 1) * RESULTSLIMIT;
        $limit = "LIMIT {$offset}, ".RESULTSLIMIT;
        
        $search = self::searchString();
    
        $sql = "SELECT SQL_CALC_FOUND_ROWS a.*, m.companyname, GROUP_CONCAT(TRIM(c.name)) AS categories
                    FROM category c 
                    INNER JOIN article_category cc ON cc.category_id = c.id 
                    INNER JOIN article a ON a.id = cc.article_id
                    INNER JOIN merchant m on m.id = a.merchant_id
                    INNER JOIN user u ON u.id = m.user_id
                    WHERE !isnull(a.publicationdate)
                    AND isnull(a.deleted)
                    AND u.isadmin = 1
                    {$search}
                    {$letterFilter}
                    GROUP BY a.id
                    ORDER BY a.publicationdate DESC
                    {$limit};";
        
        $params = array();
        if (helpers\request('letter')){
            $params[':letter'] = helpers\request('letter');
        }

        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
        
        $results = self::group( R::getAll($sql, $params) );
        $count   = reset(R::getRow('SELECT FOUND_ROWS()'));
        return array('results'=>$results, 'count'=>$count);

    }
    
    
    /**
     * Find all classifieds by category name
     * @param string $categoryName
     * @return Model_Classified[] <multitype:, multitype:Ambigous <RedBean_OODBBean, unknown> >
     */
    public static function byCategory($categoryName)
    {
        $letterFilter = helpers\request('letter')?'AND UPPER(SUBSTRING(a.title FROM 1 FOR 1)) = :letter':'';
        $page = helpers\request('page')?:1;
        $offset = ($page - 1) * RESULTSLIMIT;
        $limit = "LIMIT {$offset}, ".RESULTSLIMIT;
        
        $search = self::searchString();
        
        $sql = "SELECT SQL_CALC_FOUND_ROWS a.*, m.companyname, GROUP_CONCAT(TRIM(c.name)) AS categories
                    FROM category c 
                    INNER JOIN article_category cc ON cc.category_id = c.id 
                    INNER JOIN article a ON a.id = cc.article_id
                    INNER JOIN merchant m on m.id = a.merchant_id
                    INNER JOIN user u ON u.id = m.user_id
                    WHERE !isnull(a.publicationdate)
                    AND isnull(a.deleted)
                    AND u.isadmin = 1
                    AND c.name = :categoryName
                    {$search}
                    {$letterFilter}
                    GROUP BY a.id
                    ORDER BY a.publicationdate DESC
                    {$limit};";
        
        $params = array(':categoryName' => $categoryName);
        
        if (helpers\request('letter')){
            $params[':letter'] = helpers\request('letter');
        }
        
        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }

        $results = self::group( R::getAll($sql, $params) );
        $count   = reset(R::getRow('SELECT FOUND_ROWS()'));
        return array('results'=>$results, 'count'=>$count);
    }
    
    
    /**
     * Find a classified by the  ID - title is too ambiguos
     * @param string $id - md5 of id
     * @return Model_Classified
     */
    public static function byId($id)
    {
        $sql = "SELECT a.*,  m.companyname, GROUP_CONCAT(TRIM(c.name)) AS categories
                    FROM category c 
                    INNER JOIN article_category cc ON cc.category_id = c.id 
                    INNER JOIN article a ON a.id = cc.article_id
                    INNER JOIN merchant m on m.id = a.merchant_id
                    WHERE md5(a.id) = :id
                    GROUP BY a.id";
        
        return reset( self::group(R::getAll($sql, array(':id' => $id)) ) );
    }
    
    
    /**
     * Return a the list of categories utilised by articles
     * @return Model_Category[] <multitype:, multitype:Ambigous <RedBean_OODBBean, unknown> >
     */
    public static function categories()
    {
        $search = self::searchString();
        
        $sql = "SELECT c.*, count(*) as counter
                    FROM category c
                    INNER JOIN article_category cl ON cl.category_id = c.id
                    INNER JOIN article a ON a.id = cl.article_id
                    INNER JOIN merchant m on m.id = a.merchant_id
                    INNER JOIN user u ON u.id = m.user_id
                    WHERE !isnull(a.publicationdate)
                    AND isnull(a.deleted)
                    AND u.isadmin = 1
                    {$search}
                    group by c.name
                    ORDER BY c.name
                    ASC";
        
        $params = array();
        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
        $results = R::getAll($sql, $params);
        return helpers\arrayToObject($results); 
    }
    
    /**
     * Function to return the first letter of all company names - we need this for the AZ filter
     */
    public static function azFilter()
    {
        $search = self::searchString();
        $sql = "SELECT DISTINCT UPPER(SUBSTRING(a.title FROM 1 FOR 1)) AS letter 
                        FROM article a 
                        INNER JOIN article_category ac ON ac.article_id = a.id
                        INNER JOIN category c ON c.id = ac.category_id
                        INNER JOIN merchant m on m.id = a.merchant_id
                        INNER JOIN user u ON u.id = m.user_id
                        WHERE !isnull(a.publicationdate)
                        AND isnull(a.deleted)
                        AND u.isadmin = 1
                        {$search}
                        ORDER BY letter ASC;";
        $params = array();            
        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
                    
        return self::letterFilter( R::getAll($sql, $params) );
    }
    
    public static function azFilterCategory($categoryName)
    {
        $search = self::searchString();
        $sql = "SELECT DISTINCT UPPER(SUBSTRING(a.title FROM 1 FOR 1)) AS letter
                        FROM article a
                        INNER JOIN article_category ac ON ac.article_id = a.id
                        INNER JOIN category c ON c.id = ac.category_id
                        INNER JOIN merchant m on m.id = a.merchant_id
                        INNER JOIN user u ON u.id = m.user_id
                        WHERE !isnull(a.publicationdate)
                        AND isnull(a.deleted)
                        AND u.isadmin = 1
                        AND c.name = :categoryName
                        {$search}
                        ORDER BY letter ASC;";
    
        $params = array(':categoryName' => $categoryName);

        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
        return self::letterFilter(   R::getAll($sql, $params) );
    }
    
}