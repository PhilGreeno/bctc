<?php

/**
 *
 * Image manipulation functions
 * @author Tim Fisher - Indiepath Ltd 203
 *
 */

class image {
    
    
    public static function zoom_pad($image, $new_width=100, $new_height=100){
        // Get original width and height
        $width = imagesx ($image);
        $height = imagesy ($image);
        $origin_x = 0;
        $origin_y = 0;
        
        // create a new true color image
        $canvas = imagecreatetruecolor ($new_width, $new_height);
        imagealphablending ($canvas, false);
        
        $color = imagecolorallocatealpha ($canvas, 255, 255, 255, 0);
        imagefill ($canvas, 0, 0, $color);
        
        $final_height = $height * ($new_width / $width);
        
        if ($final_height > $new_height) {
        
            $origin_x = $new_width / 2;
            $new_width = $width * ($new_height / $height);
            $origin_x = round ($origin_x - ($new_width / 2));
        
        } else {
        
            $origin_y = $new_height / 2;
            $new_height = $final_height;
            $origin_y = round ($origin_y - ($new_height / 2));
        
        }
        
        $src_x = $src_y = 0;
        $src_w = $width;
        $src_h = $height;
        
        $cmp_x = $width / $new_width;
        $cmp_y = $height / $new_height;
        
        // calculate x or y coordinate and width or height of source
        if ($cmp_x > $cmp_y) {
        
            $src_w = round ($width / $cmp_x * $cmp_y);
            $src_x = round (($width - ($width / $cmp_x * $cmp_y)) / 2);
        
        } else if ($cmp_y > $cmp_x) {
        
            $src_h = round ($height / $cmp_y * $cmp_x);
            $src_y = round (($height - ($height / $cmp_y * $cmp_x)) / 2);
        
        }
        
        imagesavealpha ($canvas, true);
        
        imagecopyresampled ($canvas, $image, $origin_x, $origin_y, $src_x, $src_y, $new_width, $new_height, $src_w, $src_h);
        
        return $canvas;
        
    }

    // gives a nice rectangular crop of an image
    // this function depends on the constrained MINIMUM function�
    // zoom crops are useful for making rectangular thumbs instantly
    // of images�
    public static function zoom_crop($img, $w, $h){
        // min resize the bastard�
        $img 		= self::c_minresize($img, $w, $h);
        $orig_w 	= imagesx($img);
        $orig_h 	= imagesy($img);
        // set where the crop should begin width and height wise..
        // /2 will be center� however.. most portrait images have the
        // content action near the top.. so that's why I use /5 for
        // vertically cropped images� adjust to fit your need..
        $start_y 	= floor(($orig_h - $h) /2);
        $start_x 	= floor(($orig_w - $w) /2);
        // now lets create a new image�
        $new_im 	= imagecreatetruecolor($w, $h);
        imagecopy($new_im, $img, 0,0, $start_x, $start_y, $w,$h);
        return $new_im;
    }

    // constrained resize.. (max value)
    // constraining to a MAXIMUM value means that we want the image
    // to be NO MORE then $w or $h�
    // so when it resizes an image� it'll constrain it to the same dimensions
    // but it will not exceed in width or height the values of $max_width or $max_height
    // http://thedevnet.com/php/snippets/zoom-crop-image/
    public static function c_resize( $img, $max_width, $max_height){
        $FullImage_width 	= imagesx ($img); // Original image width
        $FullImage_height 	= imagesy ($img); // Original image height
        // now we check for over-sized images and pare them down
        // to the dimensions we need for display purposes
        $ratio 				= ( $FullImage_width > $max_width ) ? (real)($max_width / $FullImage_width) : $FullImage_width ;
        $new_width 			= ((int)($FullImage_width * $ratio));
        $new_height 		= ((int)($FullImage_height * $ratio));
        //check for images that are still too high
        $ratio 				= ( $new_height > $max_height ) ? (real)($max_height / $new_height) : 1 ;
        $new_width 			= ((int)($new_width * $ratio)); //mid-size width
        $new_height 		= ((int)($new_height * $ratio)); //mid-size height
        $NEW_IM 			= imagecreatetruecolor( $new_width , $new_height );
        imagecopyresampled( $NEW_IM, $img,0,0, 0,0,	$new_width, $new_height, $FullImage_width, $FullImage_height );
        return $NEW_IM;
    }

    // constrained resize.. (min value)
    // constraining to a minimum value means that we want the image
    // to be NO LESS then $w or $h�
    // so when it resizes an image� it'll constrain it to the same dimensions
    // but it will not be reduced less then a certain width or certain height�
    // this is useful for a crop function
    // http://thedevnet.com/php/snippets/zoom-crop-image/
    public static function c_minresize($img, $w, $h){
        $src_w 	= imagesx($img);
        $src_h 	= imagesy($img);
        $pct_w 	= ($w / $src_w) * 100;
        $pct_h 	= ($h / $src_h) * 100;
        if ($pct_w > $pct_h){
            $new_w 	= $src_w * ($pct_w / 100);
            $new_h 	= $src_h * ($pct_w / 100);
        } else {
            $new_w 	= $src_w * ($pct_h / 100);
            $new_h 	= $src_h * ($pct_h / 100);
        }
        $new_w *= 1.05;
        $new_h *= 1.05;
        return self::c_resize( $img, $new_w, $new_h);
    }

    /**
     *
     * Constrain an Image to a Maximum Dimension (maxd) - this is all done in memory.
     * The operation is performed on a stream of image data and returns a stream in JPEG format.
     * Useful for resizing image blobs that are stored in databases.
     *
     * @author Tim Fisher - Indiepath Ltd.
     * @param string $imageData
     * @param int $maxd
     * @return string
     */
    public static function constrainImageInMemory($imageData, $maxd) {

        /* Take a string of image data, does not care if it's JPEG, GIF, PNG etc. */
        $src = imagecreatefromstring($imageData);

        if (!$src) {
            return false;
        }

        $srcw = imagesx($src);
        $srch = imagesy($src);

        /* If there is no max dimension then leave the image alone and just convert to JPEG */
        if ($maxd == null){
            $width 	= $srcw;
            $height = $srch;
        } else {

            if ($srcw < $srch) {
                $height = $maxd;
                $width 	= floor($srcw * $height / $srch);
            } else {
                $width 	= $maxd;
                $height = floor($srch * $width / $srcw);
            }
        }
        //if image is actually smaller than you want, leave small
        /*
         if ($width > $srcw && $height > $srch){
        $width 	= $srcw;
        $height = $srch;
        } */

        $thumb = imagecreatetruecolor($width, $height);

        imagecopyresampled($thumb, $src, 0, 0, 0, 0, $width, $height, imagesx($src), imagesy($src));

        /* Take the image data and convert to JPEG data */
        $imageData = self::imageToString($thumb);
        imagedestroy($thumb);
        imagedestroy($src);
        return $imageData;
    }

    public static function imageToString($image){
        ob_start();
        imagejpeg($image, null, 85);
        $image_data = ob_get_contents();
        ob_end_clean();
        return $image_data;
    }

    public static function serveImage($ident, $width, $height, $cache = null){

        header("Content-type: image/jpeg");

        $src         = HTDOCSPATH."/media/{$ident}.jpg";
        $cachePath   = HTDOCSPATH."/media/{$ident}_{$width}_{$height}.jpg";

        // Try to find the cached version of this file
        if ($cache && is_file($cachePath)){
            passthru('cat '.$cachePath); exit;
        }

        // Zoom Crop the source
        $image = image::zoom_pad(imagecreatefromjpeg($src), $width, $height);

        if ($cache){
      //      imagejpeg($image, $cachePath, 85);
        }
        imagejpeg($image, null, 85);

        exit;

    }

    public static function upload($bean, $type, $ident)
    {
        if (isset($_FILES[$ident])){

            $files = helpers\rearrange($_FILES[$ident]);

            foreach ($files as $key=>$image){

                if ($image['error'] != 0 && $image['error'] != 4){
                    helpers\addError(array('title'=>'Image upload error', 'message'=>'There is a problem with an image you have attempted to upload - please try again.'));
                    $bean->setAttr("image{$key}", false);
                } else {
                    if ($image['error'] != 4){
                        // Process Image
                        $imageData = file_get_contents($image['tmp_name']);
                        // Resize to the maximum dimension of 800px wide or tall
                        $imageData = image::constrainImageInMemory($imageData, 800);
                        // Save main image - the thumbs are created on the fly.
                        $target 	= md5($bean->id);
                        $dest 		= HTDOCSPATH."/media/{$type}_{$target}_{$key}";
                        file_put_contents($dest.'.jpg', $imageData);
                        unlink($image['tmp_name']);
                        $bean->setAttr("image{$key}", true);
                        
                        // Remove the cached files
                        foreach (glob($dest.'_*') as $filename) {
                            unlink($filename);
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

}

/*
 *
*/





/* Example: 1.
 <?
2.
// load the image
3.
$im = imagecreatefromjpeg("test.jpg");
4.
// crop the image (make it 200 pixels wide and 100 pixels high)
5.
$im = zoom_crop($img, 200, 100);
6.
// display the image
7.
imagejpeg($im);
8.
?>
*/
