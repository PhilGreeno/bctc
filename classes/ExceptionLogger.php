<?php
/**
 * Created on 5 Sep 2012
 *
 * @author Tim Fisher
 * 
 * Description: The Logger exception handler is responsible for logging uncaught exceptions to a file for debugging.
 */
 
 class ExceptionLogger implements SplObserver
{
    /**
     * Update the error_log with information about the Exception.
     *
     * @param   SplSubject  $subject   The ExceptionHandler
     * @return  bool
     */
    public function update(SplSubject $subject)
    {
        $exception = $subject->getException();
        
        $output = 'File: ' . $exception->getFile() . PHP_EOL;
        $output .= 'Line: ' . $exception->getLine() . PHP_EOL;
        $output .= 'Message: ' . PHP_EOL . $exception->getMessage() . PHP_EOL;
        $output .= 'Stack Trace:' . PHP_EOL . $exception->getTraceAsString() . PHP_EOL;
        echo "<pre>";
        echo "\n\nThe following message was sent to your default PHP error log:\n\n";
        echo $output;
        
        return error_log($output);
    }
}
 
?>
