<?php
/**
 * Created on 5 Sep 2012
 *
 * @author Tim Fisher
 * 
 * Description: The Mailer exception handler is responsible for mailing uncaught exceptions to an administrator for notifications.
 */
 
 class ExceptionMailer Implements SplObserver
{
    
    /**
    * Mail the sysadmin with Exception information.
    *
    * @param    SplSubject $subject   The ExceptionHandler
    * @return   bool
    */
    public function update(SplSubject $subject)
    {
    	return;
        $exception = $subject->getException();
        
        // perhaps emailer also would like to know the server in question
     //   $output = 'Server: ' . $_SERVER['HOST_NAME'] . PHP_EOL;
        $output = 'File: ' . $exception->getFile() . PHP_EOL;
        $output .= 'Line: ' . $exception->getLine() . PHP_EOL;
        $output .= 'Message: ' . PHP_EOL . $exception->getMessage() . PHP_EOL;
        $output .= 'Stack Trace:' . PHP_EOL . $exception->getTraceAsString() . PHP_EOL;
    
        $headers = 'From: webmaster@yourdomain.com' . "\r\n" .
                    'Reply-To: webmaster@yourdomain.com' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();

        echo "\n\nThe following email (would be) sent to your webmaster@yourdomain.com:\n\n";
        echo $output;

        //return mail('webmaster@yourdomain.com', 'Exception Thrown', $output, $headers);
    }
    
}
?>
