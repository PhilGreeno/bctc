<?php
/**
 * Deal Handling class - common functions
 * @author Tim Fisher - Tartan Tangerine Ltd 2013
 *
 */

class dealManager extends modelManager
{
    /**
     * Create the search string
     * @return string
     */
    private static function searchString(){
        // Keyword search
        if ($keyword = helpers\request('keyword')) {
            $search = "AND (UPPER(c.name) LIKE :keyword OR UPPER(d.title) LIKE :keyword OR UPPER(d.keywords) LIKE :keyword )";
        } else {
            $search = '';
        }
        return $search;
    }
    
    public static function featured($limit=4){
        $sql = "SELECT d.*, m.companyname, m.address1, m.address2, m.address3, m.county, m.postcode,m.country, m.phone, m.url, GROUP_CONCAT(TRIM(c.name)) AS categories
                    FROM category c 
                    INNER JOIN category_deal cd ON cd.category_id = c.id 
                    INNER JOIN deal d ON d.id = cd.deal_id
                    INNER JOIN merchant m on m.id = d.merchant_id
                    INNER JOIN weights w ON m.level = w.level
                    WHERE !isnull(d.publicationdate)
                    AND isnull(d.deleted)
                    AND d.startdate < NOW()
                    AND d.enddate > NOW()
                    GROUP BY d.id
                    ORDER BY w.weight * RAND() DESC
                    LIMIT {$limit}";
    
        return self::group( R::getAll($sql) );
    }
    
    /**
     * Find all Deals, sorted by name with category data
     * @return Model_Deal[] <multitype:, multitype:Ambigous <RedBean_OODBBean, unknown> >
     */
    public static function all()
    {
        $letterFilter = helpers\request('letter')?'AND UPPER(SUBSTRING(d.title FROM 1 FOR 1)) = :letter':'';
        $page = helpers\request('page')?:1;
        $offset = ($page - 1) * RESULTSLIMIT;
        $limit = "LIMIT {$offset}, ".RESULTSLIMIT;
        
        $search = self::searchString();
        
        $sql = "SELECT SQL_CALC_FOUND_ROWS d.*, w.weight, m.companyname, m.address1, m.address2, m.address3, m.county, m.postcode,m.country, m.phone, m.url, GROUP_CONCAT(TRIM(c.name)) AS categories
                    FROM category c 
                    INNER JOIN category_deal cd ON cd.category_id = c.id 
                    INNER JOIN deal d ON d.id = cd.deal_id
                    INNER JOIN merchant m on m.id = d.merchant_id
                    INNER JOIN weights w ON m.level = w.level
                    WHERE !isnull(d.publicationdate)
                    AND isnull(d.deleted)
                    AND d.startdate < NOW()
                    AND d.enddate > NOW()
                    {$search}
                    {$letterFilter}
                    GROUP BY d.id
                    ORDER BY d.enddate ASC
                    {$limit};";
        
        $params = array();
        if (helpers\request('letter')){
            $params[':letter'] = helpers\request('letter');
        }

        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
        
        $results = self::group( R::getAll($sql, $params) );
        $count   = reset(R::getRow('SELECT FOUND_ROWS()'));
        return array('results'=>$results, 'count'=>$count);
    }
    
    
    /**
     * Find all deals by category name
     * @param string $categoryName
     * @return Model_Deal[] <multitype:, multitype:Ambigous <RedBean_OODBBean, unknown> >
     */
    public static function byCategory($categoryName)
    {
        $letterFilter = helpers\request('letter')?'AND UPPER(SUBSTRING(d.title FROM 1 FOR 1)) = :letter':'';
        $page = helpers\request('page')?:1;
        $offset = ($page - 1) * RESULTSLIMIT;
        $limit = "LIMIT {$offset}, ".RESULTSLIMIT;
        
        $search = self::searchString();
        
        $sql = "SELECT SQL_CALC_FOUND_ROWS d.*, w.weight, m.companyname, m.address1, m.address2, m.address3, m.county, m.postcode,m.country, m.phone, m.url, GROUP_CONCAT(TRIM(c.name)) AS categories
                    FROM category c 
                    INNER JOIN category_deal cd ON cd.category_id = c.id 
                    INNER JOIN deal d ON d.id = cd.deal_id
                    INNER JOIN merchant m on m.id = d.merchant_id
                    INNER JOIN weights w ON m.level = w.level
                    WHERE !isnull(d.publicationdate)
                    AND isnull(d.deleted)
                    AND c.name = :categoryName
                    AND d.startdate < NOW()
                    AND d.enddate > NOW()
                    {$search}
                    {$letterFilter}
                    GROUP BY d.id
                    ORDER BY d.enddate DESC
                    {$limit};";

        $params = array(':categoryName' => $categoryName);
        
        if (helpers\request('letter')){
            $params[':letter'] = helpers\request('letter');
        }
        
        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }

        $results = self::group( R::getAll($sql, $params) );
        $count   = reset(R::getRow('SELECT FOUND_ROWS()'));
        return array('results'=>$results, 'count'=>$count);
    }
    
    /**
     * Find all deals by merchant id
     * @param int $id
     * @return Model_Deal[] <multitype:, multitype:Ambigous <RedBean_OODBBean, unknown> >
     */
    public static function byMerchant($id)
    {
        $sql = "SELECT d.*, w.weight, m.companyname, m.address1, m.address2, m.address3, m.county, m.postcode,m.country, m.phone, m.url, GROUP_CONCAT(TRIM(c.name)) AS categories
                    FROM category c
                    INNER JOIN category_deal cd ON cd.category_id = c.id
                    INNER JOIN deal d ON d.id = cd.deal_id
                    INNER JOIN merchant m on m.id = d.merchant_id
                    INNER JOIN weights w ON m.level = w.level
                    WHERE !isnull(d.publicationdate)
                    AND isnull(d.deleted)
                    AND m.id = :id
                    AND d.startdate < NOW()
                    AND d.enddate > NOW()
                    GROUP BY d.id
                    ORDER BY d.enddate DESC;";

        return self::group( R::getAll($sql, array(':id' => $id)) );
    }
    
    
    /**
     * Find a deal by the  ID - title is too ambiguos
     * @param string $id - md5 of id
     * @return Model_Deal
     */
    public static function byId($id)
    {
        $sql = "SELECT d.*, m.companyname, m.address1, m.address2, m.address3, m.county, m.postcode,m.country, m.phone, m.url, m.firstname, m.lastname, m.lat, m.long, GROUP_CONCAT(TRIM(c.name)) AS categories
                    FROM category c 
                    INNER JOIN category_deal cd ON cd.category_id = c.id 
                    INNER JOIN deal d ON d.id = cd.deal_id
                    INNER JOIN merchant m on m.id = d.merchant_id
                    WHERE md5(d.id) = :id
                    GROUP BY d.id";
        
        return reset( self::group(R::getAll($sql, array(':id' => $id)) ) );
    }
    
    
    /**
     * Return a the list of categories utilised by categories
     * @return Model_Category[] <multitype:, multitype:Ambigous <RedBean_OODBBean, unknown> >
     */
    public static function categories()
    {
        $search = self::searchString();
        
        $sql = "SELECT c.*, count(*) as counter
                    FROM category c
                    INNER JOIN category_deal cd ON cd.category_id = c.id
                    INNER JOIN deal d ON d.id = cd.deal_id
                    WHERE !isnull(d.publicationdate)
                    AND isnull(d.deleted)
                    AND d.startdate < NOW()
                    AND d.enddate > NOW()
                    {$search}
                    group by c.name
                    ORDER BY c.name
                    ASC";
        
        $params = array();
        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
        $results = R::getAll($sql, $params);
        return helpers\arrayToObject($results);  
    }
    
    /**
     * Function to return the first letter of all deal titles - we need this for the AZ filter
     */
    public static function azFilter()
    {
        $search = self::searchString();
        
        $sql = "SELECT DISTINCT UPPER(SUBSTRING(d.title FROM 1 FOR 1)) AS letter 
                        FROM deal d
                        INNER JOIN category_deal cm ON cm.deal_id = d.id
                        INNER JOIN category c ON c.id = cm.category_id
                        WHERE d.startdate < NOW()
                        AND d.enddate > NOW()
                        AND !isnull(d.publicationdate)
                        AND isnull(d.deleted)
                        {$search}
                        ORDER BY letter ASC;";
        $params = array();            
        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
                    
        return self::letterFilter( R::getAll($sql, $params) );
    }
    
    public static function azFilterCategory($categoryName)
    {
        $search = self::searchString();
        
        $sql = "SELECT DISTINCT UPPER(SUBSTRING(d.title FROM 1 FOR 1)) AS letter
                        FROM deal d
                        INNER JOIN category_deal cm ON cm.deal_id = d.id
                        INNER JOIN category c ON c.id = cm.category_id
                        WHERE c.name = :categoryName
                        AND isnull(d.deleted)
                        AND d.startdate < NOW()
                        AND d.enddate > NOW()
                        AND !isnull(d.publicationdate)
                        {$search}
                        ORDER BY letter ASC;";
    
        $params = array(':categoryName' => $categoryName);

        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
        return self::letterFilter(   R::getAll($sql, $params) );
    }
    
}