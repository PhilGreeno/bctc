<?php
/**
 * Simple captcha class
 * @author Tim Fisher - Tartan Tangerine Ltd 2013
 *
 */

class captcha {
    
    public static function init()
    {
        $test = new stdClass();
        $test->digit1 = rand(1,10);
        $test->digit2 = rand(1,50);
        $test->sum = $test->digit1 + $test->digit2;
        $_SESSION['captcha'] = $test;
    }
    
    public static function check($answer){
        return $_SESSION['captcha']->sum == $answer?true:false;
    }
    
    
}