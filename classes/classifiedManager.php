<?php
/**
 * Classified Handling class - common functions
 * @author Tim Fisher - Tartan Tangerine Ltd 2013
 *
 */

class classifiedManager extends modelManager
{
    /**
     * Create the search string
     * @return string
     */
    private static function searchString(){
        // Keyword search
        if ($keyword = helpers\request('keyword')) {
            $search = "AND (UPPER(c.name) LIKE :keyword OR UPPER(cl.title) LIKE :keyword OR UPPER(cl.keywords) LIKE :keyword )";
        } else {
            $search = '';
        }
        return $search;
    }
    
    public static function featured($limit=4){
        $sql = "SELECT cl.*, m.companyname, GROUP_CONCAT(TRIM(c.name)) AS categories
                    FROM category c 
                    INNER JOIN category_classified cc ON cc.category_id = c.id 
                    INNER JOIN classified cl ON cl.id = cc.classified_id
                    INNER JOIN merchant m on m.id = cl.merchant_id
                    INNER JOIN weights w ON m.level = w.level
                    WHERE !isnull(cl.publicationdate)
                    AND isnull(cl.deleted)
                    GROUP BY cl.id
                    ORDER BY w.weight * RAND() DESC
                    LIMIT {$limit}";
    
        return self::group( R::getAll($sql) );
    }
    
    
    /**
     * Find all classifieds, sorted by name with category data
     * @return Model_Classified[] <multitype:, multitype:Ambigous <RedBean_OODBBean, unknown> >
     */
    public static function all()
    {
        $letterFilter = helpers\request('letter')?'AND UPPER(SUBSTRING(cl.title FROM 1 FOR 1)) = :letter':'';
        $page = helpers\request('page')?:1;
        $offset = ($page - 1) * RESULTSLIMIT;
        $limit = "LIMIT {$offset}, ".RESULTSLIMIT;
        
        $search = self::searchString();
    
        $sql = "SELECT SQL_CALC_FOUND_ROWS cl.*, w.weight, m.companyname, GROUP_CONCAT(TRIM(c.name)) AS categories
                    FROM category c 
                    INNER JOIN category_classified cc ON cc.category_id = c.id 
                    INNER JOIN classified cl ON cl.id = cc.classified_id
                    INNER JOIN merchant m on m.id = cl.merchant_id
                    INNER JOIN weights w ON m.level = w.level
                    WHERE !isnull(cl.publicationdate)
                    AND isnull(cl.deleted)
                    {$search}
                    {$letterFilter}
                    GROUP BY cl.id
                    ORDER BY cl.publicationdate DESC
                    {$limit};";
        
        $params = array();
        if (helpers\request('letter')){
            $params[':letter'] = helpers\request('letter');
        }

        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
        
        $results = self::group( R::getAll($sql, $params) );
        $count   = reset(R::getRow('SELECT FOUND_ROWS()'));
        return array('results'=>$results, 'count'=>$count);
    }
    
    
    /**
     * Find all classifieds by category name
     * @param string $categoryName
     * @return Model_Classified[] <multitype:, multitype:Ambigous <RedBean_OODBBean, unknown> >
     */
    public static function byCategory($categoryName)
    {
        $letterFilter = helpers\request('letter')?'AND UPPER(SUBSTRING(cl.title FROM 1 FOR 1)) = :letter':'';
        $page = helpers\request('page')?:1;
        $offset = ($page - 1) * RESULTSLIMIT;
        $limit = "LIMIT {$offset}, ".RESULTSLIMIT;
        
        $search = self::searchString();
        
        $sql = "SELECT SQL_CALC_FOUND_ROWS cl.*, w.weight, m.companyname, GROUP_CONCAT(TRIM(c.name)) AS categories
                    FROM category c 
                    INNER JOIN category_classified cc ON cc.category_id = c.id 
                    INNER JOIN classified cl ON cl.id = cc.classified_id
                    INNER JOIN merchant m on m.id = cl.merchant_id
                    INNER JOIN weights w ON m.level = w.level
                    WHERE !isnull(cl.publicationdate)
                    AND isnull(cl.deleted)
                    AND c.name = :categoryName
                    {$search}
                    {$letterFilter}
                    GROUP BY cl.id
                    ORDER BY cl.publicationdate DESC
                    {$limit};";
        
        $params = array(':categoryName' => $categoryName);
        
        if (helpers\request('letter')){
            $params[':letter'] = helpers\request('letter');
        }
        
        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }

        $results = self::group( R::getAll($sql, $params) );
        $count   = reset(R::getRow('SELECT FOUND_ROWS()'));
        return array('results'=>$results, 'count'=>$count);
    }
    
    
    /**
     * Find a classified by the  ID - title is too ambiguos
     * @param string $id - md5 of id
     * @return Model_Classified
     */
    public static function byId($id)
    {
        $sql = "SELECT cl.*,  m.companyname, GROUP_CONCAT(TRIM(c.name)) AS categories
                    FROM category c 
                    INNER JOIN category_classified cc ON cc.category_id = c.id 
                    INNER JOIN classified cl ON cl.id = cc.classified_id
                    INNER JOIN merchant m on m.id = cl.merchant_id
                    WHERE md5(cl.id) = :id
                        GROUP BY cl.id";
        
        return reset( self::group(R::getAll($sql, array(':id' => $id)) ) );
    }
    
    
    /**
     * Return a the list of categories utilised by categories
     * @return Model_Category[] <multitype:, multitype:Ambigous <RedBean_OODBBean, unknown> >
     */
    public static function categories()
    {
        $search = self::searchString();
        
        $sql = "SELECT c.*, count(*) as counter
                    FROM category c
                    INNER JOIN category_classified cc ON cc.category_id = c.id
                    INNER JOIN classified cl ON cl.id = cc.classified_id
                    WHERE !isnull(cl.publicationdate)
                    AND isnull(cl.deleted)
                    {$search}
                    group by c.name
                    ORDER BY c.name
                    ASC";
        
        $params = array();
        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
        $results = R::getAll($sql, $params);
        return helpers\arrayToObject($results);  
    }
    
    /**
     * Function to return the first letter of all classified titles - we need this for the AZ filter
     */
    public static function azFilter()
    {
        $search = self::searchString();
        
        $sql = "SELECT DISTINCT UPPER(SUBSTRING(cl.title FROM 1 FOR 1)) AS letter 
                    FROM classified cl
                    INNER JOIN category_classified cc ON cc.classified_id = cl.id
                    INNER JOIN category c ON c.id = cc.category_id
                    WHERE !isnull(cl.publicationdate)
                    AND isnull(cl.deleted)
                    {$search}
                    ORDER BY letter ASC;";
        
        $params = array();            
        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
                    
        return self::letterFilter( R::getAll($sql, $params) );
    }
    
    public static function azFilterCategory($categoryName)
    {
        $search = self::searchString();
        
        $sql = "SELECT DISTINCT UPPER(SUBSTRING(cl.title FROM 1 FOR 1)) AS letter
                        FROM classified cl
                        INNER JOIN category_classified cc ON cc.classified_id = cl.id
                        INNER JOIN category c ON c.id = cc.category_id
                        WHERE c.name = :categoryName
                        AND isnull(cl.deleted)
                        AND !isnull(cl.publicationdate)
                        {$search}
                        ORDER BY letter ASC;";
        
        $params = array(':categoryName' => $categoryName);

        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
        return self::letterFilter(   R::getAll($sql, $params) );
    }
    
}