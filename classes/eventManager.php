<?php
/**
 * Event Handling class - common functions
 * @author Tim Fisher - Tartan Tangerine Ltd 2013
 *
 */

class eventManager extends modelManager
{
    /**
     * Create the search string
     * @return string
     */
    private static function searchString(){
        // Keyword search
        if ($keyword = helpers\request('keyword')) {
            $search = "AND (UPPER(c.name) LIKE :keyword OR UPPER(d.title) LIKE :keyword OR UPPER(d.keywords) LIKE :keyword )";
        } else {
            $search = '';
        }
        return $search;
    }
    
    
    public static function betweenDates($start, $end)
    {
        $start = $start->format('Y-m-d H:i:s');
        $end = $end->format('Y-m-d H:i:s');
        
        $letterFilter = helpers\request('letter')?'AND UPPER(SUBSTRING(d.title FROM 1 FOR 1)) = :letter':'';
        $page = helpers\request('page')?:1;
        $offset = ($page - 1) * RESULTSLIMIT;
        $limit = "LIMIT {$offset}, ".RESULTSLIMIT;
        
        $search = self::searchString();

        $sql = "SELECT SQL_CALC_FOUND_ROWS d.*, m.companyname, GROUP_CONCAT(TRIM(c.name)) AS categories
                    FROM category c
                    INNER JOIN category_event cd ON cd.category_id = c.id
                    INNER JOIN event d ON d.id = cd.event_id
                    INNER JOIN merchant m on m.id = d.merchant_id
                    WHERE !isnull(d.publicationdate)
                    AND isnull(d.deleted)
                    AND d.enddate > '{$start}'
                    AND d.enddate < '{$end}'
                    {$search}
                    {$letterFilter}
                    GROUP BY d.id
                    ORDER BY d.startdate ASC
                    {$limit};";
                    
        $params = array();
        if (helpers\request('letter')){
            $params[':letter'] = helpers\request('letter');
        }

        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
        
        $results = self::group( R::getAll($sql, $params) );
        $count   = reset(R::getRow('SELECT FOUND_ROWS()'));
        return array('results'=>$results, 'count'=>$count);
     
    }
    
    
    /**
     * Find all deals by category name
     * @param string $categoryName
     * @return Model_Deal[] <multitype:, multitype:Ambigous <RedBean_OODBBean, unknown> >
     */
    public static function betweenDatesCategory($categoryName, $start, $end)
    {
        $start = $start->format('Y-m-d H:i:s');
        $end = $end->format('Y-m-d H:i:s');
        
        $letterFilter = helpers\request('letter')?'AND UPPER(SUBSTRING(d.title FROM 1 FOR 1)) = :letter':'';
        $page = helpers\request('page')?:1;
        $offset = ($page - 1) * RESULTSLIMIT;
        $limit = "LIMIT {$offset}, ".RESULTSLIMIT;
        
        $search = self::searchString();

        $sql = "SELECT SQL_CALC_FOUND_ROWS d.*,  m.companyname, GROUP_CONCAT(TRIM(c.name)) AS categories
                    FROM category c 
                    INNER JOIN category_event cd ON cd.category_id = c.id 
                    INNER JOIN event d ON d.id = cd.event_id
                    INNER JOIN merchant m on m.id = d.merchant_id
                    WHERE !isnull(d.publicationdate)
                    AND isnull(d.deleted)
                    AND c.name = :categoryName
                    AND d.startdate > '{$start}'
                    AND d.enddate < '{$end}'
                    {$search}
                    {$letterFilter}
                    GROUP BY d.id
                    ORDER BY d.startdate ASC
                    {$limit};";
                    
        $params = array(':categoryName' => $categoryName);
        
        if (helpers\request('letter')){
            $params[':letter'] = helpers\request('letter');
        }
        
        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }

        $results = self::group( R::getAll($sql, $params) );
        $count   = reset(R::getRow('SELECT FOUND_ROWS()'));
        return array('results'=>$results, 'count'=>$count);
       
    }
    
    /**
     * Find all deals by merchant id
     * @param int $id
     * @return Model_Deal[] <multitype:, multitype:Ambigous <RedBean_OODBBean, unknown> >
     */
  /*  public static function byMerchant($id)
    {
        $sql = "SELECT d.*, c.id as cat_id, c.name, w.weight, m.companyname, m.address1, m.address2, m.address3, m.county, m.postcode,m.country, m.phone, m.url
                    FROM category c
                    INNER JOIN category_deal cd ON cd.category_id = c.id
                    INNER JOIN deal d ON d.id = cd.deal_id
                    INNER JOIN merchant m on m.id = d.merchant_id
                    INNER JOIN weights w ON m.level = w.level
                    WHERE !isnull(d.publicationdate)
                    AND m.id = :id
                    AND d.startdate < NOW()
                    AND d.enddate > NOW()
                    ORDER BY d.publicationdate DESC;";

        return self::group( R::getAll($sql, array(':id' => $id)) );
    } */
    
    
    /**
     * Find a deal by the  ID - title is too ambiguos
     * @param string $id - md5 of id
     * @return Model_Deal
     */
    public static function byId($id)
    {
        $sql = "SELECT d.*, m.companyname, m.address1, m.address2, m.address3, m.county, m.postcode,m.country, m.firstname, m.lastname, GROUP_CONCAT(TRIM(c.name)) AS categories
                    FROM category c 
                    INNER JOIN category_event cd ON cd.category_id = c.id 
                    INNER JOIN event d ON d.id = cd.event_id
                    INNER JOIN merchant m on m.id = d.merchant_id
                    WHERE md5(d.id) = :id
                    GROUP BY d.id";
        
        return reset( self::group(R::getAll($sql, array(':id' => $id)) ) );
    }
    
    
    /**
     * Return a the list of categories utilised by categories
     * @return Model_Category[] <multitype:, multitype:Ambigous <RedBean_OODBBean, unknown> >
     */
    public static function categories($start, $end)
    {
        $start = $start->format('Y-m-d H:i:s');
        $end = $end->format('Y-m-d H:i:s');
        $search = self::searchString();
        
        $sql = "SELECT c.*, count(*) as counter
                    FROM category c
                    INNER JOIN category_event cl ON cl.category_id = c.id
                    INNER JOIN event d on d.id = cl.event_id
                    WHERE d.startdate > '{$start}'
                    AND d.enddate < '{$end}'
                    AND !isnull(d.publicationdate)
                    AND isnull(d.deleted)
                    {$search}
                    group by c.name
                    ORDER BY c.name
                    ASC";
        
        $params = array();
        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
        $results = R::getAll($sql, $params);
        return helpers\arrayToObject($results);
    }
    
    /**
     * Function to return the first letter of all event titles - we need this for the AZ filter
     */
    public static function azFilter($start, $end)
    {
        $start = $start->format('Y-m-d H:i:s');
        $end = $end->format('Y-m-d H:i:s');
        $search = self::searchString();
        
        $sql = "SELECT DISTINCT UPPER(SUBSTRING(d.title FROM 1 FOR 1)) AS letter 
                        FROM event d
                        INNER JOIN category_event cm ON cm.event_id = d.id
                        INNER JOIN category c ON c.id = cm.category_id
                        WHERE !isnull(d.publicationdate)
                        AND isnull(d.deleted)
                        {$search}
                        AND startdate > '{$start}'
                        AND enddate < '{$end}'
                        ORDER BY letter ASC;";

        $params = array();            
        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
                    
        return self::letterFilter( R::getAll($sql, $params) );
    }
    
    public static function azFilterCategory($categoryName, $start, $end)
    {
        $start = $start->format('Y-m-d H:i:s');
        $end = $end->format('Y-m-d H:i:s');
        $search = self::searchString();
        
        $sql = "SELECT DISTINCT UPPER(SUBSTRING(d.title FROM 1 FOR 1)) AS letter
                        FROM event d
                        INNER JOIN category_event cm ON cm.event_id = d.id
                        INNER JOIN category c ON c.id = cm.category_id
                        
                        WHERE c.name = :categoryName
                        AND isnull(d.deleted)
                        {$search}
                        AND d.startdate > '{$start}'
                        AND d.enddate < '{$end}'
                        AND !isnull(d.publicationdate) 
                        ORDER BY letter ASC;";
        
        $params = array(':categoryName' => $categoryName);

        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
        return self::letterFilter(   R::getAll($sql, $params) );
     }
    
}