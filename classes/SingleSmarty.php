<?php
/**
 * Created on 29 Aug 2012
 *
 * @author Tim Fisher
 * 
 * Description: Extends Smarty in a singleton pattern
 */
 
class SingleSmarty extends Smarty
{
	private static $_instance = NULL;

	

	//a not public constructor
	private function SingleSmarty($options=null){
		
		parent::__construct($options);
		
	}

	static function getInstance($options=null){
		if ( SingleSmarty::$_instance == NULL ){
			
			$options = array(
				'compile_dir'  => ROOTPATH.'/libraries/temp/',
				'cache_dir'    => ROOTPATH.'/libraries/temp/',
			//	'debugging'=>true,
			);
			
			SingleSmarty::$_instance = new SingleSmarty($options);
			
			foreach($options as $name => $value){
				SingleSmarty::$_instance->$name = $value;
	    	}
		}
     	return SingleSmarty::$_instance;
	}
	
	public function assign($tpl_var, $value=null){
	//	p($value);
		try {
			parent::assign($tpl_var, $value);
		}	
		catch (Exception $e){
			p($e);
		}	
	}
	
} 
?>
