<?php
/**
 * Created on 29 Aug 2012
 *
 * @author Tim Fisher
 * 
 * Description: 
 */
 
class View {
	
	protected $engine;
	public $content = null;

	private function __construct() {}

	public function getEngine(){
	 	return $this->engine = SingleSmarty::getInstance();
	}
	
	// proxy method calls and accessors not directly defined on 
	// the View to the Smarty instance
	
	public function __get($key){
		return $this->engine->$key;
	}
	
	public function __set($key, $value){
		$this->engine->$key = $value;
		return $this;
	}
	
	public function __call($method, $args){
		if(is_callable(array($this->engine, $method))){
	    	return call_user_func_array(array($this->engine, $method));
		}
	}
	
	public function render($view){
	    // Assign the view.
	    self::getEngine()->assign("view", $view);
	    // render the entire smarty instance
	    self::getEngine()->display("{$view}.tpl");
	}
}
 
?>
