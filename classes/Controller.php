<?php
/**
 *
 * @author timfisher
 *
 */
class Controller {

    public function wrap_call_user_func_array($c, $a, $p) {
        switch(count($p)) {
            case 0: $c->{$a}(); break;
            case 1: $c->{$a}($p[0]); break;
            case 2: $c->{$a}($p[0], $p[1]); break;
            case 3: $c->{$a}($p[0], $p[1], $p[2]); break;
            case 4: $c->{$a}($p[0], $p[1], $p[2], $p[3]); break;
            case 5: $c->{$a}($p[0], $p[1], $p[2], $p[3], $p[4]); break;
            default: call_user_func_array(array($c, $a), $p);  break;
        }
    }


}