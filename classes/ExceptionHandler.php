<?php
/**
 * Created on 5 Sep 2012
 *
 * @author Tim Fisher
 * 
 * Description: The ExceptionHandler class sends uncaught exception messages to the proper handlers.  
 * This is done using the Observer pattern, and SplObserver/SplSubject.
 */
 
class ExceptionHandler implements SplSubject{
    /**
     * An array of SplObserver objects to notify of Exceptions.
     *
     * @var array
     */
    private $_observers = array();

    /**
     * The uncaught Exception that needs to be handled.
     *
     * @var Exception
     */
    protected $_exception;

    /**
     * Constructor method for ExceptionHandler.
     *
     * @return ExceptionHandler
     */
    function __construct() { }

    /**
     * A custom method for returning the exception.
     *
     * @access  public
     * @return  Exception
     */
    public function getException() {
        return $this->_exception;
    }

    /**
     * Attaches an SplObserver to the ExceptionHandler to be notified when an
     * uncaught Exception is thrown.
     *
     * @access  public
     * @param   SplObserver        The observer to attach
     * @return  void
     */
    public function attach(SplObserver $obs) {
        $id = spl_object_hash($obs);
        $this->_observers[$id] = $obs;
    }

    /**
     * Detaches the SplObserver from the ExceptionHandler, so it will no longer
     * be notified when an uncaught Exception is thrown.
     * 
     * @access  public
     * @param   SplObserver        The observer to detach
     * @return  void
     */
    public function detach(SplObserver $obs) {
        $id = spl_object_hash($obs);
        unset($this->_observers[$id]);
    }

    /**
     * Notify all observers of the uncaught Exception so they can handle it as
     * needed.
     *
     * @access  public
     * @return  void
     */
    public function notify(){
        foreach($this->_observers as $obs) {
            $obs->update($this);
        }
    }

    /**
     * This is the method that should be set as the default Exception handler by
     * the calling code.
     *
     * @access  public
     * @return  void
     */
    public function handle(Exception $e){
        $this->_exception = $e;
        $this->notify();
    }
}
?>
