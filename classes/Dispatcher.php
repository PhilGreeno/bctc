<?php
/**
 * Created on 28 Aug 2012
 *
 * @author Tim Fisher
 *
 * Description: Dispatcher allowing for simple and complex routes
 */

class Dispatcher{

    protected static $vars;
    protected static $hooks = array();

    /**
     * Create a new route and add it to the routes array.
    */
    public static function route($pattern, $funcs) {
        list($methods,$uri) = preg_split('/\s+/',$pattern,2,PREG_SPLIT_NO_EMPTY);
        foreach (self::split($methods) as $method){
            // Use pattern and HTTP methods as route indexes
            self::$vars['ROUTES'][$uri][strtoupper($method)] = $funcs;
        }
    }

    /**
     * Attempt to match the request URI with the routes, if no defined route then pass to dispatcher
     */
    static function run() {

        if (count(self::$vars['ROUTES']) != 0){
            	
            // Detailed routes get matched first
            krsort(self::$vars['ROUTES']);
            	
            foreach (self::$vars['ROUTES'] as $uri=>$route) {

                $req = preg_replace('/^'.preg_quote('/'.WEBPATH,'/').'\b(.+)/i','\1',rawurldecode($_SERVER['REQUEST_URI']));

                if (!preg_match('/^'.preg_replace('/(?:\\\{\\\{)?@(\w+\b)(?:\\\}\\\})?/','(?P<\1>[^\/&\?]+)',str_replace('\*','(.*)',preg_quote($uri,'/'))).'\/?(?:\?.*)?$/ium',$req,$args)){
                    continue;
                }
                	
                // Is wildcard?
                $wild = is_int(strpos($uri,'*'));

                // Inspect each defined route
                foreach ($route as $method=>$proc) {

                    if (!preg_match('/HEAD|'.$method.'/', $_SERVER['REQUEST_METHOD'])){
                        continue;
                    }
                    	
                    // Save named uri captures
                    if (!$wild){
                        foreach (array_keys($args) as $key){
                            // Remove non-zero indexed elements
                            if (is_numeric($key) && $key){
                                unset($args[$key]);
                            }
                        }
                    }
                    
                    // If the request methods match then run the function and pass the args
                    if ($_SERVER['REQUEST_METHOD'] == $method) {
                        return $proc($args);
                    }
                }
            }
        }
        // If we've ended up here then we must try to dispatch
        self::dispatch();
    }

    /**
     * Dispatch the request to the correct Controller/Method
     */
    public static function dispatch() {

        // pre-process the url
        $url = $_SERVER['REQUEST_URI'];
        if (strpos($url, '?')){
            $url = substr($url, 0, strpos($url, '?'));
        }
        $url = str_replace(WEBPATH, "", $url);
        $url = explode('/', trim($url, '/'));

        // get controller name
        $controller = !empty($url[0]) ? $url[0] . 'Controller' : 'DefaultController';
        // get method name of controller
        $method = !empty($url[1]) ? $url[1] : 'index';
        // get argument passed in to the method or array of remaining args
        $arg = !empty($url[2]) ? array_slice($url, 2) : NULL;

        // Run hooks, if there are any
        array_walk(self::$hooks, function($func) use ($controller, $method, $arg){
            $func($controller, $method, $arg);
        });

        // create controller instance and call the specified method
        $cont = new $controller;
       // $cont->$method($arg);
        //call_user_func_array($cont->$method(), array($arg));
        $cont->wrap_call_user_func_array($cont, $method, $arg);
    }

    /**
     * Utility function to split and trim
     */
    static function split($str) {
        return array_map('trim', preg_split('/[|;,]/',$str,0,PREG_SPLIT_NO_EMPTY));
    }

    static function addHook($function){
        self::$hooks[] = $function;
    }
}

?>
