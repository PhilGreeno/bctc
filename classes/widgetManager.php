<?php

/**
 * This class manages the widget assignments 
 * 
 * @author Tim Fisher - Tartan Tangerine Limited 2013
 *
 */

class widgetManager {
    
    public static function browseByCategory()
    {
        // Assignments for the browseByCategory Widget
        SingleSmarty::getInstance()->assign('associationCategories', associationManager::categories());
        SingleSmarty::getInstance()->assign('merchantCategories', merchantManager::categories());
        SingleSmarty::getInstance()->assign('classifiedCategories', classifiedManager::categories());
        SingleSmarty::getInstance()->assign('dealCategories', dealManager::categories());
        SingleSmarty::getInstance()->assign('eventCategories', eventManager::categories(new DateTime('TODAY'), new DateTime('+12months')));
        SingleSmarty::getInstance()->assign('articleCategories', articleManager::categories());
        SingleSmarty::getInstance()->assign('blogCategories', blogManager::categories());
    }
    
    
}