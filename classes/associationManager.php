<?php
/**
 * Merchant Handling class - common functions
 * @author timfisher - Tartan Tangerine Ltd 2013
 *
 */

class associationManager extends modelManager
{
    /**
     * Create the search string 
     * @return string
     */
    private static function searchString(){
        // Keyword search
        if (helpers\request('keyword')) {
            $search = "AND (UPPER(c.name) LIKE :keyword OR UPPER(m.companyname) LIKE :keyword )";
        } else {
            $search = '';
        }
        return $search;
    }
    
    
    public static function featured($limit=4){
        $sql = "SELECT  m.*, GROUP_CONCAT(TRIM(c.name)) AS categories
                    FROM category c
                    INNER JOIN category_merchant cm ON cm.category_id = c.id
                    INNER JOIN merchant m ON m.id = cm.merchant_id
                    INNER JOIN weights w ON m.level = w.level
                    WHERE isnull(m.deleted)
                    AND m.association = 1
                    GROUP BY m.id
                    ORDER BY w.weight * RAND() DESC
                    LIMIT {$limit}";
        
        return self::group( R::getAll($sql) );
    }
    
    /**
     * Find all merchants, sorted by name with category data
     * @return Model_Merchant[] <multitype:, multitype:Ambigous <RedBean_OODBBean, unknown> >
     */
    public static function all()
    {
        $letterFilter = helpers\request('letter')?'AND UPPER(SUBSTRING(m.companyname FROM 1 FOR 1)) = :letter':'';
        $page = helpers\request('page')?:1;
        $offset = ($page - 1) * RESULTSLIMIT;
        $limit = "LIMIT {$offset}, ".RESULTSLIMIT;
        
        $search = self::searchString();
    
        $sql = "SELECT SQL_CALC_FOUND_ROWS m.*, w.weight, GROUP_CONCAT(TRIM(c.name)) AS categories
                    FROM category c 
                    INNER JOIN category_merchant cm ON cm.category_id = c.id 
                    INNER JOIN merchant m ON m.id = cm.merchant_id
                    INNER JOIN weights w ON m.level = w.level
                    WHERE isnull(m.deleted)
                    AND m.association = 1
                    {$search}
                    {$letterFilter}
                    GROUP BY m.id
                    ORDER BY m.companyname
                    {$limit}";

        $params = array();
        if (helpers\request('letter')){
            $params[':letter'] = helpers\request('letter');
        }

        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
        
        $results = self::group( R::getAll($sql, $params) );
        $count   = reset(R::getRow('SELECT FOUND_ROWS()'));
        return array('results'=>$results, 'count'=>$count);
        
    }
    
    
    /**
     * Find all merchants by category name
     * @param string $categoryName
     * @return Model_Merchant[] <multitype:, multitype:Ambigous <RedBean_OODBBean, unknown> >
     */
    public static function byCategory($categoryName)
    {
        $letterFilter = helpers\request('letter')?'AND UPPER(SUBSTRING(m.companyname FROM 1 FOR 1)) = :letter':'';
        $page = helpers\request('page')?:1;
        $offset = ($page - 1) * RESULTSLIMIT;
        $limit = "LIMIT {$offset}, ".RESULTSLIMIT;
        
        $searchString = self::searchString();

        $sql = "SELECT SQL_CALC_FOUND_ROWS m.*,GROUP_CONCAT(TRIM(c.name)) AS categories
                    FROM category c
                    INNER JOIN category_merchant cm ON cm.category_id = c.id
                    INNER JOIN merchant m ON m.id = cm.merchant_id
                    WHERE c.name = :categoryName
                    AND isnull(m.deleted)
                    AND m.association = 1
                    {$searchString}
                    {$letterFilter}
                    GROUP BY m.companyname
                    ORDER BY m.companyname
                    {$limit}";
        
        $params = array(':categoryName' => $categoryName);
        
        if (helpers\request('letter')){
            $params[':letter'] = helpers\request('letter');
        }
        
        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }

        $results = self::group( R::getAll($sql, $params) );
        $count   = reset(R::getRow('SELECT FOUND_ROWS()'));
        return array('results'=>$results, 'count'=>$count);

    }
    
    
    /**
     * Find a merchant by the company name
     * @param string $merchantName
     * @return Model_Merchant
     */
    public static function byName($merchantName)
    {
        $sql = "SELECT m.*, GROUP_CONCAT(TRIM(c.name)) AS categories
                    FROM category c
                    INNER JOIN category_merchant cm ON cm.category_id = c.id
                    INNER JOIN merchant m ON m.id = cm.merchant_id
                    WHERE m.companyname = :merchantName
                    AND isnull(m.deleted)
                    AND m.association = 1
                    GROUP BY m.companyname";
        
        return reset( self::group( R::getAll($sql, array(':merchantName' => $merchantName) ) ) );
    }
    
    
    /**
     * Return a the list of categories utilised by merchants
     * @return Model_Category[] <multitype:, multitype:Ambigous <RedBean_OODBBean, unknown> >
     */
    public static function categories()
    {
        $searchString = self::searchString();
        
        $sql = "SELECT c.*, count(*) as counter
                    FROM category c
                    INNER JOIN category_merchant cm ON cm.category_id = c.id
                    INNER JOIN merchant m ON m.id = cm.merchant_id
                    WHERE isnull(m.deleted)
                    AND m.association = 1
                    {$searchString}
                    group by c.name
                    ORDER BY c.name
                    ASC";
                    
        $params = array();
        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
        $results = R::getAll($sql, $params);

        return helpers\arrayToObject($results);  
    }
    
    /**
     * Function to return the first letter of all company names - we need this for the AZ filter
     */
    public static function azFilter()
    {
        $searchString = self::searchString();
        
        $sql = "SELECT DISTINCT UPPER(SUBSTRING(m.companyname FROM 1 FOR 1)) AS letter 
                    FROM merchant m
                    INNER JOIN category_merchant cm ON cm.merchant_id = m.id
                    INNER JOIN category c ON c.id = cm.category_id 
                    WHERE isnull(m.deleted)
                    AND m.association = 1
                    {$searchString}
                    ORDER BY letter ASC;"; 
        
        $params = array();            
        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
                    
        return self::letterFilter( R::getAll($sql, $params) );
    }
    
    public static function azFilterCategory($categoryName)
    {
        $searchString = self::searchString();
        
        $sql = "SELECT DISTINCT UPPER(SUBSTRING(m.companyname FROM 1 FOR 1)) AS letter
                        FROM merchant m
                        INNER JOIN category_merchant cm ON cm.merchant_id = m.id
                        INNER JOIN category c ON c.id = cm.category_id
                        WHERE c.name = :categoryName
                        AND isnull(m.deleted)
                        AND m.association = 1
                        {$searchString}
                        ORDER BY letter ASC;";
                        
        $params = array(':categoryName' => $categoryName);

        if (helpers\request('keyword')) {
            $params[':keyword'] = '%'.strtoupper(helpers\request('keyword')).'%';
        }
        return self::letterFilter(   R::getAll($sql, $params) );
    }
    
}