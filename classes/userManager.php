<?php

class userManager {
    
    /**
     * Get the current session user
     * @return Model_User
     */
    public static function session() {
       return isset($_SESSION['user'])?$_SESSION['user']:null;
    }
    
    public static function store($user){
        $_SESSION['user'] = serialize($user);
    }
    
    public static function grantMerchantAccess($user, $merchant_id){
        if ($user->isadmin){
            return true;
        }
        $merchant = reset($user->ownMerchant);
        
        if ($merchant && md5($merchant->id) == $merchant_id){
            return true;
        }
        return false;
    }
    
    
}