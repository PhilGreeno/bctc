<?php

/**
 * Ajax Web Service Handler
 * @author timfisher
 *
 */

class ajax {

    /**
     * Is the user the owner of the app
     * @return number
     */
    public function isOwner()
    {
        $id = $_POST['id'];
        
        $user = unserialize(userManager::session());
        
        // Attempt to find the widget
        $app = R::findOne('app', ' md5(id) = ? AND user_id = ? ', array(
                        $id, $user->id
                        ));
        
        return is_object($app)?$app->url:-1;
    }
    
    public function markdownPreview(){
        $data = $_POST['data'];
        
        return SingleSmarty::getInstance()->fetch("string:{''|cat:'".$data."'|markdown}");
    }
    
    public function postEmail(){

        $from     = helpers\request('name');
        $email    = helpers\request('email');
        $message  = helpers\request('message');
        $captcha  = helpers\request('captcha');
        $ident    = helpers\request('ident');

        
        // Is Captcha Correct
        if (!captcha::check($captcha)){
            return -2;
        }
        
        //Locate the item the ident referrers to.
        list($type, $id) = explode('_', $ident);
        
        $item = R::findOne($type, 'MD5(id) = ?', array ($id));
        
        if ($type == 'merchant' || $type =='deal'){
            $to = $item->user->email;
        } else {
            $to = $item->email;
        }
        
       
        $subject = 'An email for you from the Broadstone Chamber of Trade and Commerce website';
        $headers = 'From: '. $from . "\r\n" .
                        'Reply-To: webmaster@example.com' . "\r\n" .
                        'X-Mailer: PHP/' . phpversion();
        
        mail($to, $subject, $message, $headers);
        
    }
    
   
}