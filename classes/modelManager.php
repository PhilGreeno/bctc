<?php

/**
 * model data Handling class - common functions
 * @author Tim Fisher - Tartan Tangerine Ltd 2013
 *
 */

class modelManager {
    
    /**
     * Take DB results and group, putting categories into an assoc array.
     * @param array $results
     * @return stdClass[]
     */
    public static function group($results)
    {
        foreach ($results as $key=>$result){
            // Grab the categories column and explode it
            $results[$key]['categories'] = explode(',', $result['categories']);
    
        }
        return (array)helpers\arrayToObject($results);
    }
    
    
    /**
     * Get Featured by weight
     * @param INT $limit
     * @return Model_Merchant[] <multitype:, multitype:Ambigous <RedBean_OODBBean, unknown> >
     */
    public static function featured($limit=4)
    {
        $results = (array)static::all();
    
        // Order the results by weight * rand
        shuffle($results['results']); // randomize
    
        uasort($results['results'], function($a, $b){
            if($a->weight === $b->weight) {
                return (rand(-1,1));
            }
            return ( floatval($a->weight) * (mt_rand(0, 1000) / 1000)  <  floatval($b->weight) * (mt_rand(0, 1000) / 1000)  ? 1 : -1 );
        });
    
        return array_slice($results['results'], 0, $limit);
    }
    
    public static function letterFilter($results)
    {
        $letters = null;
        
        // Pre-populate the array
        foreach (range('A', 'Z') as $letter) {
            $letters[$letter] = 0;
        }
        
        // Set the hits to true
        if (is_array($results)){
            foreach ($results as $object){
                $letters[$object['letter']] = true;
            }
        }
        return $letters;
    }
   
}