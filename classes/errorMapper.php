<?php

class errorMapper {
	
	public static function doMapping($errorCode) {

		ProductRules::getInstance()->assignProperties();
		
		$oSmarty = SingleSmarty::getInstance();
		$oSmarty->assign("errorCode", $errorCode);
		return trim($oSmarty->fetch('errors/codeMappings.tpl'));

	}

}