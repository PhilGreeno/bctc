<?php

error_reporting(E_ALL);
ini_set('display_errors', '0');

// Calculate correct web-path
$aRequestUri = explode('/', $_SERVER['REQUEST_URI']);

$sPath = $aRequestUri[1] . '/'.
         $aRequestUri[2] .'/';
  /*       $aRequestUri[3] .
         ($aRequestUri[3]=='branches'
                ? '/' . $aRequestUri[4]
                : ''). '/'; */

define("WEBPATH", $sPath . "htdocs/");

define("HTDOCSPATH", ROOTPATH.'/htdocs');

// Create Database Connection
R::setup('mysql:host=localhost;dbname=bctc','root','root');

// Lock down the schema
R::freeze( true );
//R::debug(true);
//R::$writer->setUseCache(true);
