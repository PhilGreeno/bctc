<?php
/**
 * Created on 28 Aug 2012
 *
 * @author Tim Fisher
 *
 * Description: Defines paths & loads required libraries
 */

date_default_timezone_set('UTC');

define("ROOTPATH", dirname(dirname(__FILE__)));



// Set include paths
$path = PATH_SEPARATOR.ROOTPATH.'/classes/';
$path .= PATH_SEPARATOR.ROOTPATH.'/models/';
$path .= PATH_SEPARATOR.ROOTPATH.'/controllers/';
$path .= PATH_SEPARATOR.ROOTPATH.'/libraries/';
$path .= PATH_SEPARATOR.ROOTPATH.'/libraries/payPal/';
$path .= PATH_SEPARATOR.ROOTPATH.'/libraries/lessphp/';
$path .= PATH_SEPARATOR.ROOTPATH.'/libraries/smarty/libs/';
$path .= PATH_SEPARATOR.ROOTPATH.'/libraries/smarty/libs/plugins/';
$path .= PATH_SEPARATOR.ROOTPATH.'/libraries/smarty/libs/sysplugins/';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);

// Autoloader etc...
include("helpers.php");

// Load in the properties
include("properties.php");


// Instantiate the Exception handler and attach the SPL listeners
//$handler = new ExceptionHandler();
//$handler->attach(new ExceptionLogger());
//$handler->attach(new ExceptionMailer());
//set_exception_handler(array($handler, 'handle'));

// Set up the external libraries
$smarty = SingleSmarty::getInstance();
$smarty->setTemplateDir(ROOTPATH."/views/");
$smarty->setCompileDir(ROOTPATH."/temp/");
$smarty->assign("WEBPATH", WEBPATH);
$smarty->assign("ROOTPATH", ROOTPATH);

// Load smarty plugins
$smarty->loadPlugin('smarty_compiler_switch');
$smarty->loadPlugin('smarty_modifier_obfuscate');
$smarty->loadPlugin('smarty_modifier_camel_split');


// Set up and start the session
session_start();

if (isset($_REQUEST['nukesession'])){
	session_unset();
}

// For debug
include("fb.php");

include("initialise.php");



?>
