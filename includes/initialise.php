<?php




// Look to see if admin user exists, if not then create defaults	

$adminUser = R::findOne('user',' email = ? ', 
				array( 'admin@admin.com' )
			);

if ( ! isset($adminUser) ){
    
	// Create Admin User
	$adminUser = R::dispense('user');
	$adminUser->createAdmin(); 
	
} 

// Assign Session User
$user = userManager::session();

SingleSmarty::getInstance()->assign('sess_user', unserialize($user));


// Less Compiler
//$less = new lessc;
//$less->setFormatter("compressed");
//$less->checkedCompile(HTDOCSPATH."/css/main.less", HTDOCSPATH."/css/main.css");

define('RESULTSLIMIT', 5);
SingleSmarty::getInstance()->assign('resultLimit',   RESULTSLIMIT);
SingleSmarty::getInstance()->assign('page', helpers\request('page'));
SingleSmarty::getInstance()->assign('keyword', helpers\request('keyword'));




// Allowed webService classes and methods
$wsClasses = array( 'ajax'=>
                array(	'markdownPreview',
                        'postEmail',
                    )
                );


// backwards compat.
function p($obj){
	\helpers\p($obj);
}
