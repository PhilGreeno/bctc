<?php
namespace helpers;

use DateTime;

// specify parameters for autoloading classes

spl_autoload_register(NULL, FALSE);
spl_autoload_extensions('.php');
spl_autoload_register(array('\helpers\Autoloader', 'load'));


// define custom ClassNotFoundException exception class
class ClassNotFoundException extends \Exception{}
// define Autoloader class
class Autoloader{
	//attempt to autoload a specified class
	public static function load($class)	{
		if (class_exists($class, FALSE)) {
			return;
		}
		$file = $class . '.php';
		
		if (@include_once($file)){
			unset($file);
			if (!class_exists($class, FALSE)) {
				eval('class ' . $class . '{}');
				throw new ClassNotFoundException('Class ' . $class . ' not found.');
			}
		}
	}
}



function p($obj){
	echo "<pre>";
	print_r($obj);
	echo "</pre>";
}


function request($var){
	return (isset($_REQUEST[$var]) && $_REQUEST[$var] != "")?$_REQUEST[$var]:null;
}

$errors = array();
function addError($error){
    $errors[] = $error;
    \SingleSmarty::getInstance()->assign('errors', $errors);
}

function now(){
    $oDate = new DateTime('NOW');
    return $oDate->format("Y-m-d H:i:s");
}

// Re-arrange $_FILES type arrays so they are more useable
function rearrange( $arr ){
    foreach( $arr as $key => $all ){
        foreach( $all as $i => $val ){
            $new[$i][$key] = $val;
        }
    }
    return $new;
}

function array_to_obj($array, &$obj)
{
    if (is_array($array)){
        foreach ($array as $key => $value)
        {
            if (is_array($value))
            {
                $obj->$key = new \stdClass();
                array_to_obj($value, $obj->$key);
            }
            else
            {
                $obj->$key = $value;
            }
        }
        return $obj;
    }
}

function arrayToObject($array)
{
    $object = new \stdClass();
    return array_to_obj($array,$object);
}

