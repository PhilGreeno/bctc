-- phpMyAdmin SQL Dump
-- version 4.0.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 14, 2013 at 03:47 PM
-- Server version: 5.0.96-community
-- PHP Version: 5.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `broadsto_bctc`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` bigint(20) NOT NULL auto_increment,
  `title` tinytext collate utf8_bin NOT NULL,
  `author` varchar(128) collate utf8_bin default NULL,
  `url` varchar(128) collate utf8_bin default NULL,
  `publicationdate` datetime default NULL,
  `abstract` tinytext collate utf8_bin,
  `content` text collate utf8_bin,
  `keywords` varchar(64) collate utf8_bin default NULL,
  `created` datetime default NULL,
  `suggested` varchar(255) collate utf8_bin default NULL,
  `deleted` tinyint(1) default NULL,
  `pageviews` int(10) unsigned default NULL,
  `updated` datetime default NULL,
  `image1` tinyint(1) default NULL,
  `image2` tinyint(1) default NULL,
  `image3` tinyint(1) default NULL,
  `image4` tinyint(1) default NULL,
  `merchant_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `keywords` (`keywords`),
  KEY `fk_article_merchant1_idx` (`merchant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=8 ;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `title`, `author`, `url`, `publicationdate`, `abstract`, `content`, `keywords`, `created`, `suggested`, `deleted`, `pageviews`, `updated`, `image1`, `image2`, `image3`, `image4`, `merchant_id`) VALUES
(1, 0x4172642041727469636c652031, 'kjkldj', 'kjkj', '2013-04-02 13:48:10', 0x6c6b6a, 0x6c6b6a, 'dsf', '2013-03-28 10:19:28', 'sdf', NULL, 14, '2013-04-08 10:07:02', 0, NULL, NULL, NULL, 3),
(2, 0x54617274616e, 'Tim Fisher', 'www.tim.com', '2013-04-01 10:45:40', 0x5468697320697320612073756d6d617279206f6620746865207374756666207468617420776527766520676f7420746f2073656c6c, 0x546869732069732074686520636f6e74656e74202d2069736e2774206974206e69636521, 'tartan, fruit', '2013-04-01 10:39:30', 'clothing', NULL, 12, '2013-04-01 10:45:40', 1, 1, 1, 1, 53),
(3, 0x41646d696e206d616e202d20626c6f67676572, 'admin', 'www.ageuk.org.uk', '2013-04-04 13:41:12', 0x41646d696e20626c6f6767657220627567676572, 0x646a666c6a73646c206a666a7364206c6b666a7364206c666a6c, 'jkj', '2013-04-03 10:21:49', ' dsfsd', NULL, 2, '2013-04-04 13:40:33', 0, NULL, NULL, NULL, 52),
(4, 0x41646d696e206d616e202d20626c6f67676572, 'admin', 'www.ageuk.org.uk', '2013-04-04 13:47:42', 0x46534620534653462053444620205320646673646620736466736466, 0x2073667364662073646620736466207364662073646620736466207364662073646620736466, 'sdfds', '2013-04-03 10:24:37', 'sdfdsf', NULL, 46, '2013-04-04 13:47:40', 1, 1, NULL, NULL, 52),
(5, 0x4368656d697374732052205573, 'Chemist man', 'www.chemist.com', '2013-04-08 07:50:56', 0x4368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d69737420, 0x4368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d697374204368656d69737420, 'chemist', '2013-04-08 07:50:12', 'chemist', NULL, 1, '2013-04-08 07:50:56', 1, NULL, NULL, NULL, 22),
(6, 0x46756e6572616c7320617265206465616478, 'Harry H', 'www.grim.com', '2013-04-18 13:54:45', 0x46756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c20, 0x46756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c2046756e6572616c20, 'funeral, death, rememberance', '2013-04-08 07:53:52', 'rememberance', NULL, 0, '2013-04-18 13:54:45', 1, NULL, NULL, NULL, 74),
(7, 0x76626376626363, 'bvcvb', 'fgdgdfg', '2013-04-11 10:24:06', 0x67646667646667646667, 0x6466676466676466670d0a596f7572207469746c6520686572652e2e2e0d0a3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d0d0a0d0a6376786376, 'testv', '2013-04-08 10:19:58', 'hello', NULL, 8, '2013-04-11 10:24:01', 1, NULL, NULL, NULL, 53);

-- --------------------------------------------------------

--
-- Table structure for table `article_category`
--

CREATE TABLE IF NOT EXISTS `article_category` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `article_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_article_category_article1_idx` (`article_id`),
  KEY `fk_article_category_category1_idx` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `article_category`
--

INSERT INTO `article_category` (`id`, `article_id`, `category_id`) VALUES
(1, 2, 34),
(2, 1, 34),
(5, 3, 35),
(6, 4, 34),
(7, 5, 44),
(9, 6, 44),
(13, 7, 34);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(45) collate utf8_bin NOT NULL,
  `description` varchar(45) collate utf8_bin NOT NULL,
  `business` tinyint(1) default '1',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=47 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `description`, `business`) VALUES
(1, 'Trophies', 'Trophies', 1),
(2, 'Tree Surgeon ', 'Tree Surgeon ', 1),
(3, 'Travel Agent', 'Travel Agent', 1),
(5, 'Swimwear', 'Swimwear', 1),
(6, 'Supermarket ', 'Supermarket ', 1),
(7, 'Sports Centre', 'Sports Centre', 1),
(8, 'Solicitors', 'Solicitors', 1),
(9, 'Restaurant ', 'Restaurant ', 1),
(10, 'Publications', 'Publications', 1),
(11, 'Pub', 'Pub', 1),
(12, 'Photography', 'Photography', 1),
(13, 'Pet Shop', 'Pet Shop', 1),
(14, 'Ophthalmics ', 'Ophthalmics ', 1),
(15, 'Nursery', 'Nursery', 1),
(16, 'News Agents ', 'News Agents ', 1),
(17, 'Insurance', 'Insurance', 1),
(18, 'Home Interiors ', 'Home Interiors ', 1),
(19, 'Health Services ', 'Health Services ', 1),
(20, 'Hairdresser', 'Hairdresser', 1),
(21, 'Dry Cleaners', 'Dry Cleaners', 1),
(22, 'Computer Services ', 'Computer Services ', 1),
(23, 'Club', 'Club', 1),
(24, 'Chemist', 'Chemist', 1),
(25, 'Charity Shop', 'Charity Shop', 1),
(26, 'Café', 'Café oh la la', 1),
(27, 'Business Solutions ', 'Business Solutions ', 1),
(28, 'Bed and Breakfast', 'Bed and Breakfast', 1),
(29, 'Bank', 'Bank', 1),
(30, 'Architects ', 'Architects ', 1),
(31, 'Accountants', 'Accountants - Bean counters and stuff', 1),
(32, 'Web Design', 'Web Design', 1),
(33, 'Software Development', 'Things', 1),
(34, 'Fish', 'and things', 0),
(35, 'Beer', 'Beer and more beer!', 0),
(36, 'Jewellers', 'Jewellers', 1),
(37, 'Clothing', 'Clothing', 1),
(38, 'Post Office', 'Post Office', 1),
(39, 'Gift Shop', 'Gift Shop', 1),
(40, 'Flooring', 'Flooring', 1),
(41, 'Butchers', 'Butchers', 1),
(42, 'Estate Agents', 'Estate Agents', 1),
(43, 'Opticians', 'Opticians', 1),
(44, 'Shopping', 'Shopping', 0),
(45, 'Swimming', 'Swimming and swimming related items ', 0),
(46, 'Another Category', 'this is a test category', 0);

-- --------------------------------------------------------

--
-- Table structure for table `category_classified`
--

CREATE TABLE IF NOT EXISTS `category_classified` (
  `id` bigint(19) unsigned NOT NULL auto_increment,
  `classified_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_category_classified_classified1_idx` (`classified_id`),
  KEY `fk_category_classified_category1_idx` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `category_classified`
--

INSERT INTO `category_classified` (`id`, `classified_id`, `category_id`) VALUES
(2, 1, 35),
(3, 1, 34),
(4, 4, 34),
(5, 2, 35),
(6, 3, 35);

-- --------------------------------------------------------

--
-- Table structure for table `category_deal`
--

CREATE TABLE IF NOT EXISTS `category_deal` (
  `id` bigint(19) unsigned NOT NULL auto_increment,
  `deal_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `cons_fk_category_deal_deal_id_id` (`deal_id`),
  KEY `cons_fk_category_deal_category_id_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `category_deal`
--

INSERT INTO `category_deal` (`id`, `deal_id`, `category_id`) VALUES
(1, 2, 34),
(3, 3, 35),
(4, 1, 34),
(5, 2, 44),
(6, 4, 44),
(9, 5, 35);

-- --------------------------------------------------------

--
-- Table structure for table `category_event`
--

CREATE TABLE IF NOT EXISTS `category_event` (
  `id` bigint(19) unsigned NOT NULL auto_increment,
  `event_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_category_event_event1_idx` (`event_id`),
  KEY `fk_category_event_category1_idx` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `category_event`
--

INSERT INTO `category_event` (`id`, `event_id`, `category_id`) VALUES
(2, 2, 34),
(3, 3, 35),
(4, 1, 34);

-- --------------------------------------------------------

--
-- Table structure for table `category_merchant`
--

CREATE TABLE IF NOT EXISTS `category_merchant` (
  `id` bigint(20) NOT NULL auto_increment,
  `merchant_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_category_merchant_merchant1_idx` (`merchant_id`),
  KEY `fk_category_merchant_category1_idx` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=89 ;

--
-- Dumping data for table `category_merchant`
--

INSERT INTO `category_merchant` (`id`, `merchant_id`, `category_id`) VALUES
(2, 3, 22),
(5, 21, 2),
(6, 22, 24),
(7, 23, 27),
(8, 24, 29),
(9, 26, 3),
(10, 27, 31),
(11, 29, 9),
(12, 30, 30),
(13, 4, 25),
(14, 31, 20),
(15, 32, 27),
(16, 33, 19),
(17, 34, 15),
(18, 36, 23),
(19, 37, 23),
(20, 38, 10),
(21, 40, 16),
(22, 41, 19),
(23, 42, 3),
(24, 39, 27),
(25, 43, 18),
(26, 44, 6),
(27, 45, 19),
(30, 14, 9),
(31, 16, 29),
(32, 7, 12),
(33, 9, 17),
(34, 6, 14),
(35, 8, 26),
(36, 18, 31),
(37, 10, 13),
(38, 11, 28),
(39, 12, 12),
(40, 19, 22),
(41, 25, 21),
(42, 2, 5),
(43, 28, 11),
(44, 35, 19),
(45, 15, 11),
(46, 17, 7),
(47, 5, 27),
(48, 20, 1),
(49, 1, 22),
(50, 1, 27),
(56, 52, 27),
(58, 53, 33),
(63, 54, 30),
(64, 3, 30),
(65, 3, 28),
(66, 3, 26),
(67, 67, 36),
(68, 86, 37),
(69, 89, 38),
(70, 68, 12),
(71, 70, 19),
(72, 92, 39),
(73, 84, 40),
(74, 88, 41),
(75, 58, 8),
(76, 69, 8),
(77, 72, 42),
(78, 94, 43),
(79, 81, 20),
(80, 79, 37),
(81, 85, 20),
(82, 60, 9),
(83, 55, 16),
(84, 57, 9),
(85, 63, 8),
(86, 77, 9),
(87, 78, 27),
(88, 97, 27);

-- --------------------------------------------------------

--
-- Table structure for table `classified`
--

CREATE TABLE IF NOT EXISTS `classified` (
  `id` bigint(20) NOT NULL auto_increment,
  `title` tinytext collate utf8_bin NOT NULL,
  `summary` tinytext collate utf8_bin,
  `phone` varchar(12) collate utf8_bin default NULL,
  `email` varchar(128) collate utf8_bin default NULL,
  `price` float default NULL,
  `address1` varchar(128) collate utf8_bin default NULL,
  `address2` varchar(128) collate utf8_bin default NULL,
  `address3` varchar(128) collate utf8_bin default NULL,
  `postcode` varchar(45) collate utf8_bin default NULL,
  `county` varchar(45) collate utf8_bin default NULL,
  `country` varchar(45) collate utf8_bin default NULL,
  `lat` float default NULL,
  `long` float default NULL,
  `keywords` varchar(64) collate utf8_bin default NULL,
  `deleted` tinyint(1) default NULL,
  `publicationdate` datetime default NULL,
  `pageviews` int(10) unsigned default NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  `contact` varchar(255) collate utf8_bin default NULL,
  `url` varchar(255) collate utf8_bin default NULL,
  `description` text collate utf8_bin,
  `suggested` varchar(255) collate utf8_bin default NULL,
  `image1` tinyint(1) default NULL,
  `image2` tinyint(1) default NULL,
  `image3` tinyint(1) default NULL,
  `image4` tinyint(1) default NULL,
  `merchant_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `keywords` (`keywords`),
  KEY `fk_classified_merchant1_idx` (`merchant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=5 ;

--
-- Dumping data for table `classified`
--

INSERT INTO `classified` (`id`, `title`, `summary`, `phone`, `email`, `price`, `address1`, `address2`, `address3`, `postcode`, `county`, `country`, `lat`, `long`, `keywords`, `deleted`, `publicationdate`, `pageviews`, `created`, `updated`, `contact`, `url`, `description`, `suggested`, `image1`, `image2`, `image3`, `image4`, `merchant_id`) VALUES
(1, 0x476f6c6420546573742031, 0x61736164662064736620736420736466207364662073646620736466647366647366, 'lkj', 'tim@tartantangerine.com', 10, 'add1', 'add2', '', 'pcode2', 'asdasdas', 'UK', 50.7633, -1.9953, 'frog', NULL, '2013-04-04 12:32:08', 19, '2013-03-28 10:18:42', '2013-04-05 10:21:42', 'jlkj', 'fsdfsdf', 0x73646664736620736466736466, 'dsfdsf', 1, NULL, NULL, NULL, 3),
(2, 0x54617274616e20436c6173736966696564, 0x5468697320697320612074657374206f66207468652074617274616e20636c617373696669656420, '01202-682889', 'tim@tim.com', 100, '5 Warburton Road', '', '', 'BH17 8SD', 'Dorset', 'UK', 0, 0, 'tartan, fruit', NULL, '2013-04-11 10:28:58', 19, '2013-04-01 11:00:55', '2013-04-11 10:28:17', 'Tim Fisher', 'www.tim.com', 0x5468697320697320612074657374206f66207468652074617274616e20636c6173736966696564205468697320697320612074657374206f66207468652074617274616e20636c6173736966696564205468697320697320612074657374206f66207468652074617274616e20636c6173736966696564205468697320697320612074657374206f66207468652074617274616e20636c6173736966696564205468697320697320612074657374206f66207468652074617274616e20636c6173736966696564205468697320697320612074657374206f66207468652074617274616e20636c6173736966696564205468697320697320612074657374206f66207468652074617274616e20636c6173736966696564205468697320697320612074657374206f66207468652074617274616e20636c6173736966696564205468697320697320612074657374206f66207468652074617274616e20636c6173736966696564205468697320697320612074657374206f66207468652074617274616e20636c6173736966696564205468697320697320612074657374206f66207468652074617274616e20636c6173736966696564205468697320697320612074657374206f66207468652074617274616e20636c6173736966696564205468697320697320612074657374206f66207468652074617274616e20636c6173736966696564205468697320697320612074657374206f66207468652074617274616e20636c6173736966696564205468697320697320612074657374206f66207468652074617274616e20636c6173736966696564205468697320697320612074657374206f66207468652074617274616e20636c6173736966696564205468697320697320612074657374206f66207468652074617274616e20636c6173736966696564205468697320697320612074657374206f66207468652074617274616e20636c6173736966696564205468697320697320612074657374206f66207468652074617274616e20636c617373696669656420, 'clothing', 1, 1, 1, 1, 53),
(3, 0x41646d696e20436c6173736966696564, 0x736a6468666b206473666b682073646b666b736468666b6a7320666b68736420666b6a736466206b6a6473, '8981982', 'admin@admin.com', 100.99, 'y', 'oiu', 'oiu', 'oiu', 'oiu', 'UK', NULL, NULL, 'hello, there', NULL, '2013-04-04 13:44:35', 11, '2013-04-01 11:05:25', '2013-04-04 13:44:32', 'admin man', 'http://www.admin.com', 0x6a647368666b6a682073646b66686b73206468666a687364666b6a6873646b666873646b66206b6a736468666b6a2064736b666a2073646b6866206b736a646866206b6a736468666b206a73646b666a20, 'classified, advert', 0, 1, 1, 1, 52),
(4, 0x736466736466, 0x6c6b6a, '121278', 'admin@admin.com', 199.99, 'dsf', 'poi', 'jlj', 'lj', 'lkj', 'lkj', NULL, NULL, 'lkjlkj', NULL, '2013-04-01 11:54:32', 7, '2013-04-01 11:06:28', '2013-04-01 11:38:56', 'dsf', 'http://www.demo.com', 0x6c6b6a, 'sdsdf', 1, 1, 1, NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `cms`
--

CREATE TABLE IF NOT EXISTS `cms` (
  `id` bigint(20) NOT NULL auto_increment,
  `title` varchar(128) character set utf8 collate utf8_bin NOT NULL default '',
  `content` text character set utf8 collate utf8_bin NOT NULL,
  `link` varchar(45) character set utf8 collate utf8_bin NOT NULL default '',
  `active` tinyint(1) NOT NULL default '1',
  `position` int(11) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cms`
--

INSERT INTO `cms` (`id`, `title`, `content`, `link`, `active`, `position`) VALUES
(1, 'About the Broadstone Chamber', 0x41626f75742075730d0a3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d0d0a0d0a576f7273647320616e6420737475666620616e64207468696e67732e, 'About Us', 1, 1),
(2, 'Contact Us', 0x436f6e746163742055730d0a3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d0d0a0d0a486579206d617465202d20636f6e746163742075732e2e2e2e0d0a0d0a48657265206973206d7920656d61696c2061646472657373203a29, 'Contact Us', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `deal`
--

CREATE TABLE IF NOT EXISTS `deal` (
  `id` bigint(20) NOT NULL auto_increment,
  `title` tinytext collate utf8_bin NOT NULL,
  `summary` tinytext collate utf8_bin NOT NULL,
  `description` text collate utf8_bin NOT NULL,
  `conditions` text collate utf8_bin,
  `keywords` varchar(64) collate utf8_bin default NULL,
  `startdate` datetime NOT NULL COMMENT '	',
  `enddate` datetime NOT NULL,
  `value` float default NULL,
  `discount` float default NULL,
  `deleted` tinyint(1) default NULL,
  `publicationdate` datetime default NULL,
  `pageviews` int(10) unsigned default NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  `suggested` varchar(255) collate utf8_bin default NULL,
  `image1` tinyint(1) default NULL,
  `image2` tinyint(1) default NULL,
  `image3` tinyint(1) default NULL,
  `image4` tinyint(1) default NULL,
  `merchant_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `keywords` (`keywords`),
  KEY `fk_deal_merchant1_idx` (`merchant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=6 ;

--
-- Dumping data for table `deal`
--

INSERT INTO `deal` (`id`, `title`, `summary`, `description`, `conditions`, `keywords`, `startdate`, `enddate`, `value`, `discount`, `deleted`, `publicationdate`, `pageviews`, `created`, `updated`, `suggested`, `image1`, `image2`, `image3`, `image4`, `merchant_id`) VALUES
(1, 0x41727264204465616c, 0x546869732069732061206465616c20666f72206f7572207365727669636573, 0x4c6f72656d20697073756d20646f6c6f722073697420616d65742c20636f6e73656374657475722061646970697363696e6720656c69742e2051756973717565206e6f6e206c6163696e6961206d692e204675736365206574206175677565206d617373612e2041656e65616e206574206a7573746f206163206d65747573207072657469756d20766976657272612e205365642065676574206d61737361206e6973692c206567657420696d7065726469657420657261742e20457469616d20696e74657264756d2c2074656c6c757320696e2061646970697363696e6720706f72747469746f722c20617263752070757275732076756c707574617465206e6973692c2073697420616d657420636f6e64696d656e74756d206172637520746f72746f722076656c2073656d2e20566976616d757320617563746f722c207075727573206120657569736d6f642073757363697069742c2065726f73206c6967756c6120657569736d6f64206c696265726f2c2073697420616d65742063757273757320647569206c6967756c612076656c2072697375732e204e756c6c616d20736564206573742073697420616d657420747572706973206d616c65737561646120656c656966656e6420766974616520656765742076656c69742e20457469616d20736564207175616d206c616375732e20566573746962756c756d207175697320656c6974207175697320707572757320766573746962756c756d206d616c6573756164612e20496e206861632068616269746173736520706c617465612064696374756d73742e204d617572697320706f7375657265206c6163696e69612070756c76696e61722e2050656c6c656e746573717565206e6f6e206e69736c20696e20697073756d2076656e656e61746973207665686963756c612e0d0a0d0a446f6e6563207661726975732074696e636964756e74206e6973692065752073656d7065722e20496e206469676e697373696d2c20646f6c6f7220717569732073656d70657220636f6d6d6f646f2c206e756c6c6120647569206c75637475732073656d2c2076697461652074696e636964756e74206469616d206c6163757320637572737573206e6973692e2050686173656c6c7573206d616c65737561646120706f727461207475727069732c206e6f6e20636f6d6d6f646f20656e696d2074656d706f7220612e204e616d20656e696d206c616375732c2066657567696174206e656320657569736d6f642076697461652c20736167697474697320617420746f72746f722e204e616d206e697369206d61676e612c20656c656966656e64207574206c6f626f727469732069642c20636f6e67756520706c61636572617420697073756d2e204e756c6c6120666163696c6973692e205072616573656e742070656c6c656e7465737175652c20746f72746f72206964206f726e61726520706f72747469746f722c206f64696f2065726f73206c75637475732075726e612c2076697461652076656e656e61746973206f64696f2073617069656e2065676574206c656f2e204e756e632061206c616375732061207075727573206d616c6573756164612072686f6e637573206574206e656320657261742e, 0x4c696d6974656420746f2031206f666665722070657220706572736f6e, 'fraggle', '2013-04-02 12:43:38', '2013-04-11 07:50:38', 199.99, 45, NULL, '2013-04-04 13:45:56', 13, '2013-04-02 07:58:07', '2013-04-04 13:45:50', 'meat', 1, 1, 1, 1, 3),
(2, 0x54617274616e204465616c, 0x4c6f72756d20697073756d206d79206465616c756d, 0x3c7370616e20636c6173733d2268656c702d626c6f636b223e506c65617365207370656369667920616e6420626520636c6561722061626f757420616e7920636f6e646974696f6e7320726567617264696e672074686973206465616c2e3c2f7370616e3e3c7370616e20636c6173733d2268656c702d626c6f636b223e506c65617365207370656369667920616e6420626520636c6561722061626f757420616e7920636f6e646974696f6e7320726567617264696e672074686973206465616c2e3c2f7370616e3e3c7370616e20636c6173733d2268656c702d626c6f636b223e506c65617365207370656369667920616e6420626520636c6561722061626f757420616e7920636f6e646974696f6e7320726567617264696e672074686973206465616c2e3c2f7370616e3e3c7370616e20636c6173733d2268656c702d626c6f636b223e506c65617365207370656369667920616e6420626520636c6561722061626f757420616e7920636f6e646974696f6e7320726567617264696e672074686973206465616c2e3c2f7370616e3e3c7370616e20636c6173733d2268656c702d626c6f636b223e506c65617365207370656369667920616e6420626520636c6561722061626f757420616e7920636f6e646974696f6e7320726567617264696e672074686973206465616c2e3c2f7370616e3e3c7370616e20636c6173733d2268656c702d626c6f636b223e506c65617365207370656369667920616e6420626520636c6561722061626f757420616e7920636f6e646974696f6e7320726567617264696e672074686973206465616c2e3c2f7370616e3e3c7370616e20636c6173733d2268656c702d626c6f636b223e506c65617365207370656369667920616e6420626520636c6561722061626f757420616e7920636f6e646974696f6e7320726567617264696e672074686973206465616c2e3c2f7370616e3e, 0x424f474f46, 'bogof', '2013-04-02 07:59:27', '2013-04-18 07:59:27', NULL, NULL, NULL, '2013-04-11 10:33:43', 25, '2013-04-02 08:51:12', '2013-04-11 10:32:23', 'meat', 1, NULL, NULL, NULL, 53),
(3, 0x42616e6b65727321, 0x62616e6b20766e616b207661626e6b206e616b6e736b6e, 0x64667364662073662073646673646673642066, 0x42616e6b657221, 'dsf', '2013-04-01 06:26:22', '2013-04-10 12:41:22', NULL, NULL, NULL, '2013-04-02 11:27:17', 44, '2013-04-02 11:27:12', '2013-04-02 11:44:04', 'dsf', 1, NULL, NULL, NULL, 24),
(4, 0x352520446973636f756e7420617420534f53205377696d20, 0x3525206f666620746f20616c6c2042726f616473746f6e652056696c6c616765206d656d62657273, 0x3525206f666620746f20616c6c2042726f616473746f6e652056696c6c616765206d656d6265727320696e2073746f726520696e2073746f7265206f72206f6e6c696e65207573696e6720766f75636568657220636f64652077347335366276, 0x446973636f756e74206e6f7420617661696c61626c65206f6e2073616c65206974656d73206f722044656c7068696e207377696d20646973637320, 'discount, swimming, delphin, swim, discs, goggles, swimwear,', '2013-04-25 17:22:29', '2013-05-11 17:22:29', NULL, 5, NULL, '2013-04-25 16:21:45', 7, '2013-04-24 08:50:34', '2013-05-02 21:18:04', 'swimwear,discount, swimming, delphin, swim, discs, goggles, swimwear,', 0, NULL, NULL, NULL, 2),
(5, 0x437573746f6d657220496e736967687420526576696577, 0x54686520437573746f6d657220496e7369676874205265766965772074616b6573206f6e6520686f757220616e6420697320666f6c6c6f7765642062792061206e65787420646179207265706f7274206578706f73696e6720696d6d656469617465206f7074696f6e7320746f20696d70726f76652061707065616c20746f206578697374696e6720616e6420706f74656e7469616c20637573746f6d6572732e20, 0x54686520437573746f6d657220496e73696768742052657669657720696e766f6c7665732061207374727563747572656420636f6e766572736174696f6e2020286661636520746f20666163652c20536b797065206f722070686f6e652920776974682061204469726563746f72206f722073656e696f7220726570726573656e7461746976652e200d0a0d0a5468652064697363757373696f6e2074616b6573206f6e6520686f757220616e6420616464726573736573206f626a656374697665732c20637573746f6d6572207065727370656374697665732c20627573696e657373207374727563747572652c206f7269656e746174696f6e20616e642070726f6365737365732e204e6f207072696f72207265736561726368206f72207072657061726174696f6e2069732072657175697265642e200d0a0d0a4120636f6e636973652c207374727563747572656420616e6420636f6e666964656e7469616c207265706f72742077696c6c2062652070726f76696465642062792074686520656e64206f662074686520666f6c6c6f77696e6720627573696e657373206461792e204120666f6c6c6f772075702064697363757373696f6e20697320617661696c61626c6520746f20636c617269667920616e7920706f696e74732077697468696e20746865206f66666572207072696365206f6620c2a335302e20200d0a, 0x4e6f20636f6e646974696f6e73, 'Customer loyalty, business, ', '2013-04-28 16:59:57', '2013-07-31 16:59:57', 200, 75, NULL, '2013-04-28 16:34:15', 10, '2013-04-28 16:23:41', '2013-04-28 16:41:41', 'Business solutions, business, customer', 1, 0, NULL, NULL, 5);

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `id` bigint(20) NOT NULL auto_increment,
  `title` tinytext collate utf8_bin NOT NULL,
  `email` varchar(128) collate utf8_bin default NULL,
  `url` varchar(128) collate utf8_bin default NULL,
  `phone` varchar(32) collate utf8_bin default NULL,
  `contactname` varchar(128) collate utf8_bin NOT NULL,
  `address1` varchar(128) collate utf8_bin NOT NULL,
  `address2` varchar(128) collate utf8_bin default NULL,
  `address3` varchar(128) collate utf8_bin default NULL,
  `county` varchar(45) collate utf8_bin NOT NULL,
  `postcode` varchar(45) collate utf8_bin NOT NULL,
  `country` varchar(45) collate utf8_bin NOT NULL,
  `lat` float default NULL,
  `long` float default NULL,
  `startdate` datetime default NULL,
  `enddate` datetime default NULL,
  `summary` tinytext collate utf8_bin,
  `description` text collate utf8_bin,
  `keywords` varchar(64) collate utf8_bin default NULL,
  `deleted` tinyint(1) default NULL,
  `publicationdate` datetime default NULL,
  `pageviews` int(10) unsigned default NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  `suggested` varchar(255) collate utf8_bin default NULL,
  `image1` tinyint(1) default NULL COMMENT '	',
  `image2` tinyint(1) default NULL,
  `image3` tinyint(1) default NULL,
  `image4` tinyint(1) default NULL,
  `merchant_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `keywords` (`keywords`),
  KEY `fk_event_merchant1_idx` (`merchant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=5 ;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `title`, `email`, `url`, `phone`, `contactname`, `address1`, `address2`, `address3`, `county`, `postcode`, `country`, `lat`, `long`, `startdate`, `enddate`, `summary`, `description`, `keywords`, `deleted`, `publicationdate`, `pageviews`, `created`, `updated`, `suggested`, `image1`, `image2`, `image3`, `image4`, `merchant_id`) VALUES
(1, 0x41647264206576656e74, 'frog@grofo.com', 'www.ageuk.org.uk', '0120202020202', 'Frogger man', 'dsfsd', 'dsf', 'dsf', 'sdf', 'sdf', 'UK', NULL, NULL, '2013-04-03 12:59:42', '2013-04-04 12:59:42', 0x20736466736466206473667364206673646620, 0x666473206673642066736466207364662073646620736466207364667364206673646620, 'guff', NULL, '2013-04-04 13:47:31', 8, '2013-04-02 12:02:18', '2013-04-04 13:47:25', 'sdfds', 1, NULL, NULL, NULL, 3),
(2, 0x54617274616e204576656e74, 'sdf@jkk.com', 'dsfsd', 'sdf', 'sddsf', 'sdf', 'fgh', 'fgh', 'gfh', 'gfh', 'UK', 50.7633, -1.9953, '2013-04-08 13:04:20', '2013-04-19 13:04:20', 0x666768666768, 0x666768666768, 'smarty', NULL, '2013-04-11 10:44:40', 40, '2013-04-02 12:05:00', '2013-04-11 10:44:36', 'fgh', 1, NULL, NULL, NULL, 53),
(3, 0x73647364, 'admin@admin.com', 'sdsd', 'sds', 'sdsd', 'sds', 'sdf', 'sd', 'sdsd', 'sdfsdf', 'UK', NULL, NULL, '2013-04-17 13:42:39', '2013-04-26 13:42:39', 0x73647364, 0x73647364, 'ssd', NULL, '2013-04-02 12:43:24', 2, '2013-04-02 12:43:12', '2013-04-04 09:51:24', 'sdsd', 1, NULL, NULL, NULL, 52),
(4, 0x6a6867, 'admin@admin.com', 'mn', 'bm', 'nmb', 'mnbmn', 'mnb', 'mnb', 'mnb', 'mnb', 'UK', 0, 0, '2013-04-12 09:54:53', '2013-04-12 09:54:53', 0x6a68676a6867, 0x6473667364, 'hkjh', NULL, NULL, 0, '2013-04-11 08:55:28', '2013-04-11 08:55:28', 'hhkh', NULL, NULL, NULL, NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `merchant`
--

CREATE TABLE IF NOT EXISTS `merchant` (
  `id` bigint(20) NOT NULL auto_increment,
  `firstname` varchar(45) collate utf8_bin NOT NULL,
  `lastname` varchar(45) collate utf8_bin NOT NULL,
  `username` varchar(45) collate utf8_bin NOT NULL,
  `companyname` varchar(128) collate utf8_bin NOT NULL,
  `address1` varchar(128) collate utf8_bin NOT NULL,
  `address2` varchar(128) collate utf8_bin default NULL,
  `address3` varchar(128) collate utf8_bin default NULL,
  `county` varchar(45) collate utf8_bin NOT NULL,
  `postcode` varchar(45) collate utf8_bin NOT NULL COMMENT '	',
  `country` varchar(45) collate utf8_bin NOT NULL,
  `phone` varchar(12) collate utf8_bin default NULL,
  `fax` varchar(12) collate utf8_bin default NULL,
  `mobile` varchar(12) collate utf8_bin default NULL,
  `url` varchar(128) collate utf8_bin default NULL,
  `lat` float default NULL,
  `long` float default NULL,
  `user_id` bigint(20) NOT NULL,
  `level` enum('bronze','silver','gold') collate utf8_bin NOT NULL,
  `deleted` tinyint(1) default NULL,
  `summary` text collate utf8_bin,
  `description` text collate utf8_bin,
  `pageviews` int(11) default '0',
  `image1` tinyint(1) default NULL,
  `image2` tinyint(1) default NULL,
  `image3` tinyint(1) default NULL,
  `image4` tinyint(1) default NULL,
  `image5` tinyint(1) default NULL,
  `image6` tinyint(1) default NULL,
  `association` tinyint(1) NOT NULL default '0',
  `sticky` int(11) NOT NULL default '0',
  `twitter` varchar(128) collate utf8_bin default NULL,
  `facebook` varchar(128) collate utf8_bin default NULL,
  `extrafield1` tinytext collate utf8_bin,
  `keywords` varchar(64) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `cons_fk_merchant_user_id_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=101 ;

--
-- Dumping data for table `merchant`
--

INSERT INTO `merchant` (`id`, `firstname`, `lastname`, `username`, `companyname`, `address1`, `address2`, `address3`, `county`, `postcode`, `country`, `phone`, `fax`, `mobile`, `url`, `lat`, `long`, `user_id`, `level`, `deleted`, `summary`, `description`, `pageviews`, `image1`, `image2`, `image3`, `image4`, `image5`, `image6`, `association`, `sticky`, `twitter`, `facebook`, `extrafield1`, `keywords`) VALUES
(1, 'Demo', 'User', 'duser', 'Demo Limited', 'Station House', 'Station Approach', 'Broadstone', 'Dorset', 'BH18 1AA', 'UK', '01202-222222', '01202-222333', '07901-965457', 'http://www.demo.com', NULL, NULL, 2, 'bronze', 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(2, 'Amanda ', 'Bursey', 'sosswim', 'SOS Swim', '15 Moor Road', '', 'Broadstone', 'Dorset', 'BH18 8AZ', 'UK', '01202-699451', '', '07887-661245', 'www.sos-swim.co.uk', 50.7638, -1.99384, 3, 'bronze', NULL, 0x4d6f6e646179202d204672696461792020392e30302d352e33300d0a536174757264617920392e3030202d20352e30300d0a, 0x534f53205377696d2053686f7020697320612066616d696c792072756e20696e646570656e64656e74207377696d2072657461696c65722c2065766572797468696e6720796f7520736565206f6e207468697320776562736974652069732068656c6420696e2073746f636b2c20696e206f75722073686f7020696e2042726f616473746f6e6520506f6f6c652e0d0a0d0a54686520627573696e65737320626567616e206c69666520696e20313938352061732061207377696d207363686f6f6c207465616368696e6720757020746f20383030206368696c6472656e2061207765656b2e20416c6f6e67207468652077617920776520676f742061736b656420666f7220746865206f64642070616972206f6620676f67676c657320616e642062792031393939207765206861642073746172746564207468652072657461696c2073696465206f662074686520627573696e6573732e20447572696e67203230303720746865206f6e6c696e652073686f702077617320696e2069747320696e66616e63792e204279203230303820697420626563616d65206170706172656e7420746861742072756e6e696e6720616c6c20746872656520656c656d656e7473206f662074686520627573696e657373207761732061207374657020746f6f2066617220616e6420746865205363686f6f6c2077617320736f6c642c20616c6c6f77696e6720757320746f20636f6e63656e7472617465206f6e207468652072657461696c20616e64206f6e6c696e652073686f702e0d0a0d0a496e203230313020534f53205377696d2053686f702061637175697265642074686520736f6c6520554b20696d706f727420616e6420646973747269627574696f6e2072696768747320666f72205377616e7320476f67676c657320616e64206e6f7720737570706c7920746865736520746f207365766572616c20696e7465726e6174696f6e616c207377696d6d6572732c20696e206164646974696f6e20746f2061206e756d626572206f662072657461696c65727320616e64207377696d6d696e6720636c7562732e20576520616c736f20617474656e642061206e756d626572206f66207377696d206d656574732c206f70656e207761746572206576656e747320616e64207472696174686c6f6e73207468726f7567686f75742074686520796561722e0d0a0d0a41206c696665206c6f6e6720696e766f6c76656d656e7420696e207468652073706f7274206d65616e732074686174207765206861766520612068696768206c6576656c206f6620657870657269656e636520616e64206b6e6f776c6564676520616e6420612070617373696f6e20776869636820776520686f70652073686f7773210d0a, 38, 1, 1, 1, 1, 1, 1, 0, 0, 'https://twitter.com/SOSswimshop', 'https://www.facebook.com/sosswimshop', '', ''),
(3, 'Mark', 'Tointon', 'aardpress', 'Aardpress Ltd', '18 Story Lane', '', 'Broadstone', 'Dorset', 'BH18 8EQ', 'UK', '01202-640134', '01202-640040', '', 'http://www.aardpress.com', 50.7633, -1.9953, 4, 'gold', NULL, 0x54686973206973206120717569636b2073756d6d617279207769746820736f6d6520776f72647320616e64207468696e677320616e6420737475666620616e6420636f6d70757465727320616e64207468696e6773207769746820736f6d6520776f72647320616e64207468696e677320616e6420737475666620616e6420636f6d70757465727320616e64207468696e6773207769746820736f6d6520776f72647320616e64207468696e677320616e6420737475666620616e6420636f6d70757465727320616e64207468696e6773, 0x546869732069732061206c6f74206d6f726520776f726473, 233, 1, 1, 1, 1, 1, 1, 0, 2, '', '', '', 'ard, press, test'),
(4, 'Barrie', 'Corbin', 'ageconcern', 'Broadstone Age Concern Day Centre and Shop', '7 Dunyeats Road', '', 'Broadstone', 'Dorset', 'BH18 8AA', 'UK', '01202-602256', '', '', 'www.ageuk.org.uk', NULL, NULL, 5, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(5, 'Simon', 'Merry', 'trustdev', 'Trust and Confidence by Design', 'Clarendon Road', '', 'Broadstone', 'Dorset', 'BH18 9HY', 'UK', '07806 093793', '', '07806 093793', 'www.trustandconfidencebydesign.co.uk', 50.7607, -2.00227, 6, 'bronze', NULL, 0x57652068656c7020627573696e6573736573207265616c6c7920756e6465727374616e64207768617420746865697220637573746f6d65727320616e6420706f74656e7469616c20637573746f6d657273206e65656420616e64206578706563742e2041206f6e6520686f757220636f6e666964656e7469616c20e28098437573746f6d657220496e7369676874e280992052657669657720616e64207772697474656e205265706f72742077696c6c2061737365737320696620796f752061726520646f696e6720616c6c20796f752063616e20746f206761696e20616e642072657461696e20637573746f6d20616e6420746f206861766520796f757220637573746f6d6572732074656c6c696e6720676f6f642073746f726965732061626f757420796f752e, 0x53696d6f6e204d657272792069732074686520666f756e646572206f662074686520627573696e6573732c206865206c6976657320696e20436c6172656e646f6e20526f616420616e6420686520697320616c736f20746865204368616972206f6620746865204368616d626572206f662054726164652e2053696d6f6e20697320636f6d6d697474656420746f2068656c70696e67206c6f63616c2042726f616473746f6e6520627573696e657373657320616e6420636f6e73657175656e746c79206f66666572732074686520637573746f6d65722072657669657720617420612068656176696c7920646973636f756e746564207072696365206f6620c2a335302e, 12, 1, 1, NULL, NULL, NULL, 0, 0, 1, NULL, NULL, NULL, ''),
(6, 'Maria', 'Primmer', 'vocalzone', 'Vocalzone Throat Pastilles ', 'Kestrel House', '7 Moor Road', 'Broadstone', 'Dorset', 'BH18 8AZ', 'UK', '01202-658444', '01202-659599', '', 'http://www.vocalzone.com/', 50.7637, -1.99414, 7, 'bronze', NULL, 0x566f63616c7a6f6e65205468726f61742050617374696c6c657320, 0x566f63616c7a6f6e6520697320696465616c20666f722070726f66657373696f6e616c2c20616d6174657572206f72206b6172616f6b652073696e676572732e20497420697320616c736f207375697461626c6520666f722072656c696576696e67207468652069727269746174696f6e206361757365642062792065786365737369766520737065616b696e6720616e6420736f206d61792062652075736566756c20746f20616c6c2074686f73652077686f206f6674656e20737065616b20696e207075626c696320696e20612070726f66657373696f6e616c206f7220766f6c756e746172792063617061636974792e20566f63616c7a6f6e652063616e20616c736f2072656c69657665207468652069727269746174696f6e206f6620746865207468726f61742063617573656420627920736d6f6b696e67206f722074686520636f6d6d6f6e20636f6c642e, 0, 1, 1, NULL, NULL, NULL, 0, 0, 0, '', '', '', ''),
(7, 'Andrew', 'Plant', 'imagine', 'Imagine Photographic', 'Unit 5', '13B Moor Road Business Centre', 'Broadstone', 'Dorset', 'BH18 8AZ', 'UK', '01202-693916', '', '07834-904632', 'www.imagine-photographic.com', 50.764, -1.99378, 8, 'bronze', NULL, 0x436f6e74656d706f726172792070726f66657373696f6e616c20696d6167696e67, 0x496d6167696e652050686f746f67726170686963207370656369616c69736520696e20636f6e74656d706f726172792070726f66657373696f6e616c20696d6167696e672c20776865746865722020612077656464696e672c206576656e74206f7220612066756c6c20636f6d6d65726369616c2073686f6f7420496d6167696e652077696c6c20757365206f7665722033302079656172732070726f66657373696f6e616c20657870657269656e636520746f206361707475726520796f7572206d656d6f726965732e, 4, 1, NULL, NULL, NULL, NULL, 0, 0, 0, '', '', 0x4259204150504f494e544d454e54, ''),
(8, 'Vikki', 'Slade', 'mollys', 'Molly’s Delicatessen and Catering Services', '189a / 189b Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DH', 'UK', '01202-605225', '', '', 'www.mollyscatering.co.uk', NULL, NULL, 9, 'bronze', NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(9, 'David', 'Morris', 'jlmorris', 'J L Morris (Insurance Brokers) Limited', 'Manor House', '1 Macaulay Road', 'Broadstone', 'Dorset', 'BH18 8AS', 'UK', '01202-642840', '01202-658815', '', 'www.jlmorris.co.uk', 50.7617, -1.99529, 10, 'bronze', NULL, 0x496e737572616e63652042726f6b657273, 0x57652068617665206265656e2065737461626c697368656420666f72206f7665722034302079656172732c20616e64207468656e2c206173206e6f772c207468652073636f7065206f662074686520636f76657220697320746865206d6f737420696d706f7274616e7420617370656374206f6620617272616e67696e6720696e737572616e636520666f72206f7572206d616e7920636c69656e74732e20205468657265206973206120706f70756c6172206d6973636f6e63657074696f6e207468617420616c6c20706f6c696369657320617265206964656e746963616c20e28093207468657920617265206e6f74210d0a0d0a54616b696e6720696e746f206163636f756e742074686520737569746162696c697479206f662074686520696e737572616e636520636f6d70616e7920627920746865207175616c697479206f662074686569722061646d696e697374726174696f6e20616e6420746865202077617920696e20776869636820746865792068616e646c6520636c61696d732c20656163682071756f746174696f6e2069732070726573656e74656420776974682066756c6c207772697474656e20696e666f726d6174696f6e20746f20616c6c6f7720796f7520746f206d616b6520616e20696e666f726d6564206465636973696f6e206261736564206f6e206f757220207265636f6d6d656e646174696f6e20746f20796f752e0d0a0d0a5468652070726f66657373696f6e616c2073657276696365732077652070726f7669646520616c6c6f7720796f7520746f2061736b2075732061626f757420616e7920617370656374206f6620796f757220696e737572616e63657320617420616e792074696d6520647572696e6720746865206c696665206f662074686520706f6c6963792e2020576520646f206e6f742063686172676520616e792061646d696e697374726174696f6e206f7220706c6163696e6720666565732e0d0a, 1, 1, NULL, NULL, NULL, NULL, 0, 0, 0, '', '', 0x3039303020e280932031373030204d6f6e64617920746f204672696461792e202057652061726520636c6f736564206f6e2042616e6b20486f6c6964617973, ''),
(10, 'Maxine', 'Hibbs', 'petspetals', 'Pets and Petals', '9 Dunyeats Road', '', 'Broadstone', 'Dorset', 'BH18 8AA', 'UK', '01202-698077', '', '', 'www.petsandpetals.net', 50.7623, -1.99481, 11, 'bronze', NULL, '', '', 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(11, 'Jill', 'Webber', 'pineridge', 'Pine Ridge Bed and Breakfast', '2b Lower Golf Links Road', '', 'Broadstone', 'Dorset', 'BH18 8BG', 'UK', '01202-694216', '', '', 'www.pineridgebedandbreakfast.co.uk', 50.7628, -1.98834, 12, 'bronze', NULL, 0x41206c757875727920626564202620627265616b666173742c206578636c75736976656c7920666f72204164756c7473, 0x5468726565206b696e672073697a652026206f6e652073696e676c6520726f6f6d2e20416c6c207769746820656e2d73756974652c20746561202620636f66666565206d616b696e6720666163696c69746965732c20574946492c20666c61742073637265656e205456732c2068616972206472796572732e204f666620737472656574207061726b696e672e0d0a0d0a536974756174656420696e206f6e65206f662042726f616473746f6e65e2809973207072656d69657220726f6164732c2050696e65205269646765206f6666657273206120717569657420262072656c6178696e67207265747265617420666f722066616d696c696573202620667269656e6473207669736974696e672074686520617265612e20496465616c20666f7220796f757220627573696e6573732067756573747320746f6f206173206561636820726f6f6d206861732061206465736b20617265612e0d0a, 5, 1, 1, 1, 1, 1, 1, 0, 0, '', '', 0x4f70656e20616c6c20796561722c20696e636c7564696e67204368726973746d6173202620746865204e65772059656172, ''),
(12, 'David', 'Faulder', 'prism', 'Prism Imageworks Ltd', '81 Cowslip Road', '', 'Broadstone', 'Dorset', 'BH18 9QZ', 'UK', '01202-691024', '', '', 'www.prism-imageworks.co.uk', 0, 0, 13, 'bronze', NULL, 0x446f727365745c2773206f6e6c79206365727469666965642070726f7669646572206f6620476f6f676c6520427573696e6573732050686f746f73, 0x505249534d20496d616765776f726b7320697320446f727365745c2773206f6e6c79206365727469666965642070726f7669646572206f6620476f6f676c6520427573696e6573732050686f746f732e204120756e69717565207365727669636520746861742070726f7669646573207669727475616c20746f75727320746861742066656174757265206f6e20476f6f676c652053656172636865732c20476f6f676c65204d6170732c20476f6f676c6520537472656574205669657720616e6420476f6f676c652b204c6f63616c2e0d0a0d0a576520616c736f206361727279206f757420636f6d6d65726369616c2070686f746f6772617068792028696e636c7564696e6720696e646570656e64656e74207669727475616c20746f7572732920666f7220627573696e6573736573206f6620616c6c2073697a65732c207072696d6172696c7920696e20446f727365742c2048616d70736869726520616e642057696c7473686972652c20616c74686f75676820636f6d6d697373696f6e73206172652074616b656e2066726f6d206675727468657220616669656c642e204f757220637573746f6d6572732072616e67652066726f6d206c6172676520636f6d6d65726369616c206f7267616e69736174696f6e7320746f20736d616c6c20627573696e65737365732c206c6f63616c20617574686f7269746965732c20746865206c65697375726520616e6420746f757269736d20696e64757374727920616e64206368617269746965732e0d0a0d0a57652068656c7020636c69656e74732c20746f2070726f6d6f746520746865697220627573696e6573732c20776865746865722061206c6f636174696f6e2c20612070726f64756374206f72206120736572766963652e0d0a, 2, 1, 1, 1, 1, 1, 1, 0, 0, '', '', 0x4d6f6e20e28093204672692030383a333020746f2031373a3330, ''),
(14, 'Franco ', 'Groppo', 'francos', 'Franco’s Pizzeria Trattoria Italiana Bar & Restaurant ', '7 Station Approach', '', 'Broadstone', 'Dorset', 'BH18 8AX', 'UK', '01202-601166', '', '', 'www.francos-restaurant.com', 50.7626, -1.99532, 15, 'bronze', NULL, 0x4974616c69616e20666f6f6420617420697320626573742021212121, 0x45737461626c697368656420696e203139393220616e6420726566757262697368656420696e20323030382c204672616e636f5c5c5c5c5c5c5c5c5c5c5c5c5c5c5c27732052657374617572616e7420616e6420426172206973207468652070657266656374206c6f636174696f6e20666f722061205350454349414c204d45444954455252414e45414e204e49474854202d207769746820796f7572207065726665637420686f7374204672616e636f2068696d73656c662e205468652066696e657374204974616c69616e2063756973696e6520696e206120667269656e646c7920616e642072656c617865642061746d6f7370686572652c2073657276696e67206120747261646974696f6e616c204974616c69616e202f20536963696c69616e206d656e752e20416c6c206f757220666f6f642069732066726573686c7920707265706172656420616e6420636f6f6b6564206279206f757220686f75736520636865662c2077697468206d616e79206f66206f7572207370656369616c6974792064697368657320636f6d696e672066726f6d20747261646974696f6e616c20536963696c69616e20726563697065732e2050495a5a41202d205041535441202d205350454349414c4c5920464953482026204d45415420444953484553202d2041204c41204341525445204d454e552073657276656420776974682066696e65200d0a4974616c69616e2f20496e7465726e6174696f6e616c2057696e65730d0a0d0a2e5370656369616c2050617274696573206361746572656420666f72202d20426972746864617973202d20416e6e69766572736172696573202d204f666669636520506172746965732c202043687269737461696e696e677320205072652d57656464696e672047617468657274696e6773202048656e20262053746167206e69676874730d0a0d0a4c697665204d7573696320616e642f6f7220446973636f20617661696c61626c65200d0a42726f616473746f6e65e280997320426573742050617274792056656e75650d0a, 2, 1, 1, 1, 1, 1, 1, 0, 0, '', '', 0x5475657364617920746f205361747572646179202020202020204c756e6368203132706d20746f2032706d20202020204576656e696e672036706d20746f204c617465e280a62e2e, ''),
(15, 'Jenny', 'James', 'goodsysrd', 'The Goods Yard', '14 Station Approach', '', 'Broadstone', 'Dorset', 'BH18 8AX', 'UK', '01202-694400', '', '', 'www.thegoodsyard.com', 50.7636, -1.99477, 16, 'bronze', NULL, '', '', 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(16, 'Alexandra', 'Greenwood', 'hsbc', 'HSBC Bank', '222 The Broadway', '', 'Broadstone', 'Dorset', 'BH18 8DJ', 'UK', '08457-404404', '01202-454205', '', 'www.hsbc.co.uk', 50.7615, -1.99564, 17, 'bronze', NULL, 0x42616e6b, 0x42616e6b, 0, 1, NULL, NULL, NULL, NULL, 0, 0, 0, '', '', 0x4d6f6e64617920e280932046726964617920392e33302d342e3330, ''),
(17, 'Martin', 'Stockley', 'junction', 'The Junction Leisure Centre', 'Station Approach', '', 'Broadstone', 'Dorset', 'BH18 8AX', 'UK', '01202-777766', '', '', 'www.thejunctionbroadstone.co.uk', 50.7633, -1.9953, 18, 'bronze', NULL, '', '', 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(18, 'Stuart', 'Magnus', 'newtonmagnus', 'Newton, Magnus and Co Accountants', 'Arrowsmith Court', 'Station Approach', 'Broadstone', 'Dorset', 'BH18 8AT', 'UK', '01202-697611', '01202-605303', '', 'www.newtonmagnus.co.uk', 50.7629, -1.99515, 19, 'bronze', NULL, 0x4163636f756e74616e63792c20207461786174696f6e20616e64206175646974207365727669636573, 0x4e6577746f6e204d61676e757320686173206265656e2065737461626c697368656420666f72206f7665722033302079656172732e57652070726f76696465206163636f756e74616e63792c20207461786174696f6e20616e642061756469742073657276696365732e204f757220707261637469636520636f766572732042726f616473746f6e652c20506f6f6c652c20426f75726e656d6f7574682c2057696d626f726e652c204368726973746368757263682c20426c616e64666f726420616e64205761726568616d20696e20446f7273657420616e6420616c736f20696e2048616d7073686972652c2057696c7473686972652c204465766f6e20616e6420536f6d65727365742e0d0a4f7572207175616c69666965642073746166662061696d20746f2070726f7669646520616e20656666696369656e742073657276696365206f66207468652068696768657374206c6576656c2e204f757220667269656e646c7920616e6420617070726f61636861626c6520776f726b666f72636520656e646561766f757220746f206163686965766520616e2065666665637469766520776f726b696e672072656c6174696f6e736869702077697468206f757220636c69656e74732e20546865207365727669636573206f66666572656420696e636c75646520626f6f6b6b656570696e672c2056415420636f6e73756c74616e63792c20706572736f6e616c2074617820706c616e6e696e672c20627573696e657373207461786174696f6e2c20627573696e65737320706c616e6e696e672c206d616e6167656d656e74207265706f7274696e672c20e2809c6865616c746820636865636b73e2809d2c207370656369616c6973742061647669636520616e642061756469742072656c617465642073657276696365732e0d0a436c69656e74732061726520656e636f75726167656420746f206d616e616765207468656972206f776e2066696e616e6369616c20616666616972732077697468206f757220617373697374616e63652e2053686f756c642074686572652062652061206e65656420626520666f7220616476696365206f6e20626f6f6b6b656570696e67206d6574686f64732c204e6577746f6e204d61676e7573206172652070726f756420746f2068617665207175616c69666965642073746166662074686174207370656369616c69736520696e20616964696e6720636c69656e7473207769746820616476696365206f6e206163636f756e74696e6720736f667477617265207061636b616765732e200d0a0d0a41206672656520696e697469616c2064697363757373696f6e206973206f66666572656420746f206e657720636c69656e74732c2077686963682063616e20626520617420796f7572206f776e207072656d6973657320617420612074696d6520746f207375697420796f752e0d0a, 23, 1, NULL, NULL, NULL, NULL, 0, 0, 0, '', '', 0x392e30302d352e3030204d6f6e64617920e28093204672696461792c2020204c617465206170706f696e746d656e74732063616e20626520617272616e676564206966206e65636573736172792e, ''),
(19, 'Adrian ', 'Clark', 'quantum', 'Quantum Social Media', '21 Cheam Road', '', 'Broadstone', 'Dorset', 'BH18 9HB', 'UK', '01202-692143', '', '', 'www.quantumsocial-media.com', 50.7618, -2.00269, 20, 'bronze', 1, '', '', 1, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(20, 'Maryanne', 'Parman', 'wessex', 'Wessex Trophies Limited', '13H Moor Road', '', 'Broadstone', 'Dorset', 'BH18 8AZ', 'UK', '01202-650444', '', '', 'www.wessextrophies.co.uk', 50.7639, -1.99381, 21, 'bronze', NULL, 0x436f72706f72617465204177617264732c2053706f7274732054726f70686965732c204372797374616c2026204d6564616c73, 0x5765737365782054726f706869657320697320612066616d696c792072756e20627573696e65737320616e64207765207072696465206f757273656c766573206f6e206f757220536572766963652c205175616c69747920616e64204166666f72646162696c6974792e20436f72706f72617465204177617264732c2053706f7274732054726f70686965732c204372797374616c2026204d6564616c732077652063616e20737570706c7920697420616c6c20616e642077652070726f6d69736520746f2066696e642074686520706572666563742061776172642c20746f206d65657420796f757220726571756972656d656e747320616e64206275646765742e0d0a0d0a5765206861766520612073686f7020626173656420696e2042726f616473746f6e6520616e6420616e20696e7465726e6574206f6e6c696e652077656273697465206174207777772e77657373657874726f70686965732e636f2e756b207769746820736563757265207061796d656e7420666163696c69746965732e20576520776f726b207769746820616c6c20746865206d616a6f722074726f7068792077686f6c6573616c65727320696e2074686520554b2c207769746820736f6d65206f6620746865206265737420636f6d6d65726369616c2072656c6174696f6e736869707320617661696c61626c652c20776869636820616c6c6f77732074686520637573746f6d65727320746f2063686f6f73652066726f6d20746865207769646573742073656c656374696f6e206f66206177617264732c206174207665727920636f6d7065746974697665207072696365732e200d0a0d0a5573696e67206f7572206f776e20696e20686f75736520656e67726176696e6720736f6c7574696f6e7320616e64206120756e69717565206162696c69747920746f207265706c696361746520636c7562206c6f676f7320616e64206261646765732c20776520617265206120e2809c6f6e652073746f702073686f70e2809d20666f7220616c6c20796f757220706572736f6e616c697365642070726573656e746174696f6e206e656564732e200d0a0d0a496620796f75206e65656420616e792068656c7020746f2063686f6f736520796f7572206177617264206f7220796f75207265717569726520616e79206675727468657220696e666f726d6174696f6e20706c6561736520646f206e6f7420686573697461746520746f20636f6e74616374207573206f6e20303132303220363530343434206f7220656d61696c20757320617420454e514057455353455854524f50484945532e434f2e554b0d0a0d0a4f75722073686f77726f6f6d206973207475636b6564206177617920696e2042726f616473746f6e6520626568696e6420534f53205377696d2053686f702e205468652073686f77726f6f6d20646973706c617973206f76657220322c3030302074726f706869657320666f7220796f7520746f206c6f6f6b20617420616e64206f6674656e20686176652073746f636b20666f7220796f7520746f2074616b65206177617920666f722074686f736520757267656e742070726573656e746174696f6e732e2057652077696c6c206265206f70656e2061742074686520666f6c6c6f77696e672074696d657320287365652073686f77726f6f6d2074696d65732062656c6f77292c2062757420696e206164646974696f6e2c20776520617265206f70656e20666f7220656d61696c20616e642074656c6570686f6e652063616c6c7320666f7220656e71756972657320616e64206f726465727320382e3330616d20746f2038706d2c203720646179732061207765656b21210d0a0d0a57652061726520616c736f20617661696c61626c6520746f206d65657420796f75206174207468652073686f77726f6f6d206f757473696465206f662074686520636f726520686f7572732c20746f2066697420696e207769746820796f757220776f726b2f6c69666520636f6d6d69746d656e74732e204a7573742063616c6c2030313230322036353034343420286469766572746564206174206f757220636f7374207768656e207468652073686f77726f6f6d20697320636c6f73656429206f7220656d61696c20656e714077657373657874726f70686965732e636f2e756b200d0a0d0a28576520646f206e6f7420636c6f736520666f72206c756e636820616e6420617265206f6674656e207374696c6c20696e207468652073686f77726f6f6d206f75747369646520746865736520686f7572732062757420706c656173652063616c6c2f656d61696c20666972737420746f2061766f6964206120776173746564206a6f75726e6579292057652061726520636c6f736564206f6e2042616e6b20486f6c69646179732e0d0a0d0a53686f77726f6f6d20616464726573732028666f7220536174204e617669676174696f6e293a0d0a313348204d6f6f7220526f61640d0a42726f616473746f6e650d0a4e65617220506f6f6c650d0a446f727365740d0a424831382038415a0d0a0d0a284f75722073686f7020697320646f776e2074686520736c697020726f616420696e206265747765656e20534f53205377696d2053686f7020616e642074686520544949454e2052657374617572616e74290d0a4c6f6e6769747564652026204c61746974756465200d0a0d0a4f666665723a204672656520636c7562202f20636f6d70616e7920617274776f726b207768656e20796f75206f7264657220706572736f6e616c69736564206c6f676f7320666f7220796f75722074726f706869657320286578636c7564657320676c6173732074726f7068696573290d0a0d0a0d0a, 11, 1, 1, 1, 1, 1, 0, 0, 0, '', '', 0x4d4f4e44415920544f205745444e45534441592039414d20544f2033504d2054485552534441592039414d20544f2035504d204652494441592039414d20544f2033504d2053415455524441592039414d20544f2031504d, ''),
(21, 'Peter ', 'Hart', 'advanced', 'Advanced Arborcare Ltd', 'Yaffle Hill', '21a Water Tower Road', 'Broadstone', 'Dorset', 'BH18 8LL', 'UK', '', '', '07907-033433', 'www.aatreesurgeon.co.uk', NULL, NULL, 22, 'bronze', NULL, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(22, 'Mike', 'Shutt MRPharm.S', 'arrowedge', 'Arrowedge Chemists', '188b Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DP', 'UK', '01202-603277', '', '', '', 50.7601, -1.99415, 23, 'silver', NULL, 0x496e646570656e64656e7420506861726d616379, 0x42726f616473746f6e65e280997320496e646570656e64656e7420506861726d6163792c20776974682066726565204e48532073657276696365732e0d0a4d65646963696e657320757365207265766965770d0a4e6577206d65646963696e65732073657276696365200d0a4672656520686f6d652064656c6976657279206f6620707265736372697074696f6e73204d6f6e64617920746f204672696461790d0a4672656520707265736372697074696f6e20636f6c6c656374696f6e20736572766963650d0a576520616c776179732073747269766520666f7220657863656c6c656e636520696e206f757220736572766963650d0a, 10, 1, NULL, NULL, NULL, NULL, 0, 0, 0, '', '', 0x392d3120322d352e3330204d6f6e20e280932046726964617920392d32205361747572646179, ''),
(23, 'Susannah', 'Brade-Waring', 'aspirin', 'Aspirin Business Solutions', '74 Fontmell Road', '', 'Broadstone', 'Dorset', 'BH18 8NP', 'UK', '01202-283118', '', '', 'www.businessheadaches.com', 50.7633, -1.9953, 24, 'bronze', NULL, 0x7364736473, 0x73647364, 2, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, NULL, NULL, NULL, ''),
(24, 'Karen', 'Wallbridge', 'barclays', 'Barclays Bank', '204 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DS', 'UK', '01202-602106', '', '', 'www.barclays.com', 50.7606, -1.9946, 25, 'bronze', NULL, 0x42616e6b, 0x42616e6b, 4, 1, NULL, NULL, NULL, NULL, 0, 0, 0, '', '', 0x392e333020e2809320342e3330204d6f6e64617920e280932046726964617920392e33302d203220536174757264617920, ''),
(25, 'Jamie', 'Barker', 'sjbarker', 'S J Barker Dry Cleaning', '183 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DH', 'UK', '01202-605274', '', '', 'www.sjbdrycleaning.co.uk', NULL, NULL, 26, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(26, 'Annette', 'Holliday', 'bathtravel', 'Bath Travel', '191 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DH', 'UK', '01202-658008', '', '', 'www.bathtravel.com', NULL, NULL, 27, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(27, 'Tim', 'Birkett', 'birkettco', 'Birkett and Co Accountants', '204a Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DP', 'UK', '01202-657444', '01202-657441', '', 'www.birkett.co', 50.7606, -1.99465, 28, 'bronze', NULL, 0x4163636f756e74696e6720616e642074617820736572766963657320666f7220627573696e65737320616e6420696e646976696475616c73, 0x4163636f756e74696e6720616e642074617820736572766963657320666f7220627573696e65737320616e6420696e646976696475616c73, 3, 1, 1, NULL, NULL, NULL, 0, 0, 0, '', '', 0x30383a333020202d202031373a303020204d6f6e64617920746f20467269646179, ''),
(28, 'Alex ', 'Crisp', 'blackwater', 'The Blackwater Stream', '180 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DP', 'UK', '01202-642580', '', '', 'www.jdwetherspoon.co.uk/home/pubs/the-blackwater-stream', 50.7594, -1.99342, 29, 'bronze', NULL, 0x46616d696c79207075622072657374617572616e74, 0x466f726d616c6c79205c27546865205374657070696e672053746f6e65735c272c2054686520426c61636b77617465722053747265616d20697320612066616d696c79207075622072657374617572616e7420616e6420636f6d6d756e697479206d656574696e6720706f696e742e20426f617374696e6720626f746820676f6f642076616c756520666f6f6420616e6420726561736f6e61626c7920707269636564206472696e6b732c20697420697320616e20696465616c2076656e7565207768657468657220796f755c277265207468696e6b696e67206f6620612066616d696c792067657420746f676574686572206f72206120666577206472696e6b73207769746820667269656e64732e20697420686173206120626565722067617264656e207769746820706c656e7479206f662073656174696e6720616e64206120636172207061726b20666f7220636f6e76656e69656e6365206f662069747320637573746f6d6572732e, 1, 1, 1, 1, 1, NULL, 0, 0, 0, '', '', 0x37616d2d6d69646e696768742053756e2d5468757273, ''),
(29, '“J”', 'Jahir', 'bollywood', 'Bollywood Tandoori', '205 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DN', 'UK', '01202-694800', '', '', '', 50.7608, -1.99534, 30, 'bronze', NULL, '', '', 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(30, 'Neil', 'Bichard', 'bradburyb', 'Bradbury Bichard Architects', '40 Corfe Way', '', 'Broadstone', 'Dorset', 'BH18 9NE', 'UK', '01202-693988', '', '', 'www.bradburybichardpoole.co.uk', NULL, NULL, 31, 'bronze', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(31, 'Simon', 'Vallins', 'barbershop', 'Broadstone Barber Shop', '195 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DH', 'UK', '01202-699060', '', '', '', NULL, NULL, 32, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(32, '', '', 'businesscenter', 'Broadstone Business Centre', '13a – 19b Moor Road', '', 'Broadstone', 'Dorset', 'BH18 8AZ', 'UK', '', '', '07939-938885', '', NULL, NULL, 33, 'bronze', NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(33, 'Dr Anthony', 'Bolton', 'chiropractic', 'Broadstone Chiropractic Health Centre', '4 Dunyeats Road', '', 'Broadstone', 'Dorset', 'BH18 8AG', 'UK', '01202-901777', '', '', '', 50.762, -1.99563, 34, 'bronze', NULL, 0x4c656164696e67206865616c74686361726520636c696e6963, 0x42726f616473746f6e6520436869726f707261637469632043656e7472652069732061206c656164696e67206865616c74686361726520636c696e69632064656469636174656420746f2068656c70696e6720796f75206861766520612068617070792c2070726f616374697665206c6966657374796c652e200d0a4f7572206d756c74696469736369706c696e61727920636c696e6963206f666665727320636869726f707261637469632c2061637570756e63747572652c206d61737361676520616e64206578657263697365207265686162696c69746174696f6e2e204279206f66666572696e672061207465616d20617070726f6163682077652063616e2067697665206f75722070617469656e7473206d6f72652c2061732065766572792070617469656e7420697320616e20696e646976696475616c20616e6420726571756972657320646966666572656e74207479706573206f6620636172652e0d0a4f7572207068696c6f736f7068792c207768657468657220796f752068617665206261636b206f72206e65636b207061696e2c20686561646163686573206f72206d69677261696e6573206f7220612073707261696e656420616e6b6c652c20697320746f206c6f6f6b20617420796f7520617320612077686f6c6520616e642066696e64206f757420776861742069732063617573696e6720796f75722070726f626c656d2e2057652077616e7420746f206265206d6f7265207468616e206a7573742061207061696e6b696c6c657220616e6420646f206e6f74206765742064697374726163746564206279207468652073796d70746f6d732e0d0a, 6, 1, 0, 0, 0, NULL, 0, 0, 0, '', '', 0x4d6f6e6461790930393a30302d31393a303020547565736461790930393a30302d31323a3030205765646e65736461790930393a30302d31393a30302054687572736461790930393a30302d31393a303020467269646179090930393a30302d31383a3030205361747572646179094170706f696e746d656e74204f6e6c79, ''),
(34, 'Penny', 'Vaughan-Pipe', 'bcn', 'Broadstone Christian Nursery', '161 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8NU', 'UK', '01202-601748', '', '', 'www.broadstonechristiannursery.com', NULL, NULL, 35, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(35, 'David', 'Allen', 'bcim', 'The Broadstone Clinic of Integrated Medicine', '11 Dunyeats Road', '', 'Broadstone', 'Dorset', 'BH18 8AA', 'UK', '01202-692493', '', '', 'www.broadstoneclinic.co.uk', NULL, NULL, 36, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(36, 'Scott ', 'Johnson', 'bcc', 'Broadstone Conservative Club', '2 Tudor Road', '', 'Broadstone', 'Dorset', 'BH18 8AW', 'UK', '01202-694237', '', '', 'www.broadstoneconservativeclub.co.uk', NULL, NULL, 37, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(37, 'David', 'Morgan', 'bgc', 'Broadstone Golf Club', 'Wentworth Drive', '', 'Broadstone', 'Dorset', 'BH18 8DQ', 'UK', '01202-692595', '', '', 'www.broadstonegolfclub.com', NULL, NULL, 38, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(38, 'Karen', 'Foreman', 'blink', 'Broadstone Link', '65 Cowslip Road', '', 'Broadstone', 'Dorset', 'BH18 9QZ', 'UK', '01202-658538', '', '', 'www.broadstonelink.co.uk', 0, 0, 39, 'bronze', NULL, 0x4135206c6f63616c206d6167617a696e6520666f72206c6f63616c20627573696e657373206578706f7375726520, 0x5468652041352068696768207175616c69747920636f6c6f7572206d6167617a696e6573206172652068616e642064656c6976657265642061726f756e6420746865206669727374207765656b656e64206f6620746865206d6f6e746820627920612072656c6961626c65207465616d206f66206c6f63616c207265736964656e747320706c757320636f706965732061726520616c736f206d61646520617661696c61626c6520617420766172696f757320646973747269627574696f6e20706f696e7473206163726f73732042726f616473746f6e6520616e6420737572726f756e64696e6720617265617320696e636c7564696e67207468652042726f616473746f6e65204c6962726172792c2042756467656e732c20546865204a756e6374696f6e2c206e6577736167656e74732c206c6f63616c207363686f6f6c7320616e64206d616e79206f7468657220627573696e65737365732c20646f63746f727320616e642064656e74697374732e0d0a526561646572732074656e6420746f206b656570206561636820697373756520756e74696c20746865206e657874206f6e65206172726976657320676976696e6720746865207075626c69636174696f6e206c6f6e6765766974792e2041647665727469736572732062656e656669742066726f6d206120746172676574656420646973747269627574696f6e206174207665727920636f6d70657469746976652070726963657320616c736f20686176696e67206164646974696f6e616c206578706f73757265207468726f756768206469676974616c20636f7069657320617661696c61626c6520746f2076696577206f6e2074686520776562736974652e200d0a46726565207370616365206973206d61646520617661696c61626c6520696e20657665727920697373756520666f72206368617274697469657320616e64206e6f6e2d70726f666974206d616b696e67206f7267616e69736174696f6e732e0d0a, 4, 1, 1, 1, 1, 0, 0, 0, 0, '', '', 0x436f6e746163742063616e206265206d61646520766961206f7572207765627369746520617420616e792074696d65207777772e62726f616473746f6e656c696e6b2e636f2e756b20206f722074656c6570686f6e652038616d20e28093203820706d204d6f6e202d20536174, ''),
(39, 'David', 'Anderson', 'bnet', 'Broadstone.net', '67 Lytchett Drive', '', 'Broadstone', 'Dorset', 'BH18 9NP', 'UK', '01202-697376', '', '', 'www.broadstone.net', 0, 0, 40, 'bronze', NULL, 0x42726f616473746f6e65204e657420697320746865206c6f63616c20636f6d6d756e6974792077656273697465, 0x42726f616473746f6e65204e657420697320746865206c6f63616c20636f6d6d756e697479207765627369746520666f722042726f616473746f6e652c2070726f766964696e6720696e666f726d6174696f6e206f6e20616c6c206c6f63616c206368617269747920616e6420636f6d6d756e697479206f7267616e69736174696f6e732c20706c75732061206461746162617365206f6620627573696e65737365732e205370656369616c20656d70686173697320697320676976656e20746f206d656d62657273206f662042726f616473746f6e65204368616d626572206f66205472616465206d656d626572732e200d0a0d0a42726f616473746f6e65204e657420776173206372656174656420696e203139393920616e6420686173206265656e206163746976656c792075706461746564206f6e206120726567756c617220626173697320666f7220746865206c6173742031342079656172732e0d0a0d0a4f66666572733a2046726565207765627369746573206372656174656420666f722063686172697479206f7220636f6d6d756e697479206f7267616e69736174696f6e7320626173656420696e2042726f616473746f6e652e2054686572652077696c6c206265206120736d616c6c2063686172676520666f72207265676973746572696e6720616e20696e7465726e657420646f6d61696e206e616d6520616e6420666f722020686f7374696e672074686520736974652e20416c7465726e61746976656c792c20612073696e676c6520706167652063616e20626520616464656420746f2042726f616473746f6e65204e657420666f72206e6f206368617267652e0d0a0d0a4576656e747320696e20616e642061726f756e642042726f616473746f6e652063616e20626520616464656420746f20746865204576656e74732043616c656e6461722e204a75737420636f6d706c6574652074686520666f726d206f6e20687474703a2f2f7777772e62726f616473746f6e652e6e65742f636f6e746163742e68746d0d0a, 1, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '', '', '', ''),
(40, 'Jason', 'Shah', 'bnews', 'Broadstone News', '167 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DH', 'UK', '01202-699888', '', '', '', NULL, NULL, 41, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(41, 'Chris', 'Collier', 'bphysio', 'Broadstone Physiotherapy and Hydrotherapy', '24 Story Lane', '', 'Broadstone', 'Dorset', 'BH18 8EQ', 'UK', '01202-693199', '', '', 'www.broadstonephysiotherapy.com', 50.7609, -1.99412, 42, 'bronze', NULL, '', '', 1, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(42, 'Mark', 'Wheadon', 'btravel', 'Broadstone Travel Worldchoice', '209 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DN', 'UK', '01202-694081', '', '', 'www.worldchoice.co.uk', 50.7609, -1.9954, 43, 'bronze', NULL, '', '', 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(43, 'Mark ', 'Pratt', 'bblinds', 'Broadview Blinds', '57 Hatchpond Road', 'Nuffield Industrial Estate', 'POOLE', 'Dorset', 'BH17 7JZ', 'UK', '01202-679012', '01202-671885', '', 'www.broadviewblinds.co.uk', NULL, NULL, 44, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(44, 'Julia', 'Pilkington', 'budgens', 'Budgens of Broadstone', '182 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DP', 'UK', '01202-695822', '', '', 'www.budgens.co.uk', 50.7591, -1.99347, 45, 'bronze', NULL, 0x53757065726d61726b657420, 0x53757065726d61726b657420, 0, NULL, 1, 1, 1, 1, 1, 0, 0, '', '', '', ''),
(45, 'Sarah', 'Jessop', 'burwood', 'Burwood Nursing Home', '100 Dunyeats Road', '', 'Broadstone', 'Dorset', 'BH18 8AL', 'UK', '01202-693224', '', '', 'www.burwoodnursinghome.co.uk', NULL, NULL, 46, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(52, 'Admin', 'Test', 'atester', 'Admin Merchant', '5 Warburton Road', '', '', 'Dorset', 'BH17 8SD', 'UK', '01202682227', '', '', 'http://www.admin.com', 0, 0, 1, 'bronze', NULL, 0x61646d696e2074657374, 0x61646d696e2074657374696e67, 6, NULL, 1, NULL, NULL, NULL, 0, 0, 0, '', '', '', ''),
(53, 'tim', 'fisher', 'jijsss', 'Tartan Tangerine Limited', 'ij', 'ij', 'ij', 'ij', 'ij', 'i', 'ij', 'ij', 'ij', 'ij', 50.7633, -1.9953, 49, 'gold', NULL, 0x54617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d20, 0x54617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2054617274616e2054616e676572696e6520447265616d2e, 72, 1, NULL, NULL, NULL, NULL, 0, 1, 0, '', '', '', ''),
(54, 'jh', 'kjh', 'kjh', 'kjh', 'kjh', 'kjh', 'kjh', 'kjh', 'kjh', 'kjh', 'kjh', 'kjh', 'kjh', 'kjh', NULL, NULL, 22, 'bronze', NULL, 0x6b6a68, 0x6b6a68, 3, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(55, 'Indrathayalan (Dayalan)', 'Satkunanathan', 'candychocs', 'Candy Chocs', '206a Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DP', 'UK', '', '', '', '', 50.7606, -1.99468, 94, 'bronze', NULL, '', '', 2, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(56, 'Jane ', 'Millward', 'chclink', 'Canford Heath and Creekmoor Link', '5 Spindle Close', '', 'Broadstone', 'Dorset', 'BH18 9WJ', 'UK', '01202 693747', '', '', 'www.canfordheathlink.co.uk', 0, 0, 50, 'bronze', NULL, 0x436f6d6d756e697479204d6167617a696e657320616e6420427573696e657373204469726563746f7269657320666f722074686520424831372061726561, 0x436f6d6d756e697479204d6167617a696e657320616e6420427573696e657373204469726563746f7269657320666f722074686520424831372061726561, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '', '', 0x392e3030616d20e2809320362e3030706d204d6f6e64617920746f20467269646179, ''),
(57, 'Matthew', 'Horne', 'caesars', 'Caesars Bistro', '15 Dunyeats Road', '', 'Broadstone', 'Dorset', 'BH18 8AA', 'UK', '01202 694343', '', '', 'www.caesarsbistro.co.uk', 50.7624, -1.99456, 51, 'bronze', NULL, '', '', 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(58, 'Emma ', 'Hamilton Cole', 'colesmiller', 'Coles Miller Solicitors', 'Arrowsmith Court', '10 Station Approach', 'Broadstone', 'Dorset', 'BH18 8AX', 'UK', '01202 694891', '01202 694920', '', 'www.coles-miller.co.uk', 50.763, -1.99525, 52, 'bronze', NULL, 0x596f7572206c6177206669726d20666f72206c696665, 0x417420436f6c6573204d696c6c6572207765207072696465206f757273656c766573206265696e6720796f7572206c6177206669726d20666f72206c6966652e204173206f6e65206f6620446f727365745c2773206c656164696e67206c6177206669726d732077697468206f66666963657320696e2042726f616473746f6e652c20506f6f6c652c20426f75726e656d6f7574682c20436861726d696e7374657220616e642057696d626f726e6520796f752063616e2062652073757265206f6620726563656976696e67207468652062657374206c6567616c20616476696365207768657468657220697420626520666f7220796f757220627573696e657373206f7220706572736f6e616c206c6966652e0d0a0d0ae280a2204d6f76696e6720486f6d650d0ae280a22046616d696c79204d6174746572730d0ae280a2204d616b696e6720612057696c6c0d0ae280a220536166656775617264696e6720796f7572204573746174650d0ae280a220506572736f6e616c20496e6a75727920436c61696d730d0ae280a220436c696e6963616c204e65676c6967656e63650d0ae280a2204c6561736520457874656e73696f6e730d0ae280a22046726565686f6c642050757263686173650d0ae280a220436f6d6d65726369616c202620427573696e657373204c61770d0ae280a220456d706c6f796d656e74204c61770d0ae280a220506c616e6e696e670d0a, 1, 1, 1, 1, 1, NULL, 0, 0, 0, '', '', 0x4d6f6e64617920e28093204672696461792039616d20e280932035706d, ''),
(59, 'Jerry ', 'Hayes', 'crsdorset', 'Computer Repair Services (Dorset)', 'Unit 4', '13b Moor Road', 'Broadstone', 'Dorset', 'BH18 8AZ', 'UK', '01202 699116', '', '', 'www.crs.dorset.co.uk', NULL, NULL, 53, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(60, 'Phil', 'Howlet', 'costa', 'Costa Coffee', '218 – 220 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DX', 'UK', '01202 699627', '', '', 'www.costa.co.uk', 50.7615, -1.99564, 54, 'bronze', NULL, '', '', 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(61, 'Claire', 'Welch', 'countryfl', 'Country Florist', '200 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DP', 'UK', '01202 659052', '01202 601501', '', 'www.countryfloristbroadstone.co.uk', NULL, NULL, 55, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(62, 'Daniel', 'Pettitt', 'daniels', 'Daniels Fish and Chips Ltd', '179 – 181 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DH', 'UK', '01202 694798', '', '', '', 50.76, -1.9945, 56, 'bronze', NULL, 0x4669736820616e6420436869702073686f7020, 0x57652068617665206265656e2073657276696e672067726561742074617374696e67206669736820616e642063686970732073696e636520313939362e20447572696e6720746861742074696d6520776520686176652067726f776e20746f20372073686f70732e2020496e20417072696c203230313220776520707572636861736564207468652073686f7020696e2042726f616473746f6e652077686963682063616d65206f6e20746f20746865206d61726b65742064756520746f20746865207265746972656d656e74206f6620746865206f776e6572732e2020447572696e672074686520417574756d6e206f66203230313220776520656d6261726b6564206f6e20612066756c6c207265667572626973686d656e74206f66207468652073746f726520616e64206f6e207468652032357468204e6f76656d62657220323031322077652072652d6f70656e65642061732044616e69656c73204669736820262043686970732e200d0a0d0a57652061726520636f6d6d697474656420746f207573696e672074686520766572792062657374206669736820617661696c61626c65206f6e20746865206d61726b657420616e6420697420616c7761797320636f6d65732066726f6d207375737461696e61626c6520736f75726365732e200d0a54686520706f7461746f657320776520757365206172652067656e6572616c6c79204d617269732050697065727320776869636820617265206f6620636f757273652066616d6f757320666f722074686569722063726973707920636f6174696e6720616e6420736f66742c20666c7566667920696e7369646573206372656174696e6720746865207065726665637420636869702e20546865726520617265206365727461696e2074696d6573206f66207468652079656172207768656e204d617269732050697065727320617265206e6f7420617661696c61626c65206f7220617420746865697220626573742c2061742074686573652074696d65732077652063686f6f73652066726f6d206f7468657220766172696574696573206f6620706f7461746f2077686963682068617665206265656e2067726f776e207370656369666963616c6c7920746f2070726f6475636520612067726561742074617374696e6720636869702e20546865207175616c697479206f6620706f7461746f657320616e64207468652077617920746861742074686579206672792063616e206368616e676520616c6d6f7374207765656b206279207765656b20616e642044616e69656c20706572736f6e616c6c792074617374657320616e642073656c6563747320746865206265737420706f737369626c6520706f7461746f657320746f20656e7375726520796f752077696c6c2062652067657474696e6720746865206265737420706f737369626c652063686970732e202e0d0a, 0, 1, 1, NULL, NULL, NULL, 0, 0, 0, '', '', 0x4d6f6e64617920e280932053617475726461792031312e3330616d20e2809320322e3030706d202620352e3030706d20e2809320392e3330706d2009090953756e64617920352e3030706d20e2809320382e3030706d, ''),
(63, 'Katie', 'Dudley', 'dikinson', 'Dickinson Manser Solicitors', '221 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DN', 'UK', '01202 692308', '01202 601353', '', 'www.dickinsonmanser.co.uk', 50.7612, -1.99569, 57, 'bronze', NULL, '', '', 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(64, 'Carly ', 'Wing', 'dorsetlifts', 'Dorset Lifts Limited', '3 Arrowsmith Court', '', 'Broadstone', 'Dorset', 'BH18 8AX', 'UK', '01202 650361', '', '', 'www.dorsetlifts.com', NULL, NULL, 58, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(65, 'Clive ', 'Coldwell', 'dreamdoors', 'Dream Doors', '185 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DH', 'UK', '01202 695856', '', '', 'www.dreamdoorsltd.co.uk', 0, 0, 59, 'bronze', NULL, 0x4120717569636b2c206561737920616e642065636f6e6f6d6963616c20616c7465726e617469766520746f207265706c6163696e6720612077686f6c65206b69746368656e, 0x447265616d20446f6f727320696e20506f6f6c6520697320612066616d696c792072756e20627573696e6573732c207965742068617320746865206261636b696e67206f662061204e6174696f6e616c20636f6d70616e792e205765206f66666572206120717569636b2c206561737920616e642065636f6e6f6d6963616c20616c7465726e617469766520746f207265706c6163696e6720612077686f6c65206b69746368656e2e204279207265706c6163696e672074686520646f6f727320616e6420647261776572732077652063616e207472616e73666f726d20796f7572206b69746368656e2061742061206672616374696f6e206f662074686520636f7374206f66206120636f6d706c657465206e6577206b69746368656e202d206f6674656e20736176696e6720796f7520757020746f20353025202d203630252e20416c6c206f757220646f6f727320617265206d61646520746f206d656173757265202d2073696d706c792063686f6f73652066726f6d206f757220657874656e736976652072616e6765206f66207374796c657320616e6420636f6c6f75727320616e64206f75722066756c6c7920747261696e6564206372616674736d656e2077696c6c20646f2074686520726573742c20736176696e6720796f752074696d652c20686173736c6520616e64207374726573732e20576520616c736f206f66666572206120776964652072616e6765206f6620776f726b746f70732c2073696e6b732c20746170732c206170706c69616e6365732c206c69676874696e6720616e6420666c6f6f72696e672c20616c6c20696e7374616c6c656420627920736b696c6c6564207472616465736d656e2e200d0a0d0a5765206f666665722061206e6f206f626c69676174696f6e2c20667265652071756f746174696f6e207365727669636520776865726520637573746f6d657273206172652061626c6520746f207365652061637475616c2073616d706c657320696e2074686520636f6d666f7274206f66207468656972206f776e20686f6d652c20616c6c6f77696e67207468656d20746f206765742061206265747465722069646561206f6620636f6c6f75727320616e64207374796c657320746861742077696c6c2073756974207468656972207461737465732e2057652063616e206966206e656564656420637265617465204341442028436f6d70757465722041696465642044657369676e2920696d6167657320746f2073686f7720637573746f6d65727320776861742061206368616e6765206f66206c61796f7574206f7220616e20656e746972656c79206e6577206b69746368656e2077696c6c206c6f6f6b206c696b650d0a, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '', '', 0x2d46726920393a303020e2809320353a30302053617420393a303020e2809320323a3030, ''),
(66, 'Alex', 'Diben', 'expectbest', 'Expect Best', '9 Virage Business Park', '132-134 Stanley Green Road', 'POOLE', 'Dorset', 'BH15 3AP', 'UK', '01202 237072', '', '07795 326599', 'www.expectbest.co.uk', NULL, NULL, 60, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, '');
INSERT INTO `merchant` (`id`, `firstname`, `lastname`, `username`, `companyname`, `address1`, `address2`, `address3`, `county`, `postcode`, `country`, `phone`, `fax`, `mobile`, `url`, `lat`, `long`, `user_id`, `level`, `deleted`, `summary`, `description`, `pageviews`, `image1`, `image2`, `image3`, `image4`, `image5`, `image6`, `association`, `sticky`, `twitter`, `facebook`, `extrafield1`, `keywords`) VALUES
(67, 'Helen', 'Molloy', 'forum', 'Forum Jewellers', '184e Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DD', 'UK', '01202 600350', '', '', 'www.forumjewellers.co.uk', 50.7598, -1.99389, 61, 'bronze', NULL, 0x4a6577656c6c65727320, 0x546869732066616d696c792072756e20627573696e6573732c2065737461626c697368656420696e20313938322c20697320616e20696e646570656e64656e74206a6577656c6c65722077697468206f76657220343020796561727320657870657269656e63652e204c696b65206d616e79206c6f63616c20736d616c6c20627573696e65737365732077652070726f7669646520796f7520776974682074686520696e646976696475616c20617474656e74696f6e20796f75206465736972652c2077686963682068617320616c6c2062757420646973617070656172656420696e20746f6461797320e28098686967682073747265657420636861696ee280992073686f70732e20200d0a0d0a466f72756d204a6577656c6c657273206f66666572732061207375706572622072616e6765206f66207175616c697479206a6577656c6c65727920616e64207761746368657320616c6f6e6720776974682067696674206974656d7320746f207375697420616c6c206f63636173696f6e732e20416c6c206f66206f75722070726f6475637473206172652073656c656374656420666f72207468656972207175616c69747920616e642076616c756520666f72206d6f6e657920746f20796f752074686520637573746f6d657220726174686572207468616e2074686520636865617065737420636861696e2073746f7265206974656d732e, 1, 1, 1, 1, 1, 1, 0, 0, 0, '', '', 0x392e303020e2809320352e3030204d6f6e64617920746f204672696461792028392e3330205765646e657364617929, ''),
(68, 'Roy', 'Fraser', 'fraser', 'Fraser Portraits', '206 The Broadway', '', 'Broadstone', 'Dorset', 'BH18 8DP', 'UK', '01202 692433', '', '', 'www.fraserportraits.com', 50.7614, -1.99567, 62, 'bronze', NULL, 0x506f727472616974732070686f746f677261706879, 0x46726173657220506f7274726169747320696e2042726f616473746f6e6520756e64657274616b652066616d696c7920616e64206368696c6420706f7274726169742073657373696f6e7320696e2074686569722061697220636f6e646974696f6e65642073747564696f2e205468657920616c736f20756e64657274616b65207363686f6f6c2070686f746f67726170687920636f6e747261637473207468726f7567686f75742074686520554b2077697468207468656972207465616d206f6620392070686f746f67726170686572732e, 1, 1, 1, 1, 1, 1, 0, 0, 0, '', '', 0x4d6f6e2d204672692039202d20342e3330706d2c2054687572732065766520756e74696c2039706d20616e6420536174757264617920392d2031706d, ''),
(69, 'Gillian ', 'Linford', 'linfords', 'Gillian Linford Solicitors', '5a Dunyeats Road', '', 'Broadstone', 'Dorset', 'BH18 8AA', 'UK', '01202 254090', '', '', 'www.gillianlinfordlaw.com', 50.7624, -1.99509, 63, 'bronze', NULL, 0x5370656369616c6973696e67206578636c75736976656c7920696e2068656c70696e672070656f706c6520736574207468656972206166666169727320696e206f72646572, 0x57652061726520746865206f6e6c7920736f6c696369746f7273e2809920707261637469636520696e207468652042482061726561207370656369616c6973696e67206578636c75736976656c7920696e2068656c70696e672070656f706c6520736574207468656972206166666169727320696e206f726465722c20706c616e20666f72207468652066757475726520616e642061636869657665207175616c697479206f66206c6966652c206e6f7720696e20776861746576657220736974756174696f6e20796f752061726520696e2e2054686174206d65616e7320776520636f7665723a0d0ae280a20957696c6c732c2070726f6261746520616e6420696e6865726974616e6365207461780d0ae280a209506f77657273206f66204174746f726e65792c2074686520436f757274206f662050726f74656374696f6e20616e64204c6976696e672057696c6c732f416476616e6365204465636973696f6e730d0ae280a209436172652070726f766973696f6e2c2072696768747320696e2063617265206173736573736d656e74730d0a47656e6572616c6c7920737065616b696e67206f757220636c69656e747320617265206f6c6465722070656f706c6520616e642074686569722066616d696c6965732c20627574207765206861766520706c656e7479206f6620796f756e67657220636c69656e747320746f6f2e200d0a57652073656520616c6c206f757220636c69656e747320696e207468656972206f776e20686f6d6573206174206e6f20657874726120636f73742c20696e636c7564696e67207765656b656e6473206f72206576656e696e67732069662072657175697265642c20616e64206f7572206669727374206d656574696e6720697320667265652e0d0a47696c6c69616e20686173203232207965617273e2809920657870657269656e6365206173206120736f6c696369746f7220696e207468657365207370656369616c69736d7320616e6420697320612066756c6c206d656d626572206f662074686520536f6369657479206f6620547275737420616e64204573746174652050726163746974696f6e65727320616e6420536f6c696369746f727320666f722074686520456c6465726c792e20576520686f6c6420746865204c657863656c207072616374696365206d616e6167656d656e74207374616e646172642c206177617264656420616e6e75616c6c7920627920746865204c617720536f63696574792e200d0a0d0a, 1, 1, 1, 1, 1, NULL, 0, 0, 0, '', '', 0x506c656173652070686f6e6520283031323032203235343039302920666f7220616e206170706f696e746d656e74206f7220746f20636865636b20617661696c6162696c6974792c2061732047696c6c69616e206d61792077656c6c206265206f757420736565696e6720636c69656e747320616e642065766572796f6e6520656c736520776f726b7320706172742074696d652e205468617420736169642c206f6674656e20746865726520697320736f6d656f6e6520696e20746865206f666669636520696e20746865206d6f726e696e67732e, ''),
(70, 'Erica', 'McKeer', 'glow', 'Glow Health and Beauty', '215a Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DN', 'UK', '01202 699004', '', '', 'www.beautyatglow.co.uk', 50.761, -1.99552, 64, 'bronze', NULL, 0x476c6f77204865616c746820616e6420426561757479, 0x417420476c6f77207765206172652070726f756420746f206f6666657220736f6d65206f6620746865206d6f737420686967686c79207265676172646564200d0a74726561746d656e747320616e642070726f647563747320696e2074686520696e6475737472792e204f757220667269656e646c79207465616d206f66200d0a70726f66657373696f6e616c2074686572617069737473206f6666657220666972737420636c6173732c207461696c6f7265642074726561746d656e747320746f2073756974200d0a6f757220636c69656e7473206e656564732e202057652077616e74206f757220636c69656e747320746f2072656c61782c20696e20746865206b6e6f776c656467652c200d0a7468617420616c6c206f66206f757220686967686c79207175616c696669656420746865726170697374732077696c6c20776f726b20746f206d616b65207468656972200d0a7669736974206173207370656369616c2061732074686579206172652e2020476c6f7720776173206177617264656420e2809842657374204f766572616c6c200d0a496e646570656e64656e742052657461696c6572e2809920616e642072756e6e657220757020666f7220e280984265737420496e646570656e64656e7420437573746f6d6572200d0a53657276696365e2809920627920546f70206f66207468652053686f70732c20506f6f6c652052657461696c2041776172647320323031322e200d0a, 1, 1, 1, 1, 1, 1, 0, 0, 0, '', '', 0x54756573646179202020202039616d202020e2809320352e3330706d20205765646e65736461792020203130616d2020e280932038706d202054687572736461792020202039616d202020e2809320352e3330706d20204672696461792020202039616d202020e2809320352e3330706d202053617475726461792020202039616d2020202d2034706d, ''),
(71, 'Russell ', 'Sauders', 'goahead', 'Go-Ahead Investment Co Ltd', '20 Mallow Close', '', 'Broadstone', 'Dorset', 'BH18 9NT', 'UK', '01202 937735', '', '', '', NULL, NULL, 65, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(72, 'Sally', 'Carson', 'goadsby', 'Goadsby Estate Agents', '177 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DH', 'UK', '01202 692145', '', '', 'www.goadsby.com', 50.7599, -1.99448, 66, 'bronze', NULL, 0x457374617465204167656e747320, 0x45737461626c697368656420696e203139353820476f6164736279206e6f7720636f6d707269736573206f66203234206f66666963657320696e2063656e7472616c20736f75746865726e20456e676c616e642e20476f61647362792068617665207370656369616c697374207465616d7320636f766572696e6720616c6c2061737065637473206f66207265736964656e7469616c20616e6420636f6d6d65726369616c20627573696e6573732c20696e636c7564696e672053616c65732c204c657474696e67732c204c616e6420616e64204d6f7274676167652053657276696365732e, 1, 1, 1, 1, NULL, NULL, 0, 0, 0, '', '', 0x4d6f6e64617920746f20467269646179202d2020382e3435616d20e2809320362e3030706d20536174757264617920e2809320392e3030616d20e2809320352e3030706d2053756e64617920e280932031302e3030616d20e2809320342e3030706d, ''),
(73, 'Betina', 'Manchester', 'greggs', 'Greggs', '192 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DP', 'UK', '01202 698490', '', '', '', NULL, NULL, 67, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(74, 'Maris', 'Gurney', 'ahgf', 'Alfred H Griffin Funeral Directors', '6 Dunyeats Road', '', 'Broadstone', 'Dorset', 'BH18 8AG', 'UK', '01202 693275', '', '', 'www.co-operativefuneralcare.co.uk', NULL, NULL, 68, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(75, 'Paul', 'Klein', 'habitat', 'Habitat Projects Ltd.', '35 Dogwood Road', '', 'Broadstone', 'Dorset', 'BH18 9PA', 'UK', '', '', '07921 944360', 'www.habitatprojects.co.uk', NULL, NULL, 69, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(76, 'Andrew ', 'Hillier', 'hillier', 'Hillier Wilson Estate Agents', '188 The Broadway', '', 'Broadstone', 'Dorset', 'BH18 8DP', 'UK', '01202 693388', '01202 695577', '', 'www.hillierwilson.co.uk', NULL, NULL, 70, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(77, 'Ming', 'Pang', 'hochoy', 'Ho Choy Chinese Restaurant', '207 The Broadway', '', 'Broadstone', 'Dorset', 'BH18 8DN', 'UK', '01202 693952', '', '', 'www.take-a-way.co.uk/menu/1163/ho-choys/', 50.7608, -1.99534, 71, 'bronze', NULL, '', '', 3, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(78, 'Neil', 'White', 'ist', 'International Safety Technology Ltd', '11 Moor Road', '', 'Broadstone', 'Dorset', 'BH18 8AX', 'UK', '01202 692775', '', '', 'www.ist-limited.com', 50.7638, -1.99406, 72, 'bronze', NULL, '', '', 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(79, 'Irene', 'Whittle', 'irene', 'Irene Fashions Ltd', '3 Station Approach', '', 'Broadstone', 'Dorset', 'BH18 8AZ', 'UK', '01202 659700', '', '', 'www.ireneofbroadstone.co.uk', 50.7623, -1.99548, 73, 'bronze', NULL, 0x46617368696f6e20616e64207370656369616c206f63636173696f6e2077656172, 0x4972656e65e280997320686f757365732074776f20666c6f6f7273206f662066617368696f6e20616e64207370656369616c206f63636173696f6e20776561722c20696e636c7564696e672073686f65732c206a6577656c6c6572792c206163636573736f7269657320616e64206d696c6c696e6572792e205765207370656369616c69736520696e206d6f74686572206f6620746865206272696465206f7574666974732e200d0a0d0a57652068617665206120667269656e646c79207465616d206f662073746166662077686f2061726520616c7761797320686170707920746f2068656c702e20506c6561736520766973697420757320666f72206120667269656e646c792077656c636f6d652e0d0a, 1, 1, 1, 1, NULL, NULL, 0, 0, 0, '', '', 0x4f70656e696e6720686f7572733a2053617475726461792031302d3420547565736461792d4672696461792031302d3520436c6f7365642053756e64617920616e64204d6f6e646179, ''),
(80, 'Andrew ', 'Ayles', 'johnsons', 'Johnson’s Dry Cleaners', '225 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DN', 'UK', '01202 658319', '', '', 'www.johnsonscleaners.com/broadstone', NULL, NULL, 74, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(81, 'Geoff', 'Rice', 'khd', 'KHD Hairdressing', 'Manor House', '1 Macaulay Road', 'Broadstone', 'Dorset', 'BH18 8AS', 'UK', '01202 695427', '', '', 'www.khdhairdressing.co.uk', 50.7618, -1.99561, 75, 'bronze', NULL, 0x48616972647265737365727320, 0x4b484420486169726472657373696e6720686173206265656e2065737461626c697368656420696e2042726f616473746f6e6520666f7220323520796561727320696e2077686963682074696d652077655c5c5c27766520626f7567687420746f6765746865722065787065727473207370656369616c6973696e6720696e20616c6c206669656c6473206f6620686169726472657373696e672c20636c617373696320616e6420636f6e74656d706f726172792063757474696e672c20636f6c6f7572696e6720627920636f6c6f757220657870657274732c207370656369616c206f63636173696f6e20686169722c20636c697020696e20657874656e73696f6e732c207769672066697474696e6720616e6420637574207468726f6174207368617665732e0d0a5573696e6720746865206c617465737420636f6c6f757220616e64206861697220636172652070726f64756374732066726f6d20476f6c6477656c6c20616e64204b4d532063616c69666f726e69612077652077696c6c2064656c6976657220746865206c6f6f6b20616e64206665656c20796f7572206861697220686173206265656e2077616974696e6720666f722e20536f206c65742075732068656c7020796f75206c6f6f6b20616e64206665656c2067726561742e0d0a416c6c206f7572207374796c697374732061726520747261696e656420746f207468652068696768657374207374616e646172647320627920476f6c6477656c6c2061636164656d79206c6f6e646f6e20616e642061726520616c6c20737461746520726567697374657265642c20536f20796f752063616e20626520726573742061737375726564207468617420616674657220796f757220636f6c6f757220736572766963652077652077696c6c206c6561766520796f75206c6f6f6b696e6720796f757220626573742e0d0a, 1, 1, 1, 1, 1, 1, 1, 0, 0, '', '', 0x6d6f6e2d20636c6f7365642009095475657320392d35202020776564392d352e333020202020746875727320392d362e3330202066726920392d36202073617420382e33302d34, ''),
(82, 'Christine', 'Edwards', 'leonardo', 'The Leonardo Trust', '5 Dunyeats Road', '', 'Broadstone', 'Dorset', 'BH18 8AA', 'UK', '01202 698325', '', '', 'www.leonardotrust.org', NULL, NULL, 76, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(83, 'Claire', 'Rossiter', 'lloydsph', 'Lloyds Pharmacy', '192c Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DP', 'UK', '01202 692320', '', '', '', NULL, NULL, 77, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(84, 'Lee', 'Marsden', 'marsden', 'Marsden Flooring', '199 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DH', 'UK', '01202 691951', '', '', 'www.marsdenflooring.co.uk', 50.76, -1.99463, 78, 'bronze', NULL, 0x596f7572206c6f63616c2043617270657420616e6420466c6f6f72696e67205370656369616c69737473, 0x0d0a5468697320697320612066616d696c792072756e2063617270657420616e6420666c6f6f72696e6720627573696e6573732065737461626c6973686564206f7665722032302079656172732061676f2c20776974682068757362616e6420616e642077696665207465616d2054726973682026204c6565204d61727364656e206174207468652068656c6d2c2077686f206861766520612076617374206b6e6f776c65646765206f66207468652063617270657420616e6420666c6f6f72696e6720696e6475737472792e200d0a52657065617420637573746f6d65727320616e64207265636f6d6d656e646174696f6e732061726520746865206261736973206f6620746865697220627573696e6573732e205769746820736f206d756368206f662074686569722063617270657420616e6420666c6f6f72696e6720747261646520636f6d696e672066726f6d2070726576696f757320637573746f6d6572732c2074686579206b6e6f7720686f7720696d706f7274616e742061207175616c69747920736572766963652069732e0d0a0d0a5c2257652073747269766520746f2070726f766964652070726f66657373696f6e616c69736d2c2065787065727469736520616e6420696d706f7274616e746c79206120706572736f6e616c6973656420617070726f6163682e204f75722073686f77726f6f6d206f6e6c7920696e636c756465732074686520626573742070726f647563747320736f20697420697320657373656e7469616c2074686174207765206f6666657220746865206265737420736572766963657320746f20636f6d706c696d656e74207468656de2809d2e0d0a576520756e6465727374616e642074686174207374616e64696e6720696e20612073686f77726f6f6d2c20747279696e6720746f20696d6167696e6520686f77206120636172706574206f7220666c6f6f722077696c6c206c6f6f6b20696e20796f757220686f6d65206f7220636f6d6d65726369616c207072656d697365732063616e206265206461756e74696e672e20576520736f6c76652074686973206279206272696e67696e672073616d706c657320616e642062726f63687572657320746f20796f752e2054686973206d65616e732077652063616e206f6666657220796f75206f75722066726565206578706572742061647669636520746f2068656c7020796f752073656c6563742074686520626573742063686f69636573206261736564206f6e20796f7572206e6565647320696e2074686520656e7669726f6e6d656e742077686572652074686520666c6f6f72696e672077696c6c2061637475616c6c7920626520757365642e20497420697320696d706f7274616e7420746f207265636569766520696d7061727469616c2061647669636520696e20796f7572206f776e20737572726f756e64696e67732e20596f7520776f6ee28099742066696e6420616e792070757368792073616c657320706572736f6e206865726521200d0a4f75722042726f616473746f6e652073686f77726f6f6d20686173206120687567652076617269657479206f66206361727065742073616d706c65732c206361727065742074696c65732c2076696e796c2c20776f6f642026206c616d696e61746520666c6f6f72696e67207769746820736f6d657468696e6720746f207375697420616c6c206275646765747320616e64207461737465732e20576520616c736f206861766520746865206c61746573742073616d706c6573206f66204b61726e6465616e20616e6420416d7469636fe2809973206c75787572792064657369676e657220666c6f6f7273206f6e20646973706c617920776974682073616d706c657320616e642062726f63687572657320616c7761797320617661696c61626c6520746f2061737369737420796f757220666c6f6f72696e67206465636973696f6e732e200d0a576520616c736f2073746f636b20766172696f757320726f6f6d2073697a6564206361727065742c2072756720262076696e796c2072656d6e616e74732e0d0a506c65617365207573652074686520636865636b617472616465206c696e6b20746f2073656520637573746f6d657220666565646261636b2e20687474703a2f2f7777772e636865636b6174726164652e636f6d2f4d61727364656e466c6f6f72696e672f0d0a, 1, 1, NULL, NULL, NULL, NULL, 0, 0, 0, '', '', 0x4f70656e696e6720686f7572733a4d6f6e646179202d204672696461792039616d202d2035706d2053617475726461792039616d202d2032706d, ''),
(85, 'Sandy', 'Carley', 'mojos', 'Mojo’s Barber Shop', '7a Dunyeats Road', '', 'Broadstone', 'Dorset', 'BH18 8AA', 'UK', '01202 603770', '', '', '', 50.7624, -1.99492, 79, 'bronze', NULL, 0x4261726265722053686f70, 0x576520617265206120736d616c6c2077656c6c2065737461626c6973686564204261726265722053686f702e20204f66666572696e6720612072616e6765206f6620747261646974696f6e616c20746f206d6f6465726e20626172626572696e6720666f72206d656e20616e6420626f7973206f6620616c6c20616765732c20696e2061207761726d20616e6420667269656e646c792061746d6f7370686572652e205765207072696465206f757273656c766573206f6e20676f6f6420637573746f6d6572206361726520616e6420636f6d706574697469766520726174657320666f7220616c6c206f757220636c69656e74732e, 0, 1, 1, NULL, NULL, NULL, 0, 0, 0, '', '', 0x54756573646179202d20205765646e6573646179200939616d202d2020352e3330202020706d20202020202020202020202020202020202020202020202020202020202020202020202020546875727364617920e280932046726964617920090939616d20202d20362e30302020706d20090909536174757264617920090909382e3330616d202d2020332e3330202020706d, ''),
(86, 'Martin', 'Layton', 'msldesign', 'MSL Designs', '179 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DH', 'UK', '01202 659585', '', '07768 623399', 'www.msl-designs.com', 50.76, -1.9944, 80, 'bronze', NULL, 0x427573696e65737320262050726f6d6f74696f6e616c20436c6f7468696e6720616c6f6e672077697468204163636573736f726965732c20496e63656e7469766520476966747320616e64204d6f72652e2e, 0x436c6f7468696e6720666f7220796f757220627573696e6573730d0a0d0a47657420796f7572206d657373616765206163726f73730d0a6d736c2064657369676e73206f66666572206120776964652072616e676520616e64206d6f7374207374796c65732061726520617661696c61626c6520696e206c6164696573206669742e204272616e64696e6720796f757220636c6f7468696e6720636f756c646e5c27742062652073696d706c65722c207765206f6666657220612072616e6765206f6620736572766963657320696e636c7564696e673a20656d62726f69646572792c2073637265656e207072696e742c207472616e73666572207072696e742c206469676974616c207072696e7420706c757320696e20686f7573652044657369676e20616e6420617274776f726b2e0d0a0d0a4f6e652073746f7020736f6c7574696f6e730d0a5c22436f737420656666656374697665206164766572746973696e675c2220612067726561742077617920746f2070726f6d6f746520796f757220636f6d70616e792e204272616e64656420627573696e65737320616e642070726f6d6f74696f6e616c20636c6f7468696e672063616e20626520776f726e20627920616c6c20656d706c6f796565732c20656e737572696e6720612070726f66657373696f6e616c20696d6167652e20466f722070726f6d6f74696f6e7320616e64206576656e747320796f752063616e20616c736f2066696e64206120776964652072616e6765206f6620696e63656e746976652067696674732e20486f77657665722c20696620796f752063616e5c27742066696e642069742077652063616e20736f75726365206974210d0a0d0a5468657265666f72652c20796f752063616e20626520636f6e666964656e74206f6620612070726f66657373696f6e616c20736572766963652e0d0a, 2, 1, 1, NULL, NULL, NULL, 0, 0, 0, '', '', 0x4d6f6e64617920e28093204672696461792020392e3030616d20e2809320352e3030706d, ''),
(87, 'Alan and Sasha', 'Morgan', 'newdriver', 'New Driver Schools of Motoring', '13a Moor Road', '', 'Broadstone', 'Dorset', 'BH18 8AZ', 'UK', '01202 659626', '', '', 'www.newdriverschools.co.uk', NULL, NULL, 81, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(88, 'Ian ', 'Butler', 'oakley', 'Oakley Village Butchers', '229 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DN', 'UK', '01202 693096', '', '', '', 50.7614, -1.99591, 82, 'bronze', NULL, 0x4c6f63616c2066616d696c7920627574636865727320, 0x5765206f70656e6564204f616b6c657920427574636865727320696e204f63746f626572203230303420616e64206465636964656420746f20676f20646f776e2074686520467265652d52616e676520726f7574652e20496e204665627275617279203230303520776520746f6f6b206f75722066697273742064656c6976657279206f6620526564204465766f6e20426565662066726f6d20486f72746f6e2c2066697665206d696c6573206e6f727468206f662057696d626f726e652e20576520617265206f6e65206f66206f6e6c7920612068616e6466756c206f66206275746368657273206163637265646974656420627920746865204465766f6e2052656420427265656465727320536f63696574792e20496e204f63746f62657220323030352c2062656361757365206f66206f75722072657075746174696f6e20666f722073746f636b696e67206c6f63616c206d6561742c207765207765726520617070726f616368656420627920526963686172642048617264696e67206f66204b6e696768746f6e204661726d2032206d696c65732066726f6d20746865204d65726c65792073686f7020746f2073656c6c2074686569722043616e666f7264204d61676e61204c616d622e20546865207472696f2077617320636f6d706c65746564207769746820746865206172726976616c206f662074686520436f726665204d756c6c656e20506f726b2066726f6d20427269616e20427261646c65792c2043616e647973204c616e652c20436f726665204d756c6c656e20696e20323031312e20546869732066616e74617374696320506f726b2c20426565662026204c616d6220697320747261646974696f6e616c6c7920726169736564206f6e20746865206661726d7320616e64207468656e2068756d616e656c7920736c61756768746572656420617420746865206c6f63616c2066616d696c792072756e20736c61756768746572686f7573652e0d0a0d0a53696e6365207765206f70656e656420696e204d65726c657920736576656e2079656172732061676f2c207468652073686f702068617320676f6e652066726f6d20737472656e67746820746f20737472656e6774682e205374617274696e6720776974682061207374616666206f66206a7573742074687265652066756c6c2d74696d6572732c2077652068617665207365656e2074686520627573696e6573732067726f7720616e642067726f77206675656c6c65642062792074686520637573746f6d6572732064656d616e6420666f72206c6f63616c206e61747572616c6c792072656172656420667265652d72616e6765206d6561742e2044756520746f20746869732064656d616e6420616e64207573206f757467726f77696e6720746865204d65726c65792073686f702c207765206f70656e656420616e6f746865722073686f70206f6e207468652042726f616477617920696e2042726f616473746f6e6520696e204f63746f62657220323030382e0d0a0d0a5768656e6576657220796f7520676f20696e746f207468652073686f707320796f75206e6f74696365207468657920686176652061207265616c2066616d696c79206665656c20746f207468656d2c207768657265206e6f7468696e6720697320746f6f206d7563682074726f75626c652c204d792057696665204d617279202620536f6e204c656f6e2c20616c736f20776f726b20696e207468652073686f70732074686174206e6f7720626f6173742061207374616666206f662074656e2e0d0a, 3, 1, 1, 1, NULL, NULL, 0, 0, 0, '', '', 0x547565736461792d54687572736461792037616d2d35706d204672696461792037616d2d20353a3330706d2053617475726461792037616d2d33706d2053756e6461792026204d6f6e64617920436c6f736564, ''),
(89, 'Brian', 'Clemons', 'postoffice', 'Post Office (Broadstone Stores) Ltd', '14 Dunyeats Road', '', 'Broadstone', 'Dorset', 'BH18 8AG', 'UK', '01202 693260', '', '', 'www.thepostoffice.co.uk', 50.7622, -1.99465, 83, 'bronze', NULL, 0x46756c6c20506f7374204f6666696365207365727669636573, 0x5765206f666665722066756c6c20506f7374204f666669636520736572766963657320696e636c7564696e672066756c6c2020427572656175206465204368616e67652c20436172207461782c2050617373706f727420636865636b206e2073656e642c20616c6c206d61696c20736572766963657320616e64206173736f6369617465642070726f64756374732e0d0a0d0a4f7572207072696f7269747920697320746f20736572766520616c6c206f757220637573746f6d65727320656666696369656e746c7920616e64206f66666572206120667269656e646c792077656c636f6d65200d0a576520636f6d706c696d656e74206f757220706f7374206f6666696365207769746820612072616e6765206f662073746174696f6e6172792c207175616c697479206772656574696e672063617264732c200d0a4261726761696e20626f6f6b732c20612072616e6765206f6620c2a331206c696e65732c205377656574732c20436f6c64204472696e6b732c20736561736f6e616c20676f6f647320616e64207370656369616c732e0d0a576520686176652061204469676974616c2050686f746f20426f6f74682077697468206c61746573742073697a696e6720746563686e6f6c6f677920666f722050617373706f72742f20566973612070686f746f7320616e642070686f746f636f7079696e6720666163696c69746965730d0a, 1, 1, 1, NULL, NULL, NULL, 0, 0, 0, '', '', 0x4d6f6e2d46726920392e30302d20352e33302053617420392e30302d312e3030, ''),
(90, 'Richard', 'Everitt', 'royalbl', 'Royal British legion', 'Tudor Road', '', 'Broadstone', 'Dorset', 'BH18 8AW', 'UK', '01202 692688', '', '', '', NULL, NULL, 84, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(91, 'James', 'St Quinin', 'seyward', 'Seyward Window Company Limited', 'Seyward House', 'Abingdon Road,Nuffield Industrial Estate', 'POOLE', 'Dorset', 'BH17 0UG', 'UK', '01202 665723', '', '', 'www.seywardwindows.co.uk', NULL, NULL, 85, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(92, 'Alix', 'Smith', 'somethingspecial', 'Something Special', '194 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DP', 'UK', '01202 696171', '', '', 'www.somethingspecial-gifts.co.uk', 50.7603, -1.99436, 86, 'bronze', NULL, 0x576861746576657220746865206f63636173696f6e20776520686f706520746f20686176652074686174207370656369616c2067696674206f7220636172642074686174206973206a75737420726967687420666f7220796f75, 0x576520206172652061206c6f63616c2066616d696c792d72756e20627573696e65737320616e642068617665206265656e20696e2042726f616473746f6e6520666f72206e6561726c792032352079656172732e2020576520656e6a6f792061207370656369616c2072656c6174696f6e736869702077697468206d616e79206f66206f757220637573746f6d6572732077686f20636f6d65206261636b20746f207573207965617220616674657220796561722e200d0a20200d0a576861746576657220746865206f63636173696f6e20776520686f706520746f20686176652074686174207370656369616c2067696674206f7220636172642074686174206973206a75737420726967687420666f7220796f752e20536f6d657468696e67205370656369616c206973206120676966742073686f702077697468206120776964652076617269657479206f6620676966747320666f7220616c6c200d0a6f63636173696f6e732e202057652073746f636b206a6577656c6c657279206279204e6f6d696e6174696f6e732c20616e64206f7572206772656574696e6773206361726473206172652066726f6d2077656c6c206b6e6f776e207075626c69736865727320616e642020636f766572206576657279206576656e742e20204c65617468657220676f6f6473206279204c6963686669656c6420696e636c7564652077616c6c6574732c207075727365732c2070617373706f727420616e64206f746865722020646f63756d656e7420686f6c646572732e0d0a2020200d0a4f75722067616d657320616e64206a69677361772070757a7a6c657320627920476962736f6e20617265207665727920706f70756c61722020576520616c736f206f666665722070686f746f206672616d6573206f6620616c6c2073697a657320616e6420612073656c656374696f6e206f6620686f6d65776172652c20436c6172656d6f6e742026204d617920706f7420706f757272692070726f766964657320746865206672616772616e74207363656e74206f66207468652073686f702c20616e64207468657265206973206d7563682c206d756368206d6f72652e0d0a0d0a4f757220667265652067696674207772617070696e672073657276696365206d616b65732074686520636f6d706c6574652070726573656e7420706572666563742e0d0a, 5, 1, 1, 1, 1, 1, 0, 0, 0, '', '', 0x4d6f6e64617920e2809320536174757264617920392e3030616d20e2809320352e3030706d, ''),
(93, 'Jane ', 'Wharmby', 'tapper', 'Tapper Funeral Service', '173 The Broadway', '', 'Broadstone', 'Dorset', 'BH18 8DH', 'UK', '01202 694449', '', '', 'www.tapperfuneralservice.co.uk', NULL, NULL, 87, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(94, 'Keith', 'Tempany', 'tempanys', 'Tempany’s Boutique Opticians & Contact Lens Specialists', '184 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DP', 'UK', '01202 650065', '', '', 'www.tempanys.co.uk', 50.7598, -1.99391, 88, 'bronze', NULL, 0x426f757469717565204f7074696369616e73202620436f6e74616374204c656e73205370656369616c69737473, 0x4f6e2074686520326e6420466562727561727920323031332c20617070726f78696d6174656c79203336207965617273206166746572204b65697468e2809973206669727374206d616e6167657269616c20706f737420696e2042726f616473746f6e652c206174204d656c736f6e2057696e67617465206f6e207468652042726f616477617920286e6f7720436f756e747920466c6f7269737473292c2068652068617320636f6d652066756c6c20636972636c6520616e6420686173206f6666696369616c6c79206f70656e65642c20776974682061206c6974746c652068656c702066726f6d20667269656e64732c2070617469656e747320616e6420416e6e657474652042726f6f6b65204d502c2054656d70616e79e280997320426f757469717565204f7074696369616e7320616e6420436f6e74616374204c656e73205370656369616c697374732e200d0a0d0a412072656672657368696e67206e657720636f6e6365707420666f7220616e204f70746963616c20507261637469636520776974682074686520656d706861736973207374726f6e676c79206f6e207468652066617368696f6e20656c656d656e74206f6620676c617373657320696e7465727477696e65642077697468207175616c6974792c207374796c6520616e6420696e646976696475616c69747920616e64206e6f74206a7573742061206c6162656c2c20666f722074686f73652077686f206461726520746f20626520646966666572656e742e205468697320626f7574697175652073696465206f6620746865207072616374696365206973206120737570657266696369616c206c6179657220636f766572696e672074686520636f726520696465616c206f6620616d617a696e6720736572766963652077697468207468652076657279206c617465737420746563686e6f6c6f67792e0d0a0d0a417320736f6f6e20617320796f752077616c6b20696e746f20746865207072616374696365207468652062726967687420636f6c6f75727320616e642073706163696f7573206c61796f757420696e6469636174652074686174207468697320697320676f696e6720746f206265206120756e6971756520657870657269656e63652e20546865206672616d65732061726520646973706c61796564206f6e2077656c6c206c697420666c6f6174696e67207368656c7665732c2073686f77696e67206f66662074686520636f6c6f75727320616e64207374796c657320616e64206d616b696e67207468656d207374616e64206f75742e200d0a0d0a5468656e2074686572652061726520746865206672616d6573207468656d73656c7665732c204d6f7374206f6620746865206672616d65732061726520756e6971756520746f2054656d70616e79e280997320696e207468697320617265612e204f6e652072616e67652066726f6d20616e20416d65726963616e2064657369676e657220686173206f6e6c792039206f75746c65747320696e207468652077686f6c65206f662074686520554b2c206d6f7374206f662074686f73652061726520696e204c6f6e646f6e2e205468657265206172652074686520717569726b7920616e642066756e20416e6e2065742056616c656e74696e2072616e67652c2061637475616c6c792068616e642066696e697368656420696e204672616e636520696e20616d617a696e6720636f6c6f75727320616e64207374796c6573206173206f6e6c7920746865204672656e63682063616e20646f2e205468657265206973207468652073696d706c6520796574207374796c6973682047c3b67474692072616e67652066726f6d20537769747a65726c616e642074686520756e6465727374617465642068616e64206d61646520526f62657274204d617263206672616d65732c2077686f73652066616e7320696e636c756465204b6569666572205375746865726c616e6420616e6420556d6120546875726d616e2e204e6f7420666f7267657474696e6720746865205363616e64696e617669616e20696e666c75656e6365206f66207468652068616e64206372616674656420546974616e69756d2072616e67652066726f6d204f72677265656e20616e64207468652073696d706c792062656175746966756c20616e64206c69676874204c696e64626572672072696d6c6573732072616e67652e20546865206672616d657320737461727420776974682074686520436861726c65732053746f6e6520636f6c6c656374696f6e2066726f6d20c2a339302e0d0a0d0a54656d70616e79e28099732068617320696e76657374656420696e207468652076657279206c6174657374206f70687468616c6d696320646961676e6f737469632065717569706d656e742c204f63756c617220436f686572656e636520546f6d6f6772617068792c207768696368206e6f74206f6e6c792074616b657320612070696374757265206f6620746865206261636b206f662074686520657965206275742063616e20616c736f2074616b652061203344207363616e206f662074686520737562207375726661636520736f6674207469737375652c20676976696e6720616e20696d6167652062657474657220696e207175616c697479207468616e2061204d5249207363616e2e0d0a0d0a4265666f72652073657474696e672075702054656d70616e79e280997320426f757469717565204f7074696369616e73204b656974682054656d70616e7920686173206265656e20626173656420696e2042726f616473746f6e6520666f722036207965617273206275696c64696e6720757020616e2061776172642077696e6e696e6720436f6e74616374204c656e73205370656369616c69737420507261637469636520776974682061206e6174696f6e616c2072657075746174696f6e20616e6420636f6e74616374206c656e73657320617265206973207374696c6c207665727920696e737472756d656e74616c20696e20746865206e65772070726163746963652e204b6569746820697320486f6e6f726172792054726561737572657220666f7220746865204272697469736820436f6e74616374204c656e73204173736f63696174696f6e20616e642068617320617070656172656420696e20746865206e6174696f6e616c20707265737320616e64206f6e20526164696f203220676976696e6720616476696365206f6e20636f6e74616374206c656e7365732e0d0a0d0a49742069732070726f6261626c79206e6f20776f6e646572207468656e2074686174204b6569746820616e6420746865207465616d2061742054656d70616e79e28099732068617665206265656e2073686f72746c697374656420666f722032206e6174696f6e616c206f7074696369616e206177617264732e205468697320697320746865203474682079656172206f7574206f6620352074686174204b6569746820686173206265656e2073686f7274206c697374656420616e642061637475616c6c7920776f6e20436f6e74616374204c656e732050726163746974696f6e6572206f6620746865205965617220696e20323031302e200d0a0d0a546865206c61737420776f726420676f657320746f20612070617469656e742077686f206c6566742074686520666f6c6c6f77696e672074657374696d6f6e69616c206f6e206f75722046616365626f6f6b20706167652020e2809c5375706572622120496e6372656469626c65206361726520616e6420617474656e74696f6e20746f2064657461696c3b20696d7065636361626c6520616e6420667269656e646c7920736572766963653b207374796c6973682c20746f70207175616c697479206672616d657320616e64206c656e7365732e20576520776f756c646e5c277420676f20616e79776865726520656c736521e2809d200d0a0d0a, 7, 1, 1, 1, 1, 1, 1, 0, 0, '', '', 0x4d6f6e6461792c20547565736461792c205765646e657364617920262046726964617920393a30302d20353a3330205468757273646179202620536174757264617920393a303020e2809320313a3030, ''),
(95, 'Mutita', 'Metis', 'tiien', 'Tiien Thai Restaurant-Broadstone', '13 Moor Road', '', 'Broadstone', 'Dorset', 'BH18 8AZ', 'UK', '01202 693600', '', '07886 394064', '', 50.7638, -1.99398, 89, 'bronze', NULL, 0x546861692052657374617572616e7420616e642074616b652061776179, 0x4f75722042726f616473746f6e652072657374617572616e7420776173206f70656e656420696e2053657074656d626572203230313220666f6c6c6f77696e67206f6e2066726f6d207468652073756363657373206f66206f757220426f75726e656d6f7574682072657374617572616e74207768696368206f70656e656420696e204a616e7561727920323031312e202020417420546969656e20546861692072657374617572616e742042726f616473746f6e65207765206172652070726f756420746f2063726561746520657863657074696f6e616c20666f6f642074686174206973206361726566756c6c7920707265706172656420616e642062656175746966756c6c792070726573656e7465642e0d0a0d0a4865726520796f752077696c6c20657870657269656e6365206a6f7920616e64206861726d6f6e7920696e206120736572656e652079657420636f6e74656d706f7261727920656e7669726f6e6d656e7420776865726520746865206d6f7374206578636974696e67205468616920666f6f642077696c6c206265207365727665642e2020416c736f20617661696c61626c6520666f722074616b6520617761792e0d0a0d0a4f7572206865616420636865662c20506f6e672050616e2c736f7572636573206f6e6c79207468652066696e657374205468616920696e6772656469656e747320746f2070726f6475636520746865206d6f7374207375626c696d6520666c61766f7572732e2020486973207465616d20617420546969656e2077696c6c20677569646520796f75206f6e2061206d61676963616c20616476656e74757265206f66207461737465732c2061726f6d617320616e642074657874757265732e0d0a, 0, 1, 1, 1, 1, 1, 1, 0, 0, '', '', 0x54756573646179202d2053756e6461792e20204c756e6368203132206e6f6f6e202d20322e3330706d2e20204576656e696e6720352e3330202d203131706d2e2020436c6f736564206f6e204d6f6e6461792e, ''),
(96, 'Debbie', 'Riding', 'ttw', 'Through the Window Interiors and Designs', '196 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DR', 'UK', '01202 690530', '', '', 'www.throughthewindowinteriros.co.uk', NULL, NULL, 90, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(97, 'Steve and Carol', 'Dyke', 'weldrite', 'Weldrite Structures Ltd', '186b The Broadway', '', 'Broadstone', 'Dorset', 'BH18 8DP', 'UK', '01202 650035', '', '', 'www.weldrite.co.uk', 50.7602, -1.99409, 91, 'bronze', NULL, '', '', 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(98, 'Nigel', 'West', 'wests', 'NP and GM West Opticians', '214 Lower Blandford Road', '', 'Broadstone', 'Dorset', 'BH18 8DH', 'UK', '01202 696345', '', '', '', NULL, NULL, 92, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(99, 'Darren', 'Wright', 'wrights', 'Wrights Estate Agents', '211 The Broadway', '', 'Broadstone', 'Dorset', 'BH18 8DN', 'UK', '01202 697111', '', '', 'www.wrightsestateagents.com', NULL, NULL, 93, 'bronze', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, ''),
(100, 'Phil', 'Greeno', 'Phil', 'Flare Web Design Ltd', '19 Charborough', '', '', 'Dorset', 'BH8 9LB', 'UK', '07773733490', '', '', '', 0, 0, 95, 'gold', NULL, 0x576562736974652044657369676e202620446576656c6f706d656e74, 0x476574207468652077656220776f726b696e6720666f7220796f752e0d0a3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d0d0a0d0a576562736974652064657369676e2c206170706c69636174696f6e20646576656c6f706d656e7420616e6420686f7374696e6720666f7220736d616c6c20627573696e65737365732074686174206e65656420612077656c6c2066756e6374696f6e696e672c206865616c74687920616e6420616374697665207765627369746520746f2070726f6d6f7465207468656d73656c766573206f6e6c696e652e, 0, 1, 1, NULL, NULL, NULL, 0, 0, 0, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) NOT NULL auto_increment,
  `isadmin` tinyint(1) NOT NULL default '0',
  `email` varchar(128) collate utf8_bin NOT NULL,
  `password` varchar(45) collate utf8_bin NOT NULL,
  `deleted` tinyint(1) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=96 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `isadmin`, `email`, `password`, `deleted`) VALUES
(1, 1, 'admin@admin.com', '3cadb80c91cfba1d843069032b95a56026030d97', NULL),
(2, 0, 'demo@demo.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(3, 0, 'amanda@sos-swim.co.ok', 'd2c7d9f85f1baf81ccaf02373a6b8a2ccd01e723', NULL),
(4, 0, 'mark.tointon@aardpress.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(5, 0, 'broadstoneageconcern@tiscali.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(6, 0, 'simon.merry@trustandconfidencebydesign.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(7, 0, 'info@kestrelophthalmics.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(8, 0, 'andrew@imagine-photographic.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(9, 0, 'admin@mollyscatering.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(10, 0, 'info@jlmorris.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(11, 0, 'maxinehibbs@hotmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(12, 0, 'jill@pineridgebedandbreakfast.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(13, 0, 'david@prism-imageworks.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(15, 0, 'northerngirl1965@yahoo.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(16, 0, 'jenny@thegoodsyard.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(17, 0, 'alexandra.greenwood@hsbc.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(18, 0, 'enquiries@thejunctionbroadstone.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(19, 0, 'nm@newtonmagnus.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(20, 0, 'adrian@quantumsocial-media.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(21, 0, 'contactus@wessextrophies.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(22, 0, 'aatreesurgeon@hotmail.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(23, 0, 'm.shutt974@btinternet.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(24, 0, 'sue@aspirinbusiness.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(25, 0, 'karenwallbridge@hotmail.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(26, 0, 'sales@sjbdrycleaning.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(27, 0, 'broadstone@bathtravel.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(28, 0, 'tb@birkett.co', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(29, 0, 'p6476@jdwetherspoon.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(30, 0, 'no@email.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(31, 0, 'neil.j.bichard@member.riba.org', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(32, 0, 'wimbornebarber@btconnect.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(33, 0, 'no@email.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(34, 0, 'info@broadstonechiropractor.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(35, 0, 'tonpen@aol.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(36, 0, 'info@broadstoneclinic.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(37, 0, 'scottiscalling@hotmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(38, 0, 'office@broadstonegolfclub.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(39, 0, 'karen@broadstonelink.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(40, 0, 'dta@gotadsl.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(41, 0, 'jason.shah1@gmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(42, 0, 'mail@broadstonephysiotherapy.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(43, 0, 'broadstone.travel@traveleye.net', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(44, 0, 'mark.pratt@broadviewblinds.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(45, 0, 'budgens665@musgrave.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(46, 0, 'enquiries@burwoodnursinghome.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(49, 0, 'tim@tartantangerine.com', '3cadb80c91cfba1d843069032b95a56026030d97', NULL),
(50, 0, ' jane@canfordheathlink.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(51, 0, 'caesars.bistro@ntlworld.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(52, 0, 'ehamiltoncole@coles-miller.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(53, 0, 'jerry@crsdorset.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(54, 0, 'costabroadstone@hotmail.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(55, 0, 'countryflorist@hotmail.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(56, 0, 'danielsemail08@yahoo.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(57, 0, 'k.dudley@dickinsonmanser.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(58, 0, 'accounts@dorsetlifts.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(59, 0, 'poole@dreamdoorsltd.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(60, 0, 'alex@expectbest.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(61, 0, 'admin@forumjewellers.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(62, 0, 'roy@fraserportraits.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(63, 0, 'gillian@gillianlinfordlaw.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(64, 0, 'glowmail@gmx.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(65, 0, 'russell.saunders@uwclub.net', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(66, 0, 'sally.carson@goadsby.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(67, 0, 'willispip@hotmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(68, 0, 'griffin.broadstone@letsco-operate.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(69, 0, 'paul.klein@habitatprojects.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(70, 0, ' homes@hillierwilson.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(71, 0, 'hochoys@btconnect.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(72, 0, 'neil@ist-limited.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(73, 0, 'info@ireneofbroadstone.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(74, 0, 'andrew_ayles@yahoo.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(75, 0, 'khdpoole@hotmail.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(76, 0, 'info@leonardotrust.org', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(77, 0, 'lp6262@lloydspharmacy.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(78, 0, 'info@marsdenflooring.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(79, 0, 'sandycarley@hotmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(80, 0, 'martin@msl-designs.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(81, 0, 'saha@newdriverschools.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(82, 0, 'oakleyvillagebutchers@sky.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(83, 0, 'bhclemons@btinternet.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(84, 0, 'richard_everitt@sky.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(85, 0, 'enquiries@seywardwindows.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(86, 0, 'alix.smith@btconnect.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(87, 0, 'broadstone@tapperfuneralservice.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(88, 0, 'info@tempanys.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(89, 0, 'info@tiien.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(90, 0, 'info@throughthewindowinteriors.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(91, 0, 'carol@weldrite.co.uk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(92, 0, 'nigwest@btinternet.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(93, 0, 'sales@wrightsestateagents.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(94, 0, 'indran4@hotmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL),
(95, 0, 'phil@flare-web.co.uk', 'd2c7d9f85f1baf81ccaf02373a6b8a2ccd01e723', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `weights`
--

CREATE TABLE IF NOT EXISTS `weights` (
  `id` int(11) NOT NULL auto_increment,
  `level` enum('bronze','silver','gold') collate utf8_bin default NULL,
  `weight` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Dumping data for table `weights`
--

INSERT INTO `weights` (`id`, `level`, `weight`) VALUES
(1, 'bronze', 50),
(2, 'silver', 65),
(3, 'gold', 80);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `cons_fk_article_merchant_id_id` FOREIGN KEY (`merchant_id`) REFERENCES `merchant` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `article_category`
--
ALTER TABLE `article_category`
  ADD CONSTRAINT `cons_fk_article_category_article_id_id` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cons_fk_article_category_category_id_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `category_classified`
--
ALTER TABLE `category_classified`
  ADD CONSTRAINT `cons_fk_category_classified_category_id_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cons_fk_category_classified_classified_id_id` FOREIGN KEY (`classified_id`) REFERENCES `classified` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `category_deal`
--
ALTER TABLE `category_deal`
  ADD CONSTRAINT `cons_fk_category_deal_category_id_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cons_fk_category_deal_deal_id_id` FOREIGN KEY (`deal_id`) REFERENCES `deal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `category_event`
--
ALTER TABLE `category_event`
  ADD CONSTRAINT `cons_fk_category_event_category_id_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cons_fk_category_event_event_id_id` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `category_merchant`
--
ALTER TABLE `category_merchant`
  ADD CONSTRAINT `cons_fk_category_merchant_category_id_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cons_fk_category_merchant_merchant_id_id` FOREIGN KEY (`merchant_id`) REFERENCES `merchant` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `classified`
--
ALTER TABLE `classified`
  ADD CONSTRAINT `cons_fk_classified_merchant_id_id` FOREIGN KEY (`merchant_id`) REFERENCES `merchant` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `deal`
--
ALTER TABLE `deal`
  ADD CONSTRAINT `cons_fk_deal_merchant_id_id` FOREIGN KEY (`merchant_id`) REFERENCES `merchant` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `cons_fk_event_merchant_id_id` FOREIGN KEY (`merchant_id`) REFERENCES `merchant` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `merchant`
--
ALTER TABLE `merchant`
  ADD CONSTRAINT `cons_fk_merchant_user_id_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
         