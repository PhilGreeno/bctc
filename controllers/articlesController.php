<?php
/**
 * 
 * @author Tim Fisher - Tartan Tangerine Limited 2013
 *
 */

class articlesController extends Controller {

    public function index()
    {
        // Grab Complete list of merchants
        $articles = articleManager::all();
        SingleSmarty::getInstance()->assign('articles', $articles['results']);
        SingleSmarty::getInstance()->assign('resultCount',   $articles['count']);
        
        SingleSmarty::getInstance()->assign('letters', articleManager::azFilter());
        
        widgetManager::browseByCategory();
        
        View::render('articles/home');
    }
    
    /**
     * 
     * by Category
     */
    public function category()
    {
        $args = func_get_args();
        $name   = isset($args[0])?$args[0]:null;
        
        if ($name && $name != ''){
            $articles = articleManager::byCategory( urldecode($name) );
            SingleSmarty::getInstance()->assign('articles', $articles['results']);
            SingleSmarty::getInstance()->assign('resultCount',   $articles['count']);
            
            SingleSmarty::getInstance()->assign('letters', articleManager::azFilterCategory(urldecode($name)));

            widgetManager::browseByCategory();
            
            SingleSmarty::getInstance()->assign('currentCategory', urldecode($name));
        
            View::render('articles/home');
        } else {
            $this->index();
            exit;
        }
    }
    
    /**
     * Detail view
     */
    public function detail()
    {
        $args = func_get_args();
        $id   = isset($args[0])?$args[0]:null;
        if ($id && $id != ''){
            
            $article = articleManager::byId($id);
            SingleSmarty::getInstance()->assign('article', $article);
            
            // Increment the page Views
            $mBean = R::load('article', $article->id);
            $mBean->setAttr('pageviews', $article->pageviews + 1); // Double increments with FB on
            R::store($mBean);
 
            View::render('articles/detail');
        } else {
            $this->index();
            exit;
        }
        
    }


}
?>
