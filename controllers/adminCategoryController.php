<?php
/**
 *  *
 * @author Tim Fisher
 *
 * Description:
 */



class adminCategoryController extends Controller {


    public function index()
    {
        header('Location: /'.WEBPATH.'home'); die;
    }

    public function add()
    {
        // Test to see if there is a user session
        $user = unserialize(userManager::session());

        // If there is not a session then display the normal landing page
        if ($user && $user->isadmin == true){

            $name         = helpers\request('name');
            $description  = helpers\request('description');
            $business     = helpers\request('business');
            $action       = helpers\request('action');
            
            if ($action == 'add'){

                if ($name && $description){
                    $cat = R::findOne('category',' LOWER(name) = ?' , array( strtolower($name) ));
    
                    if (isset($cat) && $cat->id){
                        helpers\addError(array('title'=>'Category creation error', 'message'=>'A Category with that name already exists in the database.'));
                    }else {
                        // Create new user
                        $cat = R::dispense('category');
                        $cat->create($name, $description, $business);
                        SingleSmarty::getInstance()->assign('updated', true);
                    }
                } else {
                    helpers\addError(array('title'=>'Category creation error', 'message'=>'Please fill out all fields correctly.'));
                }

            }
            
            View::render('admin/addCategory');

        } else {

             header('Location: /'.WEBPATH.'home'); die;

        }

    }
    
    public function edit()
    {
        // Test to see if there is a user session
        $user = unserialize(userManager::session());
    
        // If there is not a session then display the normal landing page
        if ($user && $user->isadmin == true){
            
            $args = func_get_args();
            $id = $args[0];
            
            $thisCat = R::findOne('category', 'md5(id) = ?', array($id));

            if ($thisCat){
            
                $action = helpers\request('action');
                
                // We have some posted data so we need to do the update
                if ($action == 'edit'){
                    // Remove data that is no longer needed.
                    unset($_POST['action']);
                    
                    // Import the updates
                    $thisCat->import($_POST);
                    // Save
                    R::store($thisCat);
                    SingleSmarty::getInstance()->assign('updated', true);
                    
                }
            }
            
            SingleSmarty::getInstance()->assign('category', $thisCat);
            
            View::render('admin/editCategory');
 
        } else {
    
            header('Location: /'.WEBPATH.'admin'); die;
    
        }
    
    }
    
    public function listing(){
        
        // Test to see if there is a user session
        $user = unserialize(userManager::session());

        if ($user && $user->isadmin == true){
            
            $categories = R::findAll('category', 'ORDER BY name');
            
            SingleSmarty::getInstance()->assign('categories', $categories);
        
            View::render('admin/listCategories');
        
        } else {
        
            header('Location: /'.WEBPATH.'home'); die;
        
        }
        
    }
    
    public function delete(){
        // Test to see if there is a user session
        $user = unserialize(userManager::session());
        
        // If there is not a session then display the normal landing page
        if ($user && $user->isadmin == true){
        
            $args = func_get_args();
            $id = $args[0];
        
            $category = R::findOne('category', 'md5(id) = ?', array($id));
        
            if ($category){
                R::trash($category);
                header('Location: '.$_SERVER['HTTP_REFERER']); die;
            }
        }
        
        header('Location: /'.WEBPATH.'admin'); die;
    }

   
}
?>
