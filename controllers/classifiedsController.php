<?php
/**
 * 
 * @author Tim Fisher - Tartan Tangerine Limited 2013
 *
 */

class classifiedsController extends Controller {

    public function index()
    {
        // Grab Complete list of merchants
        $classifieds = classifiedManager::all();
        SingleSmarty::getInstance()->assign('classifieds', $classifieds['results']);
        SingleSmarty::getInstance()->assign('resultCount', $classifieds['count']);
        
        SingleSmarty::getInstance()->assign('letters', classifiedManager::azFilter());
        
        widgetManager::browseByCategory();
        
        View::render('classifieds/home');
    }
    
    /**
     * 
     * by Category
     */
    public function category()
    {
        $args = func_get_args();
        $name   = isset($args[0])?$args[0]:null;
        
        if ($name && $name != ''){
            $classifieds = classifiedManager::byCategory( urldecode($name) );
            SingleSmarty::getInstance()->assign('classifieds', $classifieds['results']);
            
            SingleSmarty::getInstance()->assign('resultCount', $classifieds['count']);
            
            SingleSmarty::getInstance()->assign('letters', classifiedManager::azFilterCategory(urldecode($name)));

            widgetManager::browseByCategory();
            
            SingleSmarty::getInstance()->assign('currentCategory', urldecode($name));
        
            View::render('classifieds/home');
        } else {
            $this->index();
            exit;
        }
    }
    
    /**
     * Detail view
     */
    public function detail()
    {
        $args = func_get_args();
        $id   = isset($args[0])?$args[0]:null;
        if ($id && $id != ''){
            
            $classified = classifiedManager::byId($id);
            SingleSmarty::getInstance()->assign('classified', $classified);
            
            // Increment the page Views
            $mBean = R::load('classified', $classified->id);
            $mBean->setAttr('pageviews', $classified->pageviews + 1); // Double increments with FB on
            R::store($mBean);
 
            View::render('classifieds/detail');
        } else {
            $this->index();
            exit;
        }
        
    }


}
?>
