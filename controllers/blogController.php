<?php
/**
 * 
 * @author Tim Fisher - Tartan Tangerine Limited 2013
 *
 */

class blogController extends Controller {

    public function index()
    {
        // Grab Complete list of merchants
        $articles = blogManager::all();
        SingleSmarty::getInstance()->assign('articles', $articles['results']);
        SingleSmarty::getInstance()->assign('resultCount',   $articles['count']);
        SingleSmarty::getInstance()->assign('letters', null);
        widgetManager::browseByCategory();
        
        View::render('blog/home');
    }
    
    /**
     * 
     * by Category
     */
    public function category()
    {
        $args = func_get_args();
        $name   = isset($args[0])?$args[0]:null;
        
        if ($name && $name != ''){
            $articles = blogManager::byCategory( urldecode($name) );
            SingleSmarty::getInstance()->assign('articles', $articles['results']);
            SingleSmarty::getInstance()->assign('resultCount',   $articles['count']);
            SingleSmarty::getInstance()->assign('letters', null);
            
            widgetManager::browseByCategory();
            
            SingleSmarty::getInstance()->assign('currentCategory', urldecode($name));
        
            View::render('blog/home');
        } else {
            $this->index();
            exit;
        }
    }
    
    /**
     * Detail view
     */
    public function detail()
    {
        $args = func_get_args();
        $id   = isset($args[0])?$args[0]:null;
        if ($id && $id != ''){
            
            $article = blogManager::byId($id);
            SingleSmarty::getInstance()->assign('article', $article);
            
            // Increment the page Views
            $mBean = R::load('article', $article->id);
            $mBean->setAttr('pageviews', $article->pageviews + 1); // Double increments with FB on
            R::store($mBean);
 
            View::render('blog/detail');
        } else {
            $this->index();
            exit;
        }
        
    }


}
?>
