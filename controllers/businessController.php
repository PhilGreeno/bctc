<?php
/**
 * 
 * @author Tim Fisher - Tartan Tangerine Limited 2013
 *
 */

class businessController extends Controller {

    public function index()
    {        
        // Grab Complete list of merchants
        $merchants = merchantManager::all(false, 6);
        
        // If there are no results then grab the Sponsored Merchants
        if ($merchants['count'] == 0){
            // Grab featured Merchant Mix
            $merchants = merchantManager::sticky(4);
            SingleSmarty::getInstance()->assign('merchants', $merchants);
            SingleSmarty::getInstance()->assign('resultCount',   count((array)$merchants));
        } else {
            SingleSmarty::getInstance()->assign('merchants',     $merchants['results']);
            SingleSmarty::getInstance()->assign('resultCount',   $merchants['count']);
        }
        
        SingleSmarty::getInstance()->assign('letters', merchantManager::azFilter());
        
        widgetManager::browseByCategory();

        View::render('business/home');
    }
    
    /**
     * Merchants by Category
     */
    public function category()
    {
        $args = func_get_args();
        $name   = isset($args[0])?$args[0]:null;

        if ($name && $name != ''){
            $merchants = merchantManager::byCategory( urldecode($name), 6 );
            SingleSmarty::getInstance()->assign('merchants',     $merchants['results']);
            SingleSmarty::getInstance()->assign('resultCount',   $merchants['count']);
           
            SingleSmarty::getInstance()->assign('letters', merchantManager::azFilterCategory(urldecode($name)));

            widgetManager::browseByCategory();
            
            SingleSmarty::getInstance()->assign('currentCategory', urldecode($name));
        
            View::render('business/home');
        } else {
            $this->index();
            exit;
        }
    }
    
    /**
     * Merchant Detail view
     */
    public function detail()
    {
        $args = func_get_args();
        $name   = isset($args[0])?$args[0]:null; // merchant company name
        if ($name && $name != ''){
            
            $merchant = merchantManager::byName( urldecode($name) );
            SingleSmarty::getInstance()->assign('merchant', $merchant);
            
            // Increment the page Views
            $mBean = R::load('merchant', $merchant->id);
            $mBean->setAttr('pageviews', $merchant->pageviews + 1); // Double increments with FB on
            R::store($mBean);

            // Merchant deals
            $deals = dealManager::byMerchant($merchant->id);
            SingleSmarty::getInstance()->assign('deals', $deals);
            
            // Do Captcha for email form
            captcha::init();
            
            View::render('business/detail');
        } else {
            $this->index();
            exit;
        }
        
    }


}
?>
