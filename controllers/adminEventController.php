<?php
/**
 *  *
 * @author Tim Fisher
 *
 * Description:
 */



class adminEventController extends Controller {


    public function index()
    {
        header('Location: /'.WEBPATH.'home'); die;
    }

    public function add()
    {
        // Test to see if there is a user session
        $user = unserialize(userManager::session());

        $args     = func_get_args();
        $id       = isset($args[0])?$args[0]:null;

        if (userManager::grantMerchantAccess($user, $id)){

            $action       = helpers\request('action');
            
            if ($action == 'add'){
                
                if ($user->isadmin == 1){
                    // Find the merchant
                    $merchant = R::findOne('merchant', 'id = ?', array( $_POST['merchant_id'] ));;
                    unset($_POST['merchant_id']);
                } else {
                    // Get the merchant details from the user object
                    $merchant = reset($user->ownMerchant);
                }
                
                unset($_POST['action']);
                // Now that we have the merchant details we can proceed.
                if (isset($merchant) && $merchant->id){
                    
                    // Get a new Article Bean
                    $event = R::dispense('event');
                    
                    $event->setAttr('publicationdate', null);
                    $event->setAttr('pageviews', 0);
                    $event->setAttr('created', helpers\now());
                    $event->setAttr('updated', helpers\now());
                    
                    
                    // Assign the merchant to the user
                    $merchant->ownEvent[] = $event;
                    
                    // Import the data
                    $event->import($_POST);
                    
                    // Save the data - we do this now as we need the ID for the logo upload and assign categories
                    $id = R::store($merchant);
                    
                    // Did we get images in the upload?
                    image::upload($event, 'event', 'logo');
                    
                    R::store($event);
                    SingleSmarty::getInstance()->assign('updated', true);
                    
                } else {
                    helpers\addError(array('title'=>'Event Add Error', 'message'=>'We could not locate the specified merchant detail, please select another and try again.'));
                }
               
            }
            
            $merchants = R::findAll('merchant', 'ORDER BY companyname');
            SingleSmarty::getInstance()->assign('merchants', $merchants);

            View::render('admin/addEvent');

        } else {

             header('Location: /'.WEBPATH.'home'); die;

        }

    }
    
    public function edit()
    {

        // Test to see if there is a user session
        $user = unserialize(userManager::session());
    
        $args     = func_get_args();
        $id       = isset($args[0])?$args[0]:null;
        
        $thisEvent = R::findOne('event', 'md5(id) = ?', array($id));
        
        if ($thisEvent){
            
            // If there is not a session then display the normal landing page
            if ($user && ($user->isadmin == true || $user->id == $thisEvent->merchant->user->id)){
            
                $action = helpers\request('action');
                
                // We have some posted data so we need to do the update
                if ($action == 'edit'){
                    // Remove data that is no longer needed.
                    unset($_POST['action']);
                    
                    // Grab the category posting
                    $categories = helpers\request('categories');
                    unset($_POST['categories']);
                    
                    // Only admin can change categories
                    if ($user->isadmin == true || $thisEvent->merchant->level != 'bronze'){
                        // unset all the old categories
                        foreach ($thisEvent->sharedCategory as $category){
                            unset($thisEvent->sharedCategory[$category->id]);
                        }
                        
                        // Add the new categories
                        foreach ($categories as $id){
                            $category = R::findOne('category', 'id=?', array($id));
                            if (isset($category)){
                                $thisEvent->sharedCategory[] = $category;
                            }
                        }
                    }
                    
                    // If there are any image deletes posted then we must act on them
                    $deletes = helpers\request('delete');
                    
                    if (is_array($deletes)){
                        foreach ($deletes as $key=>$value){
                            $thisEvent->setAttr("image{$key}", false);
                        }
                    }
                    unset($_POST['delete']);
                    
                    // Should we un/publish
                    $publish = helpers\request('publish');
                    unset($_POST['publish']);
                    if ($publish){
                        $thisEvent->setAttr('publicationdate', helpers\now());
                    }

                    $unpublish = helpers\request('unpublish');
                    unset($_POST['unpublish']);
                    if ($unpublish){
                        $thisEvent->setAttr('publicationdate', null);
                    }
                    
                    // Any Changes made by a non site admin will put the item back into moderation.
                    if ($user->isadmin != true){
                        $thisEvent->setAttr('publicationdate', null);
                    }
                    
                    // Set update date
                    $thisEvent->setAttr('updated', helpers\now());
                    
                    // Import the updates
                    $thisEvent->import($_POST);
                    
                    // Did we get images in the upload?
                    image::upload($thisEvent, 'event', 'logo');
                    
                    
                    // Save
                    R::store($thisEvent);

                    SingleSmarty::getInstance()->assign('updated', true);
                    
                }
            }
            
            SingleSmarty::getInstance()->assign('event', $thisEvent);
            
            // Assign the various categories
            $categories = R::findAll('category', 'WHERE business != 1 ORDER BY name');
            
            
            // Get own Categories
            $ownCategories = $thisEvent->sharedCategory;
            
            //Now mark the categories that should be pre-selected
            foreach($categories as $category){
                foreach($ownCategories as $myCat){
                    if ($myCat->id == $category->id){
                        $categories[$category->id]->setAttr('selected', true);
                    }
                }
            }
            
            SingleSmarty::getInstance()->assign('categories', $categories);
            
            $merchants = R::findAll('merchant', 'ORDER BY companyname');
            SingleSmarty::getInstance()->assign('merchants', $merchants);
            
            View::render('admin/editEvent');
 
        } else {
    
            header('Location: /'.WEBPATH.'admin'); die;
    
        }
    
    }
    
    public function listing(){
        
        // Test to see if there is a user session
        $user = unserialize(userManager::session());
        
        $args     = func_get_args();
        $id       = isset($args[0])?$args[0]:null;

        if (userManager::grantMerchantAccess($user, $id)){
            
            if ($id){
                $events = R::findAll('event', ' WHERE md5(merchant_id) = ? ORDER BY publicationdate ASC', array($id));
            } else {
                $events = R::findAll('event', ' WHERE isnull(deleted) ORDER BY publicationdate ASC'); // Non Published first
            }

            SingleSmarty::getInstance()->assign('events', $events);
        
            View::render('admin/listEvents');
        
        } else {
        
            header('Location: /'.WEBPATH.'home'); die;
        
        }
        
    }
    
    /**
     * Publish and return to referring page
     */
    public function publish()
    {
        // Test to see if there is a user session
        $user = unserialize(userManager::session());
    
        // If there is not a session then display the normal landing page
         if ($user && ($user->isadmin == true || reset($user->ownMerchant)->level != 'bronze')){
    
            $args = func_get_args();
            $id = $args[0];
    
            $event = R::findOne('event', 'md5(id) = ?', array($id));
    
            if ($event){
                $event->setAttr('publicationdate', helpers\now());
                R::store($event);
                header('Location: '.$_SERVER['HTTP_REFERER']); die;
            }
        }
    
        header('Location: /'.WEBPATH.'admin'); die;
    }
    
    public function unpublish()
    {
        // Test to see if there is a user session
        $user = unserialize(userManager::session());
    
        // If there is not a session then display the normal landing page
        if ($user && $user->isadmin == true){
    
            $args = func_get_args();
            $id = $args[0];
    
            $event = R::findOne('event', 'md5(id) = ?', array($id));
    
            if ($event){
                $event->setAttr('publicationdate', null);
                R::store($event);
                header('Location: '.$_SERVER['HTTP_REFERER']); die;
            }
        }
    
        header('Location: /'.WEBPATH.'admin'); die;
    }

    public function delete(){
        // Test to see if there is a user session
        $user = unserialize(userManager::session());
    
        $args = func_get_args();
        $id = $args[0];
        
        $event = R::findOne('event', 'md5(id) = ?', array($id));
        
        if ($event){
            
            // If there is not a session then display the normal landing page
            if ($user && ($user->isadmin == true || $user->id == $event->merchant->user->id)){

                $event->setAttr('deleted', true);
                R::store($event);
                header('Location: '.$_SERVER['HTTP_REFERER']); die;
            }
        }
    
        header('Location: /'.WEBPATH.'admin'); die;
    }
   
}
?>
