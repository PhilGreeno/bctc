<?php
/**
 *  *
 * @author Tim Fisher
 *
 * Description:
 */



class signupController extends Controller {

 	public function index()
 	{
    // Do we have any posted data?
 	  $username  = helpers\request('username');
    $email     = helpers\request('email');
    $password  = helpers\request('password');
    $conf      = helpers\request('confirmation');
    
    $error     = false;
    
    // If we have an email and password we need to check against the database
    if ($username && $email && $password && $conf){
        
        
        if ($password != $conf){
            helpers\addError(array('title'=>'Password error', 'message'=>'Passwords do not match'));
            $error = true;
        }
        
        
        if ($error == false){
            
            // make sure this users email does not already exist
            $user = R::findOne('user',' email = ? ',
                            array( $email )
                    );
            
            if ( isset($user) ){
                helpers\addError(array('title'=>'Sign up error', 'message'=>'An account already exists for that email address.'));
            } else {
                
                $user = R::dispense('user');
               
                $user->create($username, $email, $password);

                userManager::store($user);
                
                $plan = R::dispense('plan');
                $plan->assign($user, 'free');
                
                header('Location: ./signin'); exit;
            }
        } else {
            helpers\addError(array('title'=>'Sign up error', 'message'=>'Please check all details and try again.'));
        }
        
    } 
 	    
 	    
    View::render('signup');
 	}

}
?>
