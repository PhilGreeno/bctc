<?php
/**
 *  *
 * @author Tim Fisher
 *
 * Description:
 */



class adminCmsController extends Controller {


    public function index()
    {
        header('Location: /'.WEBPATH.'admin'); die;
    }

    public function add()
    {
        // Test to see if there is a user session
        $user = unserialize(userManager::session());

        // If there is not a session then display the normal landing page
        if ($user && $user->isadmin == true){

            $action = helpers\request('action');
        
            if ($action == 'add'){
                
                unset($_POST['action']);

                // Create new page
                $cms = R::dispense('cms');
                $cms->import($_POST);
                R::store($cms);
                SingleSmarty::getInstance()->assign('updated', true);
                 

            }
            
            View::render('admin/addCms');

        } else {

             header('Location: /'.WEBPATH.'admin'); die;

        }

    }
    
    public function edit()
    {
        // Test to see if there is a user session
        $user = unserialize(userManager::session());
    
        // If there is not a session then display the normal landing page
        if ($user && $user->isadmin == true){
            
            $args = func_get_args();
            $id = $args[0];
            
            $thisCms = R::findOne('cms', 'md5(id) = ?', array($id));

            if ($thisCms){
            
                $action = helpers\request('action');
                
                // We have some posted data so we need to do the update
                if ($action == 'edit'){
                    // Remove data that is no longer needed.
                    unset($_POST['action']);
                    
                    // Import the updates
                    $thisCms->import($_POST);
                    // Save
                    R::store($thisCms);
                    SingleSmarty::getInstance()->assign('updated', true);
                    
                }
            }
            
            SingleSmarty::getInstance()->assign('page', $thisCms);
            
            View::render('admin/editCms');
 
        } else {
    
            header('Location: /'.WEBPATH.'admin'); die;
    
        }
    
    }
    
    public function listing(){
        
        // Test to see if there is a user session
        $user = unserialize(userManager::session());

        if ($user && $user->isadmin == true){
            
            $pages = R::findAll('cms', 'ORDER BY position');
            
            SingleSmarty::getInstance()->assign('pages', $pages);
        
            View::render('admin/listCms');
        
        } else {
        
            header('Location: /'.WEBPATH.'admin'); die;
        
        }
        
    }
    
    public function delete(){
        // Test to see if there is a user session
        $user = unserialize(userManager::session());
        
        // If there is not a session then display the normal landing page
        if ($user && $user->isadmin == true){
        
            $args = func_get_args();
            $id = $args[0];
        
            $category = R::findOne('category', 'md5(id) = ?', array($id));
        
            if ($category){
                R::trash($category);
                header('Location: '.$_SERVER['HTTP_REFERER']); die;
            }
        }
        
        header('Location: /'.WEBPATH.'admin'); die;
    }

   
}
?>
