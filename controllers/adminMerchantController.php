<?php
/**
 *  *
 * @author Tim Fisher
 *
 * Description:
 */



class adminMerchantController extends Controller {


    public function index()
    {

    }

    public function add()
    {
        // Test to see if there is a user session
        $user = unserialize(userManager::session());

        // If there is not a session then display the normal landing page
        if ($user && $user->isadmin == true){

            $action = helpers\request('action');
            
            // We have some posted data so we need to do the update
            if ($action == 'add'){
                
                // Find the user
                $merchantUser = R::findOne('user', 'id = ?', array( $_POST['user_id'] ));;
                unset($_POST['user_id']);
                unset($_POST['action']);
                
                $categories = helpers\request('categories');
                unset($_POST['categories']);
                
                // Get a new merchant bean
                $merchant = R::dispense('merchant');
                
                $merchant->setAttr('pageviews', 0);
                
                // Assign the merchant to the user
                $merchantUser->ownMerchant[] = $merchant;

                // Import the data
                $merchant->import($_POST);
                
                // Save the data - we do this now as we need the ID for the logo upload and assign categories
                $id = R::store($merchantUser);
                
                // Assign the categories to the merchant
                foreach ($categories as $id){
                    $category = R::findOne('category', 'id=?', array($id));
                    if (isset($category)){
                        $merchant->sharedCategory[] = $category;
                    }
                }

                // Did we get images in the upload?
                image::upload($merchant, 'merchant', 'logo');
                
                R::store($merchant);
                SingleSmarty::getInstance()->assign('updated', true);

            }
            
            
            // Assign the various categories
            $categories = R::findAll('category', 'WHERE business = 1 ORDER BY name');
            SingleSmarty::getInstance()->assign('categories', $categories);
            
            $users = R::findAll('user', 'ORDER BY email');
            SingleSmarty::getInstance()->assign('users', $users);

            View::render('admin/addMerchant');

        } else {

             header('Location: /'.WEBPATH.'home'); die;

        }

    }
    
    public function edit()
    {
        // Test to see if there is a user session
        $user = unserialize(userManager::session());
    
        // If there is not a session then display the normal landing page
        $args     = func_get_args();
        $id       = isset($args[0])?$args[0]:null;
        
        $merchant = R::findOne('merchant', 'md5(id) = ?', array($id));
        
        if ($merchant){
            
            // If there is not a session then display the normal landing page
            if ($user && ($user->isadmin == true || $user->id == $merchant->user->id)){
            
                $action = helpers\request('action');
                
                // We have some posted data so we need to do the update
                if ($action == 'edit'){
                    // Remove data that is no longer needed.
                    unset($_POST['action']);
                    unset($_POST['logo']);
                    
                    // Grab the category posting
                    $categories = helpers\request('categories');
                    unset($_POST['categories']);
                    
                    // unset all the old categories
                    foreach ($merchant->sharedCategory as $category){
                        unset($merchant->sharedCategory[$category->id]);
                    }

                    // Add the new categories
                    foreach ($categories as $id){
                        $category = R::findOne('category', 'id=?', array($id));
                        if (isset($category)){
                            $merchant->sharedCategory[] = $category;
                        }
                    }

                    // If there are any image deletes posted then we must act on them
                    $deletes = helpers\request('delete');
                    
                    if (is_array($deletes)){
                        foreach ($deletes as $key=>$value){
                            $merchant->setAttr("image{$key}", false);
                        }
                    }
                    unset($_POST['delete']);
                    
                    // Import the updates
                    $merchant->import($_POST);
                    
                    // Did we get an image in the upload?
                    image::upload($merchant, 'merchant', 'logo');
                    
                    // Save
                    R::store($merchant);
                    SingleSmarty::getInstance()->assign('updated', true);
                }

                // Assign the various categories
                $categories = R::findAll('category', 'WHERE business = 1 ORDER BY name');
                
                // Get own Categories
                $ownCategories = $merchant->sharedCategory;

                //Now mark the categories that should be pre-selected
                foreach($categories as $category){
                    foreach($ownCategories as $myCat){
                        if ($myCat->id == $category->id){
                            $categories[$category->id]->setAttr('selected', true);
                        }
                    }
                }
                
                SingleSmarty::getInstance()->assign('categories', $categories);
                
                SingleSmarty::getInstance()->assign('merchant', $merchant);
                
                View::render('admin/editMerchant');
                
                
            }

    
        } else {
    
            header('Location: /'.WEBPATH.'admin'); die;
    
        }
    
    }
    
    public function listing(){
        
        // Test to see if there is a user session
        $user = unserialize(userManager::session());

        if ($user && $user->isadmin == true){
            
            $merchants = R::findAll('merchant', ' WHERE isnull(deleted) ORDER BY companyname');
            
            SingleSmarty::getInstance()->assign('merchants', $merchants);
        
            View::render('admin/listMerchants');
        
        } else {
        
            header('Location: /'.WEBPATH.'admin'); die;
        
        }
        
    }
    
    public function delete(){
        // Test to see if there is a user session
        $user = unserialize(userManager::session());
    
        // If there is not a session then display the normal landing page
        if ($user && $user->isadmin == true){
    
            $args = func_get_args();
            $id = $args[0];
    
            $merchant = R::findOne('merchant', 'md5(id) = ?', array($id));
    
            // Non destructive delete by marking records as deleted.
            if ($merchant){
                
                $merchant->delete();
    
                R::store($merchant);
    
                header('Location: '.$_SERVER['HTTP_REFERER']); die;
            }
        }
    
        header('Location: /'.WEBPATH.'admin'); die;
    }

 

}
?>
