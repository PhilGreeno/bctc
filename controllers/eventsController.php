<?php
/**
 * 
 * @author Tim Fisher - Tartan Tangerine Limited 2013
 *
 */

class eventsController extends Controller {

    public function index()
    {
        // Grab Complete list of events over next 4 weeks
        $events = eventManager::betweenDates(new DateTime('TODAY'), new DateTime('+12months'));
        SingleSmarty::getInstance()->assign('events', $events['results']);
        SingleSmarty::getInstance()->assign('resultCount',   $events['count']);
        
        SingleSmarty::getInstance()->assign('letters', eventManager::azFilter(new DateTime('TODAY'), new DateTime('+12months')));
        
        // Grab a larger resultset
        $mostEvents = eventManager::betweenDates(new DateTime('-12months'), new DateTime('+12months'));
        SingleSmarty::getInstance()->assign('mostEvents', $mostEvents['results']);
        
        widgetManager::browseByCategory();
        
        View::render('events/home');
    }
    
    /**
     * 
     * by Category
     */
    public function category()
    {
        $args = func_get_args();
        $name   = isset($args[0])?$args[0]:null;
        
        if ($name && $name != ''){

            $events = eventManager::betweenDatesCategory(urldecode($name) , new DateTime('TODAY'), new DateTime('+4weeks'));
            SingleSmarty::getInstance()->assign('events', $events['results']);
            SingleSmarty::getInstance()->assign('resultCount',   $events['count']);
            
            SingleSmarty::getInstance()->assign('letters', eventManager::azFilterCategory(urldecode($name), new DateTime('TODAY'), new DateTime('+12months') ));
            
            // Grab a larger resultset
            $mostEvents = eventManager::betweenDatesCategory(urldecode($name) , new DateTime('-12months'), new DateTime('+12months'));
            SingleSmarty::getInstance()->assign('mostEvents', $mostEvents['results']);

            // Assignments for the browseByCategory Widget
            widgetManager::browseByCategory();
            
            SingleSmarty::getInstance()->assign('currentCategory', urldecode($name));
        
            View::render('events/home');
        } else {
            $this->index();
            exit;
        }
    }
    
    /**
     * Detail view
     */
    public function detail()
    {
        $args = func_get_args();
        $id   = isset($args[0])?$args[0]:null;
        if ($id && $id != ''){
            
            $event = eventManager::byId($id);
            SingleSmarty::getInstance()->assign('event', $event);
            
            // Increment the page Views
            $mBean = R::load('event', $event->id);
            $mBean->setAttr('pageviews', $event->pageviews + 1); // Double increments with FB on
            R::store($mBean);
            
          //  $mostEvents = eventManager::betweenDates(new DateTime('-12months'), new DateTime('+12months'));
         //   SingleSmarty::getInstance()->assign('mostEvents', $mostEvents);
 
            View::render('events/detail');
        } else {
            $this->index();
            exit;
        }
        
    }


}
?>
