<?php
/**
 *  *
 * @author Tim Fisher
 *
 * Description:
 */



class signinController extends Controller {

 	public function index()
 	{
    // Do we have any posted data?
    $email     = helpers\request('email');
    $password  = helpers\request('password');
    
    // If we have an email and password we need to check against the database
    if ($email && $password){
        
        $user = R::findOne('user',' email = ? && password = ?', 
                        array( $email, sha1($password) )
                        );
        
        // Great! We found the user - log them in and redirect
        if (isset($user) && $user->id){
            userManager::store($user);
            header('Location: ./'); exit;
        } else {
            helpers\addError(array('title'=>'Sign in error', 'message'=>'Email and/or Password are incorrect.'));
        }
        
    } 
 	    
 	    
    View::render('signin');
 	}

}
?>
