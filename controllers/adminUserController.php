<?php
/**
 *  *
 * @author Tim Fisher
 *
 * Description:
 */



class adminUserController extends Controller {


    public function index()
    {
        header('Location: /'.WEBPATH.'home'); die;
    }

    public function add()
    {
        // Test to see if there is a user session
        $user = unserialize(userManager::session());

        // If there is not a session then display the normal landing page
        if ($user && $user->isadmin == true){

            $email = helpers\request('email');
            $passw = helpers\request('password');
            $isadmin = helpers\request('isadmin');
            $action = helpers\request('action');
            
            if ($action == 'add'){

                if ($email && $passw){
                    $user = R::findOne('user',' email = ?' , array( $email ));
    
                    if (isset($user) && $user->id){
                        helpers\addError(array('title'=>'User creation error', 'message'=>'A User with that email address already exists in the database.'));
                    }else {
                        // Create new user
                        $user = R::dispense('user');
                        $user->create($isadmin, $email, sha1($passw));
                        SingleSmarty::getInstance()->assign('updated', true);
                    }
                } else {
                    helpers\addError(array('title'=>'User creation error', 'message'=>'Please fill out all fields correctly.'));
                }

            }
            
            View::render('admin/addUser');

        } else {

             header('Location: /'.WEBPATH.'home'); die;

        }

    }
    
    public function edit()
    {
        // Test to see if there is a user session
        $user = unserialize(userManager::session());
        
        $args = func_get_args();
        $id = $args[0];
        
        $thisUser = R::findOne('user', 'md5(id) = ?', array($id));
    
        if ($thisUser){
            
            // If there is not a session then display the normal landing page
            if ($user && ($user->isadmin == true || $thisUser->id == $user->id)){

                $action = helpers\request('action');
                
                // We have some posted data so we need to do the update
                if ($action == 'edit'){
                    // Remove data that is no longer needed.
                    unset($_POST['action']);

                    $password = helpers\request('password');
                    
                    if ($password){
                        $_POST['password'] = sha1($password);
                    } else {
                        unset($_POST['password']);
                    }
                    
                    // Import the updates
                    $thisUser->import($_POST);
                    // Save
                    R::store($thisUser);
                    SingleSmarty::getInstance()->assign('updated', true);
                    
                }
            }
            
            SingleSmarty::getInstance()->assign('user', $thisUser);
            
            View::render('admin/editUser');
 
        } else {
    
            header('Location: /'.WEBPATH.'admin'); die;
    
        }
    
    }
    
    public function listing(){
        
        // Test to see if there is a user session
        $user = unserialize(userManager::session());

        if ($user && $user->isadmin == true){
            
            $users = R::findAll('user', ' WHERE isnull(deleted) ORDER BY email');
            
            SingleSmarty::getInstance()->assign('users', $users);
        
            View::render('admin/listUsers');
        
        } else {
        
            header('Location: /'.WEBPATH.'home'); die;
        
        }
        
    }
    
    public function delete(){
        // Test to see if there is a user session
        $user = unserialize(userManager::session());
    
        // If there is not a session then display the normal landing page
        if ($user && $user->isadmin == true){
    
            $args = func_get_args();
            $id = $args[0];
    
            $user = R::findOne('user', 'md5(id) = ?', array($id));
    
            // Non destructive delete by marking records as deleted.
            if ($user){
                $user->setAttr('deleted', true);
                
                // Loop through tree
                foreach ($user->ownMerchant as $merchant){
                    $merchant->delete();
                }
                
                R::store($user);
                
                header('Location: '.$_SERVER['HTTP_REFERER']); die;
            }
        }
    
        header('Location: /'.WEBPATH.'admin'); die;
    }

   
}
?>
