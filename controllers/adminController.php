<?php
/**
 *  *
 * @author Tim Fisher
 *
 * Description:
 */



class adminController extends Controller {

 	public function index()
 	{
 	  //  R::debug(true);
 	  
 	  // Test to see if there is a user session
 	  $user = unserialize(userManager::session());

 	  // If there is not a session then display the normal landing page
 	  if (!$user){
 	      
 	      $email = helpers\request('email');
 	      $passw = helpers\request('password');
 	      
 	      if ($email && $passw){
 	          $user = R::findOne('user',' email = ? && password = ?', 
                                    array( $email, sha1($passw) )
                                );
 	          
 	          if (isset($user) && $user->id){
 	              userManager::store($user);
 	              header('Location: /'.WEBPATH.'admin'); exit;
 	          } else {
 	              helpers\addError(array('title'=>'Sign in error', 'message'=>'Email and/or Password are incorrect.'));
 	          }
 	      }
 	      
 	      View::render('admin/login');
 	      
 	  } else {
 	      
 	      if (  $user->isadmin == false ) {
 	          
 	          $merchant = reset($user->ownMerchant);
 	          
 	          // Articles
 	          $articles = R::findAll('article', ' WHERE merchant_id = ? AND isnull(deleted) ORDER BY updated DESC', array($merchant->id));
 	          SingleSmarty::getInstance()->assign('articles', $articles);
 	          
 	          // Classifieds
 	          $classifieds = R::findAll('classified', ' WHERE merchant_id = ? AND isnull(deleted) ORDER BY updated DESC', array($merchant->id));
 	          SingleSmarty::getInstance()->assign('classifieds', $classifieds);
 	          
 	          // Deals
 	          $deals = R::findAll('deal', ' WHERE merchant_id = ? AND isnull(deleted) ORDER BY updated DESC', array($merchant->id));
 	          SingleSmarty::getInstance()->assign('deals', $deals);
 	          
 	          // Events
 	          $events = R::findAll('event', ' WHERE merchant_id = ? AND isnull(deleted) ORDER BY updated DESC', array($merchant->id));
 	          SingleSmarty::getInstance()->assign('events', $events);
 	          
 	          $sql = 'SELECT e.* FROM event e
 	                     INNER JOIN merchant m ON m.id = e.merchant_id
 	                     INNER JOIN user u ON u.id = m.user_id
 	                     WHERE (e.merchant_id = :merchantId OR u.isadmin = 1) 
 	                     AND isnull(e.deleted) 
 	                     ORDER BY e.created DESC;';
 	          
 	          $allEvents = R::getAll($sql, array(':merchantId'=>$merchant->id));
 	          SingleSmarty::getInstance()->assign('allevents', R::convertToBeans('event',$allEvents));
 	          
 	          View::render('admin/homeMerchant');
 	          
 	      } else {
 	      
     	      // Get unpublished Articles
     	      $articles = R::findAll('article', ' WHERE isnull(deleted) AND isnull(publicationdate) ORDER BY updated DESC');
     	      SingleSmarty::getInstance()->assign('articles', $articles);
     	      
     	      // Get unpublished Classifieds
     	      $classifieds = R::findAll('classified', ' WHERE isnull(deleted) AND isnull(publicationdate) ORDER BY updated DESC');
     	      SingleSmarty::getInstance()->assign('classifieds', $classifieds);
     	      
     	      // Get unpublished Deals
     	      $deals = R::findAll('deal', ' WHERE isnull(deleted) AND isnull(publicationdate) ORDER BY updated DESC');
     	      SingleSmarty::getInstance()->assign('deals', $deals);
     	      
     	      // Get unpublished Events
     	      $events = R::findAll('event', ' WHERE isnull(deleted) AND isnull(publicationdate) ORDER BY updated DESC');
     	      SingleSmarty::getInstance()->assign('events', $events);
     	      
     	      $allevents = R::findAll('event', ' WHERE isnull(deleted) ORDER BY created DESC');
     	      SingleSmarty::getInstance()->assign('allevents', $allevents);
     	      
    
     	      // Counts
     	      SingleSmarty::getInstance()->assign('merchantCount',    R::count('merchant'));
     	      SingleSmarty::getInstance()->assign('articleCount',     count( R::findAll('article', ' WHERE isnull(deleted)') ));
     	      SingleSmarty::getInstance()->assign('eventCount',       count( R::findAll('event', ' WHERE isnull(deleted)') ));
     	      SingleSmarty::getInstance()->assign('classifiedCount',  count( R::findAll('classified', ' WHERE isnull(deleted)') ));
     	      SingleSmarty::getInstance()->assign('dealCount',        count( R::findAll('deal', ' WHERE isnull(deleted)') ));
 
            View::render('admin/home');
 	      }
        
    
 	  }

 	}
 	
 	/**
 	 * CMS Controller Routing
 	 */
 	public function cms(){
 	    $args     = func_get_args();
 	    $method   = array_shift($args);
 	    $cmsController = new adminCmsController();
 	    $this->wrap_call_user_func_array($cmsController, $method, $args);
 	}
 	
 	/**
 	 * Category Controller Routing
 	 */
 	public function category(){
 	    $args     = func_get_args();
 	    $method   = array_shift($args);
 	    $categoryController = new adminCategoryController();
 	    $this->wrap_call_user_func_array($categoryController, $method, $args);
 	}
 	
 	/**
 	 * Merchant Controller Routing
 	 */
 	public function merchant(){
 	    $args     = func_get_args();
 	    $method   = array_shift($args);
 	    $merchantController = new adminMerchantController();
 	    $this->wrap_call_user_func_array($merchantController, $method, $args);
 	}
 	
 	/**
 	 * user Controller Routing
 	 */
 	public function user(){
 	    $args     = func_get_args();
 	    $method   = array_shift($args);
 	    $userController = new adminUserController();
 	    $this->wrap_call_user_func_array($userController, $method, $args);
 	}
 	
 	/**
 	 * Article Controller Routing
 	 */
 	public function article(){
 	    $args     = func_get_args();
 	    $method   = array_shift($args);
 	    $articleController = new adminArticleController();
 	    $this->wrap_call_user_func_array($articleController, $method, $args);
 	}
 	
 	/**
 	 * Classified Controller Routing
 	 */
 	public function classified(){
 	    $args     = func_get_args();
 	    $method   = array_shift($args);
 	    $classifiedController = new adminClassifiedController();
 	    $this->wrap_call_user_func_array($classifiedController, $method, $args);
 	}
 	
 	/**
 	 * Event Controller Routing
 	 */
 	public function event(){
 	    $args     = func_get_args();
 	    $method   = array_shift($args);
 	    $eventController = new adminEventController();
 	    $this->wrap_call_user_func_array($eventController, $method, $args);
 	}
 	
 	/**
 	 * Deal Controller Routing
 	 */
 	public function deal(){
 	    $args     = func_get_args();
 	    $method   = array_shift($args);
 	    $dealController = new adminDealController();
 	    $this->wrap_call_user_func_array($dealController, $method, $args);
 	}
 	
 	
 	public function imageCache(){
 	    $dest 		= HTDOCSPATH."/media/";
 	    // Remove the cached files
 	    foreach (glob($dest.'*_*_*_*.jpg') as $filename) {
 	        unlink($filename);
 	    }
 	    header('Location: '.$_SERVER['HTTP_REFERER']); die;
 	}
 	
 	
 	public function logout(){
 	    unset($_SESSION['user']);
 	    unset($_SESSION['app']);
 	    header('Location: /'.WEBPATH.'admin'); die;
 	}
 	
 	public function share(){
 	    
 	    $user = unserialize(userManager::session());
 	    
 	    if ($user){
 	        $args = func_get_args();
 	        $appId = reset($args[0]);
 	    
 	        // Does the app belong to this user?
 	        foreach ($user->ownApp as $app){
 	             
 	            if ($appId == md5($app->id)){
 	                
 	                SingleSmarty::getInstance()->assign('app', $app);
 	                
 	                View::render('share');
 	                return;
 	            }
 	            
 	        }
 	    }
 	    header('Location: /'.WEBPATH.'home'); die;
 	   
 	}

}
?>
