<?php
/**
 *  *
 * @author Tim Fisher
 *
 * Description:
 */



class adminArticleController extends Controller {


    public function index()
    {
        header('Location: /'.WEBPATH.'home'); die;
    }

    public function add()
    {
        // Test to see if there is a user session
        $user = unserialize(userManager::session());

        $args     = func_get_args();
        $id       = isset($args[0])?$args[0]:null;

        if (userManager::grantMerchantAccess($user, $id)){

            $action       = helpers\request('action');
            
            if ($action == 'add'){
                
                if ($user->isadmin == 1){
                    // Find the merchant
                    $merchant = R::findOne('merchant', 'id = ?', array( $_POST['merchant_id'] ));;
                    unset($_POST['merchant_id']);
                } else {
                    // Get the merchant details from the user object
                    $merchant = reset($user->ownMerchant);
                }
                
                unset($_POST['action']);
                // Now that we have the merchant details we can proceed.
                if (isset($merchant) && $merchant->id){
                    
                    // Get a new Article Bean
                    $article = R::dispense('article');
                    
                    $article->setAttr('publicationdate', null);
                    $article->setAttr('pageviews', 0);
                    $article->setAttr('created', helpers\now());
                    $article->setAttr('updated', helpers\now());

                    // Assign the merchant to the user
                    $merchant->ownArticle[] = $article;
                    
                    // Import the data
                    $article->import($_POST);
                    
                    // Save the data - we do this now as we need the ID for the logo upload and assign categories
                    $id = R::store($merchant);
                    
                    // Did we get images in the upload?
                    image::upload($article, 'article', 'logo'); 
                    
                    R::store($article);
                    SingleSmarty::getInstance()->assign('updated', true);
                    
                } else {
                    helpers\addError(array('title'=>'Article Add Error', 'message'=>'We could not locate the specified merchant detail, please select another and try again.'));
                }
               
            }
            
            $merchants = R::findAll('merchant', 'ORDER BY companyname');
            SingleSmarty::getInstance()->assign('merchants', $merchants);

            View::render('admin/addArticle');

        } else {

             header('Location: /'.WEBPATH.'home'); die;

        }

    }
    
    public function edit()
    {

        // Test to see if there is a user session
        $user = unserialize(userManager::session());
    
        $args = func_get_args();
        $id = $args[0];
        
        $thisArticle = R::findOne('article', 'md5(id) = ?', array($id));
        
        if ($thisArticle){
            
            // If there is not a session then display the normal landing page
            if ($user && ($user->isadmin == true || $user->id == $thisArticle->merchant->user->id)){

                $action = helpers\request('action');
                
                // We have some posted data so we need to do the update
                if ($action == 'edit'){
                    // Remove data that is no longer needed.
                    unset($_POST['action']);
                    
                    // Grab the category posting
                    $categories = helpers\request('categories');
                    unset($_POST['categories']);
                    
                    // Only admin can change categories
                    if ($user->isadmin == true || $thisArticle->merchant->level != 'bronze'){
                        // unset all the old categories
                        foreach ($thisArticle->sharedCategory as $category){
                            unset($thisArticle->sharedCategory[$category->id]);
                        }
                        
                        // Add the new categories
                        foreach ($categories as $id){
                            $category = R::findOne('category', 'id=?', array($id));
                            if (isset($category)){
                                $thisArticle->sharedCategory[] = $category;
                            }
                        }
                    }
                    
                    // If there are any image deletes posted then we must act on them
                    $deletes = helpers\request('delete');
                    
                    if (is_array($deletes)){
                        foreach ($deletes as $key=>$value){
                            $thisArticle->setAttr("image{$key}", false);
                        }
                       
                    }
                    unset($_POST['delete']);
                    
                    // Should we un/publish
                    $publish = helpers\request('publish');
                    unset($_POST['publish']);
                    if ($publish){
                        $thisArticle->setAttr('publicationdate', helpers\now());
                    }

                    $unpublish = helpers\request('unpublish');
                    unset($_POST['unpublish']);
                    if ($unpublish){
                        $thisArticle->setAttr('publicationdate', null);
                    }
                    
                    // Any Changes made by a non site admin will put the item back into moderation.
                    if ($user->isadmin != true){
                        $thisArticle->setAttr('publicationdate', null);
                    }
                    
                    // Set update date
                    $thisArticle->setAttr('updated', helpers\now());
                    
                    // Import the updates
                    $thisArticle->import($_POST);
                    
                    // Did we get images in the upload?
                    image::upload($thisArticle, 'article', 'logo');
                    
                    // Save
                    R::store($thisArticle);

                    SingleSmarty::getInstance()->assign('updated', true);
                    
                }
            }
            
            SingleSmarty::getInstance()->assign('article', $thisArticle);
            
            // Assign the various categories
            $categories = R::findAll('category', 'WHERE business != 1 ORDER BY name');
            
            
            // Get own Categories
            $ownCategories = $thisArticle->sharedCategory;
            
            //Now mark the categories that should be pre-selected
            foreach($categories as $category){
                foreach($ownCategories as $myCat){
                    if ($myCat->id == $category->id){
                        $categories[$category->id]->setAttr('selected', true);
                    }
                }
            }
            
            SingleSmarty::getInstance()->assign('categories', $categories);
            
            $merchants = R::findAll('merchant', 'ORDER BY companyname');
            SingleSmarty::getInstance()->assign('merchants', $merchants);
            
            View::render('admin/editArticle');
 
        } else {
    
            header('Location: /'.WEBPATH.'admin'); die;
    
        }
    
    }
    
    /**
     * Publish and return to referring page
     */
    public function publish()
    {
        // Test to see if there is a user session
        $user = unserialize(userManager::session());
    
        // If there is not a session then display the normal landing page
        if ($user && ($user->isadmin == true || reset($user->ownMerchant)->level != 'bronze')){
    
            $args = func_get_args();
            $id = $args[0];
    
            $thisArticle = R::findOne('article', 'md5(id) = ?', array($id));
    
            if ($thisArticle){
                $thisArticle->setAttr('publicationdate', helpers\now());
                R::store($thisArticle);
               header('Location: '.$_SERVER['HTTP_REFERER']); die;
            }
        }
        
        header('Location: /'.WEBPATH.'admin'); die;
    }
    
    public function unpublish()
    {
        // Test to see if there is a user session
        $user = unserialize(userManager::session());
    
        // If there is not a session then display the normal landing page
        if ($user && $user->isadmin == true){
    
            $args = func_get_args();
            $id = $args[0];
    
            $thisArticle = R::findOne('article', 'md5(id) = ?', array($id));
    
            if ($thisArticle){
                $thisArticle->setAttr('publicationdate', null);
                R::store($thisArticle);
                header('Location: '.$_SERVER['HTTP_REFERER']); die;
            }
        }
    
        header('Location: /'.WEBPATH.'admin'); die;
    }
    
    public function listing(){
        
        // Test to see if there is a user session
        $user = unserialize(userManager::session());
        
        $args     = func_get_args();
        $id       = isset($args[0])?$args[0]:null;

        if (userManager::grantMerchantAccess($user, $id)){
    
            if ($id){
                $articles = R::findAll('article', ' WHERE md5(merchant_id) = ? ORDER BY publicationdate ASC', array($id));
            } else {
                $articles = R::findAll('article', ' WHERE isnull(deleted) ORDER BY publicationdate ASC'); // Non Published first
            }

            SingleSmarty::getInstance()->assign('articles', $articles);
        
            View::render('admin/listArticles');
        
        } else {
        
            header('Location: /'.WEBPATH.'admin'); die;
        
        }
        
    }
    
    public function delete(){
        // Test to see if there is a user session
        $user = unserialize(userManager::session());
    
        $args = func_get_args();
        $id = $args[0];
        
        $article = R::findOne('article', 'md5(id) = ?', array($id));
        
        if ($article){
            
            // If there is not a session then display the normal landing page
            if ($user && ($user->isadmin == true || $user->id == $article->merchant->user->id)){

                $article->setAttr('deleted', true);
                R::store($article);
                header('Location: '.$_SERVER['HTTP_REFERER']); die;
            }
        }
    
        header('Location: /'.WEBPATH.'admin'); die;
    }

   
}
?>
