<?php
/**
 *  *
 * @author Tim Fisher
 *
 * Description:
 */



class staticController extends Controller {

    public function index($ident)
    {
        
        $page = R::findOne('cms', 'title = ?', array(urldecode($ident)));
        SingleSmarty::getInstance()->assign('page', $page);
        
        widgetManager::browseByCategory();
        
        // Grab a larger resultset
        $mostEvents = eventManager::betweenDates(new DateTime('-12months'), new DateTime('+12months'));
        SingleSmarty::getInstance()->assign('mostEvents', $mostEvents['results']);
        
        View::render('static');
    }


}
?>
