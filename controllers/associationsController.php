<?php
/**
 * 
 * @author Tim Fisher - Tartan Tangerine Limited 2013
 *
 */

class associationsController extends Controller {

    public function index()
    {        
        // Grab Complete list of merchants
        $merchants = associationManager::all();
        SingleSmarty::getInstance()->assign('merchants',     $merchants['results']);
        SingleSmarty::getInstance()->assign('resultCount',   $merchants['count']);
        
        SingleSmarty::getInstance()->assign('letters', associationManager::azFilter());
        
        widgetManager::browseByCategory();

        View::render('associations/home');
    }
    
    /**
     * Merchants by Category
     */
    public function category()
    {
        $args = func_get_args();
        $name   = isset($args[0])?$args[0]:null;

        if ($name && $name != ''){
            $merchants = associationManager::byCategory( urldecode($name) );
            SingleSmarty::getInstance()->assign('merchants',     $merchants['results']);
            SingleSmarty::getInstance()->assign('resultCount',   $merchants['count']);
           
            SingleSmarty::getInstance()->assign('letters', associationManager::azFilterCategory(urldecode($name)));

            widgetManager::browseByCategory();
            
            SingleSmarty::getInstance()->assign('currentCategory', urldecode($name));
        
            View::render('associations/home');
        } else {
            $this->index();
            exit;
        }
    }
    
    /**
     * Merchant Detail view
     */
    public function detail()
    {
        $args = func_get_args();
        $name   = isset($args[0])?$args[0]:null; // merchant company name
        if ($name && $name != ''){
            
            $merchant = associationManager::byName( urldecode($name) );
            SingleSmarty::getInstance()->assign('merchant', $merchant);
            
            // Increment the page Views
            $mBean = R::load('merchant', $merchant->id);
            $mBean->setAttr('pageviews', $merchant->pageviews + 1); // Double increments with FB on
            R::store($mBean);

            // Merchant deals
            $deals = dealManager::byMerchant($merchant->id);
            SingleSmarty::getInstance()->assign('deals', $deals);
            
            // Do Captcha for email form
            captcha::init();
            
            View::render('associations/detail');
        } else {
            $this->index();
            exit;
        }
        
    }


}
?>
