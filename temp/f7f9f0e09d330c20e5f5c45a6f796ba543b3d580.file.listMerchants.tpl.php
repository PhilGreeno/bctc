<?php /* Smarty version Smarty-3.1.11, created on 2013-04-21 20:56:40
         compiled from "/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/listMerchants.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1460406152516edb370d9a41-31361706%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f7f9f0e09d330c20e5f5c45a6f796ba543b3d580' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/listMerchants.tpl',
      1 => 1366577690,
      2 => 'file',
    ),
    'ea5c3c47a8e1a84af03770cc3273d76fcb337f13' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/default.tpl',
      1 => 1366557986,
      2 => 'file',
    ),
    '8d47d2679bde4a59b2fd6226caf5bfa84b8a8032' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/navbar.tpl',
      1 => 1366551475,
      2 => 'file',
    ),
    '5305d5aa8b4dcce68b0ae1849436caad059d5d14' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/sidebar.tpl',
      1 => 1366575684,
      2 => 'file',
    ),
    '774e1eab0bb52c84fb6cfee143ebdd0b05690be5' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/errors.tpl',
      1 => 1363965009,
      2 => 'file',
    ),
    'e33895229ebdafc4b6248d1072af562053bcc3b8' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/footer.tpl',
      1 => 1363965009,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1460406152516edb370d9a41-31361706',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_516edb37362833_52095269',
  'variables' => 
  array (
    'WEBPATH' => 0,
    'sess_user' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_516edb37362833_52095269')) {function content_516edb37362833_52095269($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
	<head>
		<title>BCTC Admin :: Dashboard</title>
		
		
		<meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		
		
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/fullcalendar.css" />	
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/unicorn.main.css" />
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/unicorn.blue.css" class="skin-color" />
		<link rel="stylesheet/less" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/custom.less" />

		
		
    <link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/jquery-ui.css" />
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/uniform.css" />
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/select2.css" />		

		
	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
js/less-1.3.3.min.js" type="text/javascript"></script>
	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/excanvas.min.js"></script>
    <script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/jquery.min.js"></script>
    <script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/jquery-ui.custom.js"></script>
    <script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/bootstrap.min.js"></script>
    <script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/unicorn.js"></script>
   	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/admin.js"></script>
   	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/custom.js"></script>



		
		<script>
			var WEBPATH = '<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
';
		</script>

		
	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/jquery.uniform.js"></script>
	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/select2.min.js"></script>
	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/jquery.dataTables.min.js"></script>
	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/unicorn.tables.js"></script>

		
		
	</head>
	
	<body lang="en" id="body">
	
		<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin!=1){?>
			<?php $_smarty_tpl->tpl_vars['merchant'] = new Smarty_variable(reset($_smarty_tpl->tpl_vars['sess_user']->value->ownMerchant), null, 0);?>
		<?php }?>

		
			<?php /*  Call merged included template "admin/snippets/navbar.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('admin/snippets/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '1460406152516edb370d9a41-31361706');
content_5174528828d4b5_46055601($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "admin/snippets/navbar.tpl" */?>
		
		
		
	<?php /*  Call merged included template "admin/snippets/sidebar.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('admin/snippets/sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('sub'=>'merchant','item'=>'listing'), 0, '1460406152516edb370d9a41-31361706');
content_517452882c5568_34579808($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "admin/snippets/sidebar.tpl" */?>


		<div id="content">
		

	<div id="content-header">
				<h1>Manage Merchants</h1>

			</div>
			<div id="breadcrumb">
				<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="#" class="current">Merchants</a>
			
			</div>
			<div class="container-fluid">
			
				<?php /*  Call merged included template "admin/snippets/errors.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('admin/snippets/errors.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '1460406152516edb370d9a41-31361706');
content_51745288463c37_48757055($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "admin/snippets/errors.tpl" */?>
			
				<div class="row-fluid">
					<div class="span12">

						<a href='/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/merchant/add' class='btn addButton'>
							<i class="icon-plus"></i><span>Add Merchant</span>
						</a>

						<a href='#' class='btn addButton'>
							<i class="icon-plus"></i><span>Manage Categories</span>
						</a> 

						<div class="widget-box">
							<div class="widget-title">
								<span class="icon iconCreate"><a href='/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/merchant/add' class='tip-right' title="Add New Merchant"><i class="icon-plus-sign"></i></a></span>
								<h5>Merchant Listing</h5>
							</div>
							<div class="widget-content nopadding">
								<table class="table table-bordered table-striped table-hover data-table">
									<thead>
  									<tr>
  										<th>Company Name</th>
  										<th>Contact</th>
  										<th>Email</th>
  										<th>Sticky</th>
  										<th>Views</th>
  										<th></th>
  									</tr>
									</thead>
									<tbody>
									<?php  $_smarty_tpl->tpl_vars['merchant'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['merchant']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['merchants']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['merchant']->key => $_smarty_tpl->tpl_vars['merchant']->value){
$_smarty_tpl->tpl_vars['merchant']->_loop = true;
?>
  									<tr class="gradeA">
    									<td>

    										<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/merchant/edit/<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
" class="viewAdminPage tip-left" title="View &amp; Edit Details">
											<i class="icon-edit friendlyIcon"></i>

    											<?php echo $_smarty_tpl->tpl_vars['merchant']->value->companyname;?>


    										</a>

    									</td>
    									<td><?php echo $_smarty_tpl->tpl_vars['merchant']->value->firstname;?>
 <?php echo $_smarty_tpl->tpl_vars['merchant']->value->lastname;?>
</td>
    									<td><?php echo $_smarty_tpl->tpl_vars['merchant']->value->user->email;?>
</td>
    									<td><?php echo $_smarty_tpl->tpl_vars['merchant']->value->sticky;?>
</td>
    									<td><?php echo $_smarty_tpl->tpl_vars['merchant']->value->pageviews;?>
</td>
    									<td class="taskOptions">
    										<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/merchant/edit/<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
" class="btn btn-primary btn-mini">Edit</a>
    										<a href="javascript://" onclick="admin.deleteMerchant('<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
');" class="btn btn-danger btn-mini">Delete</a>
    									</td>
  									</tr>
  								<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
								
					</div>
				</div>
			
			
				
				<div class="row-fluid">
					<?php /*  Call merged included template "admin/snippets/footer.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('admin/snippets/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '1460406152516edb370d9a41-31361706');
content_517452885280e0_07014374($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "admin/snippets/footer.tpl" */?>
				</div>
			</div>


		</div>
		
		
	</body>

</html><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-04-21 20:56:40
         compiled from "/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/navbar.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5174528828d4b5_46055601')) {function content_5174528828d4b5_46055601($_smarty_tpl) {?><div id="header">
	<h1><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin"><img src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/img/bctc.gif" width="50px" alt=""><span>BCTC Admin</span></a></h1>		
</div>


<div id="user-nav" class="navbar navbar-inverse">
    <ul class="nav btn-group">
        <li class="btn btn-inverse" ><a title="" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/user/edit/<?php echo md5($_smarty_tpl->tpl_vars['sess_user']->value->id);?>
"><i class="icon icon-user"></i> <span class="text">Profile</span></a></li>
        
        
      
        <li class="btn btn-inverse"><a title="" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/logout"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
    </ul>
</div><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-04-21 20:56:40
         compiled from "/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/sidebar.tpl" */ ?>
<?php if ($_valid && !is_callable('content_517452882c5568_34579808')) {function content_517452882c5568_34579808($_smarty_tpl) {?><div id="sidebar">
	<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
	<ul>
		<li class="<?php if (!$_smarty_tpl->tpl_vars['sub']->value){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin"><i class="icon icon-home"></i> <span>Dashboard</span></a>
		</li>
		
<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin==1){?>

		<!-- <li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='blog'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/blog"><i class="icon icon-th-list"></i> <span>Blog</span></a>
		</li> -->

		<li>
			<a href="#"><i class="icon icon-signal"></i> <span>Analytics</span></a>
		</li>

		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='category'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/category/listing"><i class="icon icon-folder-open"></i> <span>Categories</span></a>
		</li>

		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='user'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/user/listing"><i class="icon icon-user"></i> <span>Users</span></a>
		</li>
				
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='merchant'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/merchant/listing"><i class="icon icon-th-list"></i> <span>Merchants</span></a>
		</li>
<?php }?>			
<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin==0){?>
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='merchant'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/merchant/edit/<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
"><i class="icon icon-th-list"></i> <span>Listing</span></a>
		</li>
<?php }?>
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='article'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/article/listing<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin!=1){?>/<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
<?php }?>"><i class="icon icon-th-list"></i> <span>Articles</span></a>
		</li>
		
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='classified'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/classified/listing<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin!=1){?>/<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
<?php }?>"><i class="icon icon-th-list"></i> <span>Classifieds</span></a>
		</li>
		
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='event'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/event/listing<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin!=1){?>/<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
<?php }?>"><i class="icon icon-th-list"></i> <span>Events</span></a>
		</li>
		
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='deal'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/deal/listing<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin!=1){?>/<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
<?php }?>"><i class="icon icon-th-list"></i> <span>Deals</span></a>
		</li>
		
<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin==1){?>
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='cms'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/cms/listing"><i class="icon icon-th-list"></i> <span>CMS</span></a>
		</li>
<?php }?>
	
	
	</ul>

</div><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-04-21 20:56:40
         compiled from "/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/errors.tpl" */ ?>
<?php if ($_valid && !is_callable('content_51745288463c37_48757055')) {function content_51745288463c37_48757055($_smarty_tpl) {?><?php if (isset($_smarty_tpl->tpl_vars['errors']->value)){?>
<div class="container">

	<?php  $_smarty_tpl->tpl_vars['error'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['error']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['errors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['error']->key => $_smarty_tpl->tpl_vars['error']->value){
$_smarty_tpl->tpl_vars['error']->_loop = true;
?>
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<h4><?php echo $_smarty_tpl->tpl_vars['error']->value['title'];?>
</h4>
			<?php echo $_smarty_tpl->tpl_vars['error']->value['message'];?>

		</div>
	<?php } ?>

</div>
<?php }?><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-04-21 20:56:40
         compiled from "/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/footer.tpl" */ ?>
<?php if ($_valid && !is_callable('content_517452885280e0_07014374')) {function content_517452885280e0_07014374($_smarty_tpl) {?><div id="footer" class="span12">
	2012 - 2013 &copy; BCTC Admin
</div><?php }} ?>