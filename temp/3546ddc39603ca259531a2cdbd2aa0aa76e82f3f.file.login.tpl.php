<?php /* Smarty version Smarty-3.1.11, created on 2013-04-29 21:17:32
         compiled from "/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1646110433516ed137668584-60644457%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3546ddc39603ca259531a2cdbd2aa0aa76e82f3f' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/login.tpl',
      1 => 1366724701,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1646110433516ed137668584-60644457',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_516ed1376c6be8_32116493',
  'variables' => 
  array (
    'WEBPATH' => 0,
    'errors' => 0,
    'error' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_516ed1376c6be8_32116493')) {function content_516ed1376c6be8_32116493($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
    <head>
        <title>Broadstone Village Admin</title>
		<meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
				<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/bootstrap.min.css" />
				<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/unicorn.login.css" />
    </head>
    <body>
        <div id="logo">
            <img src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/img/bctc.gif" width="50px" alt="" />
        </div>
        <div id="loginbox">            
            <form id="loginform" class="form-vertical" action="" method='post'>
							<p>Enter email and password to continue.</p>
                <div class="control-group">
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-user"></i></span><input type="email" name='email' placeholder="Email" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-lock"></i></span><input type="password" name='password' placeholder="Password" />
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link" id="to-recover">Lost password?</a></span>
                    <span class="pull-right"><input type="submit" class="btn btn-inverse" value="Login" /></span>
                </div>
            </form>
            <form id="recoverform" action="#" class="form-vertical">
        				<p>Enter your e-mail address below and we will send you instructions how to recover a password.</p>
        				<div class="control-group">
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-envelope"></i></span><input type="text" placeholder="E-mail address" />
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link" id="to-login">&lt; Back to login</a></span>
                    <span class="pull-right"><input type="submit" class="btn btn-inverse" value="Recover" /></span>
                </div>
                <div class='clearfix'></div>
            </form>
        </div>
        
        <?php if (isset($_smarty_tpl->tpl_vars['errors']->value)){?>
        	<br/><br/>
    			<div class="container">
    			
      			<?php  $_smarty_tpl->tpl_vars['error'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['error']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['errors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['error']->key => $_smarty_tpl->tpl_vars['error']->value){
$_smarty_tpl->tpl_vars['error']->_loop = true;
?>
      				<div class="alert alert-error">
      					<button type="button" class="close" data-dismiss="alert">&times;</button>
        				<h4><?php echo $_smarty_tpl->tpl_vars['error']->value['title'];?>
</h4>
        				<?php echo $_smarty_tpl->tpl_vars['error']->value['message'];?>

      				</div>
      			<?php } ?>
    			
    			</div>
  			<?php }?>
        
        <script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/jquery.min.js"></script>  
        <script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/unicorn.login.js"></script> 
    </body>
</html>
<?php }} ?>