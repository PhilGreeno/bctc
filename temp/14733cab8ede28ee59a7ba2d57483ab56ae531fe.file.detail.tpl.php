<?php /* Smarty version Smarty-3.1.11, created on 2013-06-12 21:51:36
         compiled from "/Applications/MAMP/htdocs/bctc/bctc/views/articles/detail.tpl" */ ?>
<?php /*%%SmartyHeaderCode:193406894151af168894f4d8-69873063%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '14733cab8ede28ee59a7ba2d57483ab56ae531fe' => 
    array (
      0 => '/Applications/MAMP/htdocs/bctc/bctc/views/articles/detail.tpl',
      1 => 1370558370,
      2 => 'file',
    ),
    '5e94b194cab0748134f89e4a3eab5f36eb985ae6' => 
    array (
      0 => '/Applications/MAMP/htdocs/bctc/bctc/views/snippets/default.tpl',
      1 => 1370005828,
      2 => 'file',
    ),
    '6422e8e76152a83a609f52437f521ff9d0d6ea0b' => 
    array (
      0 => '/Applications/MAMP/htdocs/bctc/bctc/views/snippets/navBar.tpl',
      1 => 1371073158,
      2 => 'file',
    ),
    '7dc21faffda705d286f9e9c391864ceedf01177a' => 
    array (
      0 => '/Applications/MAMP/htdocs/bctc/bctc/views/snippets/widgets/searchBar.tpl',
      1 => 1370558286,
      2 => 'file',
    ),
    '3cb7041a1aa0987946d16b2f1ff7ab0d967eabe3' => 
    array (
      0 => '/Applications/MAMP/htdocs/bctc/bctc/views/snippets/topBar.tpl',
      1 => 1370552996,
      2 => 'file',
    ),
    '02c88ba2557b2fe726c2d9fbb7ce06c0d18be55e' => 
    array (
      0 => '/Applications/MAMP/htdocs/bctc/bctc/views/snippets/widgets/imageGrid.tpl',
      1 => 1367530638,
      2 => 'file',
    ),
    '4333c72d2b89090f39071e1dd41727b8fb59423f' => 
    array (
      0 => '/Applications/MAMP/htdocs/bctc/bctc/views/snippets/footer.tpl',
      1 => 1370594192,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '193406894151af168894f4d8-69873063',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51af1688c9c575_43961179',
  'variables' => 
  array (
    'WEBPATH' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51af1688c9c575_43961179')) {function content_51af1688c9c575_43961179($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_capitalize')) include '/Applications/MAMP/htdocs/bctc/bctc/libraries/smarty/libs/plugins/modifier.capitalize.php';
if (!is_callable('smarty_modifier_date_format')) include '/Applications/MAMP/htdocs/bctc/bctc/libraries/smarty/libs/plugins/modifier.date_format.php';
if (!is_callable('smarty_modifier_markdown')) include '/Applications/MAMP/htdocs/bctc/bctc/libraries/smarty/libs/plugins/modifier.markdown.php';
?><!doctype html>
<!--[if lt IE 7]>      <html class="no-js ie6"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie7"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js">     <!--<![endif]-->

	<head>
		
		<title>BCTC :: <?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['article']->value->title);?>
</title>
		
		<script>
			document.documentElement.className = document.documentElement.className.replace(/(\s|^)no-js(\s|$)/, '$1' + 'js' + '$2');
		</script>
		
		
		<meta charset="UTF-8" />
		<meta name="robots" content="index, follow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		

		<link rel='stylesheet' type='text/css' href='/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
css/bootstrap.min.css'>
		<link rel='stylesheet' type='text/css' href='/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
css/bootstrap-responsive.min.css'>
		<link rel='stylesheet' type='text/css' href='/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
css/main.css'>
		<link rel='stylesheet' type='text/css' href='/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
css/responsiveslides.css'>
		
		<link rel="stylesheet" type="text/css" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
css/app.css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
	
		
<link href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
css/jquery.fancybox.css" rel="stylesheet">


		<!--<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
js/less-1.3.3.min.js" type="text/javascript"></script>-->
		<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
js/jquery.js"></script>
		<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
js/bootstrap.min.js"></script>
		<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
js/app.js"></script>
		<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
js/custom.js"></script>
		
		

<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
js/jquery.fancybox.js"></script>
<script>
	$(document).ready(function() {
		$('.thumbnail').fancybox({
			openEffect  : 'none',
			closeEffect : 'none'
		});
	});
</script>





	</head>
	
	<body lang="en" id="body">



		
			<?php /*  Call merged included template "snippets/navBar.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('snippets/navBar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('active'=>'articles'), 0, '193406894151af168894f4d8-69873063');
content_51b8ed68a84b27_87181982($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "snippets/navBar.tpl" */?>
			
			<?php /*  Call merged included template "snippets/topBar.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('snippets/topBar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '193406894151af168894f4d8-69873063');
content_51b8ed68b38f26_36818983($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "snippets/topBar.tpl" */?>


		<div id="content">
		


		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span9">                   
					
					
					<div class="main_content detail-product detail-back">						
						<h3 class="title justTitle"><?php echo $_smarty_tpl->tpl_vars['article']->value->title;?>
</h3>			
						<div class="row-fluid">
							

							<div class="span8">
								
								<ul class="social pull-right">
									<li>									
										<div class="fb-like" send="false" layout="button_count" ></div>
										<script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script>
									</li>
									<li>
										<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal" >Tweet</a>
										<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
									</li>
								</ul>

		
							
								<h5>Overview</h5>
								<p><?php echo (($tmp = @$_smarty_tpl->tpl_vars['article']->value->abstract)===null||$tmp==='' ? 'n/a' : $tmp);?>
</p>				
								
								<p><strong>Published:</strong> <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['article']->value->publicationdate);?>
</p>
								<p><strong>By:</strong> <?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['article']->value->author);?>
</p>
								
								<p><?php echo smarty_modifier_markdown((($tmp = @$_smarty_tpl->tpl_vars['article']->value->content)===null||$tmp==='' ? 'n/a' : $tmp));?>
</p>
										
														
							</div>
							
							<div class="span4">
								
								<?php /*  Call merged included template "snippets/widgets/imageGrid.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('snippets/widgets/imageGrid.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('type'=>'article','obj'=>$_smarty_tpl->tpl_vars['article']->value), 0, '193406894151af168894f4d8-69873063');
content_51b8ed68c76172_88199676($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "snippets/widgets/imageGrid.tpl" */?>
								
							</div>
														
						</div>
											
					</div>
				</div>
				
				<div class="span3">
					
					<h3 class="title justTitle">Comments</h3><br/>
					
					<?php ob_start();?><?php echo md5($_smarty_tpl->tpl_vars['article']->value->id);?>
<?php $_tmp1=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ('snippets/widgets/comments.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('comment_ident'=>"articles/detail/".$_tmp1), 0);?>

					
					
					
						
				</div>
			</div>			
		</div>
		

		</div>
		
		
			<?php /*  Call merged included template "snippets/footer.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('snippets/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '193406894151af168894f4d8-69873063');
content_51b8ed68dc1844_92383625($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "snippets/footer.tpl" */?>
		
		
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-40328774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script type="text/javascript" src="//use.typekit.net/mlu5jbc.js"></script>
 <script type="text/javascript">try{Typekit.load();}catch(e){}</script> 


		
	</body>



</html><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-06-12 21:51:36
         compiled from "/Applications/MAMP/htdocs/bctc/bctc/views/snippets/navBar.tpl" */ ?>
<?php if ($_valid && !is_callable('content_51b8ed68a84b27_87181982')) {function content_51b8ed68a84b27_87181982($_smarty_tpl) {?>		<div class="navbar">
			<div class="navbar-inner main-menu">
				<div class="container-fluid">
					<div class="clearfix row-fluid">

						
						
						<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">

							Menu
						</a>						
						<div class="nav-collapse collapse">							 		                 
							<ul class="nav" id="main-nav">								
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='home'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
">Home</a></li>
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='business'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
business">Businesses</a></li>	
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='association'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
static/About+the+Broadstone+Chamber">Chamber of Trade</a></li>	
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='association'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
associations">Associations</a></li>		
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='events'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
events">Events</a></li>						
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='classifieds'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
classifieds">Classifieds</a></li>								
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='deals'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
deals">Deals</a></li>
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='articles'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
articles">Articles</a></li>								
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='blog'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
blog">Blog</a></li>								
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='contact'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
static/contact+us">Contact Us</a></li>						
							</ul>
						</div>
						
					</div>
				</div>
			</div>		
		</div>
		<div class="clearfix"></div><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-06-12 21:51:36
         compiled from "/Applications/MAMP/htdocs/bctc/bctc/views/snippets/topBar.tpl" */ ?>
<?php if ($_valid && !is_callable('content_51b8ed68b38f26_36818983')) {function content_51b8ed68b38f26_36818983($_smarty_tpl) {?>		<div class="container-fluid bannerTop" id="top-bar">
			<div class="row-fluid">
				<div class="span2 backGrad">
					<a class="brand pull-left" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
"></a>
				</div>

				<div class="span6 offset2 hidden-phone">
					<div class="ribbonTop"><div class="ribbon-stitches-top"></div><strong class="ribbon-content"><h1>Welcome to Broadstone Village</h1></strong><div class="ribbon-stitches-bottom"></div></div> 

				</div>

				

			</div>
		</div>

		<div class="container-fluid searchWrap">
			<div class="row-fluid">



					<span class="welcomeSearch pull-left hidden-phone offset1">Visit Broadstone and find local offers</span>
					




			<?php /*  Call merged included template "snippets/widgets/searchBar.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('snippets/widgets/searchBar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('active'=>'business'), 0, '193406894151af168894f4d8-69873063');
content_51b8ed68b44ce9_29480574($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "snippets/widgets/searchBar.tpl" */?>

			</div>
		</div><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-06-12 21:51:36
         compiled from "/Applications/MAMP/htdocs/bctc/bctc/views/snippets/widgets/searchBar.tpl" */ ?>
<?php if ($_valid && !is_callable('content_51b8ed68b44ce9_29480574')) {function content_51b8ed68b44ce9_29480574($_smarty_tpl) {?>				
			<div class="span5">
				<form class="search_top_form" action='/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
search' method='POST'>
					<div class="ribbon-wrapper hidden-phone">
						<div class="ribbon hidden-phone">Search!</div>
					</div>
						<ul>
							<li>
								<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
" name='keyword' placeholder="Keyword...">
							</li>
							<li>
								<select name="type">
									<option value="business" <?php if ($_smarty_tpl->tpl_vars['active']->value=='business'){?>SELECTED=SELECTED<?php }?>>Business</option>
									<option value="events" <?php if ($_smarty_tpl->tpl_vars['active']->value=='events'){?>SELECTED=SELECTED<?php }?>>Events</option>
									<option value="classifieds" <?php if ($_smarty_tpl->tpl_vars['active']->value=='classifieds'){?>SELECTED=SELECTED<?php }?>>Classifieds</option>
									<option value="deals" <?php if ($_smarty_tpl->tpl_vars['active']->value=='deals'){?>SELECTED=SELECTED<?php }?>>Deals</option>
									<option value="articles" <?php if ($_smarty_tpl->tpl_vars['active']->value=='articles'){?>SELECTED=SELECTED<?php }?>>Articles</option>
									<option value="blog" <?php if ($_smarty_tpl->tpl_vars['active']->value=='blog'){?>SELECTED=SELECTED<?php }?>>Blog</option>
								</select>
							</li>
						
							<li>
								<button class="moreButton findButton"><i class="icon-search icon-white"></i> Search Now</button>
							</li>
						</ul>
					</form>

			</div><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-06-12 21:51:36
         compiled from "/Applications/MAMP/htdocs/bctc/bctc/views/snippets/widgets/searchBar.tpl" */ ?>
<?php if ($_valid && !is_callable('content_51b8ed68b96ed7_78320456')) {function content_51b8ed68b96ed7_78320456($_smarty_tpl) {?>				
			<div class="span5">
				<form class="search_top_form" action='/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
search' method='POST'>
					<div class="ribbon-wrapper hidden-phone">
						<div class="ribbon hidden-phone">Search!</div>
					</div>
						<ul>
							<li>
								<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
" name='keyword' placeholder="Keyword...">
							</li>
							<li>
								<select name="type">
									<option value="business" <?php if ($_smarty_tpl->tpl_vars['active']->value=='business'){?>SELECTED=SELECTED<?php }?>>Business</option>
									<option value="events" <?php if ($_smarty_tpl->tpl_vars['active']->value=='events'){?>SELECTED=SELECTED<?php }?>>Events</option>
									<option value="classifieds" <?php if ($_smarty_tpl->tpl_vars['active']->value=='classifieds'){?>SELECTED=SELECTED<?php }?>>Classifieds</option>
									<option value="deals" <?php if ($_smarty_tpl->tpl_vars['active']->value=='deals'){?>SELECTED=SELECTED<?php }?>>Deals</option>
									<option value="articles" <?php if ($_smarty_tpl->tpl_vars['active']->value=='articles'){?>SELECTED=SELECTED<?php }?>>Articles</option>
									<option value="blog" <?php if ($_smarty_tpl->tpl_vars['active']->value=='blog'){?>SELECTED=SELECTED<?php }?>>Blog</option>
								</select>
							</li>
						
							<li>
								<button class="moreButton findButton"><i class="icon-search icon-white"></i> Search Now</button>
							</li>
						</ul>
					</form>

			</div><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-06-12 21:51:36
         compiled from "/Applications/MAMP/htdocs/bctc/bctc/views/snippets/widgets/imageGrid.tpl" */ ?>
<?php if ($_valid && !is_callable('content_51b8ed68c76172_88199676')) {function content_51b8ed68c76172_88199676($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['obj']->value->image1){?>		
  <a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
image/<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
_<?php echo md5($_smarty_tpl->tpl_vars['obj']->value->id);?>
_1/800/540/image.jpg" class="thumbnail" data-fancybox-group="group1" >
  	<img alt="" src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
image/<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
_<?php echo md5($_smarty_tpl->tpl_vars['obj']->value->id);?>
_1/320/240/">
  </a>												
<?php }else{ ?>
	<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
image/placeholder/800/540/image.jpg" class="thumbnail" data-fancybox-group="group1" >
  	<img alt="" src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
image/placeholder/320/240/">
  </a>		
<?php }?>
<ul class="thumbnails small">				
<?php if ($_smarty_tpl->tpl_vars['obj']->value->image2){?>				
	<li class="span4">
		<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
image/<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
_<?php echo md5($_smarty_tpl->tpl_vars['obj']->value->id);?>
_2/800/540/image.jpg" class="thumbnail" data-fancybox-group="group1"><img src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
image/<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
_<?php echo md5($_smarty_tpl->tpl_vars['obj']->value->id);?>
_2/300/180/" alt=""></a>
	</li>						
<?php }?>		
<?php if ($_smarty_tpl->tpl_vars['obj']->value->image3){?>		
	<li class="span4">
		<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
image/<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
_<?php echo md5($_smarty_tpl->tpl_vars['obj']->value->id);?>
_3/800/540/image.jpg" class="thumbnail" data-fancybox-group="group1"><img src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
image/<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
_<?php echo md5($_smarty_tpl->tpl_vars['obj']->value->id);?>
_3/300/180/" alt=""></a>
	</li>
<?php }?>			
<?php if ($_smarty_tpl->tpl_vars['obj']->value->image4){?>													
	<li class="span4">
		<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
image/<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
_<?php echo md5($_smarty_tpl->tpl_vars['obj']->value->id);?>
_4/800/540/image.jpg" class="thumbnail" data-fancybox-group="group1"><img src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
image/<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
_<?php echo md5($_smarty_tpl->tpl_vars['obj']->value->id);?>
_4/300/180/" alt=""></a>
	</li>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['obj']->value->image5){?>													
	<li class="span4">
		<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
image/<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
_<?php echo md5($_smarty_tpl->tpl_vars['obj']->value->id);?>
_5/800/540/image.jpg" class="thumbnail" data-fancybox-group="group1"><img src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
image/<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
_<?php echo md5($_smarty_tpl->tpl_vars['obj']->value->id);?>
_5/300/180/" alt=""></a>
	</li>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['obj']->value->image6){?>													
	<li class="span4">
		<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
image/<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
_<?php echo md5($_smarty_tpl->tpl_vars['obj']->value->id);?>
_6/800/540/image.jpg" class="thumbnail" data-fancybox-group="group1"><img src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
image/<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
_<?php echo md5($_smarty_tpl->tpl_vars['obj']->value->id);?>
_6/300/180/" alt=""></a>
	</li>
<?php }?>	
</ul><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-06-12 21:51:36
         compiled from "/Applications/MAMP/htdocs/bctc/bctc/views/snippets/footer.tpl" */ ?>
<?php if ($_valid && !is_callable('content_51b8ed68dc1844_92383625')) {function content_51b8ed68dc1844_92383625($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/Applications/MAMP/htdocs/bctc/bctc/libraries/smarty/libs/plugins/modifier.replace.php';
?>		<div class="container-fluid">
			
			<div class="row-fluid broadstoneMessage">

				<p class="footerBlurb ">"By surveying residents we have understood that customers expect to feel welcomed,respected, to have choice and value and to be satisfied and safe" - <span>Broadstone Chamber</span></p>	

			</div>
		</div>




		<div class="container-fluid footer">		
			

			<div class="row-fluid">					
				 <div class="span3">   
					<h4 class="">Information</h4>
					<ul>
					
					<?php  $_smarty_tpl->tpl_vars['page'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['page']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cmsPages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['page']->key => $_smarty_tpl->tpl_vars['page']->value){
$_smarty_tpl->tpl_vars['page']->_loop = true;
?>
						<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
static/<?php echo smarty_modifier_replace(trim($_smarty_tpl->tpl_vars['page']->value->title),' ','+');?>
"><?php echo $_smarty_tpl->tpl_vars['page']->value->link;?>
</a></li>
					<?php }
if (!$_smarty_tpl->tpl_vars['page']->_loop) {
?>
					<?php } ?>
					
					</ul>
				</div>
				<div class="span3">
					<h4>My Account</h4>
					<ul>
						<li><a href="#">My Account</a></li>
						<li><a href="#">Order History</a></li>
						<li><a href="#">Wish List</a></li>
						<li><a href="#">Newsletter</a></li>
					</ul>
				</div>
				<div class="span3">
					<h4>Connect with us</h4>
					<a href="#"><img src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
img/social-facebook.png" alt="Facebook" /></a>
					<a href="#"><img src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
img/social-twitter.png" alt="Twitter" /></a>
					<a href="#"><img src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
img/social-rss.png" alt="RSS" /></a>
					<a href="#"><img src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
img/social-flickr.png" alt="Flickr" /></a>
				</div>
				<div class="span3">
					<div class="company_info">
						<h4>Broadstone Chamber of Trade &amp; Commerce</h4>
						<p>
							1 Broadway, Broadstone, BH18 8BH
						</p>
					</div>
				</div>					
			</div>	
		</div><?php }} ?>