<?php /* Smarty version Smarty-3.1.11, created on 2013-04-26 16:25:34
         compiled from "/Applications/MAMP/htdocs/BCTC/BCTC/views/snippets/widgets/heroMap.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1935663222517a91de9ba6c0-02376839%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f9b229beefadcef9115aa3bef25b0c908e4a38f7' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/snippets/widgets/heroMap.tpl',
      1 => 1366993534,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1935663222517a91de9ba6c0-02376839',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_517a91deadace9_95779183',
  'variables' => 
  array (
    'WEBPATH' => 0,
    'mapper' => 0,
    'merchant' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_517a91deadace9_95779183')) {function content_517a91deadace9_95779183($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/Applications/MAMP/htdocs/BCTC/BCTC/libraries/smarty/libs/plugins/modifier.replace.php';
if (!is_callable('smarty_modifier_truncate')) include '/Applications/MAMP/htdocs/BCTC/BCTC/libraries/smarty/libs/plugins/modifier.truncate.php';
?><div id="map" class="map" style='height:280px'>&nbsp;</div>

<script src="http://maps.google.com/maps/api/js?v=3.9&amp;sensor=false&amp;key=AIzaSyCHapXCh6JyfkpnJVUGG1zBSwaHjZUI70c" type="text/javascript"></script>
<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
js/oms.min.js" type="text/javascript"></script>
<script type='text/javascript'>
//<![CDATA[ 

	// Closure - Just to be safe :)
	(function(){
    var map;
		var oms;
    var infoWindow;

    var points = [
    	<?php  $_smarty_tpl->tpl_vars['merchant'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['merchant']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['mapper']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['foo']['index']=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['merchant']->key => $_smarty_tpl->tpl_vars['merchant']->value){
$_smarty_tpl->tpl_vars['merchant']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['foo']['index']++;
?>
      	<?php if ($_smarty_tpl->tpl_vars['merchant']->value->lat!=null){?>
      		[	'latlong', 
      			'<?php echo (($tmp = @$_smarty_tpl->tpl_vars['merchant']->value->lat)===null||$tmp==='' ? 50.760106 : $tmp);?>
,<?php echo (($tmp = @$_smarty_tpl->tpl_vars['merchant']->value->long)===null||$tmp==='' ? -1.994442 : $tmp);?>
', 
      			"<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
", 
      			"<?php echo smarty_modifier_replace(trim($_smarty_tpl->tpl_vars['merchant']->value->companyname),' ','+');?>
", 
      			"<?php echo strtr(smarty_modifier_truncate($_smarty_tpl->tpl_vars['merchant']->value->summary,80,"...",true), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
", 
      			"<?php echo smarty_modifier_replace(trim(reset($_smarty_tpl->tpl_vars['merchant']->value->categories)),' ','+');?>
", 
      			<?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['foo']['index']+1;?>

      		],
      	<?php }?>
    	<?php }
if (!$_smarty_tpl->tpl_vars['merchant']->_loop) {
?>
    	 
    	<?php } ?>
    	];

    var arrayPoints = new Array(points.length);

    for (var i = 0; i < points.length; i++) {
       arrayPoints[i] = new Array(6); 
    }

    function init() 
    {
        var myOptions = { 
            zoom: 16,
            center: new google.maps.LatLng(50.761830, -1.996250),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false     
        }
        
        map = new google.maps.Map(document.getElementById('map'), myOptions);
        infoWindow = new google.maps.InfoWindow();
        codeAddress(points);
        
        var styles = [ { "elementType": "labels.text", "stylers": [ { "visibility": "off" } ] },{ "featureType": "road.arterial", "elementType": "geometry.fill", "stylers": [ { "visibility": "on" }, { "saturation": 1 }, { "lightness": 1 }, { "color": "#00B4AB" } ] },{ "featureType": "road.local", "stylers": [ { "color": "#b9efe9" } ] },{ "featureType": "road.arterial", "elementType": "geometry.stroke", "stylers": [ { "visibility": "on" }, { "weight": 5 }, { "color": "#ffffff" } ] },{ "featureType": "landscape", "elementType": "geometry", "stylers": [ { "color": "#ffffff" } ] },{ } ];
        map.setOptions({ styles: styles });
        
        var opt = { minZoom: 15, maxZoom: 17 };
  			map.setOptions(opt);
        
        oms = new OverlappingMarkerSpiderfier(map, { markersWontMove: true, markersWontHide: true });
        
        var strictBounds = new google.maps.LatLngBounds(
       	 	new google.maps.LatLng(50.759102, -2.002312),
        	new google.maps.LatLng(50.764979, -1.991326) 
        	
     		);
     		
     		// Listen for the dragend event
       google.maps.event.addListener(map, 'dragend', function() {
       
         if (strictBounds.contains(map.getCenter())) return;
    
         // We're out of bounds - Move the map back within the bounds
    
         var c = map.getCenter(),
             x = c.lng(),
             y = c.lat(),
             maxX = strictBounds.getNorthEast().lng(),
             maxY = strictBounds.getNorthEast().lat(),
             minX = strictBounds.getSouthWest().lng(),
             minY = strictBounds.getSouthWest().lat();
    
         if (x < minX) x = minX;
         if (x > maxX) x = maxX;
         if (y < minY) y = minY;
         if (y > maxY) y = maxY;
    
         map.setCenter(new google.maps.LatLng(y, x));
       });
    
      
        
    }
                                  
    function codeAddress(locations) 
    {
        for (var i = 0; i < locations.length; i++) {
            var location = locations[i];
            var lat_long = location[1].split(',');
            var myLatLng = new google.maps.LatLng(lat_long[0], lat_long[1]);
            fillArrayPositions(myLatLng, i, location[2], location[3], location[4], location[5], location[6]);
        }
        
        google.maps.event.addListenerOnce(map, 'idle', function(){
           setMarkerPosition(arrayPoints);
        });
    }
                                  
    function fillArrayPositions(myLatLng, pos, ident, companyname, summary, category, number)
    {
        arrayPoints[pos][0] = myLatLng;
        arrayPoints[pos][1] = ident;
        arrayPoints[pos][2] = companyname;
        arrayPoints[pos][3] = summary;
        arrayPoints[pos][4] = category;
        arrayPoints[pos][5] = number;
    }
                                  
  function setMarkerPosition(points)
  {
      var bounds = new google.maps.LatLngBounds();
      
      function createMarker(map, position, ident, companyname, summary, category, number, bounds)
      {

      		var iconBase = '/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
img/heromap/';
      		
      	//	var rand = Math.floor((Math.random()*5)+1);
      		
      		//var icon = icon = iconBase + 'shop-icon_'+rand+'.png';
      		var icon = icon = iconBase + 'shop-icon.png';
      		var shadow = '';
      
     			switch (category){
     				case 'Pub':
     					icon = iconBase + 'beer-icon.png';
  						shadow = iconBase + '';
     					break;
     				case 'Restaurant':
     					icon = iconBase + 'restaurant.png';
    					shadow = iconBase + '';
     					break;

            case 'Post+Office':
              icon = iconBase + 'post-office.png';
              shadow = iconBase + '';
              break;
     			} 

          var marker = new google.maps.Marker({
              position: position,
              map: map,
              draggable: false,
        			icon:icon,
        			shadow:shadow
          	});
          
          bounds.extend(position);
       //   map.fitBounds(bounds);
          
          oms.addMarker(marker);
          
          // Listeners
					google.maps.event.addListener(marker, 'click', function() {
          	$('#mapwrap img.complogo').attr('src', '/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
image/merchant_'+ident+'_1/200/128/');
          	$('#mapwrap h2.compname').html(companyname.replace(/\+/g, ' '));
          	$('#mapwrap p.comptype').html(category.replace(/\+/g, ' '));
          	$('#mapwrap p.compdescr').html(summary.replace(/\+/g, ' '));
          	$('#mapwrap a#compcat').attr('href', '/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
business/category/'+category).show("slow");
            $('#mapwrap a#compdet').attr('href', '/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
business/detail/'+companyname).show("slow");
          });
   
          google.maps.event.addListener(marker, 'mouseover', function() {
              infoWindow.setContent('<h2>'+companyname.replace(/\+/g, ' ')+'</h2>Click marker for more information');
              infoWindow.open(map, marker);
          });

					google.maps.event.addListener(map, 'click', function() { infoWindow.close(); }); 

					// OMS Listners
				/*	oms.addListener('spiderfy', function(markers) {
            for(var i = 0; i < markers.length; i ++) {
              markers[i].setIcon(iconWithColor(spiderfiedColor));
              markers[i].setShadow(null);
            } 
            iw.close();
          });
          
          oms.addListener('unspiderfy', function(markers) {
            for(var i = 0; i < markers.length; i ++) {
              markers[i].setIcon(iconWithColor(usualColor));
              markers[i].setShadow(shadow);
            }
          }); */
          
       }
          
      for (var i = 0; i < points.length; i++) {
          var point = points[i];
          if (point[0]){
						createMarker(map, point[0], point[1], point[2], point[3], point[4], point[5], bounds);
					}
      }        
     // map.setZoom(17); // Really should not set this manually - let the map.fitBounds function deal with it.        
		}
            
    // Make the Init function available to the rest of the DOM                       
		window.heroMapInit = init;

	})();
//]]> 
</script><?php }} ?>