<?php /* Smarty version Smarty-3.1.11, created on 2013-05-08 06:49:41
         compiled from "/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/editCategory.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16304295545189f5854dd6a1-67680248%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1af629a52ee43b531645c41e5aefa63e76c6d7ea' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/editCategory.tpl',
      1 => 1366576041,
      2 => 'file',
    ),
    'ea5c3c47a8e1a84af03770cc3273d76fcb337f13' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/default.tpl',
      1 => 1366557986,
      2 => 'file',
    ),
    '8d47d2679bde4a59b2fd6226caf5bfa84b8a8032' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/navbar.tpl',
      1 => 1366551475,
      2 => 'file',
    ),
    '5305d5aa8b4dcce68b0ae1849436caad059d5d14' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/sidebar.tpl',
      1 => 1366575684,
      2 => 'file',
    ),
    '774e1eab0bb52c84fb6cfee143ebdd0b05690be5' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/errors.tpl',
      1 => 1363965009,
      2 => 'file',
    ),
    'e33895229ebdafc4b6248d1072af562053bcc3b8' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/footer.tpl',
      1 => 1363965009,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16304295545189f5854dd6a1-67680248',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'WEBPATH' => 0,
    'sess_user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5189f5857f15e5_68784421',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5189f5857f15e5_68784421')) {function content_5189f5857f15e5_68784421($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
	<head>
		<title>BCTC Admin :: Dashboard</title>
		
		
		<meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		
		
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/fullcalendar.css" />	
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/unicorn.main.css" />
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/unicorn.blue.css" class="skin-color" />
		<link rel="stylesheet/less" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/custom.less" />

		
		
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/uniform.css" />
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/select2.css" />		

		
	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
js/less-1.3.3.min.js" type="text/javascript"></script>
	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/excanvas.min.js"></script>
    <script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/jquery.min.js"></script>
    <script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/jquery-ui.custom.js"></script>
    <script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/bootstrap.min.js"></script>
    <script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/unicorn.js"></script>
   	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/admin.js"></script>
   	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/custom.js"></script>



		
		<script>
			var WEBPATH = '<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
';
		</script>

		
	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/jquery.uniform.js"></script>
	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/select2.min.js"></script>
	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/unicorn.form_common.js"></script>

		
		
	</head>
	
	<body lang="en" id="body">
	
		<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin!=1){?>
			<?php $_smarty_tpl->tpl_vars['merchant'] = new Smarty_variable(reset($_smarty_tpl->tpl_vars['sess_user']->value->ownMerchant), null, 0);?>
		<?php }?>

		
			<?php /*  Call merged included template "admin/snippets/navbar.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('admin/snippets/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '16304295545189f5854dd6a1-67680248');
content_5189f5855ef2f0_24353865($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "admin/snippets/navbar.tpl" */?>
		
		
		
	<?php /*  Call merged included template "admin/snippets/sidebar.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('admin/snippets/sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('sub'=>'category','item'=>'edit'), 0, '16304295545189f5854dd6a1-67680248');
content_5189f58562e266_01880382($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "admin/snippets/sidebar.tpl" */?>


		<div id="content">
		

	<div id="content-header">
				<h1>Edit Category</h1>
				<div class="btn-group">
					<a class="btn btn-large tip-bottom" title="Manage Files"><i class="icon-file"></i></a>
					<a class="btn btn-large tip-bottom" title="Manage Users"><i class="icon-user"></i></a>
					<a class="btn btn-large tip-bottom" title="Manage Comments"><i class="icon-comment"></i><span class="label label-important">5</span></a>
					<a class="btn btn-large tip-bottom" title="Manage Orders"><i class="icon-shopping-cart"></i></a>
				</div>
			</div>
			<div id="breadcrumb">
				<a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/category/listing" class="tip-bottom">Categories</a>
				<a href="#" class="current">Edit Category</a>
			</div>
			<div class="container-fluid">
			
				
			
				<div class="row-fluid">
					<div class="span12">
					
					<?php /*  Call merged included template "admin/snippets/errors.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('admin/snippets/errors.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '16304295545189f5854dd6a1-67680248');
content_5189f5857878d4_78733273($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "admin/snippets/errors.tpl" */?>
					
					<?php if (isset($_smarty_tpl->tpl_vars['updated']->value)){?>
						<div class="alert alert-success">
							<button class="close" data-dismiss="alert">×</button>
							<strong>Success!</strong> The user data has been updated.
						</div>
					<?php }?>
						
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify icon-pencil"></i>									
								</span>
								<h5>Currently Editing : <?php echo $_smarty_tpl->tpl_vars['category']->value->name;?>
</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="#" method="post" class="form-horizontal">

									<div class="control-group">
										<label class="control-label">Name</label>
										<div class="controls">
											<input type="text" name='name' value='<?php echo $_smarty_tpl->tpl_vars['category']->value->name;?>
'  required/>
											<input type='hidden' name='action' value='edit'/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Description</label>
										<div class="controls">
											<textarea name='description' required><?php echo $_smarty_tpl->tpl_vars['category']->value->description;?>
</textarea>
											<span class="help-block">Be as descriptive as you can.</span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Business Category</label>
										<div class="controls">
											<select name='business'>
												<option value='0' <?php if ($_smarty_tpl->tpl_vars['category']->value->business==false){?>selected=selected<?php }?>>False</option>
												<option value='1' <?php if ($_smarty_tpl->tpl_vars['category']->value->business==true){?>selected=selected<?php }?>>True</option>
											</select>
											<span class="help-block">Select YES if this category applies to a Business, i.e. Florest, or Bank. Non-business categories are for use with Articles, Deals, Events etc..</span>
										</div>
									</div>
									
									
									<div class="form-actions">
										<button type="submit" class="btn btn-primary">Save</button>
									</div>
								</form>
							</div>
						</div>
								
					</div>
				</div>
			
			
				
				<div class="row-fluid">
					<?php /*  Call merged included template "admin/snippets/footer.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('admin/snippets/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '16304295545189f5854dd6a1-67680248');
content_5189f5857e9f90_67246656($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "admin/snippets/footer.tpl" */?>
				</div>
			</div>


		</div>
		
		
	</body>

</html><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-05-08 06:49:41
         compiled from "/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/navbar.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5189f5855ef2f0_24353865')) {function content_5189f5855ef2f0_24353865($_smarty_tpl) {?><div id="header">
	<h1><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin"><img src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/img/bctc.gif" width="50px" alt=""><span>BCTC Admin</span></a></h1>		
</div>


<div id="user-nav" class="navbar navbar-inverse">
    <ul class="nav btn-group">
        <li class="btn btn-inverse" ><a title="" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/user/edit/<?php echo md5($_smarty_tpl->tpl_vars['sess_user']->value->id);?>
"><i class="icon icon-user"></i> <span class="text">Profile</span></a></li>
        
        
      
        <li class="btn btn-inverse"><a title="" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/logout"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
    </ul>
</div><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-05-08 06:49:41
         compiled from "/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/sidebar.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5189f58562e266_01880382')) {function content_5189f58562e266_01880382($_smarty_tpl) {?><div id="sidebar">
	<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
	<ul>
		<li class="<?php if (!$_smarty_tpl->tpl_vars['sub']->value){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin"><i class="icon icon-home"></i> <span>Dashboard</span></a>
		</li>
		
<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin==1){?>

		<!-- <li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='blog'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/blog"><i class="icon icon-th-list"></i> <span>Blog</span></a>
		</li> -->

		<li>
			<a href="#"><i class="icon icon-signal"></i> <span>Analytics</span></a>
		</li>

		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='category'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/category/listing"><i class="icon icon-folder-open"></i> <span>Categories</span></a>
		</li>

		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='user'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/user/listing"><i class="icon icon-user"></i> <span>Users</span></a>
		</li>
				
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='merchant'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/merchant/listing"><i class="icon icon-th-list"></i> <span>Merchants</span></a>
		</li>
<?php }?>			
<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin==0){?>
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='merchant'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/merchant/edit/<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
"><i class="icon icon-th-list"></i> <span>Listing</span></a>
		</li>
<?php }?>
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='article'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/article/listing<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin!=1){?>/<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
<?php }?>"><i class="icon icon-th-list"></i> <span>Articles</span></a>
		</li>
		
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='classified'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/classified/listing<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin!=1){?>/<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
<?php }?>"><i class="icon icon-th-list"></i> <span>Classifieds</span></a>
		</li>
		
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='event'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/event/listing<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin!=1){?>/<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
<?php }?>"><i class="icon icon-th-list"></i> <span>Events</span></a>
		</li>
		
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='deal'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/deal/listing<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin!=1){?>/<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
<?php }?>"><i class="icon icon-th-list"></i> <span>Deals</span></a>
		</li>
		
<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin==1){?>
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='cms'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/cms/listing"><i class="icon icon-th-list"></i> <span>CMS</span></a>
		</li>
<?php }?>
	
	
	</ul>

</div><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-05-08 06:49:41
         compiled from "/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/errors.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5189f5857878d4_78733273')) {function content_5189f5857878d4_78733273($_smarty_tpl) {?><?php if (isset($_smarty_tpl->tpl_vars['errors']->value)){?>
<div class="container">

	<?php  $_smarty_tpl->tpl_vars['error'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['error']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['errors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['error']->key => $_smarty_tpl->tpl_vars['error']->value){
$_smarty_tpl->tpl_vars['error']->_loop = true;
?>
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<h4><?php echo $_smarty_tpl->tpl_vars['error']->value['title'];?>
</h4>
			<?php echo $_smarty_tpl->tpl_vars['error']->value['message'];?>

		</div>
	<?php } ?>

</div>
<?php }?><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-05-08 06:49:41
         compiled from "/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/footer.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5189f5857e9f90_67246656')) {function content_5189f5857e9f90_67246656($_smarty_tpl) {?><div id="footer" class="span12">
	2012 - 2013 &copy; BCTC Admin
</div><?php }} ?>