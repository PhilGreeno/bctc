<?php /* Smarty version Smarty-3.1.11, created on 2013-06-15 21:11:02
         compiled from "/Applications/MAMP/htdocs/bctc/bctc/views/articles/home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:120565217551b0f64c86bfa8-64853924%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e57d54a627b5e4c9c9104cdff29767b21b46ad1c' => 
    array (
      0 => '/Applications/MAMP/htdocs/bctc/bctc/views/articles/home.tpl',
      1 => 1370591375,
      2 => 'file',
    ),
    '5e94b194cab0748134f89e4a3eab5f36eb985ae6' => 
    array (
      0 => '/Applications/MAMP/htdocs/bctc/bctc/views/snippets/default.tpl',
      1 => 1371246467,
      2 => 'file',
    ),
    '6422e8e76152a83a609f52437f521ff9d0d6ea0b' => 
    array (
      0 => '/Applications/MAMP/htdocs/bctc/bctc/views/snippets/navBar.tpl',
      1 => 1371229815,
      2 => 'file',
    ),
    '7dc21faffda705d286f9e9c391864ceedf01177a' => 
    array (
      0 => '/Applications/MAMP/htdocs/bctc/bctc/views/snippets/widgets/searchBar.tpl',
      1 => 1371250908,
      2 => 'file',
    ),
    '3cb7041a1aa0987946d16b2f1ff7ab0d967eabe3' => 
    array (
      0 => '/Applications/MAMP/htdocs/bctc/bctc/views/snippets/topBar.tpl',
      1 => 1371226568,
      2 => 'file',
    ),
    '477615c51d8e0cfe4b82ca717171ef6fd453e2d4' => 
    array (
      0 => '/Applications/MAMP/htdocs/bctc/bctc/views/snippets/widgets/browseByCategory.tpl',
      1 => 1370432034,
      2 => 'file',
    ),
    '987b97c71f08f1b4ed16caacd9430b6f0cd9e26a' => 
    array (
      0 => '/Applications/MAMP/htdocs/bctc/bctc/views/snippets/widgets/paginator.tpl',
      1 => 1367442934,
      2 => 'file',
    ),
    '4333c72d2b89090f39071e1dd41727b8fb59423f' => 
    array (
      0 => '/Applications/MAMP/htdocs/bctc/bctc/views/snippets/footer.tpl',
      1 => 1371230346,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '120565217551b0f64c86bfa8-64853924',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51b0f64d0642e6_28744195',
  'variables' => 
  array (
    'WEBPATH' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51b0f64d0642e6_28744195')) {function content_51b0f64d0642e6_28744195($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/Applications/MAMP/htdocs/bctc/bctc/libraries/smarty/libs/plugins/modifier.date_format.php';
if (!is_callable('smarty_modifier_capitalize')) include '/Applications/MAMP/htdocs/bctc/bctc/libraries/smarty/libs/plugins/modifier.capitalize.php';
if (!is_callable('smarty_modifier_replace')) include '/Applications/MAMP/htdocs/bctc/bctc/libraries/smarty/libs/plugins/modifier.replace.php';
if (!is_callable('smarty_modifier_truncate')) include '/Applications/MAMP/htdocs/bctc/bctc/libraries/smarty/libs/plugins/modifier.truncate.php';
?><!doctype html>
<!--[if lt IE 7]>      <html class="no-js ie6"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie7"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js">     <!--<![endif]-->

	<head>
		
		<title>BCTC :: Articles</title>
		
		<script>
			document.documentElement.className = document.documentElement.className.replace(/(\s|^)no-js(\s|$)/, '$1' + 'js' + '$2');
		</script>
		
		
		<meta charset="UTF-8" />
		<meta name="robots" content="index, follow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		

		<link rel='stylesheet' type='text/css' href='/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
css/bootstrap.min.css'>
		<link rel='stylesheet' type='text/css' href='/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
css/bootstrap-responsive.min.css'>
		<link rel='stylesheet' type='text/css' href='/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
css/main.css'>
		<link rel='stylesheet' type='text/css' href='/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
css/responsiveslides.css'>
		
		<link rel="stylesheet" type="text/css" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
css/app.css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
	
		

		<!--<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
js/less-1.3.3.min.js" type="text/javascript"></script>-->
		<script type="text/javascript" src="//use.typekit.net/mlu5jbc.js"></script>
 <script type="text/javascript">try{Typekit.load();}catch(e){}</script> 
		<!--<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
js/jquery.js"></script>
		<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
js/bootstrap.min.js"></script>
		<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
js/app.js"></script>
		<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
js/custom.js"></script>-->
		<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
js/prod-ck.js"></script>

		
		



	</head>
	
	<body lang="en" id="body">



		
			<?php /*  Call merged included template "snippets/navBar.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('snippets/navBar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('active'=>'articles'), 0, '120565217551b0f64c86bfa8-64853924');
content_51bcd866e0a353_30246090($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "snippets/navBar.tpl" */?>
			
			<?php /*  Call merged included template "snippets/topBar.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('snippets/topBar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '120565217551b0f64c86bfa8-64853924');
content_51bcd866eb31c0_45060949($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "snippets/topBar.tpl" */?>


		<div id="content">
		

		<div class="container-fluid">
			<div class="row-fluid">

				<div class="span3">
					
					<?php /*  Call merged included template "snippets/widgets/browseByCategory.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('snippets/widgets/browseByCategory.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('active'=>'articles'), 0, '120565217551b0f64c86bfa8-64853924');
content_51bcd86708d0c6_81649228($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "snippets/widgets/browseByCategory.tpl" */?>
										
				</div>


				<div class="span9">  
				                 

					
					
					
					<div class="main_content detail-product">	


						<h3 class="title artTitle justTitle">Broadstone Articles &amp; News


							<span class="pull-right hidden-phone"><?php /*  Call merged included template "snippets/widgets/paginator.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('snippets/widgets/paginator.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '120565217551b0f64c86bfa8-64853924');
content_51bcd8678ccd87_41950855($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "snippets/widgets/paginator.tpl" */?></span>



						</h3>

						


					<?php  $_smarty_tpl->tpl_vars['article'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['article']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['articles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['article']->key => $_smarty_tpl->tpl_vars['article']->value){
$_smarty_tpl->tpl_vars['article']->_loop = true;
?>
						<div class="listing-item">
							<div class="row-fluid">		
												
								<div class="span12">
									<div class="featured-item">
									
										<div class="span2 hidden-phone">		
											<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
articles/detail/<?php echo md5($_smarty_tpl->tpl_vars['article']->value->id);?>
">
										<?php if ($_smarty_tpl->tpl_vars['article']->value->image1){?>
												<img class="img" src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
image/article_<?php echo md5($_smarty_tpl->tpl_vars['article']->value->id);?>
_1/152/85/" alt="<?php echo $_smarty_tpl->tpl_vars['article']->value->title;?>
" title="<?php echo $_smarty_tpl->tpl_vars['article']->value->title;?>
" class="img pull-left"/>
										<?php }else{ ?>
												<img class="img" src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
image/placeholder/152/85/" alt="<?php echo $_smarty_tpl->tpl_vars['article']->value->title;?>
" title="<?php echo $_smarty_tpl->tpl_vars['article']->value->title;?>
" class="img pull-left"/>
										<?php }?>
											</a>
						
										</div>
										<div class="span8">
											<h5><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
articles/detail/<?php echo md5($_smarty_tpl->tpl_vars['article']->value->id);?>
" class="title"><?php echo $_smarty_tpl->tpl_vars['article']->value->title;?>
</a> </h5>
											Published : <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['article']->value->publicationdate);?>
 by <?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['article']->value->author);?>
 in
											<?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['article']->value->categories; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['category']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['category']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value){
$_smarty_tpl->tpl_vars['category']->_loop = true;
 $_smarty_tpl->tpl_vars['i']->value = $_smarty_tpl->tpl_vars['category']->key;
 $_smarty_tpl->tpl_vars['category']->iteration++;
 $_smarty_tpl->tpl_vars['category']->last = $_smarty_tpl->tpl_vars['category']->iteration === $_smarty_tpl->tpl_vars['category']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['twat']['last'] = $_smarty_tpl->tpl_vars['category']->last;
?>
												<?php if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['twat']['last']){?>,&nbsp;<?php }?><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
articles/category/<?php echo smarty_modifier_replace(trim($_smarty_tpl->tpl_vars['category']->value),' ','+');?>
" class="subinfo"><?php echo trim($_smarty_tpl->tpl_vars['category']->value);?>
</a>
											<?php } ?>
											<hr/>
											<p class="hidden-phone"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['article']->value->abstract,250,"...",true);?>
</p>

										</div>								
									</div>								
								</div>
								
							</div>
						</div>
					<?php }
if (!$_smarty_tpl->tpl_vars['article']->_loop) {
?>
						<h5>There are no articles listed that match your criteria</h5>
					<?php } ?>

					<div class="clearfix"></div>

					<div class="bottomPageCount"><?php /*  Call merged included template "snippets/widgets/paginator.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('snippets/widgets/paginator.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '120565217551b0f64c86bfa8-64853924');
content_51bcd8678ccd87_41950855($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "snippets/widgets/paginator.tpl" */?></div>

					</div>
				</div>
				
			</div>			
		</div>



		</div>
		
		
			<?php /*  Call merged included template "snippets/footer.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('snippets/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '120565217551b0f64c86bfa8-64853924');
content_51bcd867a55d82_76026042($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "snippets/footer.tpl" */?>
		
		
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-40328774-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>




		
	</body>



</html><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-06-15 21:11:02
         compiled from "/Applications/MAMP/htdocs/bctc/bctc/views/snippets/navBar.tpl" */ ?>
<?php if ($_valid && !is_callable('content_51bcd866e0a353_30246090')) {function content_51bcd866e0a353_30246090($_smarty_tpl) {?>		<div class="navbar center">
			<div class="navbar-inner main-menu">
				<div class="container-fluid">
					<div class="clearfix row-fluid">

						
						
						<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">

							Menu
						</a>						
						<div class="nav-collapse collapse">							 		                 
							<ul class="nav" id="main-nav">								
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='home'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
">Home</a></li>
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='business'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
business">Businesses</a></li>	
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='association'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
static/About+the+Broadstone+Chamber">Chamber of Trade</a></li>	
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='association'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
associations">Associations</a></li>		
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='events'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
events">Events</a></li>						
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='classifieds'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
classifieds">Classifieds</a></li>								
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='deals'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
deals">Offers</a></li>
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='articles'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
articles">Articles</a></li>								
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='blog'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
blog">Blog</a></li>								
								<li <?php if ($_smarty_tpl->tpl_vars['active']->value=='contact'){?>class="active"<?php }?>><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
static/contact+us">Contact Us</a></li>						
							</ul>
						</div>
						
					</div>
				</div>
			</div>		
		</div>
		<div class="clearfix"></div><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-06-15 21:11:02
         compiled from "/Applications/MAMP/htdocs/bctc/bctc/views/snippets/topBar.tpl" */ ?>
<?php if ($_valid && !is_callable('content_51bcd866eb31c0_45060949')) {function content_51bcd866eb31c0_45060949($_smarty_tpl) {?>		<div class="container-fluid bannerTop" id="top-bar">
			<div class="row-fluid">
				<div class="span2 backGrad">
					<a class="brand pull-left" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
"></a>
				</div>

				<div class="span6 offset2 hidden-phone">
					<div class="ribbonTop"><div class="ribbon-stitches-top"></div><strong class="ribbon-content"><h1>Welcome to Broadstone Village</h1></strong><div class="ribbon-stitches-bottom"></div></div> 

				</div>

				

			</div>
		</div>

		<div class="container-fluid searchWrap">
			<div class="row-fluid">
					




			<?php /*  Call merged included template "snippets/widgets/searchBar.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('snippets/widgets/searchBar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('active'=>'business'), 0, '120565217551b0f64c86bfa8-64853924');
content_51bcd866ebe598_74609143($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "snippets/widgets/searchBar.tpl" */?>

			</div>
		</div><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-06-15 21:11:02
         compiled from "/Applications/MAMP/htdocs/bctc/bctc/views/snippets/widgets/searchBar.tpl" */ ?>
<?php if ($_valid && !is_callable('content_51bcd866ebe598_74609143')) {function content_51bcd866ebe598_74609143($_smarty_tpl) {?>			
			
				<form class="search_top_form" action='/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
search' method='POST'>
					<div class="welcomeSearch hidden-phone">...search for choice and value for money</div>
					<!-- <div class="ribbon-wrapper hidden-phone">
						<div class="ribbon hidden-phone">Search!</div>
					</div> -->
						<ul>
							<li>
								<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
" name='keyword' placeholder="Keyword...">
							</li>
							<li>
								<select name="type">
									<option value="business" <?php if ($_smarty_tpl->tpl_vars['active']->value=='business'){?>SELECTED=SELECTED<?php }?>>Business</option>
									<option value="events" <?php if ($_smarty_tpl->tpl_vars['active']->value=='events'){?>SELECTED=SELECTED<?php }?>>Events</option>
									<option value="classifieds" <?php if ($_smarty_tpl->tpl_vars['active']->value=='classifieds'){?>SELECTED=SELECTED<?php }?>>Classifieds</option>
									<option value="deals" <?php if ($_smarty_tpl->tpl_vars['active']->value=='deals'){?>SELECTED=SELECTED<?php }?>>Deals</option>
									<option value="articles" <?php if ($_smarty_tpl->tpl_vars['active']->value=='articles'){?>SELECTED=SELECTED<?php }?>>Articles</option>
									<option value="blog" <?php if ($_smarty_tpl->tpl_vars['active']->value=='blog'){?>SELECTED=SELECTED<?php }?>>Blog</option>
								</select>
							</li>
						
							<li>
								<button class="moreButton findButton"><i class="icon-search icon-white"></i> Search </button>
							</li>
						</ul>
					</form>

			

			<?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-06-15 21:11:02
         compiled from "/Applications/MAMP/htdocs/bctc/bctc/views/snippets/widgets/searchBar.tpl" */ ?>
<?php if ($_valid && !is_callable('content_51bcd866f26c11_58410167')) {function content_51bcd866f26c11_58410167($_smarty_tpl) {?>			
			
				<form class="search_top_form" action='/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
search' method='POST'>
					<div class="welcomeSearch hidden-phone">...search for choice and value for money</div>
					<!-- <div class="ribbon-wrapper hidden-phone">
						<div class="ribbon hidden-phone">Search!</div>
					</div> -->
						<ul>
							<li>
								<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
" name='keyword' placeholder="Keyword...">
							</li>
							<li>
								<select name="type">
									<option value="business" <?php if ($_smarty_tpl->tpl_vars['active']->value=='business'){?>SELECTED=SELECTED<?php }?>>Business</option>
									<option value="events" <?php if ($_smarty_tpl->tpl_vars['active']->value=='events'){?>SELECTED=SELECTED<?php }?>>Events</option>
									<option value="classifieds" <?php if ($_smarty_tpl->tpl_vars['active']->value=='classifieds'){?>SELECTED=SELECTED<?php }?>>Classifieds</option>
									<option value="deals" <?php if ($_smarty_tpl->tpl_vars['active']->value=='deals'){?>SELECTED=SELECTED<?php }?>>Deals</option>
									<option value="articles" <?php if ($_smarty_tpl->tpl_vars['active']->value=='articles'){?>SELECTED=SELECTED<?php }?>>Articles</option>
									<option value="blog" <?php if ($_smarty_tpl->tpl_vars['active']->value=='blog'){?>SELECTED=SELECTED<?php }?>>Blog</option>
								</select>
							</li>
						
							<li>
								<button class="moreButton findButton"><i class="icon-search icon-white"></i> Search </button>
							</li>
						</ul>
					</form>

			

			<?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-06-15 21:11:03
         compiled from "/Applications/MAMP/htdocs/bctc/bctc/views/snippets/widgets/browseByCategory.tpl" */ ?>
<?php if ($_valid && !is_callable('content_51bcd86708d0c6_81649228')) {function content_51bcd86708d0c6_81649228($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/Applications/MAMP/htdocs/bctc/bctc/libraries/smarty/libs/plugins/modifier.replace.php';
?>
		
		<?php if (isset($_smarty_tpl->tpl_vars['active']->value)&&$_smarty_tpl->tpl_vars['active']->value!=''){?>
		
				<h3 class="title justTitle hidden-phone">Current Category</h3>
					<div class="accordion right-col" id="accordion">
					<?php if ($_smarty_tpl->tpl_vars['active']->value=='business'){?>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
									<h3 class="browseTitle nm nl yo">Businesses <i class="icon-chevron-up"></i></h3>
								</a>
							</div>
							<div id="collapseOne" class="accordion-body collapse in">
								<div class="accordion-inner">
									<ul class="nav nav-list">
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
business<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; All</a></li>
									<?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['merchantCategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value){
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
business/category/<?php echo smarty_modifier_replace(trim($_smarty_tpl->tpl_vars['category']->value->name),' ','+');?>
<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; <?php echo $_smarty_tpl->tpl_vars['category']->value->name;?>
</a><span>(<?php echo $_smarty_tpl->tpl_vars['category']->value->counter;?>
)</span></li>
									<?php } ?>
									
									</ul>
								</div>
							</div>
						</div>
					<?php }?>
						
					<?php if ($_smarty_tpl->tpl_vars['active']->value=='association'){?>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
									<h3 class="browseTitle nl nm">Associations <i class="icon-chevron-up"></i></h3>
								</a>
							</div>
							<div id="collapseSeven" class="accordion-body collapse in">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
associations<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; All</a></li>
									<?php  $_smarty_tpl->tpl_vars['article'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['article']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['associationCategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['article']->key => $_smarty_tpl->tpl_vars['article']->value){
$_smarty_tpl->tpl_vars['article']->_loop = true;
?>
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
associations/category/<?php echo smarty_modifier_replace(trim($_smarty_tpl->tpl_vars['article']->value->name),' ','+');?>
<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; <?php echo $_smarty_tpl->tpl_vars['article']->value->name;?>
</a><span>(<?php echo $_smarty_tpl->tpl_vars['article']->value->counter;?>
)</span></li>
									<?php } ?>
									</ul>
								</div>
							</div>
						</div>
					<?php }?>
						
					<?php if ($_smarty_tpl->tpl_vars['active']->value=='classifieds'){?>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									<h3 class="browseTitle nl nm">Classifieds <i class="icon-chevron-up"></i></h3>
								</a>
							</div>
							<div id="collapseTwo" class="accordion-body collapse in">
								<div class="accordion-inner">
									<ul class="nav nav-list">
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
classifieds<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; All</a></li>
									<?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['classifiedCategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value){
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
classifieds/category/<?php echo smarty_modifier_replace(trim($_smarty_tpl->tpl_vars['category']->value->name),' ','+');?>
<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; <?php echo $_smarty_tpl->tpl_vars['category']->value->name;?>
</a><span>(<?php echo $_smarty_tpl->tpl_vars['category']->value->counter;?>
)</span></li>
									<?php } ?>
									</ul>
								</div>
							</div>
						</div>
					<?php }?>
						
					<?php if ($_smarty_tpl->tpl_vars['active']->value=='deals'){?>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
									<h3 class="browseTitle nl nm">Current Offers<i class="icon-chevron-up"></i></h3>
								</a>
							</div>
							<div id="collapseThree" class="accordion-body collapse in">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
deals<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; All</a></li>
									<?php  $_smarty_tpl->tpl_vars['deal'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['deal']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['dealCategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['deal']->key => $_smarty_tpl->tpl_vars['deal']->value){
$_smarty_tpl->tpl_vars['deal']->_loop = true;
?>
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
deals/category/<?php echo smarty_modifier_replace(trim($_smarty_tpl->tpl_vars['deal']->value->name),' ','+');?>
<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; <?php echo $_smarty_tpl->tpl_vars['deal']->value->name;?>
</a><span>(<?php echo $_smarty_tpl->tpl_vars['deal']->value->counter;?>
)</span></li>
									<?php } ?>
									</ul>
								</div>
							</div>
						</div>	
					<?php }?>
						
					<?php if ($_smarty_tpl->tpl_vars['active']->value=='events'){?>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
									<h3 class="browseTitle nl nm">Events <i class="icon-chevron-up"></i></h3>
								</a>
							</div>
							<div id="collapseFour" class="accordion-body collapse in">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
events<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; All</a></li>
									<?php  $_smarty_tpl->tpl_vars['event'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['event']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['eventCategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['event']->key => $_smarty_tpl->tpl_vars['event']->value){
$_smarty_tpl->tpl_vars['event']->_loop = true;
?>
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
events/category/<?php echo smarty_modifier_replace(trim($_smarty_tpl->tpl_vars['event']->value->name),' ','+');?>
<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; <?php echo $_smarty_tpl->tpl_vars['event']->value->name;?>
</a><span>(<?php echo $_smarty_tpl->tpl_vars['event']->value->counter;?>
)</span></li>
									<?php } ?>
									</ul>
								</div>
							</div>
						</div>
					<?php }?>
					
					<?php if ($_smarty_tpl->tpl_vars['active']->value=='articles'){?>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
									<h3 class="browseTitle nl nm">Articles <i class="icon-chevron-up"></i></h3>
								</a>
							</div>
							<div id="collapseFive" class="accordion-body collapse in">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
articles<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; All</a></li>
									<?php  $_smarty_tpl->tpl_vars['article'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['article']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['articleCategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['article']->key => $_smarty_tpl->tpl_vars['article']->value){
$_smarty_tpl->tpl_vars['article']->_loop = true;
?>
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
articles/category/<?php echo smarty_modifier_replace(trim($_smarty_tpl->tpl_vars['article']->value->name),' ','+');?>
<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; <?php echo $_smarty_tpl->tpl_vars['article']->value->name;?>
</a><span>(<?php echo $_smarty_tpl->tpl_vars['article']->value->counter;?>
)</span></li>
									<?php } ?>
									</ul>
								</div>
							</div>
						</div>
					<?php }?>
					
					<?php if ($_smarty_tpl->tpl_vars['active']->value=='blog'){?>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
									<h3 class="browseTitle nl nm">Blog <i class="icon-chevron-up"></i></h3>
								</a>
							</div>
							<div id="collapseSix" class="accordion-body collapse in">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
blog<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; All</a></li>
									<?php  $_smarty_tpl->tpl_vars['article'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['article']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['blogCategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['article']->key => $_smarty_tpl->tpl_vars['article']->value){
$_smarty_tpl->tpl_vars['article']->_loop = true;
?>
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
blog/category/<?php echo smarty_modifier_replace(trim($_smarty_tpl->tpl_vars['article']->value->name),' ','+');?>
<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; <?php echo $_smarty_tpl->tpl_vars['article']->value->name;?>
</a><span>(<?php echo $_smarty_tpl->tpl_vars['article']->value->counter;?>
)</span></li>
									<?php } ?>
									</ul>
								</div>
							</div>
						</div>
					<?php }?>
					</div>					
				<?php }?>
				
				
				<div class="hidden-phone"><!--Not visible on phones-->
				<h3 class="title justTitle">Other by Category</h3>
					<div class="accordion right-col" id="accordion">
					<?php if ($_smarty_tpl->tpl_vars['active']->value!='business'){?>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
									<h3 class="browseTitle nm nl">Businesses <i class="icon-chevron-down"></i></h3>
								</a>
							</div>
							<div id="collapseOne" class="accordion-body collapse <?php if ($_smarty_tpl->tpl_vars['active']->value=='business'){?>in<?php }?>">
								<div class="accordion-inner">
									<ul class="nav nav-list">
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
business<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; All</a></li>
									<?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['merchantCategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value){
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
business/category/<?php echo smarty_modifier_replace(trim($_smarty_tpl->tpl_vars['category']->value->name),' ','+');?>
<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; <?php echo $_smarty_tpl->tpl_vars['category']->value->name;?>
</a><span>(<?php echo $_smarty_tpl->tpl_vars['category']->value->counter;?>
)</span></li>
									<?php } ?>
									
									</ul>
								</div>
							</div>
						</div>
					<?php }?>
						
					<?php if ($_smarty_tpl->tpl_vars['active']->value!='association'){?>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
									<h3 class="browseTitle nl nm">Associations <i class="icon-chevron-down"></i></h3>
								</a>
							</div>
							<div id="collapseSeven" class="accordion-body collapse <?php if ($_smarty_tpl->tpl_vars['active']->value=='association'){?>in<?php }?>">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
associations<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; All</a></li>
									<?php  $_smarty_tpl->tpl_vars['article'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['article']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['associationCategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['article']->key => $_smarty_tpl->tpl_vars['article']->value){
$_smarty_tpl->tpl_vars['article']->_loop = true;
?>
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
associations/category/<?php echo smarty_modifier_replace(trim($_smarty_tpl->tpl_vars['article']->value->name),' ','+');?>
<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; <?php echo $_smarty_tpl->tpl_vars['article']->value->name;?>
</a><span>(<?php echo $_smarty_tpl->tpl_vars['article']->value->counter;?>
)</span></li>
									<?php } ?>
									</ul>
								</div>
							</div>
						</div>
					<?php }?>
						
					<?php if ($_smarty_tpl->tpl_vars['active']->value!='classifieds'){?>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									<h3 class="browseTitle nl nm">Classifieds <i class="icon-chevron-down"></i></h3>
								</a>
							</div>
							<div id="collapseTwo" class="accordion-body collapse <?php if ($_smarty_tpl->tpl_vars['active']->value=='classifieds'){?>in<?php }?>">
								<div class="accordion-inner">
									<ul class="nav nav-list">
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
classifieds<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; All</a></li>
									<?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['classifiedCategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value){
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
classifieds/category/<?php echo smarty_modifier_replace(trim($_smarty_tpl->tpl_vars['category']->value->name),' ','+');?>
<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; <?php echo $_smarty_tpl->tpl_vars['category']->value->name;?>
</a><span>(<?php echo $_smarty_tpl->tpl_vars['category']->value->counter;?>
)</span></li>
									<?php } ?>
									</ul>
								</div>
							</div>
						</div>
					<?php }?>
						
					<?php if ($_smarty_tpl->tpl_vars['active']->value!='deals'){?>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
									<h3 class="browseTitle nl nm">Current Offers<i class="icon-chevron-down"></i></h3>
								</a>
							</div>
							<div id="collapseThree" class="accordion-body collapse <?php if ($_smarty_tpl->tpl_vars['active']->value=='deals'){?>in<?php }?>">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
deals<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; All</a></li>
									<?php  $_smarty_tpl->tpl_vars['deal'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['deal']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['dealCategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['deal']->key => $_smarty_tpl->tpl_vars['deal']->value){
$_smarty_tpl->tpl_vars['deal']->_loop = true;
?>
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
deals/category/<?php echo smarty_modifier_replace(trim($_smarty_tpl->tpl_vars['deal']->value->name),' ','+');?>
<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; <?php echo $_smarty_tpl->tpl_vars['deal']->value->name;?>
</a><span>(<?php echo $_smarty_tpl->tpl_vars['deal']->value->counter;?>
)</span></li>
									<?php } ?>
									</ul>
								</div>
							</div>
						</div>	
					<?php }?>
						
					<?php if ($_smarty_tpl->tpl_vars['active']->value!='events'){?>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
									<h3 class="browseTitle nl nm">Events <i class="icon-chevron-down"></i></h3>
								</a>
							</div>
							<div id="collapseFour" class="accordion-body collapse <?php if ($_smarty_tpl->tpl_vars['active']->value=='events'){?>in<?php }?>">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
events<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; All</a></li>
									<?php  $_smarty_tpl->tpl_vars['event'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['event']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['eventCategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['event']->key => $_smarty_tpl->tpl_vars['event']->value){
$_smarty_tpl->tpl_vars['event']->_loop = true;
?>
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
events/category/<?php echo smarty_modifier_replace(trim($_smarty_tpl->tpl_vars['event']->value->name),' ','+');?>
<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; <?php echo $_smarty_tpl->tpl_vars['event']->value->name;?>
</a><span>(<?php echo $_smarty_tpl->tpl_vars['event']->value->counter;?>
)</span></li>
									<?php } ?>
									</ul>
								</div>
							</div>
						</div>
					<?php }?>
					
					<?php if ($_smarty_tpl->tpl_vars['active']->value!='articles'){?>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
									<h3 class="browseTitle nl nm">Articles <i class="icon-chevron-down"></i></h3>
								</a>
							</div>
							<div id="collapseFive" class="accordion-body collapse <?php if ($_smarty_tpl->tpl_vars['active']->value=='articles'){?>in<?php }?>">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
articles<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; All</a></li>
									<?php  $_smarty_tpl->tpl_vars['article'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['article']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['articleCategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['article']->key => $_smarty_tpl->tpl_vars['article']->value){
$_smarty_tpl->tpl_vars['article']->_loop = true;
?>
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
articles/category/<?php echo smarty_modifier_replace(trim($_smarty_tpl->tpl_vars['article']->value->name),' ','+');?>
<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; <?php echo $_smarty_tpl->tpl_vars['article']->value->name;?>
</a><span>(<?php echo $_smarty_tpl->tpl_vars['article']->value->counter;?>
)</span></li>
									<?php } ?>
									</ul>
								</div>
							</div>
						</div>
					<?php }?>
					
					<?php if ($_smarty_tpl->tpl_vars['active']->value!='blog'){?>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
									<h3 class="browseTitle nl nm">Blog <i class="icon-chevron-down"></i></h3>
								</a>
							</div>
							<div id="collapseSix" class="accordion-body collapse <?php if ($_smarty_tpl->tpl_vars['active']->value=='blog'){?>in<?php }?>">
								<div class="accordion-inner">
									<ul class="nav nav-list right-categories">																				
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
blog<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; All</a></li>
									<?php  $_smarty_tpl->tpl_vars['article'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['article']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['blogCategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['article']->key => $_smarty_tpl->tpl_vars['article']->value){
$_smarty_tpl->tpl_vars['article']->_loop = true;
?>
										<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
blog/category/<?php echo smarty_modifier_replace(trim($_smarty_tpl->tpl_vars['article']->value->name),' ','+');?>
<?php if ($_smarty_tpl->tpl_vars['keyword']->value){?>?keyword=<?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>">&raquo; <?php echo $_smarty_tpl->tpl_vars['article']->value->name;?>
</a><span>(<?php echo $_smarty_tpl->tpl_vars['article']->value->counter;?>
)</span></li>
									<?php } ?>
									</ul>
								</div>
							</div>
						</div>
					<?php }?>
				</div>	
			</div><!--Not visible on phones-->				<?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-06-15 21:11:03
         compiled from "/Applications/MAMP/htdocs/bctc/bctc/views/snippets/widgets/paginator.tpl" */ ?>
<?php if ($_valid && !is_callable('content_51bcd8678ccd87_41950855')) {function content_51bcd8678ccd87_41950855($_smarty_tpl) {?>
        <div class="simplePagination"></div>

		<link type="text/css" rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
css/simplePagination.css"/>
		<script type="text/javascript" src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
js/simplePagination.js"></script>
		<script>
  		$(function() {
        $('.simplePagination').pagination({
            items: <?php echo $_smarty_tpl->tpl_vars['resultCount']->value;?>
,
            itemsOnPage: <?php echo $_smarty_tpl->tpl_vars['resultLimit']->value;?>
,
            cssStyle: 'light-theme',
            hrefTextPrefix: '',
            hrefTextSuffix: '',
            currentPage: <?php echo (($tmp = @$_smarty_tpl->tpl_vars['page']->value)===null||$tmp==='' ? 1 : $tmp);?>
,
            selectOnClick: false,
            onPageClick: function(page, event){
            	
            	var removeQueryFields = function (url) {
                var fields = [].slice.call(arguments, 1).join('|'),
                    parts = url.split( new RegExp('[&?](' + fields + ')=[^&]*') ),
                    length = parts.length - 1;
                    
                if(document.URL.indexOf("?") >= 0){ 
                	return parts[0] + (length ? parts[length].slice(1) : '');
                } else {
                	return parts[0] + "?" + (length ? parts[length].slice(1) : '');
                }
            	}
            	
            	document.location.href = removeQueryFields(document.location.href, 'page') + "&page=" + page;
                        	
            }
        });
   	 	});
   	</script><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-06-15 21:11:03
         compiled from "/Applications/MAMP/htdocs/bctc/bctc/views/snippets/footer.tpl" */ ?>
<?php if ($_valid && !is_callable('content_51bcd867a55d82_76026042')) {function content_51bcd867a55d82_76026042($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/Applications/MAMP/htdocs/bctc/bctc/libraries/smarty/libs/plugins/modifier.replace.php';
?>		<div class="container-fluid">
			
			<div class="row-fluid broadstoneMessage">

				<p class="footerBlurb ">"Meeting the customer expectation to feel welcome and respected, to have choice and value, to be satisfied and safe" - <span>Broadstone Chamber</span></p>	

			</div>
		</div>




		<div class="container-fluid footer">		
			

			<div class="row-fluid">					
				 <div class="span3">   
					<h4 class="">Information</h4>
					<ul>
					
					<?php  $_smarty_tpl->tpl_vars['page'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['page']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cmsPages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['page']->key => $_smarty_tpl->tpl_vars['page']->value){
$_smarty_tpl->tpl_vars['page']->_loop = true;
?>
						<li><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
static/<?php echo smarty_modifier_replace(trim($_smarty_tpl->tpl_vars['page']->value->title),' ','+');?>
"><?php echo $_smarty_tpl->tpl_vars['page']->value->link;?>
</a></li>
					<?php }
if (!$_smarty_tpl->tpl_vars['page']->_loop) {
?>
					<?php } ?>
					
					</ul>
				</div>
				
				<div class="span6">
					<h4>Connect with us</h4>
					<a href="#"><img src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
img/social-facebook.png" alt="Facebook" /></a>
					<a href="#"><img src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
img/social-twitter.png" alt="Twitter" /></a>
					<a href="#"><img src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
img/social-rss.png" alt="RSS" /></a>
					<a href="#"><img src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
img/social-flickr.png" alt="Flickr" /></a>
				</div>
				<div class="span3">
					<div class="company_info">
						<p>Proudly presented by:
						<h4>Broadstone Chamber of Trade &amp; Commerce</h4>
						<p>Built &amp; Maintened by:</p>
						<a href="http://flare-web.co.uk" target="_blank">Flare Web Design</a>
						
					</div>
				</div>					
			</div>	
		</div><?php }} ?>