<?php /* Smarty version Smarty-3.1.11, created on 2013-04-21 18:50:48
         compiled from "/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/addArticle.tpl" */ ?>
<?php /*%%SmartyHeaderCode:145782505551743508acc989-39883745%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f557792e238f7e52793df02bb804a1babd6b5b2c' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/addArticle.tpl',
      1 => 1366543791,
      2 => 'file',
    ),
    'ea5c3c47a8e1a84af03770cc3273d76fcb337f13' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/default.tpl',
      1 => 1366557986,
      2 => 'file',
    ),
    '8d47d2679bde4a59b2fd6226caf5bfa84b8a8032' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/navbar.tpl',
      1 => 1366551475,
      2 => 'file',
    ),
    '5305d5aa8b4dcce68b0ae1849436caad059d5d14' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/sidebar.tpl',
      1 => 1366545593,
      2 => 'file',
    ),
    '774e1eab0bb52c84fb6cfee143ebdd0b05690be5' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/errors.tpl',
      1 => 1363965009,
      2 => 'file',
    ),
    'e33895229ebdafc4b6248d1072af562053bcc3b8' => 
    array (
      0 => '/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/footer.tpl',
      1 => 1363965009,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '145782505551743508acc989-39883745',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'WEBPATH' => 0,
    'sess_user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51743508dd9628_89885493',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51743508dd9628_89885493')) {function content_51743508dd9628_89885493($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
	<head>
		<title>BCTC Admin :: Dashboard</title>
		
		
		<meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		
		
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/fullcalendar.css" />	
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/unicorn.main.css" />
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/unicorn.blue.css" class="skin-color" />
		<link rel="stylesheet/less" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/custom.less" />

		
		
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/uniform.css" />
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/css/select2.css" />		
		<link rel="stylesheet" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/markdown/style.css" />		

		
	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
js/less-1.3.3.min.js" type="text/javascript"></script>
	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/excanvas.min.js"></script>
    <script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/jquery.min.js"></script>
    <script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/jquery-ui.custom.js"></script>
    <script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/bootstrap.min.js"></script>
    <script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/unicorn.js"></script>
   	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/admin.js"></script>
   	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/custom.js"></script>



		
		<script>
			var WEBPATH = '<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
';
		</script>

		
	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/jquery.uniform.js"></script>
	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/select2.min.js"></script>
	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/bootstrap-colorpicker.js"></script>
	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/js/unicorn.form_common.js"></script>
	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/markdown/jquery.markitup.js"></script>
	<script src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/markdown/set.js"></script>
	
	<script type="text/javascript" >
  $(document).ready(function()	{

  	$('#markItUp').markItUp(mySettings, 
  								{ 	root:'admin_resources/markdown/', 
  									previewParserPath:'ajax.ws/markdownPreview',
  									previewAutoRefresh:false 
  								}
  							);	
	
	});
	</script>
	

		
		
	</head>
	
	<body lang="en" id="body">
	
		<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin!=1){?>
			<?php $_smarty_tpl->tpl_vars['merchant'] = new Smarty_variable(reset($_smarty_tpl->tpl_vars['sess_user']->value->ownMerchant), null, 0);?>
		<?php }?>

		
			<?php /*  Call merged included template "admin/snippets/navbar.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('admin/snippets/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '145782505551743508acc989-39883745');
content_51743508bcc0c0_02503825($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "admin/snippets/navbar.tpl" */?>
		
		
		
	<?php /*  Call merged included template "admin/snippets/sidebar.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('admin/snippets/sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('sub'=>'article','item'=>'add'), 0, '145782505551743508acc989-39883745');
content_51743508c05373_00562277($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "admin/snippets/sidebar.tpl" */?>


		<div id="content">
		

	<div id="content-header">
				<h1>Add a New Article</h1>
				
			</div>
			<div id="breadcrumb">
				<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/article/listing<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin!=1){?>/<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
<?php }?>" class="tip-bottom">Articles</a>
				<a href="#" class="current">Add Article</a>
			</div>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
					
  					<?php /*  Call merged included template "admin/snippets/errors.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('admin/snippets/errors.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '145782505551743508acc989-39883745');
content_51743508d61fb6_51890683($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "admin/snippets/errors.tpl" */?>
  				
  					<?php if (isset($_smarty_tpl->tpl_vars['updated']->value)){?>
  						<div class="alert alert-success">
  							<button class="close" data-dismiss="alert">×</button>
  							<strong>Success!</strong> The Article data has been added.
  						</div>
  					<?php }?>
				
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>Create a new Article</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="#" method="post" class="form-horizontal" enctype='multipart/form-data'>
								
								<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin==1){?>
									<div class="control-group">
										<label class="control-label">Merchant Account</label>
										<div class="controls">
											<select name='merchant_id' class='span5'>
												<?php  $_smarty_tpl->tpl_vars['merchant'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['merchant']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['merchants']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['merchant']->key => $_smarty_tpl->tpl_vars['merchant']->value){
$_smarty_tpl->tpl_vars['merchant']->_loop = true;
?>
													<option value='<?php echo $_smarty_tpl->tpl_vars['merchant']->value->id;?>
'><?php echo $_smarty_tpl->tpl_vars['merchant']->value->companyname;?>
</option>
												<?php } ?>
											</select>
											<div class='clearfix'></div>
											<span class="help-block">This is the Article owner.</span>
										</div>
									</div>
								<?php }?>

									<div class="control-group">
										<label class="control-label">Title</label>
										<div class="controls">
											<input type="text" name='title' value='' required/>
											<input type='hidden' name='action' value='add'/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Author</label>
										<div class="controls">
											<input type="text" name='author' value='' required maxlength=128/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">url</label>
										<div class="controls">
											<input type="text" name='url' value='' maxlength=128/>
											<span class="help-block">Ensure in the correct format http://www.web.com</span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Summary</label>
										<div class="controls">
											<textarea name='abstract' required></textarea>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">Description</label>
										<div class="controls">
											<textarea id='markItUp' name='content' required rows=8></textarea>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Keywords</label>
										<div class="controls">
											<input type="text" name='keywords' value='' maxlength=64/>
											<span class="help-block">Keywords are used in search, seperate keyword phrases using a comma.</span>
										</div>
										
									</div>
									
									<div class="control-group">
										<label class="control-label">Article Categories</label>
										<div class="controls">
											<input type="text" name='suggested' value='' required maxlength=255/>
											<span class="help-block">Please enter your suggested categories, seperate category phrases using a comma.</span>
										</div>
									</div>
									
									<div class="control-group">

										<label class="control-label">Images</label>
										<div class="controls">
											<input type="file" name='logo[1]' />  <span>Primary Image</span>
											<div class='clearfix'></div>
											<input type="file" name='logo[2]' />
											<div class='clearfix'></div>
											<input type="file" name='logo[3]' />
											<div class='clearfix'></div>
											<input type="file" name='logo[4]' />
											<span class="help-block">Be aware that images will be scaled and cropped to ensure they fit within the confines of the page.</span>
										</div>
									</div>

									<div class="form-actions">
										<button type="submit" class="btn btn-primary">Save</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			
			
				
				<div class="row-fluid">
					<?php /*  Call merged included template "admin/snippets/footer.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('admin/snippets/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0, '145782505551743508acc989-39883745');
content_51743508dd0ec9_39166137($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); /*  End of included template "admin/snippets/footer.tpl" */?>
				</div>
			</div>


		</div>
		
		
	</body>

</html><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-04-21 18:50:48
         compiled from "/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/navbar.tpl" */ ?>
<?php if ($_valid && !is_callable('content_51743508bcc0c0_02503825')) {function content_51743508bcc0c0_02503825($_smarty_tpl) {?><div id="header">
	<h1><a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin"><img src="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin_resources/img/bctc.gif" width="50px" alt=""><span>BCTC Admin</span></a></h1>		
</div>


<div id="user-nav" class="navbar navbar-inverse">
    <ul class="nav btn-group">
        <li class="btn btn-inverse" ><a title="" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/user/edit/<?php echo md5($_smarty_tpl->tpl_vars['sess_user']->value->id);?>
"><i class="icon icon-user"></i> <span class="text">Profile</span></a></li>
        
        
      
        <li class="btn btn-inverse"><a title="" href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/logout"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
    </ul>
</div><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-04-21 18:50:48
         compiled from "/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/sidebar.tpl" */ ?>
<?php if ($_valid && !is_callable('content_51743508c05373_00562277')) {function content_51743508c05373_00562277($_smarty_tpl) {?><div id="sidebar">
	<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
	<ul>
		<li class="<?php if (!$_smarty_tpl->tpl_vars['sub']->value){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin"><i class="icon icon-home"></i> <span>Dashboard</span></a>
		</li>
		
<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin==1){?>

		<!-- <li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='blog'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/blog"><i class="icon icon-th-list"></i> <span>Blog</span></a>
		</li> -->

		<li>
			<a href="#"><i class="icon icon-signal"></i> <span>Analytics</span></a>
		</li>

		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='category'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/category/listing"><i class="icon icon-folder-open"></i> <span>Categories</span></a>
		</li>

		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='user'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/user/listing"><i class="icon icon-user"></i> <span>Users</span></a>
		</li>
				
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='merchant'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/merchant/listing"><i class="icon icon-th-list"></i> <span>Merchants</span></a>
		</li>
<?php }?>			
<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin==0){?>
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='merchant'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/merchant/edit/<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
"><i class="icon icon-pencil"></i> <span>Listing</span></a>
		</li>
<?php }?>
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='article'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/article/listing<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin!=1){?>/<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
<?php }?>"><i class="icon icon-pencil"></i> <span>Articles</span></a>
		</li>
		
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='classified'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/classified/listing<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin!=1){?>/<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
<?php }?>"><i class="icon icon-pencil"></i> <span>Classifieds</span></a>
		</li>
		
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='event'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/event/listing<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin!=1){?>/<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
<?php }?>"><i class="icon icon-pencil"></i> <span>Events</span></a>
		</li>
		
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='deal'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/deal/listing<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin!=1){?>/<?php echo md5($_smarty_tpl->tpl_vars['merchant']->value->id);?>
<?php }?>"><i class="icon icon-pencil"></i> <span>Deals</span></a>
		</li>
		
<?php if ($_smarty_tpl->tpl_vars['sess_user']->value->isadmin==1){?>
		<li class="<?php if ($_smarty_tpl->tpl_vars['sub']->value=='cms'){?>active<?php }?>">
			<a href="/<?php echo $_smarty_tpl->tpl_vars['WEBPATH']->value;?>
admin/cms/listing"><i class="icon icon-pencil"></i> <span>CMS</span></a>
		</li>
<?php }?>
	
	
	</ul>

</div><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-04-21 18:50:48
         compiled from "/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/errors.tpl" */ ?>
<?php if ($_valid && !is_callable('content_51743508d61fb6_51890683')) {function content_51743508d61fb6_51890683($_smarty_tpl) {?><?php if (isset($_smarty_tpl->tpl_vars['errors']->value)){?>
<div class="container">

	<?php  $_smarty_tpl->tpl_vars['error'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['error']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['errors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['error']->key => $_smarty_tpl->tpl_vars['error']->value){
$_smarty_tpl->tpl_vars['error']->_loop = true;
?>
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<h4><?php echo $_smarty_tpl->tpl_vars['error']->value['title'];?>
</h4>
			<?php echo $_smarty_tpl->tpl_vars['error']->value['message'];?>

		</div>
	<?php } ?>

</div>
<?php }?><?php }} ?><?php /* Smarty version Smarty-3.1.11, created on 2013-04-21 18:50:48
         compiled from "/Applications/MAMP/htdocs/BCTC/BCTC/views/admin/snippets/footer.tpl" */ ?>
<?php if ($_valid && !is_callable('content_51743508dd0ec9_39166137')) {function content_51743508dd0ec9_39166137($_smarty_tpl) {?><div id="footer" class="span12">
	2012 - 2013 &copy; BCTC Admin
</div><?php }} ?>