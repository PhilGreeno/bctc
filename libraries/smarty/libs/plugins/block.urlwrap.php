<?php
/**
 * filename block.urlwrap.php
*
* Smarty {urlwrap}{/urlwrap} block plugin
*
* Type:     block function<br>
* Name:     urlwrap<br>
* Purpose:  wraps a matching url pattern with <a href></a> tags
*
* @author Hasin Hayder, 23rd June, 2:20 PM
* @version 1.0.0
* @todo write a call back which will skip email addresses
*
* @param param additional parameters, none implemented here
* @param string contents of the block
* @param Smarty clever simulation of a method
* @return string string $content re-formatted
*/

/**
 *
* the array to store already anchored URLs
*
* @access private
* @var string array
*/
global $anchored_urls ;

/**
 *
 * the pattern to match any URL in the context
 *
 * @access private
 * @var string
 */

global $pattern_url ;

/**
 *
 * the pattern to match any anchored URLs
 *
 * @access private
 * @var string array
 */
global $pattern_anchored_url;

//initialize the anchored urls array()
$anchored_urls = array();

//static counter for anchored_urls
$i = 0;

$pattern_url='/'.
                '((http|https)\:\/\/)?'.
                '([\w_\-\.]+)\.'.
                '([\w+]{2,})'.
                '/i';

$pattern_anchored_url = "/<a(.*?)href\s*=\s*['|\"|\s*](.*?)['|\"|>](.*?)>(.*?)<\/a>/i";


//the main smarty block modifier
function smarty_block_urlwrap($params,$content, &$smarty)
{
    global $anchored_urls, $pattern_anchored_url, $pattern_url;
    $test = preg_replace_callback($pattern_anchored_url, "hide_anchored_urls",$content);
    $test = preg_replace_callback($pattern_url,"wrap_urls", $test);

    //now replace the ^#~#^ sections with the original anchored URLS
    foreach($anchored_urls as $key=>$val)
    {
        $test = str_replace($key, $val, $test);
    }
    return $test;

}

/**
 *
 * this is a preg_replace call back function which hides already anchored URLs with a symbol for later retrieval
 * finally return it
 *
 * @access private
 * @param matches array() the match by preg_replace_callback pattern
 * @return string;
 */
function hide_anchored_urls($matches)
{
    global $anchored_urls;
    $i +=1;
    $anchored_urls["^#~#^".$i]=$matches[0];

    return "^#~#^".$i;
}

/**
 *
 * this is a preg_replace call back function which wraps urls with anchor
 * finally return it
 *
 * @access private
 * @param matches array() the match by preg_replace_callback pattern
 * @return string;
 */
function wrap_urls($matches)
{
    return "<a href={$matches[0]}>{$matches[0]}</a>";
}
?>