<?php
/**
 * Smarty plugin
 * 
 * @package Smarty
 * @subpackage PluginsModifier
 */


function smarty_modifier_obfuscate($string)
{
    
	return Obfuscator::encrypt($string);
	
} 

?>